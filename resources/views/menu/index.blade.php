@extends('layouts.dashboard')
@section('css')
    <style>
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')
    <div class="d-block bg bg-dark" style="color:white; padding-top: 10px;visibility:hidden;" id="main">
        @include('alert.mensaje')
        <div id="mensaje"></div>
        <table class="table table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;">
            <thead>
                <th class="text-center" data-orderable="false">Imagen</th>
                <th class="text-center">Descripcion</th>
                <th class="text-center">Costo</th>
                <th class="text-center" data-orderable="false" style="width:20%"></th>
            </thead>
            <tbody class="text-center" id="lista"> 
                @include('menu.recargable.listamenu')
            </tbody>
        </table>
    </div>
    
    @can('menus.edit')
    <!-- Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar Menu:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <div id="modalMessage"></div>
                <form id="data" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <input type="hidden" id="id">
                        <div class="row ">
                            <div class="col-md-1 "></div>
                            <div class="col-md-10 ">
                                <div class="form-group text-center">
                                    {!!Form::label('Descripcion','Descripcion:')!!}
                                    <textarea name="descripcion" class="form-control border border-warning" placeholder="Especifique el servicio de comida" id="descripcion" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-1 "></div>
                            <div class="col-md-10">
                                <div class="form-group">
                                    {!!Form::label('Costo:')!!}
                                    {!!Form::text('costo',null,['id'=>'costo','class'=>'form-control','placeholder'=>'Costo', 'onkeypress'=>'return BlockEnter(event);'])!!}
                                </div>
                            </div>
                        </div>
                        
                        <div class="row ">
                            <div class="col-md-1 "></div>
                            <div class="col-md-10 text-center">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="0" id="defaultCheck1" name="sinimagen">
                                    <label class="form-check-label " for="defaultCheck1">
                                        Sin imagen
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row " >
                            <div class="col-md-1 "></div>
                            <div class="col-md-10" id="fileimagen">
                                {!!Form::label('Imagen:')!!}
                                <div class="form-group">
                                    {!!Form::file('path',['value'=>'subir imagen','id'=>'path','class'=>'btn btn-secondary col-md-12'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-1 "></div>
                            <div class="col-md-10 text-center" id="imagen" >

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-danger" onclick="actualizar();" id="btnactualizar">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endcan
    
    @can('personal.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section("script")
    {!!Html::script("js/jskevin/cedulanica.js")!!}
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!}  
    {!!Html::script("js/jskevin/kavvdt.js")!!}   
    <script>
        var table;
        var fila;
        $(document).ready( function () {
            table=createdt($('#Datos'),{buscar:'{!!session("valor")!!}',col:1,cant:[10,20,-1],cantT:[10,20,"Todo"]});
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index',1060);
        });

        @can('menus.edit')
        $('.edit').on( 'click', function () {
            fila=$(this).parents('tr');//Dejamos almacenada temporalmente la fila en la que clickeamos editar
            row=table.row(fila).data();//Tomamos el contenido de la fila 
            //Sobreescribimos en los campos editables los datos del cliente
            $("#descripcion").val(row[1]);
            $("#costo").val(row[2]);
            $("#imagen").empty();
            $("#imagen").append(row[0]);
            $("#id").val($(this).val());
        });
        function actualizar()
        {
            route="/menus/"+$("#id").val();
            var token=$("#token").val();
            var formData = new FormData($('#data')[0]);
            
            //message(["Enviando... por favor espere la confirmacion!"],{objeto:$("#modalMessage"), manual:true})
            $("#btnactualizar").attr('disabled',true);
            $("#loading").css('display','block');
             $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'POST',
                data:formData,
                contentType: false,
                processData: false,
                success: function(res){
                    
                    if(res!=1)//Si agrego una nueva imagen
                    {
                        if(res!="vacio.jpg")
                            table.cell(fila.children('td')[0]).data( '<img src="https://fatsaas-2021.s3.us-east-2.amazonaws.com/{{$carpeta}}/'+res+'"style="height:10em;border-radius: 40px;"> ');
                        else
                            table.cell(fila.children('td')[0]).data( '<img src="https://fatsaas-2021.s3.us-east-2.amazonaws.com/'+res+'"style="height:10em;border-radius: 40px;"> ');
                    }
                    
                    message(['Menu editado correctamente'],{manual:true});
                    table.cell(fila.children('td')[1]).data( $("#descripcion").val());
                    table.cell(fila.children('td')[2]).data( $("#costo").val());
                    table=$("#Datos").DataTable().draw(); 
                    
                    $("#Edit").modal('toggle');
                    $("#btnactualizar").attr('disabled',false);
                    
                    $("#loading").css('display','none');
                    $('body').animate({scrollTop:0}, 'fast');
                    
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                $("#btnactualizar").attr('disabled',false);
                $("#loading").css('display','none');
                message(jqXHR,{objeto:$("#modalMessage"), tipo:"danger"});
            }); 
        }
        //VISTA PREVIA DE LA IMAGEN A ACTUALIZAR
        $('#path').change(function(e) {
            console.log(1);
            addImage(e);
        });
        function addImage(e){
            var file = e.target.files[0],
            imageType = /image.*/;
            
            if (!file.type.match(imageType))
            return;
        
            var reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
        }
        function fileOnload(e) {
            var result = e.target.result;//USAR ESTO PARA MOSTRAR DIRECTAMENTE LA IMAGEN SIN TENER QUE CARGARLA DESDE LA NUBE
            $("#imagen").empty();
            $("#imagen").append('<image class="col-md-10" style="border-radius: 40px;" src="'+result+'">');
        }
        //Sin imagen
        $('#defaultCheck1').click(function(){
            if($("#defaultCheck1").prop('checked')==true)
            {
                $("#fileimagen").hide();
                $("#imagen").hide();
                $("#defaultCheck1").val(1);
            }
            else
            {
                $("#fileimagen").show();
                $("#imagen").show();
                $("#defaultCheck1").val(0);
            }
        });
        @endcan
        @can('menus.destroy')
        var row;
        $('#deleteModal').on('show.bs.modal', function (event) {
            {{-- Mantenemos el id del elemento seleccionado --}}        
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            $("#id").val(value);


            {{-- Mantenemos la fila del elemento seleccionado --}}
            row = button.parents('tr');
        });

        $('#delete').on( 'click', function () {
            
            var route="/menus/"+$("#id").val();
            var token=$("#token").val();
            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');

            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                success: function(){
                    message(['Se elimino correctamente'],{manual:true});
                    table.row(row).remove().draw( false );
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                    $("#delete").attr('disabled',false);
                    $('body').animate({scrollTop:0}, 'fast');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#deleteModalMessage"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });
        @endcan

        function BlockEnter(e){
            tecla = (document.all) ? e.keyCode :e.which; 
            return (tecla!=13); 
        }
    </script>
@stop