                @foreach($menu as $comida)
                    <tr style="height:5em;">
                        @if(strcmp($comida->image_name,"vacio.jpg")!=0)                        
                        <td><img src="https://fatsaas-2021.s3.us-east-2.amazonaws.com/{{$carpeta}}/{{$comida->image_name}}"  style="height:10em;border-radius: 40px;" value="{{$comida->path}}"></td> 
                        @else
                        <td><img src="https://fatsaas-2021.s3.us-east-2.amazonaws.com/{{$comida->image_name}}"  style="height:10em;border-radius: 40px;" value="{{$comida->path}}"></td> 
                        @endif
                        <td>{{$comida->description}}</td>    
                        <td>{{$comida->price}}</td> 
                        <td class="botones">
                            @can('menus.edit')
                            <button data-toggle="modal" data-target="#Edit" class="btn btn-primary edit" value="{{$comida->id}}">Editar</button>
                            @endcan
                            @can('menus.destroy')
                            <button data-toggle="modal" data-target="#deleteModal" class="btn btn-danger fa fa-trash" data-value="{{$comida->id}}"></button>
                            @endcan
                        </td>
                    </tr>  
                @endforeach