@extends('layouts.dashboard')
@section('content')
  <div class="card">
    <h4 class="card-header">Agregar al menu</h4>
    <div class="card-body">
    <div id="mensaje"></div>
      {!!Form::open(['route'=>'menus.store', 'method'=>'POST', 'id' => 'data','enctype'=>'multipart/form-data'])!!}
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        @include('menu.formulario.datos')

        <div class="text-center">
          <input class="btn btn-primary btnSent" type="button"  onclick="save('guardar');" value="Guardar">
          <input class="btn btn-primary btnSent" type="button"  onclick="save('guardarv');" value="Guardar y ver">
        </div>
      {!!Form::close()!!}
    </div>
  </div>   
  @include('layouts.cargando.cargando')

@stop
@section('script')
  {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
  <script>
    function save(decision)
    {
      var ruta = "/menus";
      var token = $("#token").val();
      var formData = new FormData($('#data')[0]);
      
      //message(["Enviando... por favor espere la confirmacion!"],{manual:true})
      $('.btnSent').prop('disabled',true);
      $("#loading").css('display','block');
      $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:formData,
        contentType: false,
        processData: false,
        success: function(){
          if(decision=="guardar")
          {
            message(["Se agrego el menu correctamente correctamente"],{manual:true});
            limpiar();
            $('.btnSent').prop('disabled',false);
            $("#loading").css('display','none');
          }

          if(decision=="guardarv")
            location.href ="/menus/1/"+$("#descripcion").val();

        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
          message(jqXHR,{tipo:"danger"});
          $('.btnSent').prop('disabled',false);
          $("#loading").css('display','none');
      });
    }
    function limpiar()
    {
      $("#descripcion").val("");
      $("#costo").val("");
      $("#path").val("");
      $("#prev").empty();
    }

    //TODO funcion para la previsualizacion de la imagen
    $('#path').change(function(e) {
      console.log(1);
      addImage(e);
    });

    function addImage(e){
      let reader = new FileReader();
      arch = e.target.files[0];
      let imag = $("#prev");
      imag.empty();

      //si no es imagen
      if((!arch.type.match('image.*'))) {
        // box.empty();
        // imag.empty();
        message(['No ingreso una Imagen! '],{objeto:$(mensaje), manual:true, tipo:"danger"});
      }
      else{
        // box.empty();
        // imag.empty();
        reader.onload = function() {
          let imagen = document.getElementById('prev');
          image = document.createElement('img');
          //le pasamos la imagen al source
          image.src = reader.result;
          image.setAttribute('class','mx-auto d-block');

          imagen.innerHTML = '';
          imagen.append(image);
        };
        reader.readAsDataURL(arch);
      }
    }

  </script>
@stop
