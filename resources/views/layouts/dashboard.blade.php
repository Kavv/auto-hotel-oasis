<html>
  <head lang="en">
    <meta charset="utf-8">
    <meta content="IE=edge, chrome=1" http-equiv="X-UA-Compatible">
    <meta content="" name="description">
    <meta content="" name="author">
    <title>
      Auto Hotel Oasis - Sistema Gerencial
    </title>
    {!!Html::style("css/cssF/vendor/bootstrap/css/bootstrap.min.css")!!}
    
    {!!Html::style("css/cssF/vendor/font-awesome/css/font-awesome.min.css")!!}
    {!!Html::style("css/cssF/css/sb-admin.css")!!}
    {!!Html::style("css/cssF/principal/style2.css")!!}
    {!!Html::style("css/app.css")!!}
    <!--{!!Html::style("https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css")!!}-->
    {!!Html::style("https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css")!!}
    {!!Html::style("https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css")!!}
  
	  <link href="/img/logo_gym.png" type="image/x-icon" rel="shortcut icon" />
    <!--  -->
    <!-- {!!Html::style("css/loading-bar-css/loading-bar.css")!!} -->
    <style>
      #module-navbar p:not(:last-child) {
        margin-bottom: 1rem;
      }

      /* Tamaño del scroll */
      #module-navbar::-webkit-scrollbar {
        width: 8px;
        height: 8px;
      }

      /* Estilos barra (thumb) de scroll */
      #module-navbar::-webkit-scrollbar-thumb {
        background: #505050;
        border-radius: 4px;
      }

      #module-navbar::-webkit-scrollbar-thumb:active {
        background-color: #000;
      }

      #module-navbar::-webkit-scrollbar-thumb:hover {
        background: #252525;
        box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
      }

      /* Estilos track de scroll */
      #module-navbar::-webkit-scrollbar-track {
        background: #a0a0a0;
        border-radius: 4px;
      }

      #module-navbar::-webkit-scrollbar-track:hover, 
      #module-navbar::-webkit-scrollbar-track:active {
        background: #8b8a8a;
      }
      .nav-item a{
        color: #f0f0f0!important;
      }
      .nav-item a:hover{
        background: #000!important;
      }
      .bootstrap-select > .dropdown-toggle, .filter-option-inner-inner {
        height: 35px;
      }


      /* Hacemos mas grande el ¨modal largo¨ de bootstrap y valisamos su responsividad */
      .modal-lg {
            min-width: 1000px;
        }
        @media (max-width: 1000px) {
            .modal-lg {
                min-width: 700px!important;
            }
        }
        @media (max-width: 700px) {
            .modal-lg {
                min-width: auto!important;
            }
        }


    </style>  
    @section('css')
    @show
  </head>
  <body class="fixed-nav sticky-footer bg-dark" id="page-top">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <label class="navbar-brand"> 
        Auto Hotel Oasis - Sistema Gerencial
      </label>
      <button aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right" data-target="#navbarResponsive" data-toggle="collapse" type="button">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav scrollbar" id="module-navbar">
          <li class="nav-item {{active(URL::route('principal.index'))}}" data-placement="right" data-toggle="tooltip" title="Dashboard">
            <a class="nav-link" href="{!!URL::to('/principal')!!}">
              <span class="nav-link-text" >
                {!!Auth::user()->name!!}
              </span>
            </a>
          </li>
          
          <!--Parqueo-->
          @can('cliente.index')
          <li class="nav-item" data-placement="right" data-toggle="tooltip" title="parqueo">
            <a class="nav-link nav-link-collapse collapsed" data-parent="#module-navbar" data-toggle="collapse" href="#collapseparqueo">
              <i class="fa fa-car"></i>
              <span class="nav-link-text">
                Parqueo
              </span>
            </a>
            <ul class="sidenav-second-level collapse {{active(['parqueo','parque/tiempo_real'],true)}}" id="collapseparqueo">
              @can('cliente.index')
                <li class="{{active('parqueo')}}">
                  {!!link_to_route('parqueo.index', $title = 'Ver Parqueo')!!} 
                </li>
              @endcan
              @can('cliente.index')
                <li class="{{active('parqueo/tiempo_real')}}">
                  {!!link_to_route('parqueo.real_time', $title = 'En tiempo real')!!} 
                </li>
              @endcan
            </ul>
          </li>
          @endcan
          
          <!--Categorias -->
          @can('cliente.index')
          <li class="nav-item" data-placement="right" data-toggle="tooltip" title="categoria">
            <a class="nav-link nav-link-collapse collapsed" data-parent="#module-navbar" data-toggle="collapse" href="#collapsecategoria">
              <i class="fa fa-tag"></i>
              <span class="nav-link-text">
                Categorias
              </span>
            </a>
            <ul class="sidenav-second-level collapse {{active(['categoria'],true)}}" id="collapsecategoria">
              @can('cliente.index')
                <li class="{{active('categoria')}}">
                  {!!link_to_route('categoria.index', $title = 'Ver Categorias')!!} 
                </li>
              @endcan
            </ul>
          </li>
          @endcan
          
          <!--Habitaciones -->
          @can('cliente.index')
          <li class="nav-item" data-placement="right" data-toggle="tooltip" title="habitacion">
            <a class="nav-link nav-link-collapse collapsed" data-parent="#module-navbar" data-toggle="collapse" href="#collapsehabitacion">
              <i class="fa fa-bed"></i>
              <span class="nav-link-text">
                Habitaciones
              </span>
            </a>
            <ul class="sidenav-second-level collapse {{active(['habitacion'],true)}}" id="collapsehabitacion">
              @can('cliente.index')
                <li class="{{active('habitacion')}}">
                  {!!link_to_route('habitacion.index', $title = 'Ver Habitacion')!!} 
                </li>
              @endcan
            </ul>
          </li>
          @endcan

          @can('cliente.index')
          <li class="nav-item" data-placement="right" data-toggle="tooltip" title="reservacion">
            <a class="nav-link nav-link-collapse collapsed" data-parent="#module-navbar" data-toggle="collapse" href="#collapsereservacion">
              <i class="fa fa-suitcase"></i>
              <span class="nav-link-text">
                Reservación
              </span>
            </a>
            <ul class="sidenav-second-level collapse {{active(['reservacion', 'reservacion/create'],true)}}" id="collapsereservacion">
              @can('cliente.create')
                <li class="{{active('reservacion/create')}}">
                  {!!link_to_route('reservacion.create', $title = 'Reservar')!!} 
                </li>
              @endcan
              @can('cliente.index')
                <li class="{{active('reservacion')}}">
                  {!!link_to_route('reservacion.index', $title = 'Ver Reservas')!!} 
                </li>
              @endcan
            </ul>
          </li>
          @endcan


          <!--CLIENTE-->
          @can('cliente.index')
          <li class="nav-item" data-placement="right" data-toggle="tooltip" title="Cliente">
            <a class="nav-link nav-link-collapse collapsed" data-parent="#module-navbar" data-toggle="collapse" href="#collapseCliente">
              <i class="fa fa-users"></i>
              <span class="nav-link-text">
                Cliente
              </span>
            </a>
            <ul class="sidenav-second-level collapse {{active(['cliente','cliente/create'],true)}}" id="collapseCliente">
              @can('cliente.index')
                <li class="{{active('cliente')}}">
                  {!!link_to_route('cliente.index', $title = 'Ver Cliente')!!}
                </li>
              @endcan
            </ul>
          </li>
          @endcan

          <!--VETADOS-->
          @can('vetado.index')
          <li class="nav-item" data-placement="right" data-toggle="tooltip" title="Vetado">
            <a class="nav-link nav-link-collapse collapsed" data-parent="#module-navbar" data-toggle="collapse" href="#collapseVetado">
              <i class="fa fa-exclamation-triangle"></i>
              <span class="nav-link-text">
                Vetado
              </span>
            </a>
            <ul class="sidenav-second-level collapse {{active(['vetado/listapersonal',
              'vetado/listacliente', 'vetado', 'vetado/filtro/personal/vetado.indexp',
              'vetado/filtro/cliente'],true)}}" id="collapseVetado">
              @can('vetado.create')
              <li class="{{active(['vetado/listapersonal', 'vetado/listacliente'])}}">
                {!!link_to_route('listac.lc', $title = 'Agregar Vetados')!!}
              </li>
              @endcan
              @can('vetado.index')
              <li class="{{active(['vetado', 'vetado/filtro/personal/vetado.indexp', 'vetado/filtro/cliente'])}}">
                {!!link_to_route('vetado.index', $title = 'Ver Vetados')!!}
              </li>
              @endcan
            </ul>
          </li>
          @endcan
          <!--SERVICIO-->
          @can('servicio.index')
          <li class="nav-item" data-placement="right" data-toggle="tooltip" title="Servicio">
            <a class="nav-link nav-link-collapse collapsed" data-parent="#module-navbar" data-toggle="collapse" href="#collapseServicio">
              <i class="fa fa-wrench"></i>
              <span class="nav-link-text">
                Servicio
              </span>
            </a>
            <ul class="sidenav-second-level collapse {{active(['servicio','servicio/create'],true)}}" id="collapseServicio">
              @can('servicio.index')
              <li class="{{active('servicio')}}">
                {!!link_to_route('servicio.index', $title = 'Ver Servicio')!!}
              </li>
              @endcan
            </ul>
          </li>
          @endcan
          <!--INVENTARIO-->
          @can('inventario.index')
          <li class="nav-item" data-placement="right" data-toggle="tooltip" title="Inventario">
            <a class="nav-link nav-link-collapse collapsed" data-parent="#module-navbar" data-toggle="collapse" href="#collapseInventario">
              <i class="fa fa-book"></i>
              <span class="nav-link-text">
                Inventario
              </span>
            </a>
            <ul class="sidenav-second-level collapse {{active(['inventario','inventario/create'],true)}}" id="collapseInventario">
              @can('inventario.index')
              <li class="{{active('inventario')}}">
                {!!link_to_route('inventario.index', $title = 'Ver Inventario')!!}
              </li>
              @endcan
            </ul>
          </li>
          @endcan
          
          <!-- ROLES -->
          @can('role.index')
          <li class="nav-item" data-placement="right" data-toggle="tooltip" title="Role">
            <a class="nav-link nav-link-collapse collapsed" data-parent="#module-navbar" data-toggle="collapse" href="#collapseRole">
              <i class="fa fa-gavel"></i>
              <span class="nav-link-text">
                Roles
              </span>
            </a>
            <ul class="sidenav-second-level collapse {{active(['roles','roles/create'],true)}}" id="collapseRole">
              @can('role.create')
              <li class="{{active('roles/create')}}">
                {!!link_to_route('roles.create', $title = 'Agregar Roles')!!}
              </li>
              @endcan
              @can('role.index')
                <li class="{{active('roles')}}">
                  {!!link_to_route('roles.index', $title = 'Ver Roles')!!}
                </li>
              @endcan
            </ul>
          </li>
          @endcan
          <!--USUARIO-->
          @can('users.index')
          <li class="nav-item" data-placement="right" data-toggle="tooltip" title="usuario">
            <a class="nav-link nav-link-collapse collapsed" data-parent="#module-navbar" data-toggle="collapse" href="#collapseUsuario">
              <i class="fa fa-address-book-o"></i>
              <span class="nav-link-text">
                Usuarios
              </span>
            </a>
            <ul class="sidenav-second-level collapse {{active(['usuarios','usuarios/create'],true)}}" id="collapseUsuario">
              @can('users.create')
              <li class="{{active('usuarios/create')}}">
                {!!link_to_route('users.create', $title = 'Agregar Usuario')!!}
              </li>
              @endcan
              @can('users.index')
              <li class="{{active('usuarios')}}">
                {!!link_to_route('users.index', $title = 'Ver Usuarios')!!}
              </li>
              @endcan
            </ul>
          </li>
          @endcan
          

          <!-- REPORTES -->
          @can('reporte.index')
          <li class="nav-item {{active('reportes')}}" data-placement="right" data-toggle="tooltip" title="Reportes">
            <a class="nav-link" href="{!!URL::to('/reportes')!!}">
              <i class="fa fa-bar-chart"></i>
              <span class="nav-link-text" >
                Reportes
              </span>
            </a>
          </li>
          @endcan
        </ul>

        <ul class="navbar-nav sidenav-toggler">
          <li class="nav-item">
            <a id="sidenavToggler" class="nav-link text-center">
              <i class="fa fa-fw fa-angle-left"></i>
            </a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          <!--BOTON SALIR-->
          <li class="nav-item">
            <a class="nav-link" data-target="#exampleModal" data-toggle="modal">
              <i class="fa fa-fw fa-sign-out"></i>
              Salir
            </a>
          </li>
        </ul>
      </div>
    </nav>
    <!--MODAL DE SALIR-->
    <div aria-hidden="true" aria-labelledy="exampleModalLabel" class="modal fade" id="exampleModal" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title mx-auto" id="exampleModalLabel">¿Seguro que desea salir?</h5>
            <button aria-hidden="Close" class="close" data-dismiss="modal" type="button">
              <span aria-hidden="true"></span>
            </button>
          </div>
          <div class="modal-body mx-auto">
            Seleccione salir cuando este listo para terminar la sesion
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancelar</button>
              <a href="{{ route('logout') }}"
                class="btn btn-primary">
                Salir
              </a>
          </div>
        </div>
      </div>
    </div>
    <div class="content-wrapper " style="padding-top: 0px; padding-bottom: 0px;" id="maestro" >
      @yield('content')
    </div>
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Derechos de autor Computer Scientists 2020</small>
        </div>
      </div>
    </footer>
    {!!Html::script("js/jsF/vendor/jquery/jquery.min.js")!!}
    {!!Html::script("js/jsF/vendor/jquery-easing/jquery.easing.min.js")!!}
    {!!Html::script("js/jsF/vendor/popper/popper.min.js")!!}
    {!!Html::script("js/jsF/vendor/bootstrap/js/bootstrap.min.js")!!}
    {!!Html::script("js/jsF/js/sb-admin.min.js")!!} 
    
    <script>
      var timer = setTimeout(SessionOut,(1000*60*10));
      var logout;
      function SessionOut() {
        $.ajax({
          url: "/",
        }).fail( function( jqXHR, textStatus, errorThrown ) {
          //console.log(jqXHR.responseJSON);
          logout = jqXHR;
          location.href = "/";
        });
        clearTimeout(timer);
        timer = setTimeout(SessionOut,(1000*60*10));
      }
    </script>
    @section('script')
    @show
  </body>
</html>
