@extends('layouts.dashboard')
@section('css')
    {!! Html::style('css/gijgo2/css/gijgo.css') !!}
    <style>
        .content-wrapper {
            background: #343a40 !important;
        }

        .border-skyblue {
            border: #4a75b4 solid 2px;
        }

        #titulo-editar-pago,
        #btns-edit {
            display: none;
        }
        .gj-datepicker{
            width: auto;
        }
    </style>
@stop
@section('content')

    <!--Arreglar toda la mierda que hiciste-->
    <div class="d-block bg bg-dark" style="padding-top: 10px; color:white;visibility:hidden;" id="main">
        @include('alert.mensaje')
        <div class="row ">
            <div class="col-md-12">
                
            </div>
        </div>
    </div>


    @can('cliente.edit')
        <!-- Modal -->
        <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Editar pago</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="modalMessage"></div>
                        <form id="data">
                            <input type="hidden" id="id" name="id">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

                            <div id="modalMessage"></div>
                            @include('leccion.formulario.datos')
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="actualizar"
                                onclick="actualizar()">Actualizar</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcan

    @can('cliente.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!! Html::script('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js') !!}
    {!! Html::script('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js') !!}
    {!! Html::script('js/jskevin/tiposmensajes.js') !!}
    {!! Html::script('js/jskevin/kavvdt.js') !!}
    {!! Html::script('js/gijgo2/js/gijgo.js') !!}

    <script>
        var table;
        var row;

        $(document).ready(function() {
            /* table = createdt($('#table'), {
                cant: [10, 20, -1],
                cantT: [10, 20, "todo"]
            }); */
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index', 1060);
        });
        

    </script>
@stop
