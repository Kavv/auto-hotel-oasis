{{-- Mostramos las reservaciones en este rango --}}
<?php 
    $colors = ['', '#ffc107','#86deff','#7bff7f','#ff7e7e']
?>
@foreach ($reservations as $index => $r)
  @if ($index == 0)
  <div class="col-md-12 text-center">
      <h3>Habitaciones Reservadas</h3>
  </div>
  <div class="col-md-12">
      <div id="msg_reservadas" class="col-md-12"></div>
  </div>
  <div class="col-md-12 d-flex justify-content-center mb-3">
      <div class="col-md-7 p-0">
          <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Buscar por nombre o cédula</span>
              </div>
              <input type="text" class="form-control" onkeyup="data_filter(this)">
          </div>
      </div>
  </div>
  @endif
  <div class="col-md-4 d_reservation" id="d_reservation{{$r->reservation}}" data-id="{{$r->reservation}}" data-first_v="{{$r->customer}}"  data-second_v="{{$r->dni}}">
      <div class="mb-4 reservation">
          <h4 class="text-center">{{$r->name}}</h4>
          <h6 class="text-center uppercase" style="color: {{$colors[$r->state_id]}}">{{$r->state}}</h6>
          <div class="row col-md-12 m-0 p-0">
              <?php $col = 12;?>

              <div class="col-md-{{$col}} px-1">
                  <p>Nombre: {{$r->customer}}</p>
                  <p>Cedula: {{$r->dni}}</p>
                  <p>Teléfono: <a href="tel:{{$r->phone}}" class="a-phone">{{$r->phone}}</a></p>

                  <?php 
                    $aux_start = new \dateTime($r->start);
                    $aux_end = new \dateTime($r->end);
                    $f_start = $aux_start->format('d-m-Y h:i a');
                    $f_end = $aux_end->format('d-m-Y h:i a');
                  ?>
                  <p>Desde: {{$f_start}}</p>
                  <p>Hasta: {{$f_end}}</p>
              </div>

              <div class="col-12">
                <button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-warning btn-block" 
                value="{{$r->reservation}}" onclick="editar(this, {'id':'#res'})">DETALLES</button>
              </div>
          </div>
      </div>
  </div>
@endforeach
