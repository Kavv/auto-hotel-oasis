
  @foreach($data as $d)
    <tr>
      <?php 
          $category = $d->category . " (C$" . $d->amount_hour .  ", C$" . $d->amount_half .  ", C$" . $d->amount_day . ")" ;
          if($d->vip == 1)
              $parking = "Parqueo: " . $vip[$d->vip] . " Vehiculo: " . $type[$d->type] . " Espacio: " . $d->quantity;
          else
              $parking = "Parqueo: " . $vip[$d->vip] . " Vehiculo: " . $type[$d->type]; 
      ?>
      <td>{{$d->start}}</td>  
      <td>{{$d->end}}</td>  
      <td>{{$d->payment}}</td>  
      <td>{{$d->customer_id}}</td>
      <td>{{$d->state_id}}</td>
      <td>{{$d->room_id}}</td>
      <td class="botones">
        <button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-pencil" value="{{$d->id}}" onclick="editar(this, {})"></button>
        @can('cliente.destroy') 
          <button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="{{$d->id}}"></button>
        @endcan
      </td>
    </tr>
  @endforeach