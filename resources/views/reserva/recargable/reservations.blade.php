@foreach ($reservations as $r)
	<div class="col-md-6 ">
		<div class="mb-4 room">
			<h3 class="text-center">{{ $r->name }}</h3>
			<div class="row col-md-12 m-0 p-0">
				<?php $col = 8; ?>
				@if ($r->image != '')
					<div class="col-md-4 p-0">
						<img src="/storage/{{ $r->image }}" data-name="{{ $r->name }}"
							class="room-img img-fluid" data-toggle="modal" data-target="#modal-img">
					</div>
				@else
					<?php $col = 12; ?>
				@endif

				<div class="col-md-{{ $col }} px-1">
					<h3>Categoría: {{ $r->category }}</h3>
					<p>{{ $r->content }}</p>
					<p for="">Valor x hora: {{ $r->amount_hour }}</p>
					<p for="">Valor x 1/2 día: {{ $r->amount_half }}</p>
					<p for="">Valor x día: {{ $r->amount_day }}</p>
					<p>{{ $r->content }}</p>
					<p for="">Parqueo: {{ $r->quantity }} / Vehiculo: {{ $r->type }} / Tipo:
						{{ $r->vip }}</p>
				</div>

				<div class="col-12">
					<button type="button" class="btn btn-success btn-block">Reservar</button>
				</div>
			</div>
		</div>
	</div>
@endforeach
