<?php
    $type = ['', 'Motocicleta', 'Carro', 'Camioneta', 'Varios'];
    $vip = ['General', 'Privado'];
    $colors = ['', '#ffc107','#86deff','#7bff7f','#ff7e7e']
?>
<div class="col-md-12 d-flex justify-content-center mb-3">
    <div class="col-md-7 p-0">
        <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text font-weight-bold">Buscar por nombre o categoría</span>
            </div>
            <input type="text" class="form-control" onkeyup="data_filter(this,'list_rooms', 'filter_room', 'd_room')">
        </div>
    </div>
</div>
@foreach ($rooms as $room)
  <div class="col-md-6 d_room" id="d_room{{$room->id}}" data-id="{{$room->id}}" data-first_v="{{$room->name}}"  data-second_v="{{$room->category}}">
      <div class="mb-4 room">
          <div class="row col-md-12 m-0 p-0">
              <?php $col = 8;?>
              @if ($room->image != "")
                  <div class="col-md-4 p-0 d-flex align-items-center justify-content-center">
                      <img src="/storage/{{$room->image}}" data-name="{{$room->name}}" class="room-img img-fluid" data-toggle="modal" data-target="#modal-img">
                  </div>
              @else
                  <?php $col = 12;?>
              @endif

              <div class="col-md-{{$col}} px-1">
                  <h3 class="">{{$room->name}}</h3>
                  <h3>Categoría: {{$room->category}}</h3>
                  <p for="">Valor x hora: {{$room->amount_hour}}</p>
                  <p for="">Valor x 1/2 día: {{$room->amount_half}}</p>
                  <p for="">Valor x día: {{$room->amount_day}}</p>
                  <p>{{$room->content}}</p>
                  <p for="">Parqueo: {{ $room->quantity }} / Vehiculo: {{ $type[$room->type] }} / Tipo: {{ $vip[$room->vip] }}
               </div>

              <div class="col-12">
                  <button type="button" class="btn btn-success btn-block" data-id="{{$room->id}}" data-toggle="modal" data-target="#add">RESERVAR</button>
              </div>
          </div>
      </div>
  </div>
@endforeach

{{-- Mostramos las reservaciones en este rango --}}
@foreach ($reservations as $index => $r)
  @if ($index == 0)
    <div class="col-md-12 text-center">
        <h3>Habitaciones Reservadas</h3>
    </div>
    <div class="col-md-12">
        <div id="msg_reservadas" class="col-md-12"></div>
    </div>
    <div class="col-md-12 d-flex justify-content-center mb-3">
      <div class="col-md-7 p-0">
          <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text font-weight-bold">Buscar por cliente o cédula</span>
              </div>
              <input type="text" class="form-control" onkeyup="data_filter(this)">
          </div>
      </div>
    </div>
  @endif
  <div class="col-md-4 d_reservation" id="d_reservation{{$r->reservation}}" data-id="{{$r->reservation}}" data-first_v="{{$r->customer}}"  data-second_v="{{$r->dni}}">
      <div class="mb-4 reservation">
          <h4 class="text-center">{{$r->name}}</h3>
          <h6 class="text-center uppercase" style="color: {{$colors[$r->state_id]}}">{{$r->state}}</h6>
          <div class="row col-md-12 m-0 p-0">
              <?php $col = 12;?>

              <div class="col-md-{{$col}} px-1">
                  <p>Nombre: {{$r->customer}}</p>
                  <p>Cedula: {{$r->dni}}</p>
                  <p>Teléfono: <a href="tel:{{$r->phone}}" class="a-phone">{{$r->phone}}</a></p>
                  <?php 
                    $aux_start = new \dateTime($r->start);
                    $aux_end = new \dateTime($r->end);
                    $f_start = $aux_start->format('d-m-Y h:i a');
                    $f_end = $aux_end->format('d-m-Y h:i a');
                  ?>
                  <p>Desde: {{$f_start}}</p>
                  <p>Hasta: {{$f_end}}</p>
              </div>

              <div class="col-12">
                <button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-warning btn-block" 
                value="{{$r->reservation}}" onclick="editar(this, {'id':'#res'})">DETALLES</button>
              </div>
          </div>
      </div>
  </div>
@endforeach
