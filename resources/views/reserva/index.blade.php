@extends('layouts.dashboard')
@section('css')
    {!!Html::style("css/css/bootstrap-datepicker.css")!!}
    <style>
        .room{
            background: #727272;
            border-radius: 10px;
            height: 23em;
            overflow-y: auto;
        }
        .room-img{
            cursor: pointer;
            transition: 0.5s;
            width: 90%!important;
            border-radius: 10px;
        }
        .room-img2{
            width: 90%!important;
        }
        .room-img:hover{
            width: 100%!important;
        }
        #complete-img{
            max-width: 550px;
        }
        .gj-datepicker {
            width: auto;
        }
        .w-120px{
            width: 120px
        }
        .w-85px
        {
            width: 85px;
        }
        #total { 
            background: #e9ecef; 
            pointer-events: none;
        }
        #edit_time{
            display: none;
        }
        .reservation {
            background: #727272;
            border-radius: 10px;
            height: 20em;
            overflow-y: auto;
        }
        .a-phone{
            color: #e9ecef;
            font-weight: bold;
        }
        .uppercase{
            text-transform: uppercase;
        }
        #state_re{
            font-size: 20px;
        }
    </style>
@stop
@section('content')

    <div class="d-block bg bg-dark" style="color:white; padding-top: 10px;visibility:hidden;" id="main">
        <div class="row ml-3 mb-3">
            <div class="col-md-12  d-flex justify-content-center">
                <div class="text-center">
                    <h3>SELECCIONE UN RANGO Y HORA</h3>
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Estado</span>
                        </div>
                        <select name="state" id="state" class="form-control">
                            <option value="0">Todas</option>
                            <option value="1">Solicitud</option>
                            <option value="2">Reservada</option>
                            <option value="3">Finalizada</option>
                            <option value="4">Rechazada</option>
                        </select>
                        <div class="input-group-append">
                          <span class="input-group-text">Desde</span>
                        </div>
                        <input id="start" class="fecha" name="start" placeholder='día/mes/año' autocomplete="off"/> 
                        <div class="input-group-append">
                          <span class="input-group-text">Hasta</span>
                        </div>
                        <input id="end" class="fecha" name="end" placeholder='día/mes/año'  autocomplete="off"/> 
                        <div class="input-group-append">
                          <button class="fa fa-search btn btn-info" onclick="consultar();"></button>
                        </div>
                    </div>
                    <em>Esto le mostrara las habitaciones disponibles en ese rango de tiempo</em>

                </div>
            </div>
        </div>
        <div id="mensaje"></div>
        <div class="row ml-3" id="free-rooms">
        </div>
    </div>

    {{-- Detail --}}
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="tile_edit">Detalle de la reservación</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="message_edit"></div>
                <form id="form_edit">
                    <input type="hidden" name="res" id="res">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    <div id="content-detail"></div>
                </form>
            </div>
          </div>
        </div>
    </div>
    
    <!-- Modal IMAGEN-->
    <div class="modal fade" id="modal-img" tabindex="-1" role="dialog" aria-labelledby="img-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="m-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="" alt="" class="img-fluid" id="complete-img">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.cargando.cargando')
    @can('cliente.destroy')
        @include('layouts.modal.deleteModal')
    @endcan
    @include('layouts.modal.confirmModal')
@stop
@section('script')
    {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
    {!!Html::script("js/js/moment.js")!!} 
    {!!Html::script("js/js/bootstrap-datepicker.js")!!} 

    {{-- Si tiene los permisos de actualizar un registro incluira la funcion js correspondiente --}}
    @can('cliente.update')
        {!!Html::script("js/ajax_crud/update.js")!!} 
    @endcan

    {{-- Si tiene los permisos de eliminar un registro incluira la funcion js correspondiente --}}
    @can('cliente.destroy')
        {!!Html::script("js/ajax_crud/delete.js")!!} 
    @endcan

    <script>
        const base = "reservacion";
        const input_delete = "res";
        const standar = "delete_success";
        var $start = $("#start");
        var $end = $("#end");
        var days, hours, minutes;
        const config_date = {
            // timezone
            timeZone: 'America/Managua',
            // Date format. See moment.js' docs for valid formats.
            format: "DD-MM-Y h:mm a",
            // Changes the heading of the datepicker when in "days" view.
            dayViewHeaderFormat: 'MMMM YYYY',
            // On show, will set the picker to the current date/time
            useCurrent: false,
            // See moment.js for valid locales.
            locale: 'Es-es',

            // Change the default icons for the pickers functions.
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-crosshairs',
                clear: 'fa fa-trash-o',
                close: 'fa fa-times'
            },

            // custom tooltip text
            tooltips: {
                today: 'Hoy',
                clear: 'Limpiar',
                close: 'Cerrar',
                selectMonth: 'Selecciona Mes',
                prevMonth: 'Mes anterior',
                nextMonth: 'Mes siguiente',
                selectYear: 'Selecciona Año',
                prevYear: 'Año anterior',
                nextYear: 'Año Siguiente',
                selectDecade: 'Selecciona Decada',
                prevDecade: 'Decada previa',
                nextDecade: 'Decada siguiente',
                prevCentury: 'Siglo anterior',
                nextCentury: 'Siglo siguiente',
                pickHour: 'Selecciona Hora',
                incrementHour: 'Aumenta hora',
                decrementHour: 'Disminuye Hora',
                pickMinute: 'Selecciona Minutos',
                incrementMinute: 'Aumenta Minutos',
                decrementMinute: 'Disminuye Minutos',
                pickSecond: 'Selecciona Segundos',
                incrementSecond: 'Aumenta Segundos',
                decrementSecond: 'Disminuye Segundos',
                togglePeriod: 'Periodo de alternancia',
                selectTime: 'Selecciona tiempo'
            },
            // Defines if moment should use scrict date parsing when considering a date to be valid
            useStrict: true,
            // Shows the picker side by side when using the time and date together
            sideBySide: true,
        }

        $(document).ready(function(){
            // Hacemo visible el contenido
            $("#main").css('visibility','visible'); 
            // Configuramos el modal de carga con 1060 en z-index
            $("#loading").css('z-index',1060);
        });

        $('#modal-img').on('show.bs.modal', function (event) {
            // Obtenemos el elemento de la imagen clickeada
            var img = $(event.relatedTarget)
            // Obtenemos la url de la imagen
            var src = img.prop('src');
            var name = img.data().name;

            // Mostramos el titulo
            $('#m-title').text('Habitación ' + name);
            // Aplicamos la imagen
            $('#complete-img').prop('src', src);
        });

        function consultar()
        {
            var start = $start.val();
            var end = $end.val();
            var state = $("#state").val();
            if(start != "" && end != "")
            {
                let route = '/'+base+'/disponibilidad/';
                $("#loading").css('display','block');

                $("#free-rooms").empty();
                $.get(route, { start: start, end: end, only_reservation: true, state: state }).
                done(function( res ) {
                    $("#free-rooms").append(res)
                    create_data_list();
                    $("#loading").css('display','none');
                });
            }
        }

        $('.fecha').datetimepicker(config_date);

        function buscar_cliente(i = 0)
        {
            // Hacemos un ajuste
            // La web se renderisa por parte así que si consulta la segunda(Detalle) parte de primero
            // La primer parte no existe, por lo tanto no es index 1 sino 0
            if($(".cedula").length == 1)
                i = 0;

            var cedula = $(".cedula").eq(i).val();
            if(i == 0)
                var mensaje = $("#msg_customer");
            else
                var mensaje = $("#msg_customer2");

            if(cedula != "" && cedula != null)
            {
                
                $("#loading").css('display','block');
                var route = '/cliente/search_dni/'+cedula;
                $.get(route, function(res){
                    if(res.code == 1)
                    {
                        $(".nombre").eq(i).val(res.data.name);
                        $(".telefono").eq(i).val(res.data.phone);
                        message(['Datos autocompletados'],{objeto: mensaje, manual:true,tipo:"info"});
                    }
                    else
                    {
                        message(['Cliente no existe'],{objeto: mensaje, manual:true,tipo:"warning"});
                        $(".nombre").eq(i).prop('disabled', false);
                    }
                    $("#loading").css('display','none');
                });
            }
        }

        function press_enter(e, funt)
        {
            tecla = (document.all) ? e.keyCode : e.which;
            if (tecla == 13)
                self[funt]();
            disabled_name();
        }
        
        function disabled_name()
        {
            var disabled = $("#nombre").prop('disabled');
            if(!disabled)
                $("#nombre").prop('disabled', true);
        }

        function update_amount(i = 0)
        {
            if($(".subtotal").length == 1)
                i = 0;

            var subtotal = $(".subtotal").eq(i).val() * 1;
            var descuento = $(".descuento").eq(i).val() * 1;
            var automatico = $(".manual").eq(i).prop('checked');

            if(automatico)
            {
                iva = subtotal * 0.15;
                $(".iva").eq(i).val(iva.toFixed(2));
            }
            else
                iva = $(".iva").eq(i).val() * 1; 

            total = (subtotal + iva) - descuento;

            $(".total").eq(i).val(total.toFixed(2));
        }

        // En base a la información que recibe muestra el detalle de la reservación en el DOM 
        function fill(res)
        {
            // Limpiamos el espacio primero
            $('#content-detail').empty();
            // Consultamos si hay algun codigo de error
            if(typeof(res.code) != "undefined")
                message(res.msg, {objeto:$("#message_edit"), manual:true,tipo:"danger"});
            else
            {
                // Caso contrario mostramos en el dom el contendio
                $('#content-detail').append(res);
                $('.edit_date').datetimepicker(config_date);
            }
            $("#loading").css('display','none');
        }
        
        function update_success(res)
        {
            //Si el codigo es 1, mostramos un msj de exito
            if(res.code == 1)
            {
                message(["Se edito la reservación correctamente"],{objeto:$("#mensaje"), manual:true});
                $("#Edit").modal('toggle');
                consultar();
            }
            else
                message(res.msg,{objeto:$("#msg_customer2"), manual:true,tipo:"danger"});

            // Reactivamos el boton de actualizar
            $("#btn_actualizar").attr("disabled",false);
            // Ocultamos la pantalla de carga
            $("#loading").css('display','none');
        }

        function change_time()
        {
            var display = $("#edit_time").css('display');
            if(display == 'none')
            {
                $("#btn-time").removeClass('fa-cog');
                $("#btn-time").addClass('fa-close');

                $("#new_start").val($("#aux_start").val());
                $("#new_end").val($("#aux_end").val());
                $("#new_start").prop("disabled",false);
                $("#new_end").prop("disabled",false);

                $("#edit_time").css('display', 'block');
            }
            else{
                $("#btn-time").removeClass('fa-close');
                $("#btn-time").addClass('fa-cog');

                $("#new_start").val("");
                $("#new_end").val("");
                $("#new_start").prop("disabled",true);
                $("#new_end").prop("disabled",true);

                $("#edit_time").css('display', 'none');
            }
        }

        function verify_new_time()
        {
            var value = $("#reservation").val();
            var room = $("#room").val();
            var start = $('#new_start').val();
            var end = $('#new_end').val();
            var route = '/'+base+'/confirmar/';

            $("#loading").css('display','block');
            $.get(route, { reservation:value, room:room, start: start, end: end, json: true }).
            done(function( res ) {
                if(res.code == 0)
                {
                    message(res.msg,{objeto:$("#message_edit"), manual:true, tipo: "danger"});
                    $('#Edit').animate({scrollTop:0}, 'fast');
                }
                if(res.code == 1)
                    message(res.msg,{objeto:$("#message_edit"), manual:true, tipo: "info"});
                    
                $("#loading").css('display','none');
            });
        }
        
        function delete_success(res)
        {
            message(res.msg,{objeto:$("#mensaje"), manual:true});
            $("#Edit").modal('toggle');
            consultar();
        }

        var list_reservations;
        function create_data_list(div = ".d_reservation", list = "list_reservations")
        {
            e = $(div);
            self[list] = [];
            var id, first_v, second_v;
            $("#loading").css('display','block');
            for(var i = 0; i < e.length; i++)
            {
                id = e.eq(i).data().id;
                first_v = e.eq(i).data().first_v;
                second_v = e.eq(i).data().second_v;

                self[list][id] = [first_v];
                self[list][id].push(second_v);
            }
            $("#loading").css('display','none');
        }

        var filter_reservations;
        function data_filter(e, list = "list_reservations", filter = "filter_reservations", div = "d_reservation")
        {
            var val1, val2;
            var find = $(e).val().toLowerCase();
            self[filter] = [];
            $("#loading").css('display','blcok');
            self[list].forEach((e, k) => {
                val1 = e[0].toLowerCase()
                val2 = e[1].toLowerCase()
                if(val1.includes(find) || val2.includes(find))
                    self[filter].push(k);
            });
            div_filter(filter, div);
        }

        function div_filter(filter, div)
        {
            $("."+div).css('display','none');
            self[filter].forEach(e => {
                $("#"+div+e).css('display','block');
            });
            $("#loading").css('display','none');
        }


        $("#finalizar").click(function(){
            var route = "/" + base + "/change_state";
            var token = $("#token").val();
            var value = $("#res").val();
            $("#finalizar").attr("disabled",true);
            $("#loading").css('display','block');
            return $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                data: {'finish': value },
                success: function(res){
                    //Si el codigo es 1, mostramos un msj de exito
                    if(res.code == 1)
                    {
                        message(["Reservación finalizada"],{objeto:$("#mensaje"), manual:true});
                        $("#Edit").modal('toggle');
                        consultar();
                    }
                    else
                        message(res.msg,{objeto:$("#msg_customer2"), manual:true,tipo:"danger"});

                    $("#confirmModal").modal('toggle');
                    // Reactivamos el boton de actualizar
                    $("#finalizar").attr("disabled",false);
                    // Ocultamos la pantalla de carga
                    $("#loading").css('display','none');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#msg_customer2"), tipo:"danger"});
                $("#confirmModal").modal('toggle');
                $("#finalizar").attr("disabled",false);
                $("#loading").css('display','none');
            });
        });

    </script>
@stop