<?php
	$type = ['', 'Motocicleta', 'Carro', 'Camioneta', 'Varios'];
	$vip = ['General', 'Privado'];
?>
<div>
	<input type="hidden" name="reservacion" id="reservation" value="{{$reservation->id}}">
	<input type="hidden" name="cuarto" id="room" value="{{$room->id}}">
	<input type="hidden" name="inicio" id="aux_start" value="{{$data->start}}">
	<input type="hidden" name="fin" id="aux_end" value="{{$data->end}}">
</div>
<div class="row">
	<div class="mb-4">
		<div class="row col-md-12 m-0 p-0">
			<?php $col = 8; ?>
			@if ($room->image != '')
				<div class="col-md-4 p-0 d-flex align-items-center justify-content-center">
					<img src="/storage/{{ $room->image }}" data-name="{{ $room->name }}" class="room-img2 img-fluid">
				</div>
			@else
				<?php $col = 12; ?>
			@endif

			<div class="col-md-{{ $col }} px-1">
				<h3 class="text-center">{{ $room->name }}</h3>
				<h3 class="text-center">Categoría: {{ $room->category }}</h3>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">Desde</span>
					</div>
					
					<input type="text" class="form-control" value="{{$data->start}}" disabled>
					<div class="input-group-append">
						<span class="input-group-text">Hasta</span>
					</div>
					<input type="text" class="form-control" value="{{$data->end}}" disabled>
					<div class="input-group-append"> 
						<button type="button" class="btn btn-warning fa fa-cog" title="Cambiar tiempo" id="btn-time" onclick="change_time();"></button>
					</div>
				</div>

				<div id="edit_time">
					<label for="">Nuevo Rango de Tiempo</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">Desde</span>
						</div>
						
						<input type="text" class="form-control edit_date" id="new_start" name="new_start" disabled>
						<div class="input-group-append">
							<span class="input-group-text">Hasta</span>
						</div>
						<input type="text" class="form-control edit_date" id="new_end" name="new_end" disabled>
						<div class="input-group-append"> 
							<button type="button" class="btn btn-warning fa fa-search" title="¿Disponibilidad?" onclick="verify_new_time();"></button>
						</div>
					</div>
				</div>

				<h4>Duración de la reservación</h4>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">Días:</span>
					</div>
					<input type="text" class="form-control" name="days" disabled value="{{$data->duration[0] . " - (C$ " . $data->t_day . ")"}}">
					<div class="input-group-append">
						<span class="input-group-text">Horas:</span>
					</div>
					<input type="text" class="form-control" name="hours" disabled value="{{$data->duration[1] . " - (C$ " . $data->t_hour . ")"}}">
					<div class="input-group-append">
						<span class="input-group-text">Minutos</span>
					</div>
					<input type="text" class="form-control" name="minutes" disabled value="{{$data->duration[2] . " - (C$ " . $data->t_min . ")"}}">
				</div>

				<h4>Precios</h4>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">x Hora:</span>
					</div>
					<input type="text" class="form-control" name="p_days" disabled
						value="{{ $room->amount_hour }}">
					<div class="input-group-append">
						<span class="input-group-text">x 1/2 Día:</span>
					</div>
					<input type="text" class="form-control" name="p_hours" disabled
						value="{{ $room->amount_half }}">
					<div class="input-group-append">
						<span class="input-group-text">x Día</span>
					</div>
					<input type="text" class="form-control" name="p_minutes" disabled
						value="{{ $room->amount_day }}">
				</div>

				
				<h4>Parqueo</h4>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">Tipo:</span>
					</div>
					<input type="text" class="form-control" disabled value="{{ $vip[$room->vip] }}">
					@if($room->vip == 1)
						<div class="input-group-append">
							<span class="input-group-text">Espacio:</span>
						</div>
						<input type="text" class="form-control" disabled value="{{ $room->quantity }}">
					@endif
					<div class="input-group-append">
						<span class="input-group-text">Vehiculo:</span>
					</div>
					<input type="text" class="form-control" disabled value="{{ $type[$room->type] }}">
				</div>

				<h4>Cliente</h4>	
				<div id="msg_customer2"></div>
				<div class="input-group">
					<div class="input-group-append">
						<span class="input-group-text w-85px">Cédula</span>
					</div>
					<input type="text" class="form-control cedula" name="cedula" placeholder="123-123456-1234X"  
					onkeypress="return press_enter(event, 'buscar_cliente')" value="{{$customer->dni}}">
					<div class="input-group-prepend">
						<button type="button" class="btn btn-info fa fa-search" onclick="buscar_cliente(1)"></button>
					</div>
				</div>
				<div class="input-group">
					<div class="input-group-append">
						<span class="input-group-text w-85px">Nombre</span>
					</div>
					<input type="text" class="form-control nombre" name="nombre" disabled value="{{$customer->name}}">
				</div>
				<div class="input-group">
					<div class="input-group-append">
						<span class="input-group-text w-85px">Teléfono</span>
					</div>
					<input type="text" class="form-control telefono" name="telefono" value="{{$customer->phone}}">
				</div>

				<div class="d-flex justify-content-end">
					<div class="col-4">
						<h4 class="text-center">Estado</h4>
						<select name="state_re" id="state_re" class="form-control text-center">
							@foreach ($states as $state)
								@if($reservation->state_id != $state->id)
									<option value="{{$state->id}}">{{$state->name}}</option>
								@else
									<option value="{{$state->id}}" selected>{{$state->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="col-md-8">
						<h4>Monto a pagar</h4>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text w-120px">Sub Total C$</span>
							</div>
							<input type="text" class="form-control subtotal" name="subtotal" value="{{$reservation->subtotal}}" onkeyup="update_amount(1)">
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text w-120px">IVA (15%)</span>
							</div>
							<input type="text" class="form-control iva" name="iva" value="{{$reservation->tax}}" onkeyup="update_amount(1)">
							<div class="input-group-prepend" style="width: 40px;">
								<span class="input-group-text w-120px">
									<input type="checkbox" class="form-control manual" title="Automatico" onchange="update_amount(1)">
								</span>
							</div>
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text w-120px">Descuento C$</span>
							</div>
							<input type="text" class="form-control descuento" name="descuento" onkeyup="update_amount(1)"  value="{{$reservation->discount}}">
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text w-120px">Total C$</span>
							</div>
							<input type="text" class="form-control total" name="total" value="{{$reservation->total}}">
						</div>
					</div>
				</div>
			</div>
			

		</div>
	</div>
</div>
<div class="row">
		<div class="col-6">
				<button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-block btn-danger"  data-value="0">ELIMINAR RESERVA</button>
		</div>
		<div class="col-6">
				<button type="button" class="btn btn-success btn-block" id="btn_actualizar" 
				onclick="actualizar({msg: '#msg_customer2', id:'#res'});">ACTUALIZAR <i class="fa fa-save"></i> </button>
		</div>
</div>
