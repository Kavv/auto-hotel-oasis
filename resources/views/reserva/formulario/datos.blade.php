<?php
	$type = ['', 'Motocicleta', 'Carro', 'Camioneta', 'Varios'];
	$vip = ['General', 'Privado'];

	if(isset($reservation))
	{
		$dni = $reservation->dni;
		$name = $reservation->name;
		$phone = $reservation->phone;
	}
	else {
		$dni = '';
		$name = '';
		$phone = '';
	}
?>
<div>
	<input type="hidden" name="inicio" id="inicio_data" value="{{$data->start}}">
	<input type="hidden" name="fin" id="fin_data" value="{{$data->end}}">
	<input type="hidden" name="cuarto" value="{{$room->id}}">
</div>
<div class="row">
	<div class="mb-4">
		<div class="row col-md-12 m-0 p-0">
			<?php $col = 8; ?>
			@if ($room->image != '')
				<div class="col-md-4 p-0 d-flex align-items-center justify-content-center">
					<img src="/storage/{{ $room->image }}" data-name="{{ $room->name }}" class="room-img2 img-fluid">
				</div>
			@else
				<?php $col = 12; ?>
			@endif

			<div class="col-md-{{ $col }} px-1">
				<h3 class="text-center">{{ $room->name }}</h3>
				<h3 class="text-center">Categoría: {{ $room->category }}</h3>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">Desde</span>
					</div>
					<input type="text" class="form-control" id="temp_start" value="{{$data->start}}" readonly>
					<div class="input-group-append">
						<span class="input-group-text">Hasta</span>
					</div>
					<input type="text" class="form-control" id="temp_end" value="{{$data->end}}" readonly>
				</div>

				<h4>Duración de la reservación</h4>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">Días:</span>
					</div>
					<input type="text" class="form-control" id="days" name="days" disabled value="{{$data->duration[0] . " - (C$ " . $data->t_day . ")"}}">
					<div class="input-group-append">
						<span class="input-group-text">Horas:</span>
					</div>
					<input type="text" class="form-control" id="hours" name="hours" disabled value="{{$data->duration[1] . " - (C$ " . $data->t_hour . ")"}}">
					<div class="input-group-append">
						<span class="input-group-text">Minutos</span>
					</div>
					<input type="text" class="form-control" id="minutes" name="minutes" disabled value="{{$data->duration[2] . " - (C$ " . $data->t_min . ")"}}">
				</div>

				<h4>Precios</h4>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">x Hora:</span>
					</div>
					<input type="text" class="form-control" id="p_days" name="p_days" disabled
						value="{{ $room->amount_hour }}">
					<div class="input-group-append">
						<span class="input-group-text">x 1/2 Día:</span>
					</div>
					<input type="text" class="form-control" id="p_hours" name="p_hours" disabled
						value="{{ $room->amount_half }}">
					<div class="input-group-append">
						<span class="input-group-text">x Día</span>
					</div>
					<input type="text" class="form-control" id="p_minutes" name="p_minutes" disabled
						value="{{ $room->amount_day }}">
				</div>

				
				<h4>Parqueo</h4>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">Tipo:</span>
					</div>
					<input type="text" class="form-control" disabled value="{{ $vip[$room->vip] }}">
					@if($room->vip == 1)
						<div class="input-group-append">
							<span class="input-group-text">Espacio:</span>
						</div>
						<input type="text" class="form-control" disabled value="{{ $room->quantity }}">
					@endif
					<div class="input-group-append">
						<span class="input-group-text">Vehiculo:</span>
					</div>
					<input type="text" class="form-control" disabled value="{{ $type[$room->type] }}">
				</div>

				
				<h4>Cliente</h4>
				<div id="msg_customer"></div>
				<div class="input-group">
					<div class="input-group-append">
						<span class="input-group-text w-85px">Cédula</span>
					</div>
					<input type="text" class="form-control cedula" name="cedula" placeholder="123-123456-1234X"  
					onkeypress="return press_enter(event, 'buscar_cliente')" value="{{$dni}}">
					<div class="input-group-prepend">
						<button type="button" class="btn btn-info fa fa-search" onclick="buscar_cliente()"></button>
					</div>
				</div>
				<div class="input-group">
					<div class="input-group-append">
						<span class="input-group-text w-85px">Nombre</span>
					</div>
					<input type="text" class="form-control nombre" name="nombre" disabled value="{{$name}}">
				</div>
				<div class="input-group">
					<div class="input-group-append">
						<span class="input-group-text w-85px">Teléfono</span>
					</div>
					<input type="text" class="form-control telefono" name="telefono" value="{{$phone}}">
				</div>

				<div class="d-flex justify-content-end">
					<div class="col-md-8">
						<h4>Monto a pagar</h4>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text w-120px">Sub Total C$</span>
							</div>
							<input type="text" class="form-control subtotal" name="subtotal"  value="{{$data->subtotal}}" onkeyup="update_amount()">
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text w-120px">IVA (15%)</span>
							</div>
							<input type="text" class="form-control iva" name="iva"  value="{{$data->tax}}" onkeyup="update_amount()">
							<div class="input-group-prepend" style="width: 40px;">
								<span class="input-group-text w-120px">
									<input type="checkbox" class="form-control manual" title="Automatico" checked onchange="update_amount()">
								</span>
							</div>
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text w-120px">Descuento C$</span>
							</div>
							<input type="text" class="form-control descuento" name="descuento"  onkeyup="update_amount()">
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text w-120px">Total C$</span>
							</div>
							<input type="text" class="form-control total" name="total"  value="{{$data->amount}}">
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
