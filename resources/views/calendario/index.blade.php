<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta content="IE=edge, chrome=1" http-equiv="X-UA-Compatible">
    <meta content="" name="description">
    <meta content="" name="author">
    <title>
        Mi Calendario - Auto Hotel Oasis
    </title>
    {!! Html::style('css/cssF/vendor/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css') !!}

    <style>
        body{
            background: #343a40;
        }
        .content-wrapper {
            background: #343a40 !important;
        }

        .bg-base {
            background: #8cd9eb;
        }

        .bg-reservation {
            background: #ffb8b8 !important;
        }

        .fs-20 {
            font-size: 20px;
        }

        /* Hacemos mas grande el ¨modal largo¨ de bootstrap y valisamos su responsividad */
        .modal-lg {
            min-width: 1000px;
        }
        @media (max-width: 1000px) {
            .modal-lg {
                min-width: 700px!important;
            }
        }
        @media (max-width: 700px) {
            .modal-lg {
                min-width: auto!important;
            }
        }
    </style>
</head>
<body>

    <div class="d-block bg bg-dark" style="padding-top: 10px; visibility:hidden;" id="main">
        @include('alert.mensaje')

        <div class="row mx-2">
            <div class="col-md-12 text-center">
                <h1 class="text-light">Bienvenido {{$customer->name . ' ' . $customer->last_name}} <br>Tu calendario de {{ $lesson->discipline }}</h1>
            </div>
            
            <div class="col-md-12 text-center fs-20">
                <p class="text-light">
                    Si por algun motivo no puedes asistir a tu lección de entrenamiento correspondiente, 
                    puedes consultar otros horarios y así reservar tu asistencia en otra lección, 
                    siempre y cuando exista disponibilidad.
                </p>
            </div>
            <div class="col-md-12 fs-20">
                <p class="text-light pl-4">
                    Es necesario aclarar que: 
                </p>
                <ol class="text-light">
                    <li>Estas reservas son temporales, por lo tanto 
                    una vez terminada la lección, tu horario volverá a su estado original.</li>
                    <li>Si excedes el limite de 5 reservaciones mensuales, 
                    es muy probable que no se te permita seguir haciendo cambios de horarios.</li>
                    <li>Si notamos alguna anomalia en tus reservas, nos pondremos en contacto contigo.</li>
                </ol>
            </div>
        </div>
        <div id="mensaje"></div>
        <div class="row d-flex justify-content-center mx-2" id="schedules">

            @include('calendario.recargable.listar_schedule');
        </div>

        <!-- Modal -->
        <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Otros Horarios</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="modalMessage"></div>
                        <form id="data">
                            <input type="hidden" id="id">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                            <input type="hidden" name="day" value="" id="day">
                            <input type="hidden" name="calendar" value="" id="calendar">
                            <input type="hidden" name="lesson" value="{{ $lesson->id }}">
                            <div id="msgmodal"></div>
                            <div id="otros_horarios"></div>

                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.modal.deleteModal')
    @include('layouts.cargando.cargando')
</body>

</html>





{!! Html::script('js/jsF/vendor/jquery/jquery.min.js') !!}
{!!Html::script("js/jsF/vendor/popper/popper.min.js")!!}
{!! Html::script('js/jsF/vendor/bootstrap/js/bootstrap.min.js') !!}
{!! Html::script('js/jskevin/tiposmensajes.js') !!}

<script>
    $(document).ready(function() {
        $("#loading").css('z-index',1060);
        $("#main").css("visibility", "visible");
    });
    var row;
    $('#deleteModal').on('show.bs.modal', function(event) {
        {{-- Mantenemos el id del elemento seleccionado --}}
        var button = $(event.relatedTarget); // Button that triggered the modal
        var value = button.data('value'); // Extract info from data-* attributes
        $("#id").val(value);

    });

    $('#delete').on('click', function() {
        var route = "/calendario/" + $("#id").val();
        var token = $("#token").val();
        $("#delete").attr('disabled', true);
        $("#loading").css('display', 'block');
        $.ajax({
            url: route,
            headers: {
                'X-CSRF-TOKEN': token
            },
            type: 'DELETE',
            dataType: 'json',
            success: function(res) {
                message(['Se elimino la reserva correctamente'], {
                    manual: true
                });

                actualizar_calendario();

                $("#delete").attr('disabled', false);
                $("#loading").css('display', 'none');
                $("#deleteModal").modal('toggle');
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            message(jqXHR, {
                objeto: $("#deleteModalMessage"),
                tipo: "danger"
            });
            $("#delete").attr('disabled', false);
            $("#loading").css('display', 'none');
        });
    });

    function editar(element) {
        var dia = $(element).data().dia;
        var leccion = $(element).val();

        $("#day").val(dia);
        route = "/calendario/otros/";
        $("#otros_horarios").empty();
        $("#loading").css('display', 'block');
        $.get(route, {
            leccion: leccion,
            dia: dia
        }, function(res) {
            $("#otros_horarios").append(res);
            $("#loading").css('display', 'none');
        });
    }

    function change_schedule(element) {
        var value = $(element).val();
        var token = $("#token").val();
        $(".reservar").attr('disabled', true);
        $("#loading").css('display', 'block');
        var route = '/calendario/';
        $("#calendar").val(value);
        $.ajax({
            url: route,
            headers: {
                'X-CSRF-TOKEN': token
            },
            type: 'POST',
            dataType: 'json',
            data: $("#data").serialize(),
            success: function(res) {
                if (res.code == 1) {
                    message(['Se ha reservado el espacio, podrá asistir a dicha lección'], {
                        manual: true
                    });
                    actualizar_calendario();
                } else {
                    message(res.msg, {
                        objeto: $("#modalMessage"),
                        manual: true,
                        tipo: "danger"
                    });
                }
                $(".reservar").attr('disabled', false);
                $("#Edit").modal('toggle');
                $("#loading").css('display', 'none');
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            $(".reservar").attr('disabled', false);
            $("#loading").css('display', 'none');
            message(jqXHR, {
                objeto: $("#modalMessage"),
                tipo: "danger"
            });
        });
    }

    function actualizar_calendario() {
        route = '/calendario/recargar';
        $("#loading").css('display', 'block');
        $("#schedules").empty();
        $.get(route, function(res) {
            $("#schedules").append(res);
            $("#loading").css('display', 'none');
        });
    }
</script>
