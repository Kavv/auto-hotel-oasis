@extends('layouts.dashboard')
@section('css')
    <style>
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')

<!--Arreglar toda la mierda que hiciste-->
    <div class="d-block bg bg-dark" style="padding-top: 10px; color:white;visibility:hidden;" id="main">
        @include('alert.mensaje')
        <div id="mensaje"></div>
        <!--class="table table-striped table-bordered"-->
        <table class="table table-bordered table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;" >
            <thead>
                <tr>
                    <th class="text-center" >Estado</th>
                    <th class="text-center" >Lección</th>
                    <th class="text-center" style="width:20%;">Cedula</th>
                    <th class="text-center" >Nombre</th>
                    <th class="text-center" >Apellido</th>
                    <th class="text-center" data-orderable="false" style="width:15%;"></th>
                </tr>
            </thead >
            <tbody class="text-center" id="lista" >
                @include('calendario.admin.listaclientes')
            </tbody>
        </table>
    </div>
    
    @include('layouts.cargando.cargando')
@stop
@section("script")
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
    {!!Html::script("js/jskevin/kavvdt.js")!!} 




    <script>

        var table;
        var fila;
    

        $(document).ready( function () {
            table=createdt($('#Datos'),{buscar:'{!!session("valor")!!}',col:1,cant:[10,20,-1],cantT:[10,20,"todo"]})
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index',1060);
        });
        

    </script>
@stop