@extends('layouts.dashboard')
@section('css')
    <style>
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')
<!--Arreglar toda la mierda que hiciste-->
    <div class="d-block bg bg-dark" style="padding-top: 10px; color:white;visibility:hidden;" id="main">
        @include('alert.mensaje')
        <div id="mensaje"></div>
        <!--class="table table-striped table-bordered"-->
        <table class="table table-bordered table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;" >
            <thead>
                <tr>
                    <th class="text-center" >Estado</th>
                    <th class="text-center" style="width:20%;">Cedula</th>
                    <th class="text-center" >Nombre</th>
                    <th class="text-center" >Lección</th>
                    <th class="text-center" >Entrenador</th>
                    <th class="text-center" >Hora reservada</th>
                    <th class="text-center" data-orderable="false" style="width:15%;"></th>
                </tr>
            </thead >
            <tbody class="text-center" id="lista" >
                @include('calendario.admin.listarreservas')
            </tbody>
        </table>
    </div>
    
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Detalle del cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <form id="data">
                        <input type="hidden" id="id">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <div id="msgmodal"></div>
                        <?php $bt_columns = 6;
                        $edit = true; ?>
                        @include('calendario.admin.cliente_detalle')
                        
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    @include('layouts.cargando.cargando')
    @include('layouts.modal.deleteModal')
@stop
@section("script")
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!}  
    {!!Html::script("js/jskevin/kavvdt.js")!!}  
    <script>
        var table;

        $(document).ready( function () {
            table= createdt($("#Datos"),{buscar:'{!!session("valor")!!}',col:1,cant:[10,20,-1],cantT:[10,20,"todo"] });
            $("#main").css("visibility", "visible"); 
            $("#loading").css('z-index',1060);
        });
        
        $('.edit').on( 'click', function () {
            
            route = "/cliente/detail/"+$(this).val();
            $.get(route, function(res){
                phones = res.phones;
                prueba = phones;
                data = res.data;
                
                $("#CC").val(data.dni);
                $("#estado").val(data.status_id);
                $("#Nombre").val(data.name);
                $("#Apellido").val(data.last_name);
                $("#Edad").val(data.age);
                $("#Sexo").val(data.gender);
                $("#Direccion").val(data.address);
                
                //Limpiamos la casilla del telefono
                $("#telefono").val("");
                // Limpiamos todos los telefonos que se estubiesen mostrando
                $("#telefonos").empty();
                for(var i=0; i < phones.length; i++)
                {
                    //enviamos el numero y especificamos que la agregacion sera manual(true)
                    addPhone(phones[i].number,true);
                }

                $("#actualizar").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });
        function addPhone(telefono=null, manual=false)
        {
            if(telefono == null)
                telefono = $("#telefono").val();
            if(telefono != "")
            {
                html=   '<div class="telefono-clone col-md-4">'+
                            '<div class="form-group">'+
                                '<div class="input-group ">'+
                                    '<input value="'+ telefono +'" class="form-control telefono" placeholder="Ingrese el numero" name="telefono[]" type="tel">'+
                                '</div>'+
                            '</div>'+
                        '</div>';

                $("#telefono").val("");
                $("#telefonos").append(html);
                if(manual == false)
                    $("#telefono").focus()
            }
        }
        var row;
        $('#deleteModal').on('show.bs.modal', function (event) {
            {{-- Mantenemos el id del elemento seleccionado --}}
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            $("#id").val(value);

            {{-- Mantenemos la fila del elemento seleccionado --}}
            row = button.parents('tr');
        });

        $('#delete').on('click', function () {
            var route="/calendario/"+$("#id").val();
            var token=$("#token").val();
            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                success: function(res){
                    message(['Se elimino la reservación correctamente'],{manual:true});
                    table.row(row).remove().draw( false );
                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                    $('body').animate({scrollTop:0}, 'fast');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#deleteModalMessage"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });
    </script>
@stop