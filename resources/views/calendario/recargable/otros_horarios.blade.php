
<div class="row d-flex justify-content-center mx-2">
    @foreach ($base_schedule as $s)
        <div class="col-md-6 mb-2">
            <div class="card" style="background: #ffb8b8;">
                <div class="card-body text-center">
                    <h5 class="card-title">{{$days[$s->day]}}</h5>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <input type="time" class="form-control text-center" value="{{$s->begin}}" disabled>
                        </div>
                        <div class="col-md-6">
                            <input type="time" class="form-control text-center" value="{{$s->end}}" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="" class="font-weight-bold m-0">Entrenador</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">{{$s->name_e . ' ' . $s->last_name_e}}</label>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary reservar" value="{{$s->id}}" onclick="change_schedule(this)">Reservar temporalmente</button>
                    <button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash d-none" data-value="{{$s->lesson}}"></button>
                </div>
            </div>
        </div>    
    @endforeach
</div>