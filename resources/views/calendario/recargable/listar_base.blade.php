
<div class="col-md-4 mt-3">
    <div class="card bg-base">
        <div class="card-body text-center">
            <h5 class="card-title">{{$days[$s->day]}}</h5>
            <div class="row mb-2">
                <div class="col-md-6">
                    <input type="time" class="form-control text-center" value="{{$s->begin}}" disabled>
                </div>
                <div class="col-md-6">
                    <input type="time" class="form-control text-center" value="{{$s->end}}" disabled>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="" class="font-weight-bold m-0">Entrenador</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">{{$staff->name . ' ' . $staff->last_name}}</label>
                </div>
            </div>
            <button type="button" data-toggle="modal" data-target="#Edit" data-dia="{{$s->day}}"
            class="btn btn-secondary edit" onclick="editar(this);" value="{{$lesson->id}}">Ver otros horarios</button>
        </div>
    </div>
</div>  