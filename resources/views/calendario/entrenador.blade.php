<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta content="IE=edge, chrome=1" http-equiv="X-UA-Compatible">
    <meta content="" name="description">
    <meta content="" name="author">
    <title>
        Mi Calendario de trabajo - Auto Hotel Oasis
    </title>
    {!! Html::style('css/cssF/vendor/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css') !!}

    <style>
        body{
            background: #343a40;
        }
        .content-wrapper {
            background: #343a40 !important;
        }

        .bg-base {
            background: #8cd9eb;
        }

        .bg-reservation {
            background: #ffb8b8 !important;
        }

        .fs-20 {
            font-size: 20px;
        }

        /* Hacemos mas grande el ¨modal largo¨ de bootstrap y valisamos su responsividad */
        .modal-lg {
            min-width: 1000px;
        }
        @media (max-width: 1000px) {
            .modal-lg {
                min-width: 700px!important;
            }
        }
        @media (max-width: 700px) {
            .modal-lg {
                min-width: auto!important;
            }
        }
        .titulos {
            border-radius: 25px;
            background: #ff5a1a;
            padding-top: 10px;
            padding-bottom: 10px;
            text-align: center;
        }
        .customer_extra {
            background: rgb(236, 177, 177);
        }
        #detalle_leccion, #cantidad {
            font-size: 18px;
            font-weight: bold;
        }
    </style>
</head>
<body>

    <div class="d-block bg bg-dark" style="padding-top: 10px; visibility:hidden;" id="main">
        @include('alert.mensaje')

        <div id="mensaje"></div>
        <div class="row d-flex justify-content-center mx-2" id="schedules">

            @include('calendario.entrenador.listar_schedule');
        </div>

        <!-- Modal -->
        <div class="modal fade" id="m-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Alumnos asignados a la lección</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p id="detalle_leccion"></p>
                                <p id="cantidad"></p>
                            </div>
                        </div>
                        <table id="table" class="table table-bordered table-hover" cellspacing="0" style="width:100%;">
                            <thead>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>De otro grupo</th>
                            </thead>
                            <tbody id="tb-athletes">
                                
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="m-restart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">¿Esta seguro de finalizar la lección?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="modalMessage"></div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <input type="hidden" name="calendar" value="" id="calendar">
                        <div id="msgmodal"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    Al dar por finalizada la lección de entrenamiento se removeran las reservaciones 
                                    que han realizado los atletas de otro turno o grupo para esta misma leccion.
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-block btn-success" onclick="restart(this)">Confirmar Finalzación</button>
                            </div>
                            <div class="col-md-6">
                                <button type="button" data-dismiss="modal" class="btn btn-block btn-danger">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.modal.deleteModal')
    @include('layouts.cargando.cargando')
</body>

</html>

{!! Html::script('js/jsF/vendor/jquery/jquery.min.js') !!}
{!! Html::script("js/jsF/vendor/popper/popper.min.js")!!}
{!! Html::script('js/jsF/vendor/bootstrap/js/bootstrap.min.js') !!}
{!! Html::script('js/jskevin/tiposmensajes.js') !!}

<script>
    $(document).ready(function() {
        $("#loading").css('z-index',1060);
        $("#main").css("visibility", "visible");
    });

    function format_hour(hour)
    {
        var aux = hour.split(':')
        var period = " a.m.";
        var new_hour;
        if(aux[0] >= 12)
        {
            period = " p.m.";
            if(aux[0] > 12)
                aux[0] = parseInt(aux[0]) - 12;
            new_hour = aux[0] + ":" + aux[1] + period;
        }
        else
            new_hour = aux[0] + ":" + aux[1] + period;
        
        return new_hour;
    }
    function show_athletes(element)
    {
        var leccion = $(element).val();
        var dia = $(element).data().dia;
        var t_dia = $(element).data().t_dia;
        var disciplina = $(element).data().disciplina;
        var begin = format_hour($(element).data().begin);
        var end = format_hour($(element).data().end);
        prueba = begin;
        var detalle = "Día " + t_dia + " - Lección de " + disciplina + " de " + begin + " a " + end;
        $("#detalle_leccion").text(detalle);
        $("#day").val(dia);
        route = "/calendario/athletes/";
        $("#tb-athletes").empty();
        $("#loading").css('display', 'block');
        $.get(route, {
            leccion: leccion,
            dia: dia
        }, function(res) {
            
            $("#tb-athletes").append(res);
            var cantidad = $("#tb-athletes tr").length;
            $("#cantidad").text("Cantidad: " + cantidad);
            $("#loading").css('display', 'none');
        });
    }

    function end_lesson(element)
    {
        var calendar = $(element).val();
        $("#calendar").val(calendar);
        $("#m-restart").modal('toggle');
    }

    function restart()
    {
        var value = $("#calendar").val();
        var token = $("#token").val();
        
        $("#loading").css('display', 'block');
        var route = '/calendario/restart';
        
        $.ajax({
            url: route,
            headers: {
                'X-CSRF-TOKEN': token
            },
            type: 'POST',
            dataType: 'json',
            data: {'calendar': value},
            success: function(res) {
                if (res.code == 1) {
                    message(['¡Se ha dado por finalizada la lección de entrenamiento!'], {
                        manual: true
                    });
                } else {
                    message(res.msg, {
                        objeto: $("#modalMessage"),
                        manual: true,
                        tipo: "danger"
                    });
                }
                
                $("#m-restart").modal('toggle');
                $("#loading").css('display', 'none');
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            $("#loading").css('display', 'none');
            message(jqXHR, {
                objeto: $("#modalMessage"),
                tipo: "danger"
            });
        });
    }
</script>
