        <div class="row ">
            @if($edit)
              <div class="col-md-{{ $bt_columns }}">
                  <div class="form-group">
                      {!!Form::label('Estado:')!!}
                      <select name="estado" id="estado" class="form-control border-warning">
                          @foreach($status as $stade)
                              <option value="{{$stade->id}}">{{$stade->name}}</option>
                          @endforeach
                      </select>
                  </div>
              </div>
            @endif
            <div class="col-md-{{ $bt_columns }} ">
                <div class="form-group text-center">
                    {!! Form::label('Cedula', 'Cedula:') !!}
                    {!! Form::text('Cedula_Cliente', null, ['class' => 'form-control border border-warning', 'placeholder' => 'xxx-xxxxxx-xxxxx', 'autocomplete' => 'off', 'onkeypress' => 'return CharCedula(event,this,"#nacional");', 'onkeyup' => 'formatonica(this,"#nacional")', 'id' => 'CC']) !!}
                </div>
            </div>
            <div class="col-md-{{ $bt_columns }}">
                <div class="form-group">
                    {!! Form::label('Nombre:') !!}
                    {!! Form::text('Nombre', null, ['id' => 'Nombre', 'class' => 'form-control border border-warning', 'placeholder' => 'Nombre del Cliente']) !!}
                </div>
            </div>
            <div class="col-md-{{ $bt_columns }}">
                <div class="form-group">
                    {!! Form::label('Apellido:') !!}
                    {!! Form::text('Apellido', null, ['id' => 'Apellido', 'class' => 'form-control border border-warning', 'placeholder' => 'Apellido del Cliente']) !!}
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('Edad:') !!}
                    {!! Form::text('Edad', null, ['id' => 'Edad', 'class' => 'form-control', 'placeholder' => 'Edad del Cliente']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('Genero:') !!}
                    {!! Form::select('Sexo', ['Masculino' => 'Masculino', 'Femenino' => 'Femenino', 'No Definir' => 'No Definir  '], null, ['placeholder' => 'Seleeciona un genero', 'class' => 'form-control', 'id' => 'Sexo']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('Números de teléfono:') !!}
            </div>
        </div>

        <div id="telefonos" class="row">
        </div>

        <div class="row ">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('Direccion:') !!}
                    {!! Form::text('Direccion', null, ['id' => 'Direccion', 'class' => 'form-control', 'placeholder' => 'Direccion del Cliente']) !!}
                </div>
            </div>
        </div>
