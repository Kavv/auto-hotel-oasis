
  @foreach($customers as $customer)
    <?php 
      $begin = strtotime($customer->begin);
      $begin = date('h:i a', $begin);
      $end = strtotime($customer->end);
      $end = date('h:i a', $end);
    ?>
    <tr @if($customer->vetoed_id != null) class = "text-danger" @endif>
      <td>{{$customer->state}}</td> 
      <td>{{$customer->dni}}</td> 
      <td >{{$customer->name . " " . $customer->last_name}}</td>    
      <td>{{$customer->code}}</td> 
      <td>{{$customer->tname . " " . $customer->tlast}}</td>
      <td >{{$begin . " - " . $end}}</td>  
      <td class="botones d-flex justify-content-center">
        <button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-eye" value="{{$customer->id}}"></button>
        <form action="/calendario/horario_atleta" target="_blank" method="GET" class="m-0">
          <input type="hidden" name="cliente" value="{{$customer->id}}">
          <button type="submit" class="btn btn-warning edit fa fa-calendar"></button>
        </form>
        <button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="{{$customer->reservation}}"></button>
      </td>
    </tr>
  @endforeach