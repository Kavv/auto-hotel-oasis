
  @foreach($clientes as $cliente)
    <tr @if($cliente->vetoed_id != null) class = "text-danger" @endif>
      <td>{{$cliente->state}}</td> 
      <td>{{$cliente->code}}</td> 
      <td>{{$cliente->dni}}</td> 
      <td >{{$cliente->name}}</td>    
      <td >{{$cliente->last_name}}</td>  
      <td class="botones" >
        <form action="/calendario/horario_atleta" target="_blank" method="GET">
          <input type="hidden" name="cliente" value="{{$cliente->id}}">
          <button type="submit" class="btn btn-primary edit">Reservaciones</button>
        </form>
      </td>
    </tr>
  @endforeach