@foreach($trainers as $trainer)
    <tr @if($trainer->vetoed_id != null) class = "text-danger" @endif>
        <td>{{$trainer->dni}}</td> 
        <td>{{$trainer->name}}</td>    
        <td>{{$trainer->last_name}}</td>  
        <td>{{$trainer->age}}</td>   
        <td>{{$trainer->address}}</td>
        <td class="botones">
            <form action="/calendario/horario_entrenador" target="_blank" method="GET">
                <input type="hidden" name="entrenador" value="{{$trainer->id}}">
                <button type="submit" class="btn btn-primary edit">Calendario Asignado</button>
            </form>
        </td>
    </tr>
@endforeach