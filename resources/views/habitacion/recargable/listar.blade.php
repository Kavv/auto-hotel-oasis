
  @foreach($data as $d)
    <tr>
      <?php 
          $category = $d->category . " (C$" . $d->amount_hour .  ", C$" . $d->amount_half .  ", C$" . $d->amount_day . ")" ;
          if($d->vip == 1)
              $parking = "Tipo: " . $vip[$d->vip] . " / Vehiculo: " . $type[$d->type] . " / Espacio: " . $d->quantity;
          else
              $parking = "Tipo: " . $vip[$d->vip] . " / Vehiculo: " . $type[$d->type]; 
      ?>
      <td>{{$d->name}}</td>  
      <td>{{$category}}</td>  
      <td>{{$d->people}}</td>
      <td>{{$parking}}</td>  
      <td class="botones p-0">
        <button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-pencil" value="{{$d->id}}" onclick="editar(this, {})"></button>
        @can('cliente.destroy') 
          <button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="{{$d->id}}"></button>
        @endcan
      </td>
    </tr>
  @endforeach