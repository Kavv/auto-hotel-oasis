
                    
<div class="row">
    <div class="col-6">
        <label for="">Nombre de la habitación</label>
        <input type="text" name="nombre" class="nombre form-control">
    </div>
    <div class="col-6">
        <label for="">Categoría</label>
        <select name="categoria" class="categoria form-control">
            <option value=""></option>
            @foreach($categories as $c)
                <option value="{{$c->id}}">{{$c->name . " (C$" . $c->amount_hour .  ", C$" . $c->amount_half .  ", C$" . $c->amount_day . ")" }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <label for="">Parqueo asignado</label>
        <select name="parqueo" class="parqueo form-control">
            <option value=""></option>
            @foreach($parking as $p)
                <?php 
                    if($p->vip == 1)
                        $text = "Parqueo: " . $vip[$p->vip] . " Vehiculo: " . $type[$p->type] . " Espacio: " . $p->quantity;
                    else
                        $text = "Parqueo: " . $vip[$p->vip] . " Vehiculo: " . $type[$p->type]; 
                ?>
                <option value="{{$p->id}}">{{ $text }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-6">
        <label for="">Descripción</label>
        <input type="hidden" name="descripcion_id" class="descripcion_id">
        <input type="text" name="descripcion" class="descripcion form-control">
    </div>
</div>
<div class="row">
    <div class="col-6">
        <label for="">Cantidad de personas</label>
        <input type="text" name="personas" class="personas form-control">
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <label for="">Portada</label>
                <input type="file" name="portada" class="form-control portada" accept="image/*">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="prev"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <label for="">Productos de la habitación</label>
                <div class="input-group mb-3">
                    <select class="form-control select_product">
                        <option value=""></option>
                        @foreach ($products as $p)
                            <option value="{{$p->id}}">{{$p->name}}</option>
                        @endforeach
                    </select>
                    <div class="input-group-append">
                        <button class="btn btn-success fa fa-plus" type="button" onclick="add_product({{$index}})"></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="lista_productos row"></div>
            </div>
        </div>
    </div>
</div>