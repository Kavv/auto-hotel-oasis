@extends('layouts.dashboard')
@section('css')
    <style>
        
    </style>
@stop
@section('content')

    <div class="d-block bg bg-dark" style="color:white; padding-top: 10px;visibility:hidden;" id="main">
        <div class="row my-4 pl-4" id="div_agregar">
            <button class="btn btn-success fa fa-plus" data-toggle="modal" data-target="#add">Agregrar un registro</button>
        </div>

        <div style="overflow-x: auto; min-width:80%;">
            @include('alert.mensaje')
            <div id="mensaje"></div>
            <table class="table table-hover table-dark text-center" cellspacing="0" id="Datos" style="width:100%;">
                <thead>
                    <th style="">Nombre</th>
                    <th style="">Categoria</th>
                    <th style="">Personas</th>
                    <th style="">Parqueo</th>
                    <th data-orderable="false"></th>
                </thead>
                <tbody class="text-center" id="lista"> 
                    @include('habitacion.recargable.listar')
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="add" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="tile_madd">Agregar un registro</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="msj_add"></div>
                <form action="" id="form_data" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    <?php $index = 0;?>
                    @include('habitacion.formulario.datos')
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-success" id="btn_guardar">Guardar <i class="fa fa-save"></i> </button>
            </div>
          </div>
        </div>
    </div>

    @can('cliente.edit')
        <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Editar Registro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="message_edit"></div>
                        <form id="form_edit" enctype="multipart/form-data">
                            @method('PUT')
                            <input type="hidden" id="id" name="id">
                            <?php $index = 1;?>
                            @include('habitacion.formulario.datos')
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="btn_actualizar"
                                onclick="actualizar({msg: '#message_edit', type: 'POST', ctype: false})">Actualizar</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcan


    @can('cliente.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!}  
    {!!Html::script("js/jskevin/kavvdt.js")!!} 

    {{-- Si tiene los permisos de crear un registro incluira la funcion js correspondiente --}}
    @can('cliente.create')
        {!!Html::script("js/ajax_crud/create.js")!!} 
    @endcan
    
    {{-- Si tiene los permisos de actualizar un registro incluira la funcion js correspondiente --}}
    @can('cliente.update')
        {!!Html::script("js/ajax_crud/update.js")!!} 
    @endcan

    {{-- Si tiene los permisos de eliminar un registro incluira la funcion js correspondiente --}}
    @can('cliente.destroy')
        {!!Html::script("js/ajax_crud/delete.js")!!} 
    @endcan

    <script>
        var table;
        const base = "habitacion";
        $(document).ready(function(){
            // Convertimos la tabla a una DataTable
            table = createdt($("#Datos"),{cant:[10,20,-1],cantT:["10","20","Todo"]});
            // Hacemo visible el contenido
            $("#main").css('visibility','visible'); 
            // Configuramos el modal de carga con 1060 en z-index
            $("#loading").css('z-index',1060);
        });
        
        // Por efectos de visibilidad utilizamos este metodo click
        $("#btn_guardar").click(function(){
            // Llamamos a la funcion que guarda el nuevo registro
            save({
                msg: '#msj_add', 
            });
        });
        // Resultado exitoso del create
        function success(res)
        {
            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code === 1)
            {
                // Mostramos un msj de exito
                message(["Se agrego el registro correctamente"],{manual:true});
                // Llamamos a la funcion que agrega una fila a la tabla
                addRow(res.data);
                // Ocultamos el modal de agregación
                $("#add").modal('hide');
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{objeto:$("#msj_add"), manual:true,tipo:"danger"});
            }

            $("#btn_guardar").attr("disabled",false);
            $("#loading").css('display','none');
        }

        function addRow(data)
        {
            // Almacenara los html de las acciones
            var botones = "";
            // Si tiene permisos de edicion se adjunta el html del btn para editar
            @can('cliente.update') 
                botones += '<button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-pencil" value="'+data.id+'" onclick="editar(this, {})"></button>';
            @endcan
            // Si tiene permisos de eliminación se adjunta el html del btn para eliminar
            @can('cliente.destroy') 
                botones += '<button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="'+data.id+'"></button>';
            @endcan

            // Hacemos referencia a la variable que contienen la DataTable
            // Accedemos a su propiedad row y al metodo add
            // Especificando ahí los datos de cada columna en la tabla
            table.row.add( [
                $(".nombre").eq(0).val(),
                $(".categoria option:selected").eq(0).text(),
                $(".parqueo option:selected").eq(0).text(),
                $(".descripcion").eq(0).val(),
                botones,
            ] ).draw( false );
        }

        // Resultado exitoso del detalle o edit
        function fill(res)
        {
            // removemos la imagen en caso de que exista una previa
            $(".prev").eq(1).empty()

            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code == 1)
            {
                // Extraemos la información del registro
                let data = res.data;
                // Rellenamos los inputs con los valores correspondientes
                $(".nombre").eq(1).val(data.name);
                $(".categoria").eq(1).val(data.category_id);
                $(".personas").eq(1).val(data.people);
                $(".parqueo").eq(1).val(data.parking_id);
                if (typeof data.description !== 'undefined')
                {
                    $(".descripcion_id").eq(1).val(data.description.id);
                    $(".descripcion").eq(1).val(data.description.content);
                }

                // Si existe un nombre de imagen
                if(data.image != null)
                {
                    // La mostramos
                    var html_img = "<img class='mx-auto d-block' src='storage/"+data.image+"'></img>";
                    $(".prev").eq(1).empty()
                    $(".prev").eq(1).append(html_img)
                }

                data.items.forEach(item => {
                    add_product(1, item.id, item.name);
                });
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{manual:true,tipo:"danger"});
                $("#Edit").modal('hide');
            }
            // Ocultamos la pantalla de carga
            $("#loading").css('display','none');
        }

        // Resultado exitoso del update
        function update_success(res)
        {
            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code == 1)
            {
                // Mostramos un mensaje de error
                message(["Se edito el registro correctamente"],{manual:true});
                data = res.data;
                var nombre = $(".nombre").eq(1).val();
                var categoria = $(".categoria option:selected").eq(1).text();
                var personas = $(".personas").eq(1).val();
                var parqueo = $(".parqueo option:selected").eq(1).text();
                var descripcion = $(".descripcion").eq(1).val();
                // Plasmamos en la tabla los nuevos valores
                table.cell(fila.children('td')[0]).data( nombre );
                table.cell(fila.children('td')[1]).data( categoria  );
                table.cell(fila.children('td')[2]).data( personas );
                table.cell(fila.children('td')[3]).data( parqueo );
                // Ocultamos el modal
                $("#Edit").modal('hide');
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{objeto:$("#message_edit"), manual:true,tipo:"danger"});
            }
            // Reactivamos el boton de actualizar
            $("#btn_actualizar").attr("disabled",false);
            // Ocultamos la pantalla de carga
            $("#loading").css('display','none');
        }

        function add_product(modal = 0, value = null, text = null)
        {
            // Obtenemos el select con el que se esta trabajando actualmente
            var $select = $(".select_product option:selected").eq(modal);
            // Del mismo sacamos el valor y el texto
            if(value == null)
                value = $select.val();
            if(text == null)
                text = $select.text();

            // Si ya existe un elemento en el dom con el identificador del producto, terminamos la funcion.
            var exist = $("#p"+modal+value).length;
            if(exist > 0)
                return;

            // Si el valor o el texto es vacio no se lleva a cabo la agregación
            if(value != "" && text != "")
            {
                // Generamos el html
                var html = ''+
                '<div class="col-md-12" id="div_'+modal+value+'">' +
                    '<div class="input-group mb-3">' +
                        '<input id="p'+modal+value+'" type="hidden" name="productos[]" value="'+value+'">' +
                        '<input type="text" class="form-control productos" disabled value="'+text+'">' +
                        '<div class="input-group-append">' +
                            '<button type="button" class="btn btn-danger fa fa-trash" onclick="remove_product(\''+modal+value+'\')"></button>' +
                        '</div>' +
                    '</div>' +
                '</div>';
                // Agregamos el producto a la lista
                $(".lista_productos").eq(modal).append(html);
            }
        }
        function remove_product(code)
        {
            // Debido a un bug el code tiene un espacio vacio al inicio, hay que removerlo con esta funcion trim
            code = code.trim();
            // Buscamos el elemento en el dom y eliminamos
            $("#div_"+code).remove();
        }

        $('#Edit').on('hidden.bs.modal', function (e) {
            $(".lista_productos").empty();
        });

        var index_form;
        $('#Edit').on('show.bs.modal', function (e) {
            index_form = 1;
        });
        $('#add').on('show.bs.modal', function (e) {
            index_form = 0;
        });
        
    $('.portada').change(function(e) {
        console.log("entra");
      addImage(e);
    });

    function addImage(e){
      let reader = new FileReader();
      arch = e.target.files[0];
      let imag = $(".prev").eq(index_form);
      imag.empty();

      //si no es imagen
      if((!arch.type.match('image.*'))) {
        // box.empty();
        // imag.empty();
        message(['No ingreso una Imagen! '],{objeto:$(mensaje), manual:true, tipo:"danger"});
      }
      else{
        // box.empty();
        // imag.empty();
        reader.onload = function() {
          let imagen = $('.prev')[index_form];
          image = document.createElement('img');
          //le pasamos la imagen al source
          image.src = reader.result;
          image.setAttribute('class','mx-auto d-block');

          imagen.innerHTML = '';
          imagen.append(image);
        };
        reader.readAsDataURL(arch);
      }
    }
    </script>
@stop