@extends('layouts.dashboard')
@section('css')
    <style>
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')
    <div class="d-block bg-dark" style="color:white; padding-top: 10px;visibility:hidden;" id="main">
        
        <div class="row my-4 pl-4" id="div_agregar">
            <button class="btn btn-success" data-toggle="modal" data-target="#add">Agregrar un registro <span class="fa fa-plus"></span></button>
        </div>
        @include('alert.mensaje')
        <div id="mensaje"></div>
        <table class="table table-hover table-dark text-center" cellspacing="0" id="Datos" style="width:100%;" >
            <thead>
                <th class="text-center">Nombre</th>
                <th class="text-center" data-orderable="false" style="width:20%"></th>
            </thead>
            <tbody id="lista"> 
                @include('servicio.recargable.listaservicios')
            </tbody>
        </table>
    </div>


    <div class="modal fade" id="add" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="tile_madd">Agregar un registro</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="msj_add"></div>
                <form action="" id="form_data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    @include('servicio.formulario.datos')
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-success" id="btn_guardar">Guardar <i class="fa fa-save"></i> </button>
            </div>
          </div>
        </div>
    </div>

    @can('servicio.edit')
    <!-- Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="message_edit"></div>
                    <form id="form_edit" autocomplete = off>
                        <input type="hidden" id="id">
                        <!--UNA VEZ TERMINADA LA CLASE DE IS REGRESAR LOS FORMULARIOS A SU TAMAñO GRANDE E IMPORTARLO AQUI-->

                        @include('servicio.formulario.datos')
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="btn_actualizar"
                            onclick="actualizar({msg: '#message_edit'})">Actualizar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan
    
    @can('servicio.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!}  
    {!!Html::script("js/jskevin/kavvdt.js")!!}   

    

    {{-- Si tiene los permisos de crear un registro incluira la funcion js correspondiente --}}
    @can('cliente.create')
        {!!Html::script("js/ajax_crud/create.js")!!} 
    @endcan
    
    {{-- Si tiene los permisos de actualizar un registro incluira la funcion js correspondiente --}}
    @can('cliente.update')
        {!!Html::script("js/ajax_crud/update.js")!!} 
    @endcan

    {{-- Si tiene los permisos de eliminar un registro incluira la funcion js correspondiente --}}
    @can('cliente.destroy')
        {!!Html::script("js/ajax_crud/delete.js")!!} 
    @endcan

    <script>
    
        var table;
        var fila;
        const base = "servicio";
        $(document).ready( function () {
            table=createdt($('#Datos'),{buscar:'{!!session("valor")!!}',cant:[10,20,-1],cantT:[10,20,"Todo"]});
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index',1060);
        });
        
        
        // Por efectos de visibilidad utilizamos este metodo click
        $("#btn_guardar").click(function(){
            // Llamamos a la funcion que guarda el nuevo registro
            save({
                msg: '#msj_add', 
            });
        });
        // Resultado exitoso del create
        function success(res)
        {
            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code === 1)
            {
                // Mostramos un msj de exito
                message(["Se agrego el registro correctamente"],{manual:true});
                // Llamamos a la funcion que agrega una fila a la tabla
                addRow(res.data);
                // Ocultamos el modal de agregación
                $("#add").modal('hide');
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{objeto:$("#msj_add"), manual:true,tipo:"danger"});
            }

            $("#btn_guardar").attr("disabled",false);
            $("#loading").css('display','none');
        }

        function addRow(data)
        {
            // Almacenara los html de las acciones
            var botones = "";
            // Si tiene permisos de edicion se adjunta el html del btn para editar
            @can('cliente.update') 
                botones += '<button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-pencil" value="'+data.id+'"></button>';
            @endcan
            // Si tiene permisos de eliminación se adjunta el html del btn para eliminar
            @can('cliente.destroy') 
                botones += '<button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="'+data.id+'"></button>';
            @endcan

            // Hacemos referencia a la variable que contienen la DataTable
            // Accedemos a su propiedad row y al metodo add
            // Especificando ahí los datos de cada columna en la tabla
            table.row.add( [
                data.name,
                botones,
            ] ).draw( false );
        }

        @can('servicio.edit')
        $('.edit').on( 'click', function () {
            fila=$(this).parents('tr');//Dejamos almacenada temporalmente la fila en la que clickeamos editar
            var row=table.row(fila).data();//Tomamos el contenido de la fila 
            //Sobreescribimos en los campos editables los datos del cliente
            $(".nombre").eq(1).val(row[0]);
            $("#id").val($(this).val());
        });
        //Actualizacion de fila donde no es posible actualizar id
        // Resultado exitoso del update
        function update_success(res)
        {
            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code == 1)
            {
                message(['Servicio editado correctamente'],{manual:true});
                table.cell(fila.children('td')[0]).data( $(".nombre").eq(1).val());
                table=$("#Datos").DataTable().draw();

                $("#Edit").modal('toggle');
                $('body').animate({scrollTop:0}, 'fast');
                // Ocultamos el modal
                $("#Edit").modal('hide');
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{objeto:$("#message_edit"), manual:true,tipo:"danger"});
            }
            // Reactivamos el boton de actualizar
            $("#btn_actualizar").attr("disabled",false);
            // Ocultamos la pantalla de carga
            $("#loading").css('display','none');
        }
        
        @endcan
    </script>
@stop