@extends('layouts.dashboard')
@section('content')
  <div class="card">
    <h4 class="card-header">Agregar Servicio</h4>
    <div class="card-body">
      <div id="mensaje"></div>
      <form action="{{route('servicio.store')}}" method="POST" id="data" autocomplete = off>

        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <input type="text" style="display:none;">
      
        <div class="row ">
          <div class="col-md-12 d-flex justify-content-center">
            <div class="form-group col-md-5 text-center">
                {!!Form::label('Nombre:')!!}
                {!!Form::text('Nombre',null,['id'=>'nombre','class'=>'form-control border border-warning','placeholder'=>'Nombre del servicio'])!!}
            
                <input type="text" style="display:none;">
            </div>
          </div>
        </div>
        <div class="row ">
          <div class="col-md-12 d-flex justify-content-center">
            <div class="form-group col-md-6 text-center">
              <input class="btn btn-primary btnSent" type="button" onclick="save('save');" value="Guardar">
              <input class="btn btn-primary btnSent" type="button" onclick="save('savev');" value="Guardar y ver">
            </div>
          </div>
        </div>
        
      </form>
    </div>
  </div>     
  @include('layouts.cargando.cargando')
@stop
@section('script')
  {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
  <script>
    function save(condition)
    {
      var ruta="/servicio";
      var token=$("#token").val();

      $(".btnSent").attr("disabled",true);
      $("#loading").css('display','block');
      $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:$("#data").serialize(),
        success: function(result){
          if(result === 1)
          {
            if(condition=="save")
            {
              message(["Servicio agregado exitosamente"],{manual:true})
              $("#nombre").val("");
              $("#monto").val("");
              $(".btnSent").attr("disabled",false);
              $("#loading").css('display','none');
            }
            if(condition=="savev")
            {
              location.href="/servicio/serviciov/1/"+$("#nombre").val();
            }
          } else {
            message(result,{manual:true, tipo:'danger'});
            $(".btnSent").attr("disabled",false);
            $("#loading").css('display','none');
          }
          $('body').animate({scrollTop:0}, 'fast');
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
        message(jqXHR,{tipo:"danger"});
        $(".btnSent").attr("disabled",false);
        $("#loading").css('display','none');
        $('body').animate({scrollTop:0}, 'fast');
      });
    }
  </script>
@stop