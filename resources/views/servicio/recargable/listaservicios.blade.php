            @foreach($servicios as $servicio)
                <tr>
                    <td class="text-center">{{$servicio->name}}</td> 
                    <td class="text-center">
                        @can('servicio.index')
                        <button data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-pencil" value="{{$servicio->id}}"></button>
                        @endcan
                        @can('servicio.destroy')
                        <button data-toggle="modal" data-target="#deleteModal" class="btn btn-danger fa fa-trash" data-value="{{$servicio->id}}"></button>
                        @endcan
                    </td>
                </tr>
            @endforeach