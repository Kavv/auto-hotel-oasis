<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Vue-Laravel</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="content" id="app">
        <example-component v-bind:cates="{{$categories}}"
                        v-bind:carus="{{ json_encode($carus) }}"
                        v-bind:images="{{ json_encode($images) }}"
                        videos="{{$videos}}" title="{{$title}}">
        </example-component>

        {{--<div class="row" style="margin-left: 0px; background: #343a40">--}}
            {{--<div class="video-container img-fluid black col-md-12" id="header">--}}
                {{--<video id="vid" autoplay="" loop="" controls="" height="720" class="col-md-12">--}}
                    {{--<!-- Recorre todos los videos y crea una source para cada video pero supongo q solo hay uno -->--}}
                    {{--<source id="source" src="" type="video/mp4">--}}
                {{--</video>--}}
                {{--<button id="boton_video" type="button" class="btn btn-primary tu" data-toggle="modal" data-target="#exampleModal1">Cambiar</button>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row" style="margin-left: 0px">--}}
            {{--<thead>--}}
                {{--<tr>--}}
                    {{--<th>Source</th>--}}
                {{--</tr>--}}
            {{--</thead>--}}
            {{--<tr v-for="item in items">--}}
                {{--<td>@{{ item }}</td>--}}
            {{--</tr>--}}
        {{--</div>--}}
    </div>

    <!-- Scripts -->
    <script type="text/javascript" src="js/app.js"></script>

</body>
</html>
