
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <!-- Fila de la cedula -->
        <div class="row  d-flex justify-content-center">
          <div class="col-md-3">
            <label for="">Codigo de la clase</label>
            <input type="text" name="codigo" id="code" class="form-control border-warning" placeholder="Ej: CrossFit-A">
          </div>
          <div class="col-md-3">
            <label for="">Alumnos base</label>
            <input type="text" name="alumnos_base" id="base" class="form-control border-warning" placeholder="Cantidad de alumnos">
          </div>
          <div class="col-md-3">
            <label for="">Alumnos excedentes</label>
            <input type="text" name="alumnos_excedentes" id="max" class="form-control border-warning" placeholder="Cantidad de alumnos">
          </div>
        </div>

        <div class="row mt-1">
          <div class="col-md-4">
            <label for="">Disciplina</label>
            <select class="form-control border border-warning selectpicker" id="disciplina" name="disciplina"
            required data-live-search="true" title="Seleccione una disciplina" data-width="100%">
              <option value="">Selecciona una disciplina</option>
              @foreach($disciplines as $discipline)
              <option value='{!!$discipline->id!!}'>{!!$discipline->name!!}</option>
              @endforeach
            </select>
          </div>
          <div class="col-md-4">
            <label for="">Servicio</label>
            <select class="form-control border border-warning selectpicker" id="services" name="servicios[]"
            required data-live-search="true" title="Seleccione los servicios" data-width="100%" multiple>
              <option value="">Selecciona un servicio</option>
            </select>
          </div>
          <div class="col-md-4">
            <label for="">Profesor</label>
            <select class="form-control border border-warning" id="teacher" name="profesor">
              <option value="">Selecciona un profesor</option>
              @foreach($staff as $teacher)
              <option value='{!!$teacher->id!!}'>{!! $teacher->name .' '. $teacher->last_name . " / " . $teacher->dni!!}</option>
              @endforeach
            </select>
          </div>
        </div>
        
        <div id="schedule">
          <div class="row d-flex justify-content-center ">
            <h4 class="my-2 py-2">Especifica el horario que tendra esta clase</h4>
            <p>Si no especifica un rango de tiempo entonces el sistema asumira que ese días no se impartirá la clase </p>
          </div>
          <div id="rows-schedule">
            @foreach($days as $day)
              <div class="row mt-2 d-flex justify-content-center">
                <div class="input-group mb-3 col-md-6">
                    <div class="input-group-prepend col-md-3 p-0 text-center ">
                        <input type="hidden" name="dias[]" value="{{$day->id}}" class="days">
                        <span class="input-group-text col-md-12 p-0 dias">{{$day->day}}</span>
                    </div>
                    <input type="hidden" name="id_calendar[]" class="id_calendar">
                    <input type="time" name="desde[]" class="form-control hours desde">
                    <input type="time" name="hasta[]" class="form-control hours hasta">
                </div>
              </div>
            @endforeach
          </div>
        </div>