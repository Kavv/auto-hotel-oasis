@extends('layouts.dashboard')
@section('css')
    {!! Html::style('css/gijgo2/css/gijgo.css') !!}
    {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css') !!}
    <style>
        .content-wrapper {
            background: #343a40 !important;
        }

        .border-skyblue {
            border: #4a75b4 solid 2px;
        }

        #titulo-editar-pago,
        #btns-edit {
            display: none;
        }
        .gj-datepicker{
            width: auto;
        }
    </style>
@stop
@section('content')

    <!--Arreglar toda la mierda que hiciste-->
    <div class="d-block bg bg-dark" style="padding-top: 10px; color:white;visibility:hidden;" id="main">
        @include('alert.mensaje')
        <div class="row ">
            <div class="col-md-12">
                <div class="form-group">
                    <div id="mensaje"></div>
                    <table id="table" class="table table-bordered table-hover table-dark" cellspacing="0" style="width:100%;">
                        <thead>
                            <th>Código</th>
                            <th>Disciplina</th>
                            <th>Base</th>
                            <th>Excedente</th>
                            <th>Profesor</th>
                            <th></th>
                        </thead>
                        <tbody id="tb-clases">
                            @include('leccion.recargable.listaclases')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    @can('cliente.edit')
        <!-- Modal -->
        <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Editar pago</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="modalMessage"></div>
                        <form id="data">
                            <input type="hidden" id="id" name="id">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

                            <div id="modalMessage"></div>
                            @include('leccion.formulario.datos')
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="actualizar"
                                onclick="actualizar()">Actualizar</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcan

    @can('cliente.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js') !!}
    {!! Html::script('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js') !!}
    {!! Html::script('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js') !!}
    {!! Html::script('js/jskevin/tiposmensajes.js') !!}
    {!! Html::script('js/jskevin/kavvdt.js') !!}
    {!! Html::script('js/gijgo2/js/gijgo.js') !!}

    <script>
        var table;
        var row;

        $(document).ready(function() {
            table = createdt($('#table'), {
                cant: [10, 20, -1],
                cantT: [10, 20, "todo"]
            });
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index', 1060);
        });
        
        var row;
        var prueba;

        function calendar(value, element){
            prueba = element;
            row = $(element).parents('tr');
            row = table.row( row );

            var child = $(element).parents('tr').next('tr');

            if ($(element).hasClass('btn-secondary')) {
                //child.css('display', 'none');
                row.child.hide();
                $(element).removeClass('btn-secondary');
                $(element).addClass('btn-info');
            }
            else {
                // Si esta fila no tiene hijos, entonces los creamos
                // Así solo se carga una sola vez
            
                $("#loading").css('display','block');
                // Obtenemos el horario de la leccion
                route = '/leccion/calendar/' + value;
                $.get(route,  function(res){
                    row.child( res ).show();
                    $("#loading").css('display','none');
                });
                $(element).removeClass('btn-info');
                $(element).addClass('btn-secondary');
            }
        }
        var aux_discipline = [];
        $('.edit').on( 'click', function () {
            
            fila = $(this).parents('tr');//Dejamos almacenada temporalmente la fila en la que clickeamos editar
            // Limpiamos todos los campos del formulario edit
            limpiar();
            $("#id").val($(this).val());
            // Llamamos a la ruta show para obtener los datos de la lección
            route = "/leccion/show/"+$(this).val();
            $.get(route, function(res){
                // Verificamos que el objeto devuelto tiene la lección
                if (typeof res.lesson !== 'undefined')
                {
                    // almacenasmos la lección y el calendario
                    var lesson = res.lesson;
                    var services = res.services;
                    var calendar = res.calendar;
                    // Rellenamos los inputs del formulario
                    $("#code").val(lesson.code);
                    $("#base").val(lesson.base);
                    $("#max").val(lesson.max);

                    $('#disciplina').selectpicker('val', lesson.discipline_id);
                    $("disciplina").change();
                    aux_discipline = [];
                    for(var i = 0; i < services.length; i++)
                        aux_discipline.push(services[i].id);
                    
                    $("#teacher").val(lesson.staff_id);
                    // Recorremos el horario de la lección
                    var index_hour = 0;
                    var count_days = $(".days").length;
                    for(var i = 0; i < count_days; i++)
                    {
                        // Este for va obteniendo el valor de cada uno de los inputs de los días para poder asignar
                        // correctamente el horario obtenido al día correspondiente
                        var day = $(".days").eq(i).val();
                        if(day == calendar[index_hour].day)
                        {
                            $(".id_calendar").eq(i).val(calendar[index_hour].id);
                            $(".desde").eq(i).val(calendar[index_hour].begin);
                            $(".hasta").eq(i).val(calendar[index_hour].end);
                            index_hour++;
                        }
                        if(index_hour == calendar.length)
                            break;
                    }
                }
                else{
                    message(res.msg, {objeto:$("#modalMessage"), tipo:"danger"});
                }

                $("#loading").css('display','none');
            });
        });

        function actualizar()
        {
            route="/leccion/"+$("#id").val();
            var token=$("#token").val();
            $("#actualizar").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data:$("#data").serialize(),
                success: function(res){
                    if(res.code == 1)
                    {
                        message(['Leccion actualizada correctamente'],{manual:true});
                        table.cell(fila.children('td')[0]).data( $("#code").val());
                        table.cell(fila.children('td')[1]).data( $("#disciplina option:selected").val());
                        table.cell(fila.children('td')[2]).data( $("#base").val());
                        table.cell(fila.children('td')[3]).data( $("#max").val());
                        table.cell(fila.children('td')[4]).data( $("#teacher option:selected").text());
                        
                        table = $("#table").DataTable().draw();
                        $("#Edit").modal('toggle');
                        $('body').animate({scrollTop:0}, 'fast');
                    }
                    else
                    {
                        $('#Edit').animate({scrollTop:0}, 'fast');
                        message(res.msg,{objeto:$("#modalMessage"), manual:true,tipo:"danger"});
                    }
                    $("#actualizar").attr('disabled',false);
                    $("#loading").css('display','none');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                $('#Edit').animate({scrollTop:0}, 'fast');
                $("#actualizar").attr('disabled',false);
                $("#loading").css('display','none');
                message(jqXHR,{objeto:$("#modalMessage"), tipo:"danger"});
            });
        }
        function limpiar()
        {
            $("#id").val("");
            $("#code").val("");
            $("#base").val("");
            $("#max").val("");
            $("#services").val("");
            $("#teacher").val("");
            $(".hours").val("");
        }

        
        var row;
        $('#deleteModal').on('show.bs.modal', function (event) {
            {{-- Mantenemos el id del elemento seleccionado --}}
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            $("#id").val(value);

            {{-- Mantenemos la fila del elemento seleccionado --}}
            row = button.parents('tr');
        });

        $('#delete').on('click', function () {
            var route="/leccion/"+$("#id").val();
            var token=$("#token").val();
            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                success: function(res){
                    message(['Se elimino la lección correctamente'],{manual:true});
                    table.row(row).remove().draw( false );
                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                    $('body').animate({scrollTop:0}, 'fast');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#deleteModalMessage"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });
        $("#disciplina").change(function(){ 
        var leccion = $(this).val();
        if(leccion != "")
        {
            var route = '/leccion/servicios/'+ leccion + '/true';
            $("#loading").css('display','block'); 
            $("#services").empty();
            $.get(route, function(res){
                $("#services").append(res);
                $('#services').selectpicker('refresh');

                if(aux_discipline.length > 0)
                {
                    console.log(aux_discipline);
                    $('#services').selectpicker('val', aux_discipline);
                    aux_discipline = [];
                }

                $("#loading").css('display','none');
            }).fail(function() {
                message(['Ocurrio un error al cargar los servicios'],{tipo:"danger", manual:true});
                $("#loading").css('display','none');
            });
        }
        });
    </script>
@stop
