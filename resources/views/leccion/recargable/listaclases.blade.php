
@foreach($lessons as $lesson)
  <tr>
    <td>{{$lesson->code}}</td>  
    <td>{{$lesson->discipline}}</td> 
    <td>{{$lesson->base}}</td>  
    <td>{{$lesson->max}}</td> 
    <td>{{$lesson->name . ' ' . $lesson->last_name . ' / ' . $lesson->dni}}</td>
    <td class="botones" >
      <button type="button" class="btn btn-info edit fa fa-calendar"  onclick="calendar({{$lesson->id}}, this)"></button>
      <button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-warning edit fa fa-pencil" value="{{$lesson->id}}"></button>
      <button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="{{$lesson->id}}"></button>
    </td>
  </tr>
@endforeach