@extends('layouts.dashboard')
@section('content')

    @include('layouts.cargando.cargando')

    {!!Html::style("css/gijgo2/css/gijgo.css")!!}
    
    <div class="card" id="main" style="display:none;">
        <h4 class="card-header text-center">REPORTES</h4>
        <div class="card-body">
            <div class="row mt-5 justify-content-center d-flex">
                <div class="col-md-6">
                    <label>Que tipo de reporte desea realizar?</label>
                    <select id="reporte" class="custom-select" onchange="Reporte('option');">
                        <option ></option>
                        <option value="clientes_nuevos">Clientes Nuevos</option>
                        <option value="clientes_eliminados">Clientes Eliminados</option>
                        <option value="personal_nuevos">Personal Nuevos</option>
                        <option value="personal_eliminados">Personal Eliminados</option>
                        <option value="reservaciones_agregadas">Reservaciones Agregadas</option>
                        <option value="reservaciones_dates">Reservaciones</option>
                    </select>
                </div>
            </div>

            <div class="row mt-5 justify-content-center">
                <div class="col-md-3">
                    <label>Fecha de inicio</label>
                    <input id="start" class="datepicker border border-warning" name="start"   placeholder='año-mes-dia' readonly  onchange="Reporte('start');"/> 
                </div>
                <div class="col-md-3">
                    <label>Fecha de fin</label>
                    <input id="end" class="datepicker border border-warning" name="end"   placeholder='año-mes-dia' readonly  onchange="Reporte('end');"/> 
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-12">
                    <div id="container"></div>
                </div>
            </div>
        </div>



        <div id="mensaje"></div>
        <table class="table table-bordered table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;" >
            <thead id="tableHeader">
            
            </thead>
            <tbody id="tableBody"> 
            
            </tbody>
        </table>


    </div>
    <style>
        #container {
            min-width: 310px;
            max-width: 800px;
            height: 400px;
            margin: 0 auto
        }
    </style>
@section('script')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
{!!Html::script("js/gijgo2/js/gijgo.js")!!}

{!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!}
    {!!Html::script("js/jskevin/tiposmensajes.js")!!}
    {!!Html::script("js/jskevin/kavvdt.js")!!}   


<script>


    
    var config = {
        chart: {
          renderTo: "container",
          type: "column"  
        },
        
        lang: {
            months: [
                'Enero', 'Febrero', 'Marzo', 'Abril',
                'Mayo', 'Junio', 'Julio', 'Agosto',
                'Septembre', 'Octobre', 'Novembre', 'Décembre'
            ],
            weekdays: [
                'Domingo', 'Lunes', 'Martes', 'Miercoles',
                'Jueves', 'Viernes', 'Sabado'
            ],
            downloadCSV: "Descargar CSV",
            downloadJPEG: "Descargar JPEG image",
            downloadPDF: "Descargar PDF document",
            downloadPNG: "Descargar PNG image",
            downloadSVG: "Descargar SVG vector image",
            downloadXLS: "Descargar XLS",
            loading: "Cargando...",
            noData: "No hay informacion que mostrar",
            openInCloud: "Abrir en Highcharts",
            printChart: "Imprimir Grafica",
            shortMonths:["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            viewData: "Ver tabla",
            viewFullscreen: "Ver pantalla completa",
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ['viewFullscreen', 'printChart', 'separator', 'downloadPDF', 'downloadPNG','downloadJPEG', 'downloadSVG', 'separator', 'downloadCSV', 'downloadXLS']
                }
            }
        },
        title: {
            text: ''
        },

        subtitle: {
            text: ''
        },

        yAxis: {
            title: {
                text: 'Cantidad'
            },/*,
            labels: {
            formatter: function() {
                return this.value + ' %';
            }*/
        },
        xAxis: {
            title: {
                text: 'Fechas'
            },
            categories:[],
            tickInterval: 1,
            layout: 'vertical',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        /*
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2010
            }
        },*/

        series: [{
            name: '',
            data: [],
            extra: [],
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        },
        tooltip: {
        },
    };
    var pru;
    var diagrama;
    $(document).ready(function(){
        $("#main").css('display','flex');
    });

    var $start = $("#start");
    var $end = $("#end");
    var temp_start, temp_end;
    function Reporte(input = null)
    {
        if($start.val() != "" && $end.val() != "")
        {
            if($start.val() != temp_start && input == "start" || input == "option" || input == "end" && $end.val() != temp_end)
            {

               // if(input == "start")
                    temp_start = $start.val();
                //if(input == "end")
                    temp_end = $end.val();
                var caso;
                var ruta = "";
                switch ($("#reporte").val()) {
                    case "clientes_nuevos":
                        ruta =  "/reportes/clientesNuevos/"+$start.val()+"/"+$end.val();
                        standard(ruta);
                    break;
                    case "clientes_eliminados":
                        ruta =  "/reportes/clientesEliminados/"+$start.val()+"/"+$end.val();
                        standard(ruta);
                    break;
                    case "personal_nuevos":
                        ruta =  "/reportes/personalNuevos/"+$start.val()+"/"+$end.val();
                        standard(ruta);
                    break;
                    case "personal_eliminados":
                        ruta =  "/reportes/personalEliminados/"+$start.val()+"/"+$end.val();
                        standard(ruta);
                    break;
                    case "reservaciones_agregadas":
                        ruta =  "/reportes/reservacionesNuevas/"+$start.val()+"/"+$end.val();
                        standard(ruta);
                    break;
                    case "reservaciones_dates":
                        ruta =  "/reportes/reservacionesDates/"+$start.val()+"/"+$end.val();
                        allReservations(ruta);
                    break;
                    default:
                    break;
                }
            }
        }
    }
    
    function standard(ruta)
    {
        if(ruta != "")
        {
            $.get(ruta, function(res){
                if(res.state != false)
                {
                    if(diagrama)
                    {
                        diagrama.destroy();
                        config.series[0].data=[];
                        config.xAxis.categories=[];
                        config.series[0].name = "";
                        config.title.text = "";
                        config.title.subtitle = "";
                        delete config.tooltip.formatter;
                    }
                    config.series[0].name = res.name;
                    config.title.text = res.title;
                    config.title.subtitle = "Desde: "+ $start.val() + " Hasta:" + $end.val();
                    for(data in res.data)
                    {
                        console.log(res);
                        config.series[0].data.push(res.data[data].cantidad);
                        config.xAxis.categories.push(data);
                    }

                    diagrama = new Highcharts.chart(config);
                }
            });
        }
    }

    function allReservations(ruta)
    {

        if(ruta != "")
        {
            $.get(ruta, function(res){
                prueba = res;
                if(res.state != false)
                {
                    if(diagrama)
                    {
                        diagrama.destroy();
                        config.series[0].data=[];
                        config.xAxis.categories=[];
                        config.series[0].name = "";
                        config.title.text = "";
                        config.title.subtitle = "";
                        delete config.tooltip.formatter;
                    }
                    config.series[0].name = res.name; 
                    config.title.text = res.title;
                    config.title.subtitle = "Desde: "+ $start.val() + " Hasta:" + $end.val();

                    config.series[0].extra=[];
                    config.series[0].startDate=[];
                    config.series[0].endDate=[];
                    
                    config.tooltip.formatter = function() {
                        return 'Cliente: <b>'+this.series.userOptions.extra[this.point.index] + "<br>" +
                            'Fecha de inicio: <b>'+this.series.userOptions.startDate[this.point.index] + "<br>" +
                            'Fecha de Fin: <b>'+this.series.userOptions.endDate[this.point.index];
                    };

                    for(data in res.data)
                    {
                        config.series[0].data.push(1);
                        config.series[0].extra.push(res.data[data].cliente);
                        config.series[0].startDate.push(res.data[data].begin_date);
                        config.series[0].endDate.push(res.data[data].end_date);
                        config.xAxis.categories.push(res.data[data].begin_date + " / " + res.data[data].end_date);
                    }

                    diagrama = new Highcharts.chart(config);
                    //recargarTabla(res);
                }
            });
        }
    }
    // MUESTRA UNA DT CON TODAS LAS RESERVACIONES EN EL RANGO DE FECHA
    var table;
    function recargarTabla(res)
    {
        var $header = $("#tableHeader");
        var $body = $("#tableBody");


        $header.append( "<tr><th>Cliente</th>" + 
                        "<th>Reservación inicio</th>" + 
                        "<th>Reservación fin</th></tr>");
                        
        for(data in res.data)
        {
            $body.append("<tr><td>" + res.data[data].cliente + "</td>" +
                         "<td>" + res.data[data].begin_date + "</td>" +
                         "<td>" + res.data[data].end_date + "</td></tr>");
        }
        table = createdt($('#Datos'),{col:1,cant:[10,20,-1],cantT:[10,20,"Todo"]});
    }

    var prueba;
    $('#start').datepicker({
        locale: 'es-es',
        format: 'yyyy-mm-dd',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        header: true,
    });
    $('#end').datepicker({
        locale: 'es-es',
        format: 'yyyy-mm-dd',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        header: true,
    });

</script>


@endsection
@stop
