<html>
    <head>
        <title>
            Reporte
        </title>
        <link href="/img/logosantana.jpg" type="image/x-icon" rel="shortcut icon" />
        <link rel="stylesheet" href="css/report/report.css">
    </head>

    <body>
    <br>
    <!-- Datos superiores, Encabezado -->
    <div class="top-data">
        <div>
            <img src="https://res.cloudinary.com/fguzman/image/upload/v1562814371/LOGO-Santana/logo-negro-2.png" style="position: absolute; width: 15%;">
        </div>
        <div style="position: absolute; width: 15%; right:0; text-align:center; font-size:80%">
            Fecha de emisión
            <br>
            {!!date('d/m/Y')!!}
        </div>

        <div id="datos_fac" style="text-align:center;">
            <h2  style="margin: 0; "> ALQUILER SANTANA </h2>
            <h2  style="margin: 0; "> REPORTE </h2>
            {!!$report_title!!}
        </div>
        <div class="cabecera"  style="width:100%;">
            {!!$report_data!!}
        </div>
        <table class="" cellspacing="0" id="Datos" style="width:100%; margin-top:10px;" >
            <thead>
                <tr>
                    {!!$thead!!}
                </tr>
            </thead >
            <tbody class="text-center" id="lista" >
                {!!$tbody!!}
            </tbody>
        </table>

    </div>

</html>