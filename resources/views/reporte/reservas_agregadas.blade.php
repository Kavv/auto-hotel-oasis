<html>
    <head>
        <title>
            Reporte
        </title>
        <link href="/img/logosantana.jpg" type="image/x-icon" rel="shortcut icon" />
        <link rel="stylesheet" href="css/report/report.css">
    </head>

    <body>
        <br>
        <!-- Datos superiores, Encabezado -->
        <div class="top-data">
            <div>
                <img src="https://res.cloudinary.com/fguzman/image/upload/v1562814371/LOGO-Santana/logo-negro-2.png" style="position: absolute; width: 15%;">
            </div>
            <div style="position: absolute; width: 15%; right:0; text-align:center; font-size:80%">
                Fecha de emisión
                <br>
                {!!date('d/m/Y')!!}
            </div>

            <div id="datos_fac" style="text-align:center;">
                <h2  style="margin: 0; "> ALQUILER SANTANA </h2>
                <h2  style="margin: 0; "> REPORTE </h2>
                {!!$report_title!!}
            </div>
            <div class="cabecera"  style="width:100%;">
                {!!$report_data!!}
            </div>
        </div>

        <br>
        @for($k = 0; $k < count($reservations); $k++)
            @if($k != 0)
            <div class="page-break"></div>
            @endif
        <div class="report" id="factura{{$k}}">
            <hr style="border: 1px dashed #244ab8;">
            <div>
                <h3 style="text-align: left">Cliente: {{$customer[$k]->name." ".$customer[$k]->last_name}} / {{$customer[$k]->dni}}</h3>
                <h3 style="text-align: left">Direccion: {{$reservations[$k]->event_address}}</h3>
                <h3 style="text-align: left">Se reservo durante: <br> {{$FI[$k]}} / {{$FF[$k]}}</h3>
            </div>

            <!-- fecha -->
            <table>
                <thead>
                <tr style="text-align: center">
                    <th> DIA </th>
                    <th> MES </th>
                    <th> AÑO </th>
                </tr>
                </thead>
                <tbody style="text-align: center;">
                    <td> {{$dates[$k][0]}} </td>
                    <td> {{$dates[$k][1]}} </td>
                    <td> {{$dates[$k][2]}} </td>
                </tbody>
            </table>
            <!--fin fecha -->

            <br>

            <div style="width:100%; margin-bottom: 20px;" >
                <table style="width:100%; text-align:center;">
                    <thead>
                        <tr>
                            <th style="width:10%">Cant.</th>
                            <th style="width:70%; text-align: left;">Descripcion</th>
                            <th style="width:10%">P/Unitario</th>
                            <th style="width:10%">TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            //La cantidad de filas multiplicado por la pagina de factura actual (EN DESUSO)
                            $factura = ($reservations[$k]->invoice_row * 0);
                            //
                            $items = $reservations[$k]->items()->orderBy('index')->get();
                            $menu = $reservations[$k]->menu()->orderBy('index')->get();
                            $filas_totales = count($items) + count($menu);
                            $pago = 0;
                            //index_i = indice para los items, index_m = indice para los elementos del menu
                            $index_i = 0; $index_m = 0;
                            //Nota: Estamos obviando si el evento se guardo con un formato de multiples facturas (todas se mostraran en una sola)
                            //Ademas tambien obviamos el proceso de autocompletación de filas vacias/sobrantes.
                            //Se realiza la iteración desde 1 hasta que sea igual ala cantidad de filas totales
                            for($j = 1; $j <= $filas_totales; $j++)
                            {
                                //Si existe el indice en el array items
                                if(isset($items[$index_i]))
                                {
                                    //Verifica si el indice de fila establecido en el registro original es igual al actual indice
                                    if($items[$index_i]->index == $j)
                                    {
                                        $temp_pago = $items[$index_i]->qu * $items[$index_i]->up * $items[$index_i]->day;
                                        echo
                                        '<tr>'.
                                            '<td>'.$items[$index_i]->qu.'</td>'.
                                            '<td style="text-align: left;">'.$items[$index_i]->name.' - dias('.$items[$index_i]->day.')</td>'.
                                            '<td>'.$items[$index_i]->up.'</td>'.
                                            '<td>'.number_format($temp_pago, 2, '.', ',').'</td>'.
                                        '</tr>';
                                        $pago += $temp_pago;
                                        $index_i++;
                                    }
                                }
                                //Si existe en indice en el array menu
                                if(isset($menu[$index_m]))
                                {
                                    //Verifica si el indice de fila establecido en el registro original es igual al actual indice
                                    if($menu[$index_m]->index == $j)
                                    {
                                        $temp_pago = $menu[$index_m]->qu * $menu[$index_m]->up * $menu[$index_m]->day;
                                        echo
                                        '<tr>'.
                                            '<td>'.$menu[$index_m]->qu.'</td>'.
                                            '<td style="text-align: left;">'.$menu[$index_m]->description.' - dias('.$menu[$index_m]->day.')</td>'.
                                            '<td>'.$menu[$index_m]->up.'</td>'.
                                            '<td>'.number_format($temp_pago, 2, '.', ',').'</td>'.
                                        '</tr>';
                                        $pago += $temp_pago;
                                        $index_m++;
                                    }
                                }
                            }
                        ?>

                    </tbody>
                    <tfoot>
                        <?php

                            $iva = $reservations[$k]->tax;
                            $descuento = $reservations[$k]->discount;


                            if($iva == 0 && $descuento == 0)
                            {
                                echo
                                    '<tr>
                                        <td colspan="3" style="text-align: right"> <b> Gran Total </b></td>
                                        <td style="text-align: center">'.$pago.'</td>
                                    </tr>';
                            }

                            if($iva != 0 && $descuento == 0)
                            {
                                $temp_iva = $pago*$iva;
                                $total = $pago + $temp_iva;
                                echo
                                    '<tr>'.
                                        '<td colspan="3" style="text-align: right"> <b>Sub Total</b></td>'.
                                        '<td style="text-align: center">'.$pago.'</td>'.
                                    '</tr>'.
                                    '<tr>'.
                                        '<td colspan="3" style="text-align: right"> <b>IVA</b></td>'.
                                        '<td style="text-align: center">'.$temp_iva.'</td>'.
                                    '</tr>'.
                                    '<tr>'.
                                        '<td colspan="3" style="text-align: right"> <b> Gran Total </b></td>'.
                                        '<td style="text-align: center">'.$total.'</td>'.
                                    '</tr>';
                            }

                            if($iva == 0 && $descuento != 0)
                            {

                                $temp_descuento = $pago*$descuento;
                                $total = $pago - $temp_descuento;
                                echo
                                    '<tr>'.
                                        '<td colspan="3" style="text-align: right"> <b>Sub Total</b></td>'.
                                        '<td style="text-align: center">'.$pago.'</td>'.
                                    '</tr>'.
                                    '<tr>'.
                                        '<td colspan="3" style="text-align: right"> <b>Descuento</b></td>'.
                                        '<td style="text-align: center">'.$temp_descuento.'</td>'.
                                    '</tr>'.
                                    '<tr>'.
                                        '<td colspan="3" style="text-align: right"> <b> Gran Total </b></td>'.
                                        '<td style="text-align: center">'.$total.'</td>'.
                                    '</tr>';
                            }

                            if($iva != 0 && $descuento != 0)
                            {
                                $temp_iva = $pago*$iva;
                                $temp_descuento = $pago*$descuento;
                                $total = $pago-$temp_descuento+$temp_iva;
                                echo
                                    '<tr>'.
                                        '<td colspan="3" style="text-align: right"> <b>Sub Total</b></td>'.
                                        '<td style="text-align: center">'.$pago.'</td>'.
                                    '</tr>'.
                                    '<tr>'.
                                        '<td colspan="3" style="text-align: right"> <b>Descuento</b></td>'.
                                        '<td style="text-align: center">'.$temp_descuento.'</td>'.
                                    '</tr>'.
                                    '<tr>'.
                                        '<td colspan="3" style="text-align: right"> <b>IVA</b></td>'.
                                        '<td style="text-align: center">'.$temp_iva.'</td>'.
                                    '</tr>'.
                                    '<tr>'.
                                        '<td colspan="3" style="text-align: right"> <b> Gran Total </b></td>'.
                                        '<td style="text-align: center">'.$total.'</td>'.
                                    '</tr>';
                            }

                            ?>


                    </tfoot>
                </table>
            </div>
        </div>
        @endfor
    </body>

</html>