<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="">
    {!!Html::style("css/cssF/errores/style403.css")!!}
    {!!Html::style("css/cssF/vendor/bootstrap/css/bootstrap.min.css")!!}
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
</head>
<body>
<div class="d-flex justify-content-center align-items-center" style="position: fixed; height: 100%; width: 100%; overflow: auto;">
    <div class="center_body col-md-12 text-center">
        <h2>PROHIBIDO</h2>
        <img class="img-fluid mt-5" src="/img/403.png" alt="403">
        <p>
            ¡No tienes permiso para acceder!.
        </p>
        <a href="/" id="btn_principal"> Pagina Principal </a>
    </div>
</div>
</body>
</html>