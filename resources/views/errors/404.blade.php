<!DOCTYPE html>
<html>
	<head>
	 	<title></title>
	 	<link rel="stylesheet" type="text/css" href="">
		{!!Html::style("css/cssF/errores/style404.css")!!}
    	{!!Html::style("css/cssF/vendor/bootstrap/css/bootstrap.min.css")!!}
	 	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
	</head>
	<body>
		<div class="d-flex justify-content-center align-items-center" style="position: fixed; height: 100%; width: 100%; overflow: auto;">
 			<div class="center_body col-md-12 text-center">
 				<img class="img-fluid mt-5" src="/img/error.png" alt="404">
			  	<h2>PAGINA NO ENCONTRADA</h2>
			  	<p>
			  		¡Ooooops! ¡Ocurrio un error en la pagina!.
			  		<br>
			  		Si el error permanece contacte a los administradores 
			  	</p>
			  	<a href="/" id="btn_principal"> Pagina Principal </a>
 			</div>
		</div>
	</body>
</html>