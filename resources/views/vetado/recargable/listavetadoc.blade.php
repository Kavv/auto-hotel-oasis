
                @foreach($clientes as $cli)
                    <tr>
                        <td>{{$cli->dni}}</td> 
                        <td>{{$cli->name}}</td>    
                        <td>{{$cli->phone}}</td>
                        <td>
                            @can('vetado.create')
                            <button data-toggle="modal" data-target="#Add" class="btn btn-danger vetar" value="{{$cli->id}}">Vetar</button>
                            @endcan
                        </td>
                    </tr>
                @endforeach 