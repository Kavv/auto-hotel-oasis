@foreach($vetados as $vetado)
    <tr>
        <td class="text-center">{{$vetado->dni}}</td> 
        <td>{{$vetado->name}}</td>    
        <td>{{$vetado->last_name}}</td>  
        <td >{{$vetado->vetado['description']}}</td>  
        <td>
            <button data-toggle="modal" data-target="#Edit" class="btn btn-primary edit" value="{{$vetado->dni}}" >Detalle</button>
            @can('vetado.destroy')
            <button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="{{$vetado->dni}}"></button>
            @endcan
        </td>
    </tr>
@endforeach