
                @foreach($personal as $trabajador)
                    <tr>
                        <td>{{$trabajador->dni}}</td> 
                        <td>{{$trabajador->name}}</td>    
                        <td>{{$trabajador->last_name}}</td>
                        <td>
                            @can('vetado.create')
                            <button data-toggle="modal" data-target="#Add" class="btn btn-danger vetar" value="{{$trabajador->dni}}">Vetar</button>
                            @endcan
                        </td>
                    </tr>
                @endforeach