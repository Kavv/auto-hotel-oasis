@extends('layouts.dashboard')
@section('content')
    <div class="d-block bg-dark" style="padding-top: 10px; color:white;visibility:hidden;" id="main">
        <div id="mensaje"></div>
        <div class="row ">
            <div class="col-md-4">
                <div class="form-group">
                    <select onchange="filtro(this,0);" class="form-control" id="tipofil">
                        <option >Cliente</option>
                        <option Selected>Personal</option>
                    </select>
                </div>
            </div>
        </div>
        @can('personal.index')
        <table class="table table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;" >
            <thead >
                <th class="text-center">Cedula</th>
                <th class="text-center">Nombre</th>
                <th class="text-center">Apellido</th>
                <th class="text-center" data-orderable="false" style="width:20%"></th>
            </thead>
            <tbody class="text-center" id="lista"> 
                @include('vetado.recargable.listavetadop')
            </tbody>
        </table>
        <input type="hidden"  value="0" id="comodin">
        @endcan
    </div> 
    
    @can('personal.index')
    <!-- Modal -->
    <div class="modal fade" id="Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" style="display: block;visibility:hidden;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Esta apunto de vetar a:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <form id="data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <input type="hidden" id="id">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-12 ">
                                <div class="form-group text-center">
                                    {!!Form::label('Cedula','Cedula:')!!}
                                    {!!Form::label('','',['class' => 'badge badge-pill badge-info','id'=>'cedula'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Nombre','Nombre:')!!}
                                    {!!Form::label('','',['class' => 'badge badge-pill badge-info','id'=>'nombre'])!!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Apellido','Apellido:')!!}
                                    {!!Form::label('','',['class' => 'badge badge-pill badge-info','id'=>'apellido'])!!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!!Form::label('Direccion','Direccion:')!!}
                                    {!!Form::label('','',['class' => '','id'=>'direccion'])!!}
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!!Form::label('Fecha','Fecha de nacimiento:')!!}
                                    {!!Form::label('','',['class' => 'badge badge-pill badge-info','id'=>'fechan'])!!}
                                </div>
                            </div>
                        </div> 
                        <div class="row ">
                            <div class="col-md-12 ">
                                <div class="form-group text-center">
                                    {!!Form::label('Descripcion','Descripcion:')!!}
                                    <textarea class="form-control border border-warning" placeholder="Será vetado debido a: " id="descripcion" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="vetado();" id="vetar" >Vetar</button>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endcan
    @include('layouts.cargando.cargando')
    
@stop
@section("script")
    {!!Html::script("js/jskevin/cedulanica.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!}  
    {!!Html::script("js/jskevin/kavvdt.js")!!} 
    <script>
        var servi=0;
        var item=0;
        var table, t_items;
        var fila;
        
        $(document).ready( function () {
            @can('personal.index')
            table = createdt($('#Datos'),{col:1, cant:[10,25,-1], cantT:[10,25,'Todo']});
            @endcan

            @can('inventario.index')
            t_items = createdt($('#t_items'),{col:0,cant:[5,-1],cantT:[5, 'Todo']})
            @endcan
            
            $("#Add").removeAttr("style");
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index',1060);
        });
        //Filtramos por el campo cedula    
        function filtro(val1=0,val2=0)
        {
            var ruta;
            if($("#tipofil").val()=="Cliente")
                location.href ="/vetado/listacliente";
            else
                location.href ="/vetado/listapersonal";
        }
        @can('vetado.create')
        function vetado()
        {
            console.log("entro");
            var ruta="/vetado/vetar";
            var token=$("#token").val();
            $("#vetar").attr("disabled",true);
            $("#loading").css('display','block');
            $.ajax({
                url: ruta,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data:{"dni":$("#cedula").text(),"descripcion":$("#descripcion").val(),"tipo":"personal"},
                success: function(){
                    message(["Se vetó al personal correctamente "],{manual:true})
                    table.row(fila).remove().draw( false );
                    $("#Add").modal('toggle');
                    $("#vetar").attr("disabled",false);
                    $("#loading").css('display','none');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#modalMessage"), tipo:"danger"});
                $("#vetar").attr("disabled",false);
                $("#loading").css('display','none');
            });
        }
        $('.vetar').on( 'click', function () {
            fila=$(this).parents('tr');
            var ruta="/personal/show/"+$(this).val();

            $("#descripcion").val("");
            $("#data").find("label.badge-pill").text("")
            $("#direccion").text("");


            $("#loading").css('display','block');
            $.get(ruta,function(res)
            {
                $("#loading").css('display','none');
                if(res.data)
                {
                    $("#cedula").text(res.data.dni);
                    $("#nombre").text(res.data.name);
                    $("#apellido").text(res.data.last_name);
                    $("#direccion").text(res.data.address);
                    $("#fechan").text(res.data.age);
                }
                else
                {
                    message(res.message,{manual:true, tipo:"danger"});
                    $("#Add").modal('toggle');
                }
            });
        });
        @endcan
    </script>
@stop