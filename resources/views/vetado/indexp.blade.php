@extends('layouts.dashboard')
@section('css')
    <style>
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')
    <div class="d-block bg-dark" style="padding-top: 10px; color:white;visibility:hidden;" id="main">
    
        @include('alert.mensaje')
        <div id="mensaje"></div>
        <div class="row ">
            <div class="col-md-4">
                <div class="form-group">
                    <select onchange="filtro();" class="form-control" id="tipofil">
                        <option >Cliente</option>
                        <option Selected>Personal</option>
                    </select>
                </div>
            </div>
        </div>
        <table class="table table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;" >
            <thead>
                <tr>
                    <th class="text-center" style="max-width:20%">Cedula</th>
                    <th class="text-center" style="max-width:20%">Nombre</th>
                    <th class="text-center" style="max-width:20%">Apellido</th>
                    <th class="text-center" style="max-width:20%;">Vetado por:</th>
                    <th class="text-center" data-orderable="false" style="width:20%"></th>
                </tr>
            </thead>
            <tbody class="text-center" id="lista"> 
                @include('vetado.recargable.lvp')
            </tbody>
        </table>
        
        <input type="hidden"  value="0" id="comodin">
    </div> 
    <!-- Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" style="display: block;visibility:hidden;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Esta apunto de vetar a:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <form id="data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <input type="hidden" id="id">
                        <div class="row ">
                            <div class="col-md-3 "></div>
                            <div class="col-md-6 ">
                                <div class="form-group text-center">
                                    {!!Form::label('Cedula','Cedula:')!!}
                                    {!!Form::label('','',['class' => 'badge badge-pill badge-info','id'=>'cedula'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Nombre','Nombre:')!!}
                                    {!!Form::label('','',['class' => 'badge badge-pill badge-info','id'=>'nombre'])!!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!!Form::label('Apellido','Apellido:')!!}
                                    {!!Form::label('','',['class' => 'badge badge-pill badge-info','id'=>'apellido'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!!Form::label('Direccion','Direccion:')!!}
                                    {!!Form::label('','',['class' => '','id'=>'direccion'])!!}
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!!Form::label('Fecha','Fecha de nacimiento:')!!}
                                    {!!Form::label('','',['class' => 'badge badge-pill badge-info','id'=>'fechan'])!!}
                                </div>
                            </div>
                        </div> 
                        <div class="row ">
                            <div class="col-md-3 "></div>
                            <div class="col-md-12 ">
                                <div class="form-group text-center">
                                    {!!Form::label('Descripcion','Descripcion:')!!}
                                    <textarea class="form-control border border-warning" placeholder="Será vetado debido a: " id="descripcion" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        @can('inventario.index')
                        <div class="row ">
                            <div class="col-md-3 "></div>
                            <div class="col-md-12 ">
                                <div class="form-group text-center">
                                    {!!Form::label('dañado','Si daño algun articulo, seleccionelo:', ['class'=>'font-weight-bold'])!!}
                                    <table class="table table-hover table-dark" cellspacing="0" id="t_items" style="width:100%;">
                                        <thead >
                                            <th class="text-center" style=" max-width:30%;">Nombre</th>
                                            <th class="text-center">Costo Objeto</th>
                                            <th class="text-center" style=" max-width:20%;" data-orderable="false">Cantidad</th>
                                            <th class="text-center" style=" max-width:20%;" data-orderable="false">Descripción</th>
                                        </thead>
                                        <tbody class="text-center" id=""> 
                                            @include('vetado.recargable.inventario')
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endcan
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            @can('vetado.edit')
                            <button id="btnActualizar" type="button" class="btn btn-danger" onclick="actualizar();" >Actualizar</button>
                            @endcan
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @can('vetado.destroy')
        @include('layouts.modal.deleteVetadoModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section("script")
    {!!Html::script("js/jskevin/cedulanica.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/kavvdt.js")!!}   
    <script>
    
        var table;
        var fila;
        $(document).ready( function () {
            table=createdt($('#Datos'),{buscar:'{!!session("valor")!!}',col:1,cant:[10,20,-1],cantT:[10,20,"Todo"]});
            
            @can('inventario.index')
            t_items = createdt($('#t_items'),{col:0,cant:[5,-1],cantT:[5, 'Todo']})
            @endcan
            
            $("#Edit").removeAttr("style");
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index',1060);
        });

        function filtro()
        {
            if($("#tipofil").val()=="Cliente")
            {
                location.href ="/vetado/filtro/cliente";
            }
            else
            {
                location.href ="/vetado/filtro/personal/vetado.indexp";
            }
        }
        
         $('.edit').on( 'click', function () {
            fila=$(this).parents('tr');//Dejamos almacenada temporalmente la fila en la que clickeamos editar
            var row=table.row(fila).data();
            var ruta="/vetado/descripcion/personal/"+$(this).val();

            limpiar_items();
            $("#loading").css('display','block');

            $.get(ruta,function(res)
            {
                $("#loading").css('display','none');
    
                $("#cedula").text(res.vetado.dni);
                $("#nombre").text(res.vetado.name);
                $("#apellido").text(res.vetado.last_name);
                $("#fechan").text(res.vetado.age);
                $("#direccion").text(res.vetado.address);
                $("#descripcion").val(row[3]);
                
                for(var x in res.items)
                {
                    console.log(x);
                    var can = $("#item_"+res.items[x].item_id).val()*1;
                    $("#item_"+res.items[x].item_id).val(res.items[x].quantity + can);
                    $("#des_"+res.items[x].item_id).val(res.items[x].description);
                }
            });
        });
        @can('vetado.edit')
        function actualizar()
        {
            console.log("entro");
            var ruta="/vetado/"+$("#cedula").text();
            var token=$("#token").val();
            
            $("#btnActualizar").attr('disabled',true);
            $("#loading").css('display','block');

            $.ajax({
                url: ruta,
                headers: {'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data:{"descripcion":$("#descripcion").val(),"tipo":"personal"},
                success: function(){
                    message(["Se edito correctamente "],{manual:true})
                    table.cell(fila.children('td')[3]).data( $("#descripcion").val());
                    table=$("#Datos").DataTable().draw();

                    $("#Edit").modal('toggle');
                    $("#btnActualizar").attr('disabled',false);
                    $("#loading").css('display','none');
                    $('body').animate({scrollTop:0}, 'fast');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#modalMessage"), tipo:"danger"});
                $("#btnActualizar").attr('disabled',false);
                $("#loading").css('display','none');
            });
        }

        

        var item_cant = [];
        var item_des = [];
        $(".items").change(function(){
            var value = $(this).val();
            var index = $(this).data('id').toString();
            if(value != '')
            {
                item_cant[index] = value;
            }
            else // Si la cantidad es vacia
            {
                //Se elimina el elemento cantidad
                delete item_cant[index];
            }

        });
        $(".des").change(function(){
            var value = $(this).val();
            var index = $(this).data('id').toString();
            if(value != '')
            {
                item_des[index] = value;
            }
            else // Si la cantidad es vacia
            {
                //Se elimina el elemento cantidad
                delete item_des[index];
            }
        });

        function limpiar_items()
        {
            $("#t_items").find('input').val("");
        }

        @endcan
        @can('vetado.destroy')
        var row;
        $('#deleteModal').on('show.bs.modal', function (event) {
            {{-- Mantenemos el id del elemento seleccionado --}}
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            $("#id").val(value);

            {{-- Mantenemos la fila del elemento seleccionado --}}
            row = button.parents('tr');
        });
        $('#delete').on( 'click', function () {
            var route="/vetado/"+$("#id").val();
            var token=$("#token").val();

            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                data: {'tipo':'personal'},
                success: function(){
                    message(['Se elimino la vetacion correctamente'],{manual:true});
                    table.row(row).remove().draw( false );
                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                    $('body').animate({scrollTop:0}, 'fast');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#deleteModalMessage"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });
        @endcan
    </script>
@stop