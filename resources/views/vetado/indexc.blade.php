@extends('layouts.dashboard')
@section('css')
    <style>
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')
    <div class="d-block bg-dark" style="padding-top: 10px; color:white;visibility:hidden;" id="main">
    
        @include('alert.mensaje')
        <div id="mensaje"></div>
        <table class="table table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;" >
            <thead >
                <th class="text-center" style=" max-width:20%;">Cedula</th>
                <th class="text-center" style=" max-width:20%;">Nombre</th>
                <th class="text-center" style=" max-width:20%;">Apellido</th>
                <th class="text-center" style=" max-width:20%;">Vetado por:</th>
                <th class="text-center" data-orderable="false" style="width:20%"></th>
            </thead>
            <tbody class="text-center" id="lista"> 
                @include('vetado.recargable.lvc')
            </tbody>
        </table>
        
        <input type="hidden"  value="0" id="comodin">
    </div> 
    <!-- Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" style="display: block;visibility:hidden;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Esta apunto de vetar a:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <form id="data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <input type="hidden" id="id">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Cedula</span>
                            </div>
                            <input type="text" class="form-control" id="cedula">
                            <div class="input-group-append">
                                <span class="input-group-text">Nombre</span>
                            </div>
                            <input type="text" class="form-control" id="nombre">
                            <div class="input-group-append">
                                <span class="input-group-text">Telefono</span>
                            </div>
                            <input type="text" class="form-control" id="phone">
                        </div>
                        <div class="row ">
                            <div class="col-md-3 "></div>
                            <div class="col-md-12 ">
                                <div class="form-group text-center">
                                    {!!Form::label('Descripcion','¿Por qué será vetado el cliente?', ['class' => 'font-weight-bold'])!!}
                                    <textarea class="form-control border border-warning" placeholder="Será vetado debido a: " id="descripcion" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            @can('vetado.edit')
                            <button type="button" class="btn btn-danger" onclick="Actualizar2();" id="actualizar" >Actualizar</button>
                            @endcan
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @can('vetado.destroy')
        @include('layouts.modal.deleteVetadoModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section("script")
    {!!Html::script("js/jskevin/cedulanica.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!}
    {!!Html::script("js/jskevin/kavvdt.js")!!}   
    <script>
    
        var table;
        var fila;
        $(document).ready( function () {
            table = createdt($('#Datos'),{buscar:'{!!session("valor")!!}',col:1,cant:[10,20,-1],cantT:[10,20,"Todo"]});
            
            $("#Edit").removeAttr("style");
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index',1060);
        });
        function filtro()
        {
            if($("#tipofil").val()=="Cliente")
                location.href ="/vetado/filtro/cliente";
            else
                location.href ="/vetado/filtro/personal/vetado.indexp";
        }
        
        $('.edit').on( 'click', function () {
            fila=$(this).parents('tr');//Dejamos almacenada temporalmente la fila en la que clickeamos editar
            var row=table.row(fila).data();
            var ruta="/vetado/descripcion/cliente/"+$(this).val();
            
            $("#descripcion").val("");
            $("#data").find("label.badge-pill").text("")
            $("#direccion").text("");

            $("#loading").css('display','block');

            $.get(ruta,function(res)
            {
                $("#loading").css('display','none');
                if(res.vetado.dni)
                {
                    $("#cedula").val(res.vetado.dni);
                    $("#nombre").val(res.vetado.name);
                    $("#phone").val(res.vetado.phone);
                    $("#descripcion").val(row[3]);
                    $("#id").val(res.vetado.id);
                }
                else
                {
                    message(res.message,{manual:true, tipo:"danger"});
                    $("#Edit").modal('toggle');
                }
            });
        });
        @can('vetado.edit')
        function Actualizar2()
        {
            route="/vetado/"+$("#id").val();
            var token=$("#token").val();
            $("#actualizar").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data:{"descripcion":$("#descripcion").val(),"tipo":"cliente"},
                success: function(res){
                    if(res==1)
                    {
                        message(['Se edito correctamente'],{manual:true});
                        table.cell(fila.children('td')[3]).data( $("#descripcion").val());
                        table=$("#Datos").DataTable().draw();
                        $("#Edit").modal('toggle');
                        $('body').animate({scrollTop:0}, 'fast');
                    }
                    else
                    {
                        message(['El campo descripcion esta vacio'],{objeto:$("#modalMessage"), manual:true, tipo:"danger"});
                    }
                    $("#actualizar").attr('disabled',false);
                    $("#loading").css('display','none');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#modalMessage"), tipo:"danger"});
                $("#actualizar").attr('disabled',false);
                $("#loading").css('display','none');
            });
        }
        @endcan

        @can('vetado.destroy')
        var row;
        $('#deleteModal').on('show.bs.modal', function (event) {
            {{-- Mantenemos el id del elemento seleccionado --}}
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            $("#id").val(value);

            {{-- Mantenemos la fila del elemento seleccionado --}}
            row = button.parents('tr');
        });
        $('#delete').on( 'click', function () {
            var route="/vetado/"+$("#id").val();
            var token=$("#token").val();
            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');

            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                data: {'tipo':'cliente'},
                success: function(){
                    message(['Se elimino la vetacion correctamente'],{manual:true});
                    table.row(row).remove().draw( false );
                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                    $('body').animate({scrollTop:0}, 'fast');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#deleteModalMessage"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });
        
        @endcan
    </script>
@stop