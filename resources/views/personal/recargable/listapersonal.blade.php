                @foreach($personal as $trabajador)
                    <tr @if($trabajador->vetoed_id != null) class = "text-danger" @endif>
                        <td>{{$trabajador->dni}}</td> 
                        <td>{{$trabajador->name}}</td>    
                        <td>{{$trabajador->last_name}}</td>  
                        <td>{{$trabajador->age}}</td>   
                        <td>{{$trabajador->address}}</td>
                        <td class="botones">
                            <button type="button" onclick="detalles(this)" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit" value="{{$trabajador->id}}">Detalle</button>
                            @can('personal.destroy')
                            <button data-toggle="modal" data-target="#deleteModal" class="btn btn-danger fa fa-trash" data-value="{{$trabajador->id}}"></button>
                            @endcan
                        </td>
                    </tr>
                @endforeach