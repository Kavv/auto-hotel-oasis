<!-- Modal -->
<div class="modal fade" id="modalBusquedaPersonal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle"> Personal Encontrado </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="data">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="Nombre:">Nombre:</label>
                            <input id="mpNombre" class="form-control" placeholder="Nombre del Cliente" name="Nombre" type="text" disabled>
                        </div>
                        <div class="col-md-6">
                            <label for="Apellido:">Apellido:</label>
                            <input id="mpApellido" class="form-control" placeholder="Nombre del Cliente" name="Nombre" type="text" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label class="pt-2" for="Edad:">Edad:</label>
                        </div>
                        <div class="col-md-3 pt-1 text-left">
                            <input id="mpEdad" class="form-control" placeholder="Nombre del Cliente" name="Nombre" type="text" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="Direccion:">Direccion:</label>
                            <input id="mpDireccion" class="form-control" placeholder="Nombre del Cliente" name="Nombre" type="text" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>