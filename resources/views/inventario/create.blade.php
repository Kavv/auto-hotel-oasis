@extends('layouts.dashboard')
@section('content')
  <div class="card">
    <h4 class="card-header">Agregar Inventario</h4>
    <div class="card-body">
      <div id="mensaje">
        @include('alert.mensaje')
      </div>
      <form action="{{route('inventario.store')}}" method="POST" id="data" autocomplete = off>
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

        @include('inventario.formulario.datos')

        <div class="text-center">
          <input class="btn btn-primary btnSent" type="button" value="Guardar" onclick="save('save');">
          <input class="btn btn-primary btnSent" type="button" value="Guardar y ver" onclick="save('savev');">
        </div>
      </form>
    </div>
  </div>    
   
  @can('servicio.create')
  <div class="modal fade" id="addService" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar Nuevo Servicio</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="modalMessage"></div>
          <div class="row ">
            <div class="col-md-12 text-center">
              <div class="form-group">
                {!!Form::label('Nombre')!!}
                {!!Form::text('serviceName',null,['id'=>'serviceName','class'=>'form-control border border-warning','placeholder'=>'Nombre del servicio'])!!}
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="button" class="btn btn-primary" onclick="serviceSave();">Guardar</button>
        </div>
      </div>
    </div>
  </div>
  @endcan

  @include('layouts.cargando.cargando')
@stop
@section('script')
  {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
  <script>
  
    $("#loading").css('z-index',1060);
    //Agrega el item mediante una consulta AJAX
    function save(condition)
    {
      var ruta="/inventario";
      var token=$("#token").val();
      $(".btnSent").attr("disabled",true);
      $("#loading").css('display','block');

      $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:$("#data").serialize(),
        success: function(result){
          if(result === 1)
          {
            if(condition=="save")
            {
              message(["Articulo agregado exitosamente"],{manual:true})
              limpiar();
              $(".btnSent").attr("disabled",false);
              $("#loading").css('display','none');
            }
            if(condition=="savev")
            {
              location.href="/inventario/inventariov/1/"+$("#servicios").val()+"/"+$("#nombre").val();
            }
          } else {
            message(result, {manual:true, tipo:'danger'});
            $(".btnSent").attr("disabled",false);
            $("#loading").css('display','none');
          }
          $('body').animate({scrollTop:0}, 'fast');
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
        message(jqXHR,{tipo:"danger"});
        $(".btnSent").attr("disabled",false);
        $("#loading").css('display','none');
        $('body').animate({scrollTop:0}, 'fast');
      });
    }
    function limpiar()
    {
      if($("#checkServicio").prop('checked') == true)
        $("#servicios").val("");
      if($("#checkNombre").prop('checked') == true)
        $("#nombre").val("");
      if($("#checkCantidad").prop('checked') == true)
        $("#cantidad").val("");
      if($("#checkCA").prop('checked') == true)
        $("#CA").val("");
      if($("#checkCO").prop('checked') == true)
        $("#CO").val("");
    }
    
    @can('servicio.create')
    function serviceSave()
    {
      var ruta="/servicio";
      var token=$("#token").val();
      $("#btnAddService").attr("disabled",true);
      $("#loading").css('display','block');
      $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data: {"Nombre":$('#serviceName').val()},
        success: function(result){
          if(result === 1)
          {
            $.get("{{route('inventario.servicelist')}}", function(res){

              htmlCode = "<option ></option>";
              focus = $("#serviceName").val();
              for(i=0; i< res.length; i++)
              {
                if(focus != res[i].name)
                  htmlCode += "<option value='" + res[i].id + "'>" + res[i].name + "</option>";
                else
                  htmlCode += "<option selected value='" + res[i].id + "'>" + res[i].name + "</option>";

              }

              $("#servicios").empty();
              $("#servicios").append(htmlCode);
              $("#addService").modal("toggle");
              $("#serviceName").val("");

              $("#btnAddService").attr("disabled",false);
              $("#loading").css('display','none');
              message(["Servicio agregado exitosamente!"],{manual:true})
              $('body').animate({scrollTop:0}, 'fast');
            });
          } else {
            message(result,{objeto:$("#modalMessage"), manual:true, tipo:'danger'});
            $("#btnAddService").attr("disabled",false);
            $("#loading").css('display','none');
          }
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
        message(jqXHR,{objeto:$("#modalMessage"), tipo:"danger"});
        $("#btnAddService").attr("disabled",false);
        $("#loading").css('display','none');
      });
    }
    @endcan
  </script>
@stop