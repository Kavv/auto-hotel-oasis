                @foreach($inventario as $item)
                    <tr>
                        <td>{{$item->servicio['name']}}</td> 
                        <td>{{$item->name}}</td>    
                        <td>{{$item->quantity}}</td>  
                        <td>{{$item->rental_price}}</td>  
                        <td>{{$item->buy_price}}</td>  
                        <td class="botones">
                            @can('inventario.edit')
                            <button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-pencil" value="{{$item->id}}" onclick="editar(this, {})"></button>
                            @endcan
                            @can('cliente.destroy') 
                              <button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="{{$item->id}}"></button>
                            @endcan
                        </td>
                    </tr>
                @endforeach

