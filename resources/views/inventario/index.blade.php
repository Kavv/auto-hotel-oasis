@extends('layouts.dashboard')
@section('css')
    <style>
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')
    <div class="d-block bg-dark" style="color:white; padding-top: 10px;visibility:hidden;" id="main">

        <div class="row my-4 pl-4" id="div_agregar">
            <button class="btn btn-success " data-toggle="modal" data-target="#add"><span class="fa fa-plus"></span> Agregrar un registro</button>
        </div>
        @include('alert.mensaje')
        <div id="mensaje"></div>
        <table class="table table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;">
            <thead >

                <th class="text-center" style=" max-width:20%;">Servicio</th>
                <th class="text-center" style=" max-width:30%;">Nombre</th>
                <th>Cantidad</th>
                <th>Costo Alquiler</th>
                <th>Costo Objeto</th>
                <th class="text-center" data-orderable="false" style="width:20%"></th>
            </thead>
            <tbody class="text-center" id="lista"> 
                @include('inventario.recargable.listainventario')
            </tbody>
        </table>
        
    </div>


    <div class="modal fade" id="add" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="tile_madd">Agregar un registro</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="msj_add"></div>
                <form action="" id="form_data" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    <?php $index = 0;?>
                    @include('inventario.formulario.datos')
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-success" id="btn_guardar">Guardar <i class="fa fa-save"></i> </button>
            </div>
          </div>
        </div>
    </div>


    @can('inventario.edit')
    <!-- Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="message_edit"></div>
                    <form id="form_edit">
                        <input type="hidden"  id="id">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <!--UNA VEZ TERMINADA LA CLASE DE IS REGRESAR LOS FORMULARIOS A SU TAMAñO GRANDE E IMPORTARLO AQUI-->
                        <?php $index = 1;?>
                        @include('inventario.formulario.datos')

                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="btn_actualizar"
                            onclick="actualizar({msg: '#message_edit'})">Actualizar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan

    @can('servicio.create')
    <!-- MODAL PARA AGREGAR SERVICIO -->
    <div class="modal fade" id="addService" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Agregar Nuevo Servicio</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            <div id="modalMessageService"></div>
            <div class="row ">
                <div class="col-md-12 text-center">
                <div class="form-group">
                    {!!Form::label('Nombre')!!}
                    {!!Form::text('serviceName',null,['id'=>'serviceName','class'=>'form-control border border-warning','placeholder'=>'Nombre del servicio'])!!}
                </div>
                </div>
            </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary" onclick="serviceSave();">Guardar</button>
            </div>
        </div>
        </div>
    </div>
    @endcan



    @can('personal.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!}  
    {!!Html::script("js/jskevin/kavvdt.js")!!} 

    {{-- Si tiene los permisos de crear un registro incluira la funcion js correspondiente --}}
    @can('cliente.create')
        {!!Html::script("js/ajax_crud/create.js")!!} 
    @endcan

    {{-- Si tiene los permisos de actualizar un registro incluira la funcion js correspondiente --}}
    @can('cliente.update')
        {!!Html::script("js/ajax_crud/update.js")!!} 
    @endcan

    {{-- Si tiene los permisos de eliminar un registro incluira la funcion js correspondiente --}}
    @can('cliente.destroy')
        {!!Html::script("js/ajax_crud/delete.js")!!} 
    @endcan

    <script>
        var servi=0;
        var item=0;
        var table;
        var fila;
        const base = "inventario";
        $(document).ready( function () {
            table=createdt($('#Datos'),{buscar:'{!!session("valor")!!}',col:1,cant:[10,20,-1],cantT:[10,20,"Todo"]});
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index',1060);
        });
        
        // Por efectos de visibilidad utilizamos este metodo click
        $("#btn_guardar").click(function(){
            // Llamamos a la funcion que guarda el nuevo registro
            save({
                msg: '#msj_add', 
            });
        });
        // Resultado exitoso del create
        function success(res)
        {
            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code === 1)
            {
                // Mostramos un msj de exito
                message(["Se agrego el registro correctamente"],{manual:true});
                // Llamamos a la funcion que agrega una fila a la tabla
                addRow(res.data);
                // Ocultamos el modal de agregación
                $("#add").modal('hide');
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{objeto:$("#msj_add"), manual:true,tipo:"danger"});
            }

            $("#btn_guardar").attr("disabled",false);
            $("#loading").css('display','none');
        }

        function addRow(data)
        {
            // Almacenara los html de las acciones
            var botones = "";
            // Si tiene permisos de edicion se adjunta el html del btn para editar
            @can('cliente.update') 
                botones += '<button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-pencil" value="'+data.id+'" onclick="editar(this, {})"></button>';
            @endcan
            // Si tiene permisos de eliminación se adjunta el html del btn para eliminar
            @can('cliente.destroy') 
                botones += '<button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="'+data.id+'"></button>';
            @endcan

            // Hacemos referencia a la variable que contienen la DataTable
            // Accedemos a su propiedad row y al metodo add
            // Especificando ahí los datos de cada columna en la tabla
            table.row.add( [
                $(".servicio option:selected").eq(0).text(),
                data.name,
                data.quantity,
                data.rental_price,
                data.buy_price,
                botones,
            ] ).draw( false );
        }

        // Resultado exitoso del detalle o edit
        function fill(res)
        {
            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code == 1)
            {
                // Extraemos la información del registro
                let data = res.data;
                // Rellenamos los inputs con los valores correspondientes
                $(".servicio").eq(1).val(data.service_id);
                $(".nombre").eq(1).val(data.name);
                $(".cantidad").eq(1).val(data.quantity);
                $(".CA").eq(1).val(data.rental_price);
                $(".CO").eq(1).val(data.buy_price);
                $("#id").val(data.id);
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{manual:true,tipo:"danger"});
                $("#Edit").modal('hide');
            }
            // Ocultamos la pantalla de carga
            $("#loading").css('display','none');
        }

        // Resultado exitoso del update
        function update_success(res)
        {
            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code == 1)
            {
                // Mostramos un mensaje de error
                message(["Se edito el registro correctamente"],{manual:true});
                data = res.data;
                var servicio = $(".servicio option:selected").eq(1).text();
                var nombre = $(".nombre").eq(1).val();
                var cantidad = $(".cantidad").eq(1).val();
                var ca = $(".CA").eq(1).val();
                var co = $(".CO").eq(1).val();
                // Plasmamos en la tabla los nuevos valorestable.cell(fila.children('td')[0]).data( $("#servicios option[value="+ $("#servicios").val() +"]").text());
                table.cell(fila.children('td')[0]).data( servicio );
                table.cell(fila.children('td')[1]).data( nombre );
                table.cell(fila.children('td')[2]).data( cantidad );
                table.cell(fila.children('td')[3]).data( ca );
                table.cell(fila.children('td')[4]).data( co );
                // Ocultamos el modal
                $("#Edit").modal('hide');
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{objeto:$("#message_edit"), manual:true,tipo:"danger"});
            }
            // Reactivamos el boton de actualizar
            $("#btn_actualizar").attr("disabled",false);
            // Ocultamos la pantalla de carga
            $("#loading").css('display','none');
        }

           
        @can('servicio.create')
        function serviceSave()
        {
            var ruta="/servicio";
            var token=$("#token").val();
            $("#btnAddService").attr("disabled",true);
            $("#loading").css('display','block');
            $.ajax({
                url: ruta,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data: {"Nombre":$('#serviceName').val()},
                success: function(result){
                    if(result === 1)
                    {
                        $.get("{{route('inventario.servicelist')}}", function(res){

                            htmlCode = "<option ></option>";
                            focus = $("#serviceName").val();
                            for(i=0; i< res.length; i++)
                            {
                                if(focus != res[i].name)
                                htmlCode += "<option value='" + res[i].id + "'>" + res[i].name + "</option>";
                                else
                                htmlCode += "<option selected value='" + res[i].id + "'>" + res[i].name + "</option>";
                            }
                            
                            $(".servicio").empty();
                            $(".servicio").append(htmlCode);
                            $("#addService").modal("toggle");
                            $("#serviceName").val("");

                            $("#btnAddService").attr("disabled",false);
                            $("#loading").css('display','none');
                            
                            var divMessage = ['#msj_add', "#message_edit"];

                            message(["Servicio agregado exitosamente!"],{objeto:$(divMessage[modalTipe]), manual:true})
                            $('body').animate({scrollTop:0}, 'fast');
                        });
                    } else {
                        message(result,{objeto:$("#modalMessageService"), manual:true, tipo:'danger'});
                        $("#btnAddService").attr("disabled",false);
                        $("#loading").css('display','none');
                    }
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#modalMessageService"), tipo:"danger"});
                $("#btnAddService").attr("disabled",false);
                $("#loading").css('display','none');
            });
        }
        @endcan

        let modalTipe = 0;
        $('#add').on('show.bs.modal', function (e) {
            modalTipe = 0;
        })
        $('#Edit').on('show.bs.modal', function (e) {
            modalTipe = 1;
        })
    </script>
@stop