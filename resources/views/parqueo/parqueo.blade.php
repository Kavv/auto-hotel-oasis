@extends('layouts.dashboard')
@section('css')

<style>
  .cantidad{
    font-size: 40px;
    height: 4.2em;
    width: 100%;
    text-align: center;
    font-weight: bold;
  }
  .arriba{
    height: 5em;
  }
  .abajo{
    height: 5em;
  }
  .btn_actualizar{
    height: 10em;
    font-weight: bold;
  }
  .i-special{
    font-size: 53px;
    align-items: center;
    display: flex;
    justify-content: center;
  }
  .msg{
    font-size: 26px;
    font-weight: bold;
  }
  .msg li{
    list-style: none;
  }
  .content-wrapper {
    background: #dceaff!important;
  }
</style>

@stop



@section('content')
<div id="parking_bg">
  <div class="row px-4 pt-2">
    <div class="col-sm-12 text-center">
      <h1>PARQUEO GENERAL</h1>
    </div>
  </div>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
  @foreach ($general_parking as $p)
    <div class="row px-4 pt-4">
      <div class="col-sm-12 text-center">
        <h2>Parqueo {{$type[$p->type]}} - Espacio Maximo {{$p->quantity}}</h2>
        <div id="msg_{{$p->id}}" class="msg"></div>
      </div>
    </div>
    <div class="row px-4 d-flex justify-content-center">
      <div class="col-sm-3">
        <?php $actual = $p->quantity - $p->temp; ?>
        <input type="text" class="cantidad" id="c_{{$p->id}}" data-max="{{$p->quantity}}" value="{{$actual}}" disabled>
      </div>
      <div class="col-sm-3">
        <button class="btn btn-block btn-info arriba" onclick="sumar('c_{{$p->id}}', 1);"><i class="i-special fa fa-sort-up"></i></button>
        <button class="btn btn-block btn-info abajo" onclick="sumar('c_{{$p->id}}', -1);"><i class="i-special fa fa-sort-down"></i></button>
      </div>
      <div class="col-sm-3">
        <button class="btn btn-block btn-success btn_actualizar" onclick="actualizar('{{$p->id}}')">ACTUALIZAR</button>
      </div>
    </div>
  @endforeach
</div>
  @include('layouts.cargando.cargando')
@stop
@section('script')
{!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
{!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
{!!Html::script("js/jskevin/tiposmensajes.js")!!}  
{!!Html::script("js/jskevin/kavvdt.js")!!} 
  <script>
    $cantidad = $("#cantidad");
    
    const base = "parqueo";
    $(document).ready(function(){
      // Hacemo visible el contenido
      $("#main").css('visibility','visible'); 
      // Configuramos el modal de carga con 1060 en z-index
      $("#loading").css('z-index',1060);
    });

    function sumar(input, int)
    {
      $input = $("#"+input);
      value = parseInt($input.val()) + parseInt(int);
      $input.val(value);
    }


    function actualizar(value)
    {
      var route = "/" + base + "/update_quantity";
      var token = $("#token").val();
      var max = $("#c_"+value).data().max;
      var actual = $("#c_"+value).val();
      var quantity = parseInt(max) - parseInt(actual);
      $("#btn_actualizar").attr("disabled",true);
      $("#loading").css('display','block');
      return $.ajax({
          url: route,
          headers: {'X-CSRF-TOKEN': token},
          type: 'POST',
          dataType: 'json',
          data: {'quantity': quantity, 'parking': value},
          success: function(res){
            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code == 1)
            {
                // Mostramos un mensaje de error
                message(["¡Actualizado!"],{objeto:$("#msg_"+value), manual:true});
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{objeto:$("#msg_"+value), manual:true,tipo:"danger"});
            }
            // Reactivamos el boton de actualizar
            $("#btn_actualizar").attr("disabled",false);
            // Ocultamos la pantalla de carga
            $("#loading").css('display','none');
          }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
          message(jqXHR,{objeto:$("#msg_"+value), tipo:"danger"});
          $("#btn_actualizar").attr("disabled",false);
          $("#loading").css('display','none');
      });
    }
    

  </script>
  
@stop