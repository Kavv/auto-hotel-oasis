
  @foreach($parking as $p)
    <?php 
      $type = ["", 'Motocicleta', 'Carro', 'Camioneta', 'Varios'];
      $vip = ['General', 'Privado']; 
    ?>
    <tr>
      <td>{{$type[$p->type]}}</td>  
      <td>{{$vip[$p->vip]}}</td>
      <td>{{$p->quantity}}</td>  
      <td class="botones">
        <button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-pencil" value="{{$p->id}}" onclick="editar(this, {})"></button>
        @can('cliente.destroy') 
          <button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="{{$p->id}}"></button>
        @endcan
      </td>
    </tr>
  @endforeach