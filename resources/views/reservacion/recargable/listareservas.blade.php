        @foreach($reservas as $reserva)
            <tr>
                <td>{{$reserva->clienteTrash['name']}}</td>    
                <td>{{$reserva->clienteTrash['dni']}}</td>
                <td>{{$reserva->created_at}}</td> 
                
                <td class="botones">
                    <button  data-toggle="modal" data-target="#infore" class="btn btn-primary detalle" value="{!!$reserva->id!!}">Detalle</button>
                    @can('reservacion.destroy')
                    <button data-toggle="modal" data-target="#deleteModal" class="btn btn-danger fa fa-trash" data-value="{{$reserva->id}}"></button>
                    @endcan
                </td>
            </tr>
        @endforeach