                  <table class="table table-hover" cellspacing="0" id="tablaarti" style="width:100%;">  
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Cantidad</th>
                        <th>Costo Alquiler</th>
                        <th class="text-center" data-orderable="false" style="width:10%"></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody id="tablaarticulos">
                      @foreach($inventario as $item)
                        <tr id="artiCant{!!$item->id!!}">
                          <td class="itemname">{!!$item->name!!}</td>
                          <!-- ACA SERIA EL IF -->
                          @if(array_key_exists($item->id, $reserved_i))
                            <td id="c_artiCant{!!$item->id!!}"><?php echo $item->quantity - $reserved_i[$item->id] ?></td>
                          @else
                            <td id="c_artiCant{!!$item->id!!}">{!!$item->quantity!!}</td>
                          @endif
                          <td class="cost">{!!$item->rental_price!!}</td>
                          <td>
                            <button  value="{!!$item->id!!}" type="button" class="btn btn-primary" onclick="mostrar(this);" data-toggle="modal" data-target="#Add"  >Reservar</button>
                          </td>
                          <td>{!!$item->servicio['id']!!}Servicio</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>