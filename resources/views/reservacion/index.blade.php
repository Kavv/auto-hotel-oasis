@extends('layouts.dashboard')
@section('css')
    <style>
        
    </style>
@stop
@section('content')

    <div class="d-block bg bg-dark" style="color:white; padding-top: 10px;visibility:hidden;" id="main">
            <div style="overflow-x: auto; min-width:80%;">
                @include('alert.mensaje')
                <div id="mensaje"></div>
                <table class="table table-hover table-dark text-center" cellspacing="0" id="Datos" style="width:100%;">
                    <thead>
                        <th style="" >Nombre</th>
                        <th style="" >Cedula</th>
                        <th style="" >Fecha Facturacion</th>
                        <th data-orderable="false" ></th>
                    </thead>
                    <tbody class="text-center" id="lista"    > 
                        @include('reservacion.recargable.listareservas')
                    </tbody>
                </table>
            </div>
    </div>

    

    @can('personal.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!}  
    {!!Html::script("js/jskevin/kavvdt.js")!!}  
    <script>
        var table;
        var resinfo = new Object();
        var des = new Object();
        $(document).ready(function(){
            table = createdt($("#Datos"),{cant:[10,20,-1],cantT:["10","20","Todo"]});
            $("#main").css('visibility','visible'); 
            $("#loading").css('z-index',1060);
        });

        var reserva;
        var prue;
        var facturas;
        

        @can('reservacion.destroy')
        
        var row;
        $('#deleteModal').on('show.bs.modal', function (event) {
            {{-- Mantenemos el id del elemento seleccionado --}}        
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            $("#id").val(value);


            {{-- Mantenemos la fila del elemento seleccionado --}}
            row = button.parents('tr');
        });

        $('#delete').on( 'click', function () {
            var route="/reservacion/"+$("#id").val();
            var token=$("#token").val();
            
            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                success: function(){
                    message(['Se elimino correctamente'],{manual:true});
                    table.row(row).remove().draw( false );
                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                    $('body').animate({scrollTop:0}, 'fast');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#deleteModalMessage"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });
        @endcan

    </script>
@stop