@extends('layouts.dashboard')
@section('content')
  <!--Referencias para el calendario-->
  {!!Html::style("css/gijgo2/css/gijgo.css")!!}
  <form action="{{route('reservacion.store')}}" method="POST" id='formulario' target="_blank">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="false" >
      <div class="carousel-inner">
        <div id="parte1" class="carousel-item active">
          <div class="card">
            <h4 class="card-header">Reservar</h4>
            <div class="card-body">
              @include('alert.errors')
              <div id="mensajereserva"></div>
              <div class="row ">
                <div class="col-md-3 "></div>
                <div class="col-md-6 ">
                  <div class="form-group text-center">
                    {!!Form::label('Cedula','Cedula:')!!}
                    <div  class="input-group ">
                      <div class="input-group-prepend">
                        <div class="input-group-text">
                          <input type="checkbox" id="nacional" checked>
                          <label class="form-check-label badge badge-pill badge-info" for="defaultCheck1">
                              Nica
                          </label>
                        </div>
                      </div>
                      {!!Form::text('Cedula_Cliente',$reservacion['cliente']['dni'],['class'=>'form-control border border-warning','placeholder'=>'xxx-xxxxxx-xxxxx', 'autocomplete'=>'off', 'onkeypress'=>'return CharCedula(event,this);', 'onkeyup'=>'formatonica(this)','id'=>'Cedu'])!!}  
                      <div class="input-group-append">
                        <button class="btn btn-primary fa fa-search" onclick="buscar();"  type="button"></button>
                        <button class="btn btn-primary fa fa-eraser" onclick="limpiarCliente();"  type="button"></button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row ">
                <div class="col-md">
                  <div class="form-group">
                      {!!Form::label('Nombre:')!!}
                      {!!Form::text('Nombre_Contacto',$reservacion['cliente']->name." ".$reservacion['cliente']->last_name,['id'=>'Nom','class'=>'form-control border border-warning','placeholder'=>'Nombre completo', 'onkeypress'=>'return BlockEnter(event)','readonly'])!!}
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      {!!Form::label('Direccion del evento:')!!}
                      {!!Form::text('Direccion_Local',$reservacion->event_address,['id'=>'Dir','class'=>'form-control border border-warning','placeholder'=>'Direccion donde se realizara el evento', 'onkeypress'=>'return BlockEnter(event)'])!!}
                  </div>
                </div>
              </div> 

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      {!!Form::label('Fecha de Inicio:')!!}
                      <input id="datepicker" class="border border-warning" name="Fecha_Inicio" width="276" value="{!!$reservacion->begin_date!!}"  placeholder="año-mes-dia" readonly /> 
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      {!!Form::label('Fecha de Fin:')!!}
                      <input id="datepicker2" class="border border-warning" name="Fecha_Fin"  width="276" value="{!!$reservacion->end_date!!}"  placeholder="año-mes-dia" readonly /> 
                  </div>
                </div>
              </div>
            </div>

          </div>

            <!--TENGO QUE AGREGAR LO QUE EL USUARIO APARTO PARA ESE DIA-->

          <div id="tablas" style="display:none;">
            @can('servicio.index')
            @can('inventario.index')
            <div class="row tablaA" style="" >
              <div class="col-md-4">
                <div class="list-group" id="list-tab" role="tablist" style="height:20em; overflow:scroll;">
                  <table id="tablaservicios" cellspacing="0" style="width:102%;">
                    <thead>
                      <th class="text-center">Servicios</th>
                    </thead>
                    <tbody>
                      @foreach($servicios as $servicio)
                        <tr><td>
                          <a class="list-group-item list-group-item-action serviciobuscar" id='{!!$servicio->id!!}' data-toggle="list" href='#panelarticulo'>{!!$servicio->name!!}</a>
                        </td></tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              <!--/*PERFECCIONAR LA TABLA ESPECIFICAR UN ALTO ESTATICO PARA CADA DIRECCION Y UN SCROLL EN "Y"*/-->
              <div class="col-md-8 tablaA" style="height:20em; overflow:scroll; ">
                <div class="tab-content" id="nav-tabContent">
                  <div  data-spy="scroll" class="scrollspy-example tab-pane fade"data-target="#list-tab" data-offset="0" id="panelarticulo" role="tabpanel" aria-labelledby="{!!$servicio->ID_Servicio!!}">
                    {!!$articulo!!}
                  </div>
                </div>
              </div>
            </div>
            @endcan
            @endcan
            @can('menus.index')
            <div class="row">
              <div class="col-md-4" ></div>
                <button type="button" id="btnmenu" class="btn btn-success col-md-4" data-toggle="modal" data-target="#serviespecial" >Servicio de comida</button>
            </div>
            @endcan
            <!--Tabla de articulos reservados-->
            <input type="hidden" id="contf" value=0>
            <input type="hidden" id="dias">
            <table class="table table-hover TablaA" id="TablaA"  style="width: 100%">
              <thead>
                <tr id="fila0" value="0">
                  <th >Nombre</th>
                  <th data-orderable="false">Cantidad</th>
                  <th data-orderable="false">Costo Alquiler</th>
                  <th data-orderable="false">Dias Alquilados</th>
                  <th data-orderable="false">Costo Total</th>
                  <th data-orderable="false"></th>
                </tr>
              </thead>
              <tbody id="articuloren">
              </tbody>
            </table>
          </div>
          <button type="button" class="btn btn-primary btn-lg btn-block"OnClick='Enviar();' id="reser">Actualizar</button> 
        </div> 

        <!--Segunda parte (Pre visualizacion de los datos finales)-->
        <div id="parte2" class="carousel-item">
          <div class="card">
            <h4 class="card-header">Reservar</h4>
            <div class="card-body">
              <button type="button" class="btn btn-primary float-right" OnClick='CambioPag();'>Regresar</button>
              <div class="row ">
                <div class="col-md">
                  <div class="form-group">
                      {!!Form::label('DD','',['id'=>'lf','class' => 'badge badge-pill badge-info', 'style'=>'font-size: 90%;'])!!}
                  </div>
                </div>
              </div>

              <div class="row ">
                <div class="col-md-6 ">
                  <div class="form-group">
                      {!!Form::label('','Cliente:')!!}
                      {!!Form::label('','Nombre:',['id'=>'lnom','class' => 'badge badge-pill badge-info', 'style'=>'font-size: 90%;'])!!}
                      {!!Form::label('','/')!!}
                      {!!Form::label('','Cedula:',['id'=>'lce','class' => 'badge badge-pill badge-info', 'style'=>'font-size: 90%;'])!!}
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      {!!Form::label('','Direccion: ')!!}
                      {!!Form::label('','',['id'=>'ldir','class' => 'badge badge-pill badge-info', 'style'=>'font-size: 90%;'])!!}
                  </div>
                </div>
              </div> 

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      {!!Form::label('','Fecha de Inicio: ')!!}
                      {!!Form::label('','',['id'=>'lfi','class' => 'badge badge-pill badge-info', 'style'=>'font-size: 90%;'])!!}
                  </div>
                </div>
              </div> 
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      {!!Form::label('','Fecha de Fin: ')!!}
                      {!!Form::label('','Fecha de Fin:',['id'=>'lff','class' => 'badge badge-pill badge-info', 'style'=>'font-size: 90%;'])!!}
                  </div>
                </div>
              </div> 

              
              <div class="row">
                <div  class="input-group">
                  <!-- botones para definir la longitud de la factura -->
                  <div class="col col-md-6" style="display:flex; justify-content:flex-start">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><b>Filas de la factura</b></span>
                    </div>
                    <input type="text" id="filafactura" value="" data-value="{{$reservacion->invoice_row}}"
                    class="form-control col-md-2 text-center" onkeypress="return BlockEnter(event)">
                    <div class="input-group-append">
                      <button class="btn btn-info " onclick="tablafactura();" type="button">Aplicar</button>
                    </div>
                  </div>
                  <!-- botones para definir el iva-->
                  <div class="col col-md-6" style="display:flex; justify-content:flex-end">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        @if($reservacion->tax==0)
                          <input type="checkbox" id="iva">
                        @else
                          <input type="checkbox" id="iva" checked>
                        @endif
                        <label class="form-check-label badge badge-pill badge-info" for="defaultCheck1">
                            IVA
                        </label>
                      </div>
                    </div>
                    <input type="text" id="valoriva" value="{!!$reservacion->tax*100!!}" class="form-control col-md-2 text-center" onkeypress="return BlockEnter(event)">
                    <div class="input-group-append">
                      <span class="input-group-text"><b>%</b></span>
                      <button class="btn btn-info" onclick="eliminardetalles();"  type="button">Aplicar</button>
                    </div>
                  </div>
                </div>
                
              </div>

              <div class="row mt-2">
                <div  class="input-group">
                
                  <!-- botones para definir el descuento-->
                  <div class="col col-md-12" style="display:flex; justify-content:flex-end">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        @if($reservacion->discount == 0)
                          <input type="checkbox" id="descuento">
                        @else
                          <input type="checkbox" id="descuento" checked>
                        @endif
                        <label class="form-check-label badge badge-pill badge-info" for="defaultCheck1">
                            Descuento
                        </label>
                      </div>
                    </div>
                    <input type="text" id="valorDescuento" value="{!!$reservacion->discount*100!!}" class="form-control col-md-1 text-center" onkeypress="return BlockEnter(event)">
                    <div class="input-group-append">
                      <span class="input-group-text"><b>%</b></span>
                      <button class="btn btn-info" onclick="eliminardetalles();"  type="button">Aplicar</button>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="row mb-2">
                <!-- botones para seleccionar las factura-->
                <div class="input-group">                
                  <div class="col col-md-12" style="display:flex; justify-content:center">
                    <div class="input-group-prepend">
                      <button class="btn btn-info fa fa-angle-left" onclick="cambiofac(-1);" type="button"></button>
                    </div>
                    <span class="input-group-text"><b id="numfac">Factura #1</b></span>
                    <div class="input-group-append">
                      <button class="btn btn-info fa fa-angle-right" onclick="cambiofac(1);" type="button"></button>

                      <button class="btn btn-info fa fa-print" style="margin-left: 1px;" onclick="" type="submit"></button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                    <table class="table table-hover text-center" id="tablaB" name="TB">
                      <thead>
                        <tr id="fila0"  value="0">
                          <th>Nombre</th>
                          <th>Cantidad</th>
                          <th>Costo Alquiler</th>
                          <th>Dias Alquilados</th>
                          <th>Costo Total</th>
                        </tr>
                      </thead>
                      <tbody  id="artifin">
                      </tbody>
                    </table>   
                    <input class="btn btn-primary btn-lg btn-block btnSent" type="button" value="Actualizar" Onclick='reservaupdate();' > 
                  </div>
                </div>
              </div> 
              <input type="hidden" id="arre" name="lista" value=" ">
              <input type="hidden" id="ac" name="accion" value=" ">
              <input type="hidden" id="facactual" name="facactual" value="0">
              
              
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>

  @can('inventario.index')
  <!-- Modal para añadir la cantidad de articulos -->
  <div class="modal fade" id="Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Cuanto desea reservar?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          <input type="hidden" id="id">
          <div id="mensajearticulo"></div>
          <div class="row">
            <div class="col-md-12 text-center">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="0" id="manual" name="sinimagen">
                <label class="form-check-label " for="defaultCheck1">
                    Automatico (No recomendado)
                </label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
                {!!Form::label('Cantidad:')!!}
            </div>
            <div class="col-md-10">
                {!!Form::text('Cantidad',null,['id'=>'cant','class'=>'form-control','autocomplete'=>'off','onkeypress'=>'return valida(event)'])!!}
            </div>
          </div>
          <div class="row" id="rowprice" style="visibility:hidden;">
            <div class="col-md-2">
                {!!Form::label('Precio:')!!}
            </div>
            <div class="col-md-10">
                {!!Form::text('Precio',null,['id'=>'precio','class'=>'form-control','autocomplete'=>'off','onkeypress'=>'return valida(event)'])!!}
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          {!!link_to('#TablaA',$title='Añadir',$attributes=['id'=>'anadir','class'=>'btn btn-primary','onclick'=>'AddToListArticulo();'],$secure = null)!!}
        </div>
      </div>
    </div>
  </div>
  @endcan
  @can('menus.index')
  <!-- Modal para añadir una comida o un pedido especial-->
  <div class="modal fade bd-example-modal-lg" id="serviespecial" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Reserva especial</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token2">
          <input type="hidden" id="id2">
          <input type="hidden" id="returnLista">
          <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="false">
            <div class="carousel-inner" style="overflow:scroll;">
            <!-- Aplicar un tamaño de altura, refrescar el menu al agregar, mostrar msjs -->
              <div id="menuparte1" class="carousel-item active">
                <div class="row" style="height:50em">
                  <div id="menuLista" class="col-md-12" style="overflow-y:scroll;">
                    @include('reservacion.menu.menu')
                  </div>
                </div>
              </div>
              @can('menus.create')
              <div id="menuparte2" class="carousel-item">
                @include('reservacion.menu.añadir')
              </div>
              @endcan
            </div>
          </div>
        </div>
        <div class="modal-footer" style="display: flex; justify-content:center">
          @can('menus.create')
          <input id="btnGuardarComida" class="btn btn-success" type="button" style="display:none;" onclick="save('guardar');" value="Guardar">
          {!!link_to('#TablaA',$title='Agregar nuevo plato',$attributes=['onclick'=>'CambioPagMenu();','class'=>'btn btn-primary', 'id'=>'btnCambio'],$secure = null)!!}
          @endcan
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  @endcan
  <div id="factura" ></div>

  @include('layouts.cargando.cargando')
@stop
@section('script')
  {!!Html::script("js/jskevin/cedulanica.js")!!} 
  {!!Html::script("js/gijgo2/js/gijgo.js")!!}
  {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
  {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
  {!!Html::script("js/jskevin/tiposmensajes.js")!!}  
  {!!Html::script("js/jskevin/reservacion.js")!!}
  {!!Html::script("js/jskevin/kavvdt.js")!!}
  <script>

    var table,TA,Tarti;
    var fila;
    var data;
    var prue;
    var carpeta = "{{$carpeta}}";
    //Esta fuera para que sealize de inmedianto en cuanto inice los procesos de carga. y se oculte al estar listo el doc.
    $("#loading").css('display','block');
    
    $(document).ready( function () {
      @can('inventario.index')
      //Cargamos la tabla de articulos 
      columArticulos=[{"targets": [ 4 ],"visible": false}];
      Tarti=createdt($('#tablaarti'),{buscar:'',dom:"f",columdef:columArticulos,searchC:4});//Datatable articulos

      @endcan
      
      dias();// Validamos la fehca
      $("#dias").val(diff);//Almacenamos la cantidad de dias

      @can('servicio.index')
      createdt($('#tablaservicios'),{dom:"f"});// Datatable servicio
      @endcan

      TA=createdt($("#TablaA"),{dom:"",ordering:false});// Datatable lista articulos solicitados
      
      @can('menus.index')
      columMenu=[{ className:"comidades", "targets": [ 1 ] },
          { className:"comidacos", "targets": [ 2 ] },
          { className:"comidacant", "targets": [ 3 ] },
          { className:"botones", "targets": [ 4 ] }];
      // Datatable menu
      table=createdt($('#tablamenu'),{pagT:"first_last_numbers",col:1,dom:'"fp"',cant:[5],cantT:["5"],columdef:columMenu});
      
      // Descripcion de los items
      var d_item=({!!json_encode($item_list)!!});
      var d_menu=({!!json_encode($menu_list)!!});
      var l_items = d_item.length;
      var l_menu = d_menu.length;
      var t_items = l_items + l_menu;
        //index_i = indice para los items, index_m = indice para las comida del menu
      var index_i = 0, index_m = 0;
      //Recorremos la cantidad de items totales facturados
      for(i = 1; i <= t_items; i++)
      {
        //Si el indice de items es menos a la cantidad maxima existente
        if(index_i < l_items)
        {
          //para mantener el orden original en el que se agregaron los elementos es necesario comparar
          //El index del item con el index de la actual iteración
          if(d_item[index_i].index == i)
          {
            //Capturamos la fila de la dt de articulos
            Farticulo = Tarti.row('tr[id=artiCant'+d_item[index_i].item_id+']').node();
            articulo = Tarti.row(Farticulo).data();// Capturamos los datos de la fila
            //Agregamos la fila a la tabla de articulos reservados
            AddToList({articulo:articulo,cantAdd:d_item[index_i].quantity,id:d_item[index_i].item_id});
            index_i++;
          }
        }
        //Si el indice de menu es menos a la cantidad maxima existente
        if(index_m < l_menu)
        {
          //para mantener el orden original en el que se agregaron los elementos es necesario comparar
          //El index del menu con el index de la actual iteración
          if(d_menu[index_m].index == i)
          {
            //Capturamos la fila de la dt de menu
            Farticulo=table.row('tr[id=menucant'+d_menu[index_m].menu_id+']').node();
            articulo = table.row(Farticulo).data();
            AddToList({articulo:articulo,cantAdd:d_menu[index_m].quantity,indexname:1,tipo:"Menu",id:d_menu[index_m].menu_id});
            index_m++;
          }
        }
      }
      @endcan
      $("#loading").css('display','none');
      $("#tablas").show();
      //Sobre ponemos el layout de carga al modal
      $("#loading").css('z-index',1060);
    });

    @can('menus.create')
    //Guardar el nuevo plato
    function save(decision)
    {
      var ruta = "/reservacion/reservamenu";
      var token = $("#tokenmenu").val();
      var formData = new FormData($('#datamenu')[0]);
      console.log(formData);
      message(["Guardando... por favor espere"],{objeto:$("#mensajemenuc"),manual:true})
      
      $("#loading").css('display','block');
      $("#btnGuardarComida").attr("disabled",true);
      $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:formData,
        contentType: false,
        processData: false,
        success: function(result){
          if(decision=="guardar")
          {
            RecargarMenu(result);
            limpiarMenu();
            CambioPagMenu();
          }
          $("#btnGuardarComida").attr("disabled",false);
          $("#loading").css('display','none');
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
          message(jqXHR,{objeto:$("#mensajemenuc"),tipo:"danger"});
          $("#btnGuardarComida").attr("disabled",false);
          $("#loading").css('display','none');
      });
    }
    @endcan
    var senddata={};
    function reservaupdate()
    {
      ruta = "/reservacion/"+{!!$reservacion->id!!};
      token = $("#token").val();
      
      $(".btnSent").attr("disabled",true);
      $("#loading").css('display','block');
      
      senddata = {
                  "Cedula_Cliente":$("#Cedu").val(), 
                  "Nombre_Contacto":$("#Nom").val(),
                  "Direccion_Local":$("#Dir").val(),
                  "Fecha_Inicio": $("#datepicker").val(),
                  "Fecha_Fin": $("#datepicker2").val(),
                  "accion": "guardar",
                  "lista": A,
                };
      $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'PUT',
        dataType: 'json',
        data:senddata,
        success: function(result){
          CambioPag();
          message(["Actualizado correctamente!"],{objeto:$("#mensajereserva"),manual:true})
          //location.href="#mensajereserva";
          $(".btnSent").attr("disabled",false);
          $("#loading").css('display','none');
          $('body').animate({scrollTop:0}, 'fast');
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
          message(jqXHR,{objeto:$("#mensajereserva"),tipo:"danger"});
          CambioPag();
          $(".btnSent").attr("disabled",false);
          $("#loading").css('display','none');
          $('body').animate({scrollTop:0}, 'fast');
      });
    }

  </script>


@stop