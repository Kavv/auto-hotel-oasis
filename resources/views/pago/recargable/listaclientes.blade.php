
  @foreach($customers as $customer)
    <tr @if($customer->vetoed_id != null) class = "text-danger" @endif >
      <?php 
        $service_text = $customer->service . "(" . $customer->amount . ")";
      ?>
      <td class="botones" >
        <button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit" value="{{$customer->id}}" onclick="details(this)">Pagar</button>
      </td>
      <td >{{$customer->next_payment}}</td>  
      <td >{{$customer->lesson}}</td>  
      <td >{{$service_text}}</td>  
      <td >{{$customer->state}}</td>  
      <td>{{$customer->dni}}</td> 
      <td >{{$customer->name}}</td>    
      <td >{{$customer->last_name}}</td>  
    </tr>
  @endforeach