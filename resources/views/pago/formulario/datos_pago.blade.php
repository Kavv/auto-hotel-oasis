

        <div class="row mt-3">
            <div class="col-12">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1">Lección</span>
                </div>
  
                <select class="form-control border border-warning selectpicker" id="leccion" name="leccion"
                required data-live-search="true" title="Seleccione una lección">
                  <option value="">Selecciona una lección</option>
                  @foreach($lessons as $lesson)
                  <option value='{!!$lesson->id!!}'>{!!$lesson->code!!}</option>
                  @endforeach
                </select>
  
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon2">Servicio</span>
                </div>
                
                <select class="form-control border border-warning" id="servicios" name="servicio">
                  <option value="">Selecciona un servicio</option>
                </select>
  
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon3">Monto</span>
                </div>
                <input type="text" class="form-control border-warning" name="monto" id="monto" placeholder="Monto a pagar">
              </div>
            </div>
          </div>
          <div class="row ">
            <div class="col-md-3">
              <div class="form-group">
                <label for=""># Recibo</label>
                  <input id="recibo" class="form-control" name="recibo"  placeholder='Número de recibo'/> 
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="">Fecha pago</label>
                  <input id="fecha_pago" class="border border-warning" name="fecha_de_pago" value="<?php echo date('Y-m-d'); ?>" placeholder='año-mes-dia' readonly/> 
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="">Servicio Inicio</label>
                  <input id="inicio" class="border border-warning" name="servicio_inicio"  placeholder='año-mes-dia' readonly/> 
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="">Servicio Fin</label>
                  <input id="corte" class="border border-warning" name="servicio_corte"  placeholder='año-mes-dia' readonly/> 
              </div>
            </div>
        </div>