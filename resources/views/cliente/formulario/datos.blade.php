<div class="row ">
    <div class="col-md-6">
        {!! Form::label('Cedula', 'Cedula:') !!}
        {!! Form::text('Cedula_Cliente', null, ['class' => 'form-control border border-warning CC', 'placeholder' => 'xxx-xxxxxx-xxxxx', 'autocomplete' => 'off', 'onkeypress' => 'return CharCedula(event,this,"#nacional");', 'onkeyup' => 'formatonica(this,"#nacional")']) !!}
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('Nombre:') !!}
            {!! Form::text('Nombre', null, ['class' => 'form-control border border-warning Nombre', 'placeholder' => 'Nombre del Cliente']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('Telefono:') !!}
            {!! Form::text('telefono', null, ['class' => 'form-control border border-warning telefono', 'placeholder' => 'Número de teléfono']) !!}
        </div>
    </div>
</div>
