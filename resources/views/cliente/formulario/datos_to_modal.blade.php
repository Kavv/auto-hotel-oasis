        

          <div class="row ">
            <div class="col-md-6">
              <div class="form-group">
                  {!!Form::label('Nombre:')!!}
                  {!!Form::text('Nombre',null,['id'=>'Nombre','class'=>'form-control border border-warning','placeholder'=>'Nombre del Cliente'])!!}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                  {!!Form::label('Apellido:')!!}
                  {!!Form::text('Apellido',null,['id'=>'Apellido','class'=>'form-control border border-warning','placeholder'=>'Apellido del Cliente'])!!}
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                {!!Form::label('Edad:')!!}
                <div  class="input-group ">
                  {!!Form::text('Edad',null,['id'=>'Edad','class'=>'form-control','placeholder'=>'Edad del Cliente'])!!}
                  <div class="input-group-append">
                    <button class="btn btn-primary fa fa-eye" onclick="mostraredad();"  type="button"></button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                  {!!Form::label('Sexo:')!!}
                  {!!Form::select('Sexo', ['Masculino' => 'Masculino', 'Femenino' => 'Femenino'], null, ['placeholder' => 'Sexo','class'=>'form-control', 'id'=>'Sexo'])!!}
              </div>
            </div>
          </div> 
          <div id="telefonos">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  {!!Form::label('Numero de teléfono:')!!}
                  <div  class="input-group ">
                    {!!Form::tel('telefono[]',null,['id'=>'telefono','class'=>'form-control','placeholder'=>'Ingrese el numero'])!!}
                    <div class="input-group-append">
                      <button class="btn btn-primary fa fa-plus" onclick="addPhone();"  type="button"></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row ">
            <div class="col-md-12">
              <div class="form-group">
                  {!!Form::label('Direccion:')!!}
                  {!!Form::text('Direccion',null,['id'=>'Direccion','class'=>'form-control','placeholder'=>'Direccion del Cliente'])!!}
              </div>
            </div>
          </div>

          