
  @foreach($data as $d)
    <tr @if($d->vetoed_id != null) class = "text-danger" @endif>
      <td>{{$d->dni}}</td> 
      <td >{{$d->name}}</td>    
      <td ><a href="tel:{{$d->phone}}">{{$d->phone}}</a></td>
      <td class="botones" >
        <button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-pencil" value="{{$d->id}}" onclick="editar(this, {})"></button>
        @can('cliente.destroy') 
        <button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="{{$d->id}}"></button>
        @endcan
      </td>
    </tr>
  @endforeach