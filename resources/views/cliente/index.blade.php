@extends('layouts.dashboard')
@section('css')
  {!!Html::style("css/gijgo2/css/gijgo.css")!!}
    <style>
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')

<!--Arreglar toda la mierda que hiciste-->
    <div class="d-block bg bg-dark" style="padding-top: 10px; color:white;visibility:hidden;" id="main">

        <div class="row my-4 pl-4" id="div_agregar">
            <button class="btn btn-success" data-toggle="modal" data-target="#add">Agregrar un registro <i class="fa fa-plus"></i></button>
        </div>
        <div id="mensaje"></div>
        <!--class="table table-striped table-bordered"-->
        <table class="table table-bordered table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;" >
            <thead>
                <tr>
                    <th class="text-center" style="width:20%;">Cedula</th>
                    <th class="text-center" >Nombre</th>
                    <th class="text-center" >Teléfono</th>
                    <th class="text-center" data-orderable="false" style="width:15%;"></th>
                </tr>
            </thead >
            <tbody class="text-center" id="lista" >
                @include('cliente.recargable.listaclientes')
            </tbody>
        </table>
    </div>
    

    @can('cliente.create')
        <div class="modal fade" id="add" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="tile_madd">Agregar un registro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div id="msg_add"></div>
                    <form action="" id="form_data" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <?php $index = 0;?>
                        @include('cliente.formulario.datos')
                    </form>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="btn_guardar" onclick="save({msg: '#msg_add'});">Guardar <i class="fa fa-save"></i> </button>
                </div>
            </div>
            </div>
        </div>
    @endcan

    @can('cliente.edit')
        <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Editar Registro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="message_edit"></div>
                        <form id="form_edit" enctype="multipart/form-data">
                            @method('PUT')
                            <input type="hidden" id="id" name="id">
                            <?php $index = 1;?>
                            @include('cliente.formulario.datos')
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="btn_actualizar"
                                onclick="actualizar({msg: '#message_edit'})">Actualizar</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcan
    
    @can('cliente.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section("script")
    {!!Html::script("js/jskevin/cedulanica.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
    {!!Html::script("js/jskevin/kavvdt.js")!!} 
    {!!Html::script("js/gijgo2/js/gijgo.js")!!}


    {{-- Si tiene los permisos de crear un registro incluira la funcion js correspondiente --}}
    @can('cliente.create')
        {!!Html::script("js/ajax_crud/create.js")!!} 
    @endcan
    
    {{-- Si tiene los permisos de actualizar un registro incluira la funcion js correspondiente --}}
    @can('cliente.update')
        {!!Html::script("js/ajax_crud/update.js")!!} 
    @endcan

    {{-- Si tiene los permisos de eliminar un registro incluira la funcion js correspondiente --}}
    @can('cliente.destroy')
        {!!Html::script("js/ajax_crud/delete.js")!!} 
    @endcan

    <script>

        var table;
        var fila;
        const base = "cliente";
    

        $(document).ready( function () {
            table=createdt($('#Datos'),{buscar:'{!!session("valor")!!}',col:1,cant:[10,20,-1],cantT:[10,20,"todo"]})
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index',1060);
        });

        // Resultado exitoso del create
        function success(res)
        {
            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code === 1)
            {
                // Mostramos un msj de exito
                message(["Se agrego el registro correctamente"],{manual:true});
                // Llamamos a la funcion que agrega una fila a la tabla
                addRow(res.data);
                // Ocultamos el modal de agregación
                $("#add").modal('hide');
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{objeto:$("#msj_add"), manual:true,tipo:"danger"});
            }

            $("#btn_guardar").attr("disabled",false);
            $("#loading").css('display','none');
        }

        function addRow(data)
        {
            // Almacenara los html de las acciones
            var botones = "";
            // Si tiene permisos de edicion se adjunta el html del btn para editar
            @can('cliente.update') 
                botones += '<button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-pencil" value="'+data.id+'" onclick="editar(this, {})"></button>';
            @endcan
            // Si tiene permisos de eliminación se adjunta el html del btn para eliminar
            @can('cliente.destroy') 
                botones += '<button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="'+data.id+'"></button>';
            @endcan

            // Hacemos referencia a la variable que contienen la DataTable
            // Accedemos a su propiedad row y al metodo add
            // Especificando ahí los datos de cada columna en la tabla
            var aux_tel = $(".telefono").eq(0).val();
            table.row.add( [
                $(".CC").eq(0).val(),
                $(".Nombre").eq(0).val(),
                "<a href='tel:" + aux_tel + "'>"+aux_tel+"</a>",
                botones,
            ] ).draw( false );
        }
        function fill(res){
            data = res.data;
            
            $(".CC").eq(1).val(data.dni);
            $(".Nombre").eq(1).val(data.name);
            $(".telefono").eq(1).val(data.phone);

            $("#actualizar").attr('disabled',false);
            $("#loading").css('display','none');
        }
        function update_success(res)
        {
            if(res.code==1)
            {
                message(['Cliente editado correctamente'],{manual:true});
                var aux_tel = $(".telefono").eq(1).val();
                var aux_html = "<a href='tel:" + aux_tel + "'>"+aux_tel+"</a>";
                table.cell(fila.children('td')[0]).data( $(".CC").eq(1).val());
                table.cell(fila.children('td')[1]).data( $(".Nombre").eq(1).val());
                table.cell(fila.children('td')[2]).data( aux_html );
                /* fila.children('td.botones').children('input.delete').removeAttr( "onclick");//Eliminamos la funcion onclick del boton eliminar
                fila.children('td.botones').children('input.delete').attr( "onclick",'erase(\''+$("#cedula").val()+'\');');//Agregamos la funcion onclick con el nuevo parametro */
                table=$("#Datos").DataTable().draw();
                $("#Edit").modal('toggle');
                $('body').animate({scrollTop:0}, 'fast');
            }
            else
            {
                message(res.message,{objeto:$("#message_edit"), manual:true,tipo:"danger"});
            }
            $("#btn_actualizar").attr('disabled',false);
            $("#loading").css('display','none');
        }

        campocedula = $("#cedula");
        function buscar()
        {
            cedula=campocedula.val();
            if(cedula.length>0)
            {
                if(table.row(fila).data()[0] != $("#cedula").val())
                {
                    var ruta="/cliente/show/"+cedula;
                    $("#loading").css('display','block');
                    $.get(ruta,function(res){
                        $("#loading").css('display','none');
                        if(res.data)
                            message(['La cedula ya esta en uso por otro cliente!'],{objeto:$("#msgmodal"),tipo:"danger",manual:true});
                        else
                            message(['La cedula es valida, es posible editar!'],{objeto:$("#msgmodal"),tipo:"success",manual:true})
                    });
                }
                else
                    message(['La cedula es valida, es posible editar!'],{objeto:$("#msgmodal"),tipo:"success",manual:true})
            }
        }
        function limpiar()
        {
            $("#cedula").val("");
            $("#nombre").val("");
            $("#apellido").val("");
            $("#edad").val("");
            $("#sexo").val("");
            $("#direccion").val("");
            $("#ce").val("");
            
            $("#telefono").val("");
            $("#password").val("");
            $(".telefono-clone").remove();
        }
        $("#nacional").on("change",function()
        {
            if($("#nacional").prop('checked')==true)//Si es idenficacion Nica
            {
                campocedula.val("");
            }
        });
        function mostraredad()
        {
            if(edad!=0 && !isNaN(edad))
                $("#edad").val(edad);
        }
        
    </script>
@stop