@extends('layouts.dashboard')
@section('css')
  <style>
    .dataTables_scroll{
      height: 30em;
      overflow-y: auto;
    }
  </style>
@stop
@section('content')
  <div class="card">
    <h4 class="card-header">Agregar Role</h4>
    <div class="card-body">
      <div id="mensaje"></div>
      <form action="{{route('roles.store')}}" method="POST" id="data">

        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
      
        <div class="row">
          <div class="col-md-6">
              {!!Form::label('Nombre del rol:')!!}
              {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control border border-warning','placeholder'=>'Nombre del Role'])!!}
          </div>
          <div class="col-md-6 ">
              {!!Form::label('Descripcion del rol:')!!}
              {!!Form::text('descripcion',null,['id'=>'descripcion','class'=>'form-control','placeholder'=>'Descripcion del Role'])!!}
          </div>
        </div>
        
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8 mt-4" style="height:30em;width:auto; overflow:hidden">
            <table class="table table-hover table-dark text-center " cellspacing="0" id="Datos" style="width:100%; overflow:scroll" >
              <thead>
                <th>Selecciona los permisos</th>
              </thead>
              <tbody id="lista">
                @foreach($permisos as $permiso)
                  <tr>
                    <td>
                      <label>
                        {{ Form::checkbox('permiso[]', $permiso->id, null)}}
                        {{ $permiso->name }}
                        <em> ({{ $permiso->description ?: 'sin descripcion' }}) </em>
                      </label>
                    </td>
                  </tr>
                @endforeach 
              </tbody>
            </table>
          </div>
        </div>
        <div class="text-center">
          <button class="btn btn-primary btnSent" type="button" onclick="save('save');" value="Guardar">Guardar</button>
          <button class="btn btn-primary btnSent" type="button" onclick="save('savev');" value="Guardar y ver">Guardar y ver</button>
        </div>
      </form>
    </div>
  </div>     
  @include('layouts.cargando.cargando')
@stop
@section('script')
  {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
  {!!Html::script("js/jskevin/kavvdt.js")!!} 
  <script>
    $(document).ready(function() {
      createdt($("#Datos"),{dom:bootstrapDesingSlim})
    });
    function save(condition)
    {
      var ruta="/roles";
      var token=$("#token").val();
      $(".btnSent").attr("disabled",true);
      $("#loading").css('display','block');

      $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:$("#data").serialize(),
        success: function(result){
          if(result === 1)
          {
            if(condition=="save")
            {
              message(["Role agregado exitosamente"],{manual:true})
              $("#nombre").val("");
              $("#descripcion").val("");
              $('input:checked').prop('checked',false);

              $(".btnSent").attr("disabled",false);
              $("#loading").css('display','none');
            }
            if(condition=="savev")
            {
              location.href="ver/"+$("#nombre").val();
            }
          } else {
            message(result,{manual:true, tipo:'danger'});
            $(".btnSent").attr("disabled",false);
            $("#loading").css('display','none');
          }
          $('body').animate({scrollTop:0}, 'fast');
           
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
        message(jqXHR,{tipo:"danger"});
        $(".btnSent").attr("disabled",false);
        $("#loading").css('display','none');
        $('body').animate({scrollTop:0}, 'fast');
      });
    }
  </script>
@stop
