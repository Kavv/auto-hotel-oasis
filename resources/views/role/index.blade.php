@extends('layouts.dashboard')
@section('css')
    <style>
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')
    <div class="d-block bg-dark" style="color:white; padding-top: 10px;visibility:hidden;" id="main">
        @include('alert.mensaje')
        <div id="mensaje"></div>
        <table class="table table-hover table-dark text-center " cellspacing="0" id="Datos" style="width:100%;" >
          <thead>
            <th>Nombre</th>
            <th>Descripcion</th>
            <th data-orderable="false"></th>
          </thead>
          <tbody id="lista">
            @foreach($roles as $role)
              <tr>
                <td>{{$role->name}}</td>
                <td>{{$role->description}}</td>
                <td class="botones">
                    @can('role.edit')
                    <button data-toggle="modal" data-target="#Edit" class="btn btn-primary edit" value="{{$role->id}}">Editar</button>
                    @endcan
                    @can('role.destroy')
                    <button data-toggle="modal" data-target="#deleteModal" class="btn btn-danger fa fa-trash" data-value="{{$role->id}}"></button>
                    @endcan
                </td>
              </tr>
            @endforeach 
          </tbody>
        </table>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <form id="data">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}" id="id">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                      <!--UNA VEZ TERMINADA LA CLASE DE IS REGRESAR LOS FORMULARIOS A SU TAMAñO GRANDE E IMPORTARLO AQUI-->
                      <div class="row">
                        <div class="col-md-6">
                          {!!Form::label('Nombre del rol:')!!}
                          {!!Form::text('nombre',null,['id'=>'nombre','class'=>'form-control border border-warning','placeholder'=>'Nombre del Role'])!!}
                        </div>
                        <div class="col-md-6">
                            {!!Form::label('Descripcion del rol:')!!}
                            {!!Form::text('descripcion',null,['id'=>'descripcion','class'=>'form-control','placeholder'=>'Descripcion del Role'])!!}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 " style="height:20em;width:auto; overflow:scroll">
                          <table class="table table-hover table-dark text-center " cellspacing="0" id="permisos" style="width:100%;" >
                            <thead>
                              <th>Nombre</th>
                            </thead>
                            <tbody id="lista">
                              @foreach($permisos as $permiso)
                                <tr>
                                  <td>
                                    <label>
                                      {{ Form::checkbox('permiso[]', $permiso->id, null)}}
                                      {{ $permiso->name }}
                                      <em> ({{ $permiso->description ?: 'sin descripcion' }}) </em>
                                    </label>
                                  </td>
                                </tr>
                              @endforeach 
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        @can('role.edit')
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar();" id="actualizar" >Actualizar</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @can('personal.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!}  
    {!!Html::script("js/jskevin/kavvdt.js")!!}   
    <script>
    
        var table;
        var fila;
        $(document).ready( function () {
            table=createdt($('#Datos'),{buscar:'{{$valor}}' ,cant:[10,20,-1],cantT:[10,20,"Todo"]});
            createdt($('#permisos'),{dom:''});
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index',1060);
        });
        $('.edit').on( 'click', function () {
            $('input:checked').prop('checked',false);
            fila=$(this).parents('tr');//Dejamos almacenada temporalmente la fila en la que clickeamos editar
            row=table.row(fila).data();//Tomamos el contenido de la fila 
            $("#nombre").val(row[0]);
            $("#descripcion").val(row[1]);
            $("#id").val($(this).val());

            ruta="/roles/show/"+$(this).val();
            $("#loading").css('display','block');
            $.get(ruta,function(res){
                $("#loading").css('display','none');
                res.forEach(function(permiso) {
                  $('input[value="'+ permiso.id +'"]').prop('checked',true);
                });
            });
        });
        @can('role.edit')
        //Actualizacion de fila donde no es posible actualizar id
        function actualizar()
        {
            route="/roles/"+$("#id").val();
            var token=$("#token").val();
            $("#actualizar").attr('disabled',true);
            $("#loading").css('display','block');

            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data:$("#data").serialize(),
                success: function(res){
                    if(res === 1)
                    {
                        message(['Role editado correctamente'],{manual:true});
                        table.cell(fila.children('td')[0]).data( $("#nombre").val());
                        table.cell(fila.children('td')[1]).data( $("#descripcion").val());
                        table=$("#Datos").DataTable().draw();
                        
                        $("#Edit").modal('toggle');
                        $('body').animate({scrollTop:0}, 'fast');
                    } else {
                        message(res, {objeto:$("#modalMessage"), manual:true, tipo:'danger'});
                    }
                    $("#actualizar").attr('disabled',false);
                    $("#loading").css('display','none');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                $("#actualizar").attr('disabled',false);
                $("#loading").css('display','none');
                message(jqXHR,{objeto:$("#modalMessage"), tipo:"danger"});
            });
        }
        @endcan
        @can('role.destroy')
        var row;
        $('#deleteModal').on('show.bs.modal', function (event) {
            {{-- Mantenemos el id del elemento seleccionado --}}        
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            $("#id").val(value);


            {{-- Mantenemos la fila del elemento seleccionado --}}
            row = button.parents('tr');
        });

        $('#delete').on( 'click', function () {
            var route="/roles/"+$("#id").val();
            var token=$("#token").val();
            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');
            
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                success: function(res){
                    message(['Se elimino el role correctamente'],{manual:true});
                    table.row(row).remove().draw( false );  
                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                    $('body').animate({scrollTop:0}, 'fast');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#deleteModalMessage"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });
        @endcan
    </script>
@stop