<table>
    @if($i == 0)
        <thead>
        <tr>
            <th style="text-align: center"> Se agregaron 0 reservaciones </th>
            <th>  </th>
            <th>  </th>
            <th style="text-align: center"><b> Fecha de Emision: </b></th>
        </tr>
        <tr>
            <th style="text-align: center"> Desde: {{ $start }}</th>
            <th>  </th>
            <th>  </th>
            <th style="text-align: center"> {{ date("d - m - y") }} </th>
        </tr>
        <tr>
            <th style="text-align: center">  Hasta: {{ $end }}</th>
        </tr>
        </thead>
    @endif
    @if( $opc == "ingresos")
        @if($i > 0)
            <table style="width:100%; ">
                <thead>
                <tr style="text-align: center">
                    <th style="text-align: center"><b> Nombre </b></th>
                    <th style="text-align: center"><b> Cedula </b></th>
                    <th style="text-align: center"><b> Fecha de Emisión </b></th>
                    <th style="text-align: center"><b> Total </b></th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="3" style="text-align: center"> Ingresos totales durante {{$start}} / {{$end}}  </td>
                        <td style="text-align: center"></td>
                    </tr>
                </tbody>
            </table>
        @endif
    @endif
</table>
