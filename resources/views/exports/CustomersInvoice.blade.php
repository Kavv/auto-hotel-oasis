<table>
    <thead>
    <tr>
        @if($opt == 'clientes_agregados')
            <th style="text-align: center"> Se agregaron {{ $count }} Clientes </th>
        @elseif($opt == 'clientes_eliminados')
            <th style="text-align: center"> Se eliminaron {{ $count }} de Clientes </th>
        @elseif($opt == 'personal_agregados')
            <th style="text-align: center"> Se agregaron {{ $count }} registros al Personal </th>
        @elseif($opt == 'personal_eliminados')
            <th style="text-align: center"> Se eliminaron {{ $count }} registros al Personal </th>
        @endif
        <th>  </th>
        <th>  </th>
        <th style="text-align: center"><b> Fecha de Emision: </b></th>
    </tr>
    <tr>
        <th style="text-align: center"> Desde: {{ $ini }}</th>
        <th>  </th>
        <th>  </th>
        <th style="text-align: center"> {{ date("d - m - y") }} </th>
    </tr>
    <tr>
        <th style="text-align: center">  Hasta: {{ $fin }}</th>
    </tr>
    </thead>
</table>

<table>
    <thead>
    <tr>
        @if(($opt == "clientes_agregados") || ($opt == "clientes_eliminados"))
            <th style="text-align: center"><b>Cedula </b></th>
            <th style="text-align: center"><b>Nombre </b></th>
            <th style="text-align: center"><b>Apellido </b></th>
            <th style="text-align: center"><b>Edad </b></th>
            <th style="text-align: center"><b>Sexo </b></th>
            <th style="text-align: center"><b>Dirección </b></th>
        @else
            <th style="text-align: center"><b>Cedula </b></th>
            <th style="text-align: center"><b>Nombre </b></th>
            <th style="text-align: center"><b>Apellido </b></th>
            <th style="text-align: center"><b>Edad </b></th>
            <th style="text-align: center"><b>Dirección </b></th>
        @endif

    </tr>
    </thead>

    <tbody>
        @if(($opt == "clientes_agregados") || ($opt == "clientes_eliminados"))
            @for($i=0; $i < $count; $i++)
                <tr>
                    <td style="text-align: center"> {{ $customer[$i]->dni }} </td>
                    <td style="text-align: center"> {{ $customer[$i]->name }} </td>
                    <td style="text-align: center"> {{ $customer[$i]->last_name }} </td>
                    <td style="text-align: center"> {{ $customer[$i]->age }} </td>
                    <td style="text-align: center"> {{ $customer[$i]->gender }} </td>
                    <td style="text-align: center"> {{ $customer[$i]->address }} </td>
                </tr>
            @endfor
        @else
            @for($i=0; $i < $count; $i++)
                <tr>
                    <td style="text-align: center"> {{ $customer[$i]->dni }} </td>
                    <td style="text-align: center"> {{ $customer[$i]->name }} </td>
                    <td style="text-align: center"> {{ $customer[$i]->last_name }} </td>
                    <td style="text-align: center"> {{ $customer[$i]->age }} </td>
{{--                    <td> {{ $customer[$i]->gender }} </td>--}}
                    <td style="text-align: center"> {{ $customer[$i]->address }} </td>
                </tr>
            @endfor
        @endif
    </tbody>
</table>