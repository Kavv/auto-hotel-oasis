        

          <div class="row ">
            <div class="col-md-2"></div>
            <div class="col-md-4">
              <div class="form-group">
                <label><a>Nombre y Apellido:</a></label>
                {!!Form::text('name',null,['id'=>'fullName','class'=>'form-control border border-warning','placeholder'=>'Nombres Apellidos'])!!}
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                {!!Form::label('Descripcion:')!!}
                {!!Form::text('description',null,['id'=>'description','class'=>'form-control','placeholder'=>'Usuario creado para...'])!!}
              </div>
            </div>
          </div>

           <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-4">
              <div class="form-group">
                {!!Form::label('Contraseña:')!!}
                <input type="text" class="form-control border border-warning pass"  name="password" id="password" placeholder="*******" onkeyup="coinciden(); " >
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                {!!Form::label('Confirmar Contraseña:')!!}
                <input type="text" class="form-control border border-warning pass"  name="confcontraseña" id="confPassword" placeholder="*******" onkeyup="coinciden(); ">
                <span id="msjpass" style="color:red;"></span>
              </div>
            </div>
          </div> 

          
          