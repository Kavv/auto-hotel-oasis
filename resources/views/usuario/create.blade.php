@extends('layouts.dashboard')
@section('css')
<style>
  .pass {
    -webkit-text-security: disc;
  }
</style>

@stop

@section('content')
  <div class="card" >
    <h4 class="card-header">Agregar Usuario</h4>
    <div class="card-body">
    <div id="mensaje"></div>
      <form id="data" autocomplete="off">

        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <div class="row ">
          <div class="col-md-12 d-flex justify-content-center">
            <div class="form-group col-md-6 text-center">
                <label><a>Nombre de Usuario:</a></label>
                {!!Form::text('userName',null,['id'=>'userName','class'=>'form-control border border-warning','placeholder'=>'Usuario123','autocomplete'=>'off'])!!}
            </div>
          </div>
        </div>
        @include('usuario.formulario.datos')

        @can('role.index')
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8 " style="height:20em;width:auto; overflow:scroll">
            <table class="table table-hover table-dark text-center " cellspacing="0" id="roles" style="width:100%;" >
              <thead>
                <th>Roles a desempeñar</th>
              </thead>
              <tbody id="lista">
                @foreach($roles as $role)
                  <tr>
                    <td>
                      <label>
                        {{ Form::checkbox('role[]', $role->id, null)}}
                        {{ $role->name }}
                        <em> ({{ $role->description ?: 'sin descripcion' }}) </em>
                      </label>
                    </td>
                  </tr>
                @endforeach 
              </tbody>
            </table>
          </div>
        </div>
        @endcan
        <div class="text-center">
          <button class="btn btn-primary btnSent" type="button" onclick="save('save')"  >Guardar</button>
          <button class="btn btn-primary btnSent" type="button" onclick="save('savev')" >Guardar y ver</button>
        </div >
      </form>
    </div>
  </div>

  @include('layouts.cargando.cargando')
@stop
@section('script')
  {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
  {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
  {!!Html::script("js/jskevin/tiposmensajes.js")!!}  
  {!!Html::script("js/jskevin/kavvdt.js")!!}   
  <script>
    $(document).ready(function() {
      createdt($('#roles'),{dom:''});
    });
    var correcto=true;

    function coinciden()
    {
        var contra1=$("#password").val();
        var contra2=$("#confPassword").val();
        /*si estan vacios los campos*/
        if(contra1.length==0)
        {
          $("#msjpass").empty();
          correcto=true;/*enrealidad no esta correcto, es para no crear conflicto*/
          return;
        }
        /*validar que sean iguales*/
        if(contra1!=contra2)
        {
            if(correcto==true)/*condicion con el fin de evitar sobre escribir cada momento en #msjpass*/
            {
              jQuery("#msjpass").append("No coinciden las contraseñas!");
              correcto=false;
              return;
            }
        }
        else
        {
          $("#msjpass").empty();
          /*Validar longitud*/
          if(contra1.length < 6)
          {
            jQuery("#msjpass").append("La longitud de la contraseña debe ser mayor a 5");
            correcto=false;
            return;
          }
          else
          {
            correcto=true;
          }
        }
    }

    function save(condition)
    {
      var ruta="/usuarios";
      var token=$("#token").val();
      $(".btnSent").attr("disabled",true);
      $("#loading").css('display','block');

      $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:$("#data").serialize(),
        success: function(result){
          if(result==1)
          {
            if(condition=="save")
            {
              message(["Usuario agregado exitosamente"],{manual:true})
              limpiar();
              $(".btnSent").attr("disabled",false);
              $("#loading").css('display','none');
              $('body').animate({scrollTop:0}, 'fast');
            }
            if(condition=="savev")
            {
              location.href="usuariov/1/"+$("#userName").val();
            }
          }
          else
          {
            message(['Hay un problema con su contraseña'],{manual:true,tipo:'danger'});
            $(".btnSent").attr("disabled",false);
            $("#loading").css('display','none');
            $('body').animate({scrollTop:0}, 'fast');
          }
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
        message(jqXHR,{tipo:"danger"});
        $(".btnSent").attr("disabled",false);
        $("#loading").css('display','none');
        $('body').animate({scrollTop:0}, 'fast');
      });
    }
    function limpiar()
    {
      $("#userName").val("");
      $("#fullName").val("");
      $("#description").val("");
      $("#password").val("");
      $("#confPassword").val("");
      $('input:checked').prop('checked',false);
    }
    /*
    function cifrar(input)
    {
      longitud = input.value.length;
      input.value = "*".repeat(longitud);
    }*/
  </script>
@stop