            
                @foreach($usuarios as $usuario)
                    @if($usuario->email != "Sistema Alquiler Santana")
                    <tr>
                        <td>{{$usuario->email}}</td> 
                        <td>{{$usuario->name}}</td>    
                        <td>{{$usuario->description}}</td>  
                        <td>
                            @can('role.index')
                            <button data-toggle="modal" data-target="#Edit" class="btn btn-primary edit" value="{{$usuario->id}}">Editar</button>
                            @endcan
                            @can('users.destroy')
                            <button data-toggle="modal" data-target="#deleteModal" class="btn btn-danger fa fa-trash" data-value="{{$usuario->id}}"></button>
                            @endcan
                        </td>
                    </tr>
                    @endif
                @endforeach