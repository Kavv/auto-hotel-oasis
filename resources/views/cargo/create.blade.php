@extends('layouts.dashboard')
@section('content')
<div class="card" >
  <h4 class="card-header" >Agregar Cargo</h4>
  <div class="card-body">
    <div id="mensaje"></div>
    @include('alert.mensaje')
    <form id="data" autocomplete = off >
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <div class="row">
            <div class="col-md-12 d-flex justify-content-center">
                <div class="form-group col-md-6 text-center">
                    {!!Form::label('Nombre del Cargo:')!!}
                    {!!Form::text('Nombre_Cargo',null,['id'=>'Nom', 'class'=>'form-control border border-warning','placeholder'=>'ej: Conductor'])!!}
                    <input type="text" style="display:none;">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 d-flex justify-content-center">
                <div class="form-group col-md-6 text-center">
                  <input class="btnSent btn btn-primary" type="button" value="Guardar" onclick="guardarcargo('guardar');">
                  <input class="btnSent btn btn-primary" type="button" value="Guardar y ver" onclick="guardarcargo('guardarv');">
                </div>
            </div>
        </div>
    </form>
  </div>
</div>   
@can('personal.index')
<div class="card" id="cardA">
  <h4 class="card-header">Asignar cargo a personal</h4>
  <div id="msjtabla"></div>
  <div class="card-body text-center bg-dark">

    <a class="btn btn-primary " href="#tablaB" onclick="add()">Agregar</a>
    <div class="row ">
      <div class="form-group">
      </div>
    </div>

    <!--TABLA A-->
    <form id="cargo_personal">
      <div class="row bg-dark" style="color:White; visibility:hidden;" id="tablaA"> 
        <div class="col-md-4 " id="lc" style="height:30em; overflow:scroll; padding-left: 0px;">
          @include('cargo.listacargo.listacargo')
        </div>
        <div class="col-md-8" style="height:30em; overflow:scroll;padding-left: 0px; ">
          <div class="tab-content" id="nav-tabContent">
              <div  data-spy="scroll" class="tab-pane fade show active" role="tabpanel" >
                <table class="table table-hover table-dark" id="tablapersonal" cellspacing="0" style="width:100%;" >
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Apellido</th>
                      <th>Cedula</th>
                      <th class="text-center" data-orderable="false" ></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($personal as $persona)
                        <tr @if($persona->vetoed_id != null) class="text-danger" @endif>
                          <td >{!!$persona->name!!}</td>
                          <td >{!!$persona->last_name!!}</td>
                          <td >{!!$persona->dni!!}</td>
                          <td class="btn-primary text-center">
                            <input type="checkbox" name = "personal[]" value="{{$persona->id}}" OnClick='addpersonal(this,"{!!$persona->name!!}","{!!$persona->last_name!!}","{!!$persona->dni!!}");'>
                          </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>  

<div class="card" id="cardB" style="display:none;">
  <h4 class="card-header text-center">Visualizacion previa del personal con sus nuevos cargos</h4>
  <div class="card-body text-center">
    <!--TABLA B-->
    <div  id="tablaB"> 
      <div class="col-md-12" style=" overflow:scroll;padding-left: 0px;padding-right: 0px; ">
        <div class="tab-content" id="nav-tabContent">
            <div  data-spy="scroll" class="tab-pane fade show active" role="tabpanel" >
              <table class="table table-hover table-dark " >
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Cedula</th>
                    <th>Cargo</th>
                  </tr>
                </thead>
                <tbody id="tb">
                </tbody>
              </table>
            </div>
        </div>
      </div>
      <div class="text-center">
        <button class="btnSent btn btn-primary" id="guardar" onclick="guardar();" >Guardar</button>
        <button class="btnSent btn btn-primary" onclick="retornar();" >Regresar</button>
      </div>
    </div>
  </div>
</div>
@endcan
@include('layouts.cargando.cargando')

@stop
@section("script")
  {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
  {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
  {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
  {!!Html::script("js/jskevin/kavvdt.js")!!} 
  <script>

    var tablaCargos,tablaPersonal;
    $(document).ready( function () {
      @can('personal.index')
      tablaCargos = createdt($("#tablacargos"),{dom:"f"});
      tablaPersonal = createdt($("#tablapersonal"),{dom:"f"});
      @endcan
      $("#tablaA").css("visibility", "visible");
    });
    function guardarcargo(decision)
    {
      var ruta = "/cargo";
      var token = $("#token").val();
      $(".btnSent").attr("disabled",true);
      $("#loading").css('display','block');
      $('body').animate({scrollTop:0}, 'fast');
      $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:$("#data").serialize(),
        success: function(result){
          if(result==1)
          {
            if(decision=="guardar")
            {
              message(["Se agrego el cargo correctamente"],{manual:true});
              recargalista();
              $(".btnSent").attr("disabled",false);
              $("#loading").css('display','none');
            }
            if(decision=="guardarv")
            {
              location.href="/cargo/cargov/1/"+$("#Nom").val();
            }
          }
          else
          {
            message(["El nombre del cargo ya existe"],{manual:true,tipo:"danger"});
            $(".btnSent").attr("disabled",false);
            $("#loading").css('display','none');
          }
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
        message(jqXHR,{tipo:"danger"});
        $(".btnSent").attr("disabled",false);
        $("#loading").css('display','none');
      });
    }
    @can('personal.index')
    //PARA MAYOR EFICIENCIA HACER UN OBJETO PRINCIPAL CON UNA COLECCION DE OBJETO(similar a un tree) para poder hacer una agregacion grande
    //ejemplo: agregar persona1 con cargo x,y. persona2 con cargo x,y pero persona 3 con cargo a,b al mismo tiempo.
    var listapersonal=new Object();
    var listacargo=new Object();
    //agregamos o eliminamos en la lista personal
    function addpersonal(elemento,nombre,apellido,cedula)
    {
      if(elemento.checked==true)
        listapersonal[cedula]=[nombre,apellido,cedula];
      else
        delete listapersonal[cedula];
    }
    //agregamos o eliminamos en la lista cargo
    function addcargo(elemento,cargo,key)
    {
      if(elemento.checked==true)
        listacargo[key]=[cargo,key];
      else
        delete listacargo[key];
    }
    //MEJORAR ESTA FUNCION "Add"
    function add()
    {
      if(!jQuery.isEmptyObject(listapersonal) && !jQuery.isEmptyObject(listacargo))
      {
        $("#cardA").hide();//Escondemos el primer grupo de tablas
        //Quitamos el filtro para evitar conflicto en el backend
        tablaCargos.search("").draw(); 
        tablaPersonal.search("").draw();
        $("#tb").empty();//Limpiamos la tabla B
        for(var obj in listapersonal)//Asignamos el contenido
        {
          $("#tb").append(function(){
            var cadena="<tr><td>"+listapersonal[obj][0]+"</td><td>"+listapersonal[obj][1]+"</td><td>"+listapersonal[obj][2]+"</td><td>";

            for(var objc in listacargo)
            {
              cadena+='"'+listacargo[objc][0]+'"  ';
            }
            cadena+="</td></tr>";
            return cadena;
          });
        }
        $("#cardB").show();//Mostramos la carta B que contiene la tabla
      }
    }
    function guardar()
    {
      /*HACER FUNCIONAR ESTO*/
      var ruta = "/cargo/cargosave";
      var token = $("#token").val();
      $(".btnSent").attr("disabled",true);
      $("#loading").css('display','block');
      
      $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:$("#cargo_personal").serialize(),
        success: function(result)
        {
          message(["Cargos asignados correctamente"],{manual:true,objeto:$("#msjtabla")})
          retornar();
          $(".btnSent").attr("disabled",false);
          $("#loading").css('display','none');
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
        message(jqXHR,{tipo:"danger",objeto:$("#msjtabla")});
        $(".btnSent").attr("disabled",false);
        $("#loading").css('display','none');
      });
    }


    function recargalista()
    {
      $("#Nom").val("");
      ruta="/cargo/listacargo";
      $.get(ruta,function(res){
        $("#lc").empty();
        $("#lc").append(res);
        createdt($("#tablacargos"),{dom:"f"});
        $("#cardA").show();
        $("#cardB").hide();
        listacargo=new Object();
      });
    }
    function retornar()
    {
      $("#cardA").show();
      $("#cardB").hide();
    }
    @endcan
  </script>
@stop