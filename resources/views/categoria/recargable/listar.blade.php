
  @foreach($data as $d)
    <tr>
      <td>{{$d->name}}</td>  
      <td>{{$d->amount_hour}}</td>  
      <td>{{$d->amount_half}}</td>
      <td>{{$d->amount_day}}</td>  
      <td class="botones">
        <button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-pencil" value="{{$d->id}}" onclick="editar(this, {})"></button>
        @can('cliente.destroy') 
          <button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="{{$d->id}}"></button>
        @endcan
      </td>
    </tr>
  @endforeach