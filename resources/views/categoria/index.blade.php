@extends('layouts.dashboard')
@section('css')
    <style>
        
    </style>
@stop
@section('content')

    <div class="d-block bg bg-dark" style="color:white; padding-top: 10px;visibility:hidden;" id="main">


        <div class="row my-4 pl-4" id="div_agregar">
            <button class="btn btn-success fa fa-plus" data-toggle="modal" data-target="#add">Agregrar un registro</button>
        </div>

        <div style="overflow-x: auto; min-width:80%;">
            @include('alert.mensaje')
            <div id="mensaje"></div>
            <table class="table table-hover table-dark text-center" cellspacing="0" id="Datos" style="width:100%;">
                <thead>
                    <th style="">Nombre</th>
                    <th style="">C$ x Hora</th>
                    <th style="">C$ x 1/2 Día</th>
                    <th style="">C$ x Día</th>
                    <th data-orderable="false"></th>
                </thead>
                <tbody class="text-center" id="lista"> 
                    @include('categoria.recargable.listar')
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="add" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="tile_madd">Agregar un registro</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="msj_add"></div>
                <form action="" id="form_data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    @include('categoria.formulario.datos')
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-success" id="btn_guardar">Guardar <i class="fa fa-save"></i> </button>
            </div>
          </div>
        </div>
    </div>

    @can('cliente.edit')
        <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Editar Registro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="message_edit"></div>
                        <form id="form_edit">
                            <input type="hidden" id="id" name="id">
                            @include('categoria.formulario.datos')
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="btn_actualizar"
                                onclick="actualizar({'msg': '#message_edit'})">Actualizar</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcan

    @can('cliente.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!}  
    {!!Html::script("js/jskevin/kavvdt.js")!!} 

    {{-- Si tiene los permisos de crear un registro incluira la funcion js correspondiente --}}
    @can('cliente.create')
        {!!Html::script("js/ajax_crud/create.js")!!} 
    @endcan
    
    {{-- Si tiene los permisos de actualizar un registro incluira la funcion js correspondiente --}}
    @can('cliente.update')
        {!!Html::script("js/ajax_crud/update.js")!!} 
    @endcan

    {{-- Si tiene los permisos de eliminar un registro incluira la funcion js correspondiente --}}
    @can('cliente.destroy')
        {!!Html::script("js/ajax_crud/delete.js")!!} 
    @endcan

    <script>
        var table;
        const base = "categoria";
        $(document).ready(function(){
            // Convertimos la tabla a una DataTable
            table = createdt($("#Datos"),{cant:[10,20,-1],cantT:["10","20","Todo"]});
            // Hacemo visible el contenido
            $("#main").css('visibility','visible'); 
            // Configuramos el modal de carga con 1060 en z-index
            $("#loading").css('z-index',1060);
        });
        
        // Por efectos de visibilidad utilizamos este metodo click
        $("#btn_guardar").click(function(){
            // Llamamos a la funcion que guarda el nuevo registro
            save({
                msg: '#msj_add', 
            });
        });
        // Resultado exitoso del create
        function success(res)
        {
            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code === 1)
            {
                // Mostramos un msj de exito
                message(["Se agrego el registro correctamente"],{manual:true});
                // Llamamos a la funcion que agrega una fila a la tabla
                addRow(res.data);
                // Ocultamos el modal de agregación
                $("#add").modal('hide');
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{objeto:$("#msj_add"), manual:true,tipo:"danger"});
            }

            $("#btn_guardar").attr("disabled",false);
            $("#loading").css('display','none');
        }

        function addRow(data)
        {
            // Almacenara los html de las acciones
            var botones = "";
            // Si tiene permisos de edicion se adjunta el html del btn para editar
            @can('cliente.update') 
                botones += '<button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit fa fa-pencil" value="'+data.id+'" onclick="editar(this, {})"></button>';
            @endcan
            // Si tiene permisos de eliminación se adjunta el html del btn para eliminar
            @can('cliente.destroy') 
                botones += '<button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="'+data.id+'"></button>';
            @endcan

            // Hacemos referencia a la variable que contienen la DataTable
            // Accedemos a su propiedad row y al metodo add
            // Especificando ahí los datos de cada columna en la tabla
            table.row.add( [
                data.name,
                data.amount_hour,
                data.amount_half,
                data.amount_day,
                botones,
            ] ).draw( false );
        }

        // Resultado exitoso del detalle o edit
        function fill(res)
        {
            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code == 1)
            {
                // Extraemos la información del registro
                let data = res.data;
                // Rellenamos los inputs con los valores correspondientes
                $(".nombre").eq(1).val(data.name);
                $(".hora").eq(1).val(data.amount_hour);
                $(".mediodia").eq(1).val(data.amount_half);
                $(".dia").eq(1).val(data.amount_day);
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{manual:true,tipo:"danger"});
                $("#Edit").modal('hide');
            }
            // Ocultamos la pantalla de carga
            $("#loading").css('display','none');
        }

        // Resultado exitoso del update
        function update_success(res)
        {
            //Si el codigo es 1 (retorno exitoso) entonces 
            if(res.code == 1)
            {
                // Mostramos un mensaje de error
                message(["Se edito el registro correctamente"],{manual:true});
                data = res.data;
                // Plasmamos en la tabla los nuevos valores
                table.cell(fila.children('td')[0]).data( data.name );
                table.cell(fila.children('td')[1]).data( data.amount_hour  );
                table.cell(fila.children('td')[2]).data( data.amount_half );
                table.cell(fila.children('td')[3]).data( data.amount_day );
                // Ocultamos el modal
                $("#Edit").modal('hide');
            }
            else
            {
                // En caso de retonar un codigo diff a 1 entonces
                // Mostramos un mensaje de error dictado desde el controlador
                message(res.msg,{objeto:$("#message_edit"), manual:true,tipo:"danger"});
            }
            // Reactivamos el boton de actualizar
            $("#btn_actualizar").attr("disabled",false);
            // Ocultamos la pantalla de carga
            $("#loading").css('display','none');
        }

    </script>
@stop