<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Phone_staff extends Model
{
    
    protected $table="phones_staff";
    public $incrementing = false;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=[
        'staff_id','phone_id'
    ];
}
