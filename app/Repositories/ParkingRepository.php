<?php 

namespace App\Repositories;

use App\Parking;

class ParkingRepository extends BaseRepository
{
  const RELATIONS = [
    'cars.tickets'
  ];

  public function __construct(Parking $parking)
  {
    parent::__construct($parking);
  }

  public function real_time()
  {
      $general_parking = $this->model->where('vip', false)->get();
      return $general_parking;
  }

  public function update_quantity(Request $request)
  {
      $general_parking = Parking::find($request['parking']);
      if($general_parking)
      {
          $general_parking->temp = $request['quantity'];
          $general_parking->save();
          return response()->json([
              'code' => 1,
              'data' => $general_parking,
          ]);
      }
      return response()->json([
          'code' => 0,
          'msg' => ['Esta ocurriendo un error, contactar al TI']
      ]);
  }

}