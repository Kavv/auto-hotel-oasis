<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\DatabaseConnection\Connection;

class DatabaseConnectionProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //new Connection();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }
}
