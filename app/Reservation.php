<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
    protected $table='reservations';
    public $primaryKey='id';
    public $incrementing = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'start', 'end', 'customer_id', 'state_id', 'room_id', 'subtotal', 'tax', 'discount', 'total', 'user_id'   
    ];

    public function room()
    {
        return $this->hasOne('App\Room', 'id', 'room_id');
    }
    public function customer()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }

}
