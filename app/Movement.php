<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movement extends Model
{
    protected $table='movements';
    public $primaryKey='id';
    public $incrementing = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [ 
        'document_id', 'number', 'amount', 'date', 'user_id', 'customer_id', 'service_id', 'lesson_id', 'start', 'end'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer','customer_id','id');
    }
    
    public function service()
    {
        return $this->belongsTo('App\Service','service_id','id');
    }
}
