<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{    
    use SoftDeletes;
    protected $table='rooms';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'amount', 'category_id', 'description_id', 'parking_id', 'image', 'people'
    ];

    public function description()
    {
        return $this->belongsTo('App\Description','description_id','id');
    }
    public function items()
    {
        return $this->belongsToMany('App\Item', 'items_rooms', 'room_id', 'item_id');
    }
    public function parking()
    {
        return $this->hasOne('App\Parking', 'id', 'parking_id');
    }
}
