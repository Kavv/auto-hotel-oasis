<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Calendar extends Model
{
    protected $table='calendars';
    public $primaryKey='id';
    public $incrementing = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'lesson_id','day', 'begin', 'end'
    ];
    public function customers()
    {
        // Uno a muchos
        return $this->belongsToMany('App\Customer', 'reservations', 'calendar_id', 'customer_id');
    }
}
