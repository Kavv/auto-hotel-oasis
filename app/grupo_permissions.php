<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class grupo_permissions extends Model
{
    protected $table='grupo_permissions';
    public $primaryKey='id';
    public $incrementing = true;
    protected $fillable = [
        'id_permissions','id_grupo',
    ];
}
