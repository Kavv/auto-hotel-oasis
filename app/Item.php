<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Item extends Model
{
    
    protected $table='items';
    public $primaryKey='id';
    public $incrementing = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'id','service_id','quantity','name','rental_price','buy_price',
    ];

    public function servicio()
    {
        return $this->belongsTo('App\Service','service_id','id');
    }
}
