<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Phone extends Model
{
    
    protected $table='phones';
    public $primaryKey ='id';
    public $incrementing = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    
    protected $fillable = [
        'number'
    ];
    
    public function customers()
    {
        return $this->belongsToMany('App\Customer', 'customers_phones', 'phone_id', 'customer_id');
    }
    public function staff()
    {
        return $this->belongsToMany('App\Staff', 'phones_staff', 'phone_id', 'staff_id');
    }
}
