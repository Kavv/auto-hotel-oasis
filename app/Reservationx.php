<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Reservationx extends Model
{
    
    protected $table='reservations';
    public $primaryKey='id';
    public $incrementing = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'customer_id','begin_date','end_date','event_address','tax','invoice_row','discount',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Customer','customer_id','id')->withTrashed();
    }
    public function clienteTrash()
    {
        return $this->belongsTo('App\Customer','customer_id','id')->withTrashed();
    }

    public function items()
    {
        return $this->belongsToMany('App\Item','items_reservations','reservation_id','item_id')
                ->withPivot(['quantity as qu', 'unit_price as up', 'amount as amount', 'day as day', 'index as index'])
                ->withTimestamps()
                ->withTrashed();
    }
    
    public function menu()
    {
        return $this->belongsToMany('App\Menu','menu_reservations','reservation_id','menu_id')
                ->withPivot(['quantity as qu', 'unit_price as up', 'amount as amount', 'day as day', 'index as index'])
                ->withTimestamps()
                ->withTrashed();
    }
}
