<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Discharged extends Model
{
    
    protected $table='discharged';
    public $primaryKey ='id';
    public $incrementing = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quantity','description', 'item_id', 'staff_id','customer_id',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Customer','customer_id','id')->withTrashed();
    }

    public function personal()
    {
        return $this->belongsTo('App\Staff','staff_id','id')->withTrashed();
    }
}
