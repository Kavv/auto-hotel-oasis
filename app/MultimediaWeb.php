<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MultimediaWeb extends Model
{
    use SoftDeletes;
    protected $table='multimedia_web';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'image_url', 'selection', 'order', 'categories_id'
    ];

    //Retornar la categoria padre
    public function Categoria()
    {
        return $this->belongsTo('App\Category','categories_id','id');
    }
    
}
