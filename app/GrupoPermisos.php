<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoPermisos extends Model
{
    protected $table = 'grupopermisos';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['nombre','descripcion'];
}
