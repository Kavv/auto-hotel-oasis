<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticuloAdd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'ID_Servicio.required' => 'Especificar el servicio es obligatorio',
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "ID_Servicio"=>'required|exists:services,id',
            'Nombre'=>'required|unique:items,name|max:100',
            'Cantidad'=>'nullable|numeric',
            'Costo_Alquiler'=>'nullable|numeric',
            'Costo_Objeto'=>'nullable|numeric',
        ];
    }
}
