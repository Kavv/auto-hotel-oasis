<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParkingSave extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'=>'required|numeric',
            'vip'=>'required',
            'quantity'=>'integer',
        ];
    }
    // It is an example about "How u can change the input name in the msg"
    public function attributes()
    {
        return [
            'type' => 'tipo de vehiculo',
            'quantity' => 'cantidad'
        ];
    }

    // It's an example about "How u can change the message"
    public function messages()
    {
        return [
            'vip.required' => 'Debe especificar un tipo de parqueo'
        ];
    }
}
