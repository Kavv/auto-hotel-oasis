<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PersonalAdd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            /* 'Cedula_Personal'=>
            [
                Rule::unique('staff','dni')->where(function ($query) {
                    $query->where('deleted_at', null);
                }),'required','max:30'
            ], */
            'Nombre'=>'required|alpha_spaces|max:30',
            'Apellido'=>'required|alpha_spaces|max:30',
            'Direccion'=>'required|max:200',
            'Edad'=>'required|numeric'
        ];
    }
}
