<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant

class clienteAdd extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    protected $connection;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->connection = new Connection($this->user()->hostname,$this->user()->hostname->website);
        $dbName = $this->user()->hostname->website->uuid;
        return [
            'Cedula_Cliente'=>
            [
                Rule::unique('alquileres_martha_varela.customers','dni')->where(function ($query) {
                    $query->where('deleted_at', null);
                }),'required','max:30'
            ],
            'Nombre'=>'required:name|alpha_spaces|max:30',
            'Apellido'=>'required|alpha_spaces|max:30',
            'Edad'=>'required|numeric',
            'Sexo'=>'required|alpha|max:15',
            'Direccion' => 'max:200'
        ];
    }
}
