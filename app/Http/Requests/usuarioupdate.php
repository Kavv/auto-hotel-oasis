<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class usuarioupdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function messages()
    {
        return [
            'userName.required' => 'El nombre de usuario es obligatorio.',
            'userName.unique' => 'El nombre del usuario ya esta en uso! ingrese uno diferente.',
            'userName.max' => 'El nombre no debe ser mayor a 30 caracteres.',
            'name.required' => 'El nombre y apellido son requerido.',
            'name.max' => 'El nombre y apellido no debe ser mayor a 30 caracteres.',
            'name.alpha_space' => 'El nombre y apellido debe contener solamente letras y espacios.',
            'password.required' => 'La contraseña es obligatoria.',
            'password.min' => 'La contraseña debe tener un minimo de 6 caracteres.',
            'description.required' => 'La descripcion es obligatoria.',
            'description.max' => 'La descripcion no debe ser mayor a 100 caracteres.'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userName'=>'required|max:30',
            'name'=>'required|max:30|alpha_spaces',
            'description'=>'max:100',
            'password'=>'min:6'
        ];
    }
}
