<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use App\Phone;
use App\Position;
use App\cargo_personal;
use App\Http\Requests\PersonalAdd;
use App\Http\Requests\PersonalUpdate;
use Redirect;
use Session;
use DB;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant
use Illuminate\Support\Facades\Hash;

class PersonalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //Enviamos el host y el website relacionado al usuario actual
//        $this->middleware(function ($request, $next) {
//            $this->Connection = new Connection(Auth::user()->hostname,Auth::user()->hostname->website);
//            return $next($request);
//        });
    }
    public function index($msj=null,$cedula=null)
    { 
        $personal=Staff::all();
        if($msj=="2")
        {
            Session::flash('message','Personal editado exitosamente');
            Session::flash('tipo','info');
        }
        if($msj=="1")
        {
            Session::flash('message','Personal agregado exitosamente');
            Session::flash('tipo','info');
        }
        Session::flash('valor',$cedula);
        $Cargos=Position::all();


        return view('personal.index',compact('personal','Cargos'));
    }

    public function pcargo($id)
    {
        $Cargos = Position::all();
        if($id == 0){
            $personal = Staff::all();
        }
        else{
            $cargo = Position::where('id', '=', $id)->first();
            $personal = $cargo->personal;
        }
        return view('personal.recargable.listapersonal',compact('Cargos','personal'));
    }

    public function create()
    {
        $Cargos=Position::all();
        return view('personal.create',['message'=>"",'Cargos'=>$Cargos]);
    }
    public function store(Request $request)
    { 
        $this->validacion($request);
        $personal = Staff::where('dni','ilike',$request['Cedula_Personal'])->first();
        if($personal)
        {
            if($personal->vetoed_id != null)
                return response()->json(['La cedula del personal ya habia sido registrada!', 
                                         'El personal con esta cedula esta vetado']);
            else
                return response()->json(['La cedula del personal ya habia sido registrada!']);
        }  
        // Validamos que si selecciono el cargo de entrenador debe especificar una contraseña
        // El cargo con id = 1 es Entrenador ]\
        if(isset($request['cargos']))
        {
            // array_search retorna el indice del elemento que se esta buscando
            // Se busca el "1" en el arreglo
            // retorna falso unicamente si no encuentra el valor
            $is_trainer = array_search(1, $request['cargos']);
            // Consultamos si la contraseña es vacia, si se encontro el cargo de entrenador
            // Si is_trainer es diferente a falso (1,2,3, etc) 
            if($request['password'] == "" && $is_trainer !== false  )
                return response()->json(['Si el personal es entrenador, debe especificarle una contraseña']);
        }
        

        if($request['password'] != "")
            $password = Hash::make($request['password']);
        else
            $password = null;
        $personal = Staff::create([
            'dni'=>$request['Cedula_Personal'],    
            'name'=> $request['Nombre'],
            'last_name'=>$request['Apellido'],
            'address'=>($request['Direccion']),
            'age'=> $request['Edad'],
            'password'=> $password,
        ]);
        if($request['cargos'])
            $personal->cargos()->sync($request['cargos']);

        $this->syncPhones($request,$personal);

        return 1;
    }
    public function destroy($id)
    {
        $usuario=Staff::find($id);
        $usuario->delete();
        return 1;
    }
    //Actualiza los datos del personal
    public function update($id,Request $request)
    {
        $this->validacion($request);
        $personal=Staff::find($id);
        if($personal['dni']!=$request['Cedula_Personal'])
        {
            $consulta=Staff::Where('dni','ilike',$request['Cedula_Personal'])->get();
            if(count($consulta)==0)
                $personal['dni']=$request['Cedula_Personal'];
            else
                return response()->json(['No se actualizo: La cedula ingresada ya estaba registrada']);
        }
            
        // Validamos que si selecciono el cargo de entrenador debe especificar una contraseña
        // El cargo con id = 1 es Entrenador ]\
        if(isset($request['cargos']))
        {
            // array_search retorna el indice del elemento que se esta buscando
            // Se busca el "1" en el arreglo
            // retorna falso unicamente si no encuentra el valor
            $is_trainer = array_search(1, $request['cargos']);
            // Consultamos si la contraseña es vacia,
            // Si is_trainer es diferente a false (1,2,3, etc) 
            if($request['password'] == "" && $is_trainer !== false  )
                return response()->json(['Si el personal es entrenador, debe especificarle una contraseña']);
        }
        if($request['password'] != "")
        {
            $password = Hash::make($request['password']);
            $personal['password'] = $password;
        }
        $personal['name']=$request['Nombre'];
        $personal['last_name']=$request['Apellido'];
        $personal['address']=$request['Direccion'];
        $personal['age']=$request['Edad'];
        $personal->save();

        $personal->cargos()->sync($request['cargos']);

        $this->syncPhones($request,$personal);
        return 1;
    }
    public function show($cedula)
    {
        $personal=Staff::where('dni','=',$cedula)->first();
        if($personal)
        {
            if($personal->vetoed_id != null)
                return response()->json([
                    'data' => $personal,
                    'message' => ['El personal ya existe, no podra ser registrado!', 
                                  'El personal con esta cedula esta vetado']
                ]);
            else
                return response()->json([
                    'data' => $personal,
                    'message' => ['El personal ya existe, no podra ser registrado!']
                ]);
        }
        return response()->json([
            'message' => ['Opss!... Ocurrio un error, vuelve a intentarlo!']
        ]);
    }
    //retorna los ID  de los cargos para poder llenar las checkbox
    public function cargos($id=null)
    {
        $personal=Staff::find($id);
        return response()->json($personal->cargos()->get());
    }
    
    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'Cedula_Personal' => 'required|max:30',
                'Nombre' => 'required|alpha_spaces|max:30',
                'Apellido' => 'required|alpha_spaces|max:30',
                'Direccion' => 'max:200',
                'Edad'=>'nullable|numeric'
            ];
        }
        $this->validate($objeto, $rule, $message);
    }
    
    public function syncPhones($request = null, $staff)
    {
        $phone_id=[];
        for($i=0; $i < count($request['telefono']); $i++)
        {
            //No poseo una validacion para el telefono, pero si excede los 24 caracteres no se guardara (no da aviso) CAMBIAR A FUTURO
            if($request['telefono'][$i] != "" && strlen($request['telefono'][0])<25 )
            {
                $phone = Phone::where('number', $request['telefono'][$i])->first();
                if($phone)
                   $phone_id[$i] = $phone->id;
                else
                {
                    $phone_id[$i] = Phone::create([
                        'number' => $request['telefono'][$i]
                    ]);
                    $phone_id[$i] = $phone_id[$i]->id;
                }
            }
        }
        
        if($phone_id)
            $staff->phones()->sync($phone_id);
        
    }

    public function telefonos($id)
    {
        $staff = Staff::find($id);
        $phones = $staff->phones;
        return response()->json($phones);
    }
}
