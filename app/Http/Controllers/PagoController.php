<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Service;
use App\Status;
use Carbon\Carbon;
use App\Movement;
use App\Lesson;
use Auth;
use Illuminate\Support\Facades\DB;

class PagoController extends Controller
{
    public function index()
    {
        $services = Service::all();
        $status = Status::all();
        $lessons = Lesson::all();
        return view('pago.index', compact('services', 'status', 'lessons'));
    }

    public function create()
    {
        //$actual_day =  Carbon::now()->format('d');
        //$customers = Customer::where('payday', $actual_day)->get(); 
        // Retornamos los datos del cliente, estado y servicio... 
        // si el cliente tiene n servicios el registro del cliente se mostrara n veces especificando cada uno de estos servicios 
        $customers = Customer::leftJoin('status', 'status_id', '=', 'status.id')
                    ->join('customers_services', 'customers.id', '=', 'customers_services.customer_id')
                    ->join('services', 'customers_services.service_id', '=', 'services.id')
                    ->join('customers_lessons', 'customers.id', '=', 'customers_lessons.customer_id')
                    ->join('lessons', 'customers_lessons.lesson_id', '=', 'lessons.id')
                    ->join('movements', 'movements.customer_id', '=', 'customers.id')
                    ->select('customers.id', 'dni', 'customers.name', 'last_name', 'status.name as state', 'lessons.code as lesson', 'services.name as service', 'services.amount', DB::raw('max(movements.end) as next_payment'))
                    ->where('movements.deleted_at', '=', null)
                    ->where('status.id', '!=', 2)
                    ->groupBy('customers.id', 'dni', 'customers.name', 'last_name', 'state', 'lesson', 'service', 'services.amount')
                    ->get();
        $services = Service::all();
        $status = Status::all();
        $lessons = Lesson::withCount('customers')->get();

        return view('pago.create', compact('customers', 'services', 'status', 'lessons'));
    }
 
    public function store(Request $request)
    {
        $this->validacion($request);
        // Buscamos el cliente
        $customer = Customer::find($request['id']);
        // Obtenemos el usuario actual
        $digitizer = Auth::user()->id;
        // Esta funcion devuelve el numero de recibo que se utilizara para el movimiento
        $receipt = $this->receipt_number($request['recibo']);
        // Validamos
        // Si el # de recibo es diferente de 0 y diferente de vacio entonces
        if($receipt != 0 && $receipt != "")
        {
            // Verificamos si existe otro movimiento con ese mismo numero de recibo
            $exist = Movement::where('document_id', 1)->where('number', $receipt)->first();
            // En caso de existir devolvemos un mensaje de error
            if($exist)
            {
                return response()->json([
                    'code' => 2,
                    'msg' => "El numero de recibo $receipt ya se encuentra en uso" ,
                ]);
            }
        }
        else
        {
            return response()->json([
                'code' => 2,
                'msg' => "Ocurrio un error al generar el numero de recibo" ,
            ]);
        }
        // Aprobamos las validaciones anteriores entonces
        // Consultamos si el cliente existe para generar finalmente el nuevo movimiento/pago
        if($customer)
        {
            $movimiento = Movement::create([
                'document_id' => 1,
                'number' => $receipt,
                'amount' => $request['monto'],
                'date' => $request['fecha_de_pago'],
                'user_id' => $digitizer,
                'customer_id' => $customer->id,
                'lesson_id' => $request['leccion'],
                'service_id' => $request['servicio'],
                'start' => $request['servicio_inicio'],
                'end' => $request['servicio_corte'],
            ]);

            // Mediante esta funcion verificamos si el pago que estamos editando es el ultimo corte
            $this->update_with_last_payment($customer->id, $request);

        }
        // Obtenemos todos los pagos que actualmente ha efectuado el cliente
        $payments = $customer
                    ->movements()
                    ->join('lessons', 'lesson_id', '=', 'lessons.id')
                    ->join('services', 'service_id', '=', 'services.id')
                    ->join('users', 'user_id', '=', 'users.id')
                    ->select('movements.id', 'number' , 'movements.amount' , 'date', 'lessons.code', 'services.name as service', 'users.email as digitizer')
                    ->get();

        // Con el fin de retornarlos a la vista y actualizar la tabla
        return response()->json([
            'code' => 1,
            'payments' => $payments,
        ]);
    }

    
    public function update(Request $request, $id)
    {
        $this->validacion($request);
        // Obtenemos y verificamos el numero de recibo
        $receipt = $this->receipt_number($request['recibo'], $id);
        $movement = Movement::find($id);
        if($movement)
        {
            // Obtenemos el id del usuario actual
            $digitizer = Auth::user()->id;
            // Calculamos la diferencia que genera la edición del monto actual, para poder ajustarlo en la vista
            $diff = $movement->amount - $request['monto'];

            // Editamos el movimiento/pago
            $movement->number = $receipt;
            $movement->amount = $request['monto'];
            $movement->date = $request['fecha_de_pago'];
            $movement->lesson_id = $request['leccion'];
            $movement->service_id = $request['servicio'];
            $movement->start = $request['servicio_inicio'];
            $movement->end = $request['servicio_corte'];
            $movement->user_id = $digitizer;
            $movement->save();

            $customer_id = $movement->customer_id;
            // Mediante esta funcion verificamos si el pago que estamos editando es el ultimo corte
            $this->update_with_last_payment($customer_id, $request);
            

            return response()->json([
                'code' => 1,
                'receipt' => $receipt,
                'diff' => $diff
            ]);
        }
        else
        {
            return response()->json([
                'code' => 0,
                'msg' => ['¡Ocurrio un error, No se encontro el pago a editar!']
            ]);
        }
    }

    public function destroy($id)
    {
        $movement = Movement::find($id);
        $amount = $movement->amount;
        $customer_id = $movement->customer_id;
        
        // Consultamos el ultimo pago (excluyendo el que se esta intentando eliminar)
        $last_payment = Movement::where('customer_id', $customer_id)
                        ->where('id', '!=', $id)->orderBy('end', 'desc')->first();


        // Si existe un ultimo pago entonces
        if($last_payment)
        {

            // Eliminamos el pago que se habia especificado
            $movement->delete();

            // Buscamos el cliente correspondiente
            $customer = Customer::find($customer_id);
            // Sincronizamos la leccion y el servicio correspondiente al ultimo pago existente
            $customer->lessons()->sync($last_payment->lesson_id);
            $customer->services()->sync($last_payment->service_id);
        }
        else
        {
            return response()->json([
                'code' => 2,
                'msg' => ['No se elimino, porque el cliente debe tener minimo un pago asignado (este es el único)'],
            ]);
        }
        
        
        return response()->json([
            'code' => 1,
            'amount' => $amount,
        ]);
    }


    public function show($id)
    {
        $data = Movement::find($id);

        if($data)
            return response()->json([
                'code' => 1,
                'data' => $data
            ]);
        else
            return response()->json([
                'code' => 0,
                'msg' => ['¡Ocurrio un error - No se encontró el pago!']
            ]);
    }

    private function update_with_last_payment($customer, $request)
    {
        // Obtenemos todos los pagos del cliente al cual se le estaba editando el pago
        // Estos registros deben ser mayor a la fecha de corte el pago recien editado
        // Ordenamos de manera descendente y tomamos el primero de la lista
        // Ese registro seria el ultimo corte a aplicarse
        $last_mov = Movement::where('customer_id',$customer)->where('end', '>', $request['servicio_corte'])->orderBy('end', 'desc')->first();
        // Si la consulta retorno algun registro entonces significa que ese registro es el ultimo y NO el recien editado
        if($last_mov)
        {
            // Definimos las variables lesson y service con los datos del registro obtenido
            $lesson = $last_mov['lesson_id'];
            $service = $last_mov['service_id'];
        }
        else
        {
            // En caso contrario
            // Definimos las variables lesson y service con los datos del pago recien editado
            $lesson = $request['leccion'];
            $service = $request['servicio'];
        }
        // Obtenemos el registro completo del cliente para posteriormente sincronizar la leccion y el servicio
        $customer = Customer::find($customer);
        $customer->lessons()->sync($lesson);
        $customer->services()->sync($service);

    }
   
    public function actualizarinfo(Request $request)
    {
        $rule = [
            'id' => 'required|numeric',
            'state'=>'required',
        ];
        $this->validacion($request, $rule);

        $customer = Customer::find($request['id']);
        if($customer)
        {
            $customer->status_id = $request['state'];
            $customer->save();
            return response()->json([
                'code' => 1
            ]);
        }
        else
        {
            return response()->json([
                'code' => 0,
                'msg' => ['¡Ocurrio un error - No se encontró el cliente!']
            ]);
        }

    }
    
    
    public function receipt_number($number, $id = 0)
    {
        // Almacenamos el numero de recibo que se envio desde la vista
        $receipt = $number;
        // Si es diferente a 0 y diferente a vacio entonces
        if($receipt != 0 && $receipt != "")
        {
            // Si id es 0 entonces significa que estamos creando agregando un nuevo pago
            // Si id es diff de 0 entonces significa que estamos editando un pago
            if($id == 0)
                // Verificamos si existe un movimiento que ya tenga este numero de recibo en uso
                $exist = Movement::where('document_id', 1)->where('number', $receipt)->first();
            else
                // Verificamos si existe un movimiento diferente al que estamos editando con el mismo numero de recibo
                $exist = Movement::where('document_id', 1)->where('number', $receipt)->where('id', '!=', $id)->first();
            // Si existe entonces retornamos un mensaje de error
            if($exist)
            {
                abort(response()->json([
                    'code' => 2,
                    'msg' => ["El numero de recibo $receipt ya se encuentra en uso"],
                ], 200));
            }
        }
        else
        {
            // Si el numero de recibo es 0 o vacio entonces hacemos uso de "sec_recibo_negativo"
            // Una secuencia creada en pgsql para generar numeros en negativo de manera decreciente (-1, -2, -3)
            // Ya que la creación del movimiento requiere un numero de recibo
            $receipt = DB::select("select nextval('sec_recibo_negativo')");
            $receipt = $receipt[0]->nextval;
        }
        // Retornamos el numero de recibo que se asignara al movimiento
        return $receipt;
    }


    public function payments($id)
    {
        $cliente = Customer::find($id);
        $telefonos = $cliente->phones()->get();
        $lesson = $cliente->lessons()->first();
        $services = $cliente->services()->first();

        $payments = $cliente
                    ->movements()
                    ->join('lessons', 'lesson_id', '=', 'lessons.id')
                    ->join('services', 'service_id', '=', 'services.id')
                    ->join('users', 'user_id', '=', 'users.id')
                    ->select('movements.id', 'number' , 'movements.amount' , 'date', 'lessons.code', 'services.name as service', 'users.email as digitizer')
                    ->get();

        if($cliente)
        {
            return response()->json([
                'data' => $cliente,
                'phones' => $telefonos,
                'lesson' => $lesson,
                'services' => $services,
                'payments' => $payments,
            ]);
        }
        return response()->json([
            'message' => ['Opss!... Ocurrio un error, vuelve a intentarlo!']
        ]);
    }
    public function payments_list(Request $request)
    {
        $begin = $request['begin'];
        $end = $request['end'];
        // Convertimos la cadena de texto a tiempo para poder formatear la fecha y validar
        $b_aux = strtotime($begin);
        $e_aux = strtotime($end);

        // Consultamos si fechas existe en el calendario (ejemplo el 30 de febrero es una fecha que no existe)
        $begin_is_valid = checkdate(date('m',$b_aux),date('d',$b_aux),date('Y',$b_aux));
        $end_is_valid = checkdate(date('m',$e_aux),date('d',$e_aux),date('Y',$e_aux));

        // Si alguna validación es equivalente a falso retornamos un msj de error
        if(!$begin_is_valid || !$end_is_valid)
            return response()->json([
                'code' => 2,
                'msg' => ["Una de las fechas no es valida"],
            ]);
        
        if($begin == "" || $end == "")
            return response()->json([
                'code' => 2,
                'msg' => ["¡Debe especificar ambas fechas!"],
            ]);

        $movements = Movement::whereBetween('date', [$begin, $end])
                    ->join('lessons', 'lesson_id', '=', 'lessons.id')
                    ->join('services', 'service_id', '=', 'services.id')
                    ->join('users', 'user_id', '=', 'users.id')
                    ->join('customers', 'customer_id', '=', 'customers.id')
                    ->where('customers.deleted_at', '=', null)
                    ->where('movements.deleted_at', '=', null)
                    ->select('movements.id', 'number' , 'movements.amount' , 'date', 'services.name as service', 'lessons.code as lesson', 'users.email as digitizer', 'customers.dni', 'movements.start', 'movements.end')
                    ->get();
        
        return response()->json([
            'data' => $movements,
        ]);
    }

    public function filtrar(Request $request)
    {
        // Almacenamos el rango de tiempo establecido
        $begin = $request['begin'];
        $end = $request['end'];

        // Si ambos son 0 entonces se retornan todos los clientes con su fecha de cobro
        if($begin == 0 && $end == 0)
        {
            $customers = Customer::leftJoin('status', 'status_id', '=', 'status.id')
                        ->join('customers_services', 'customers.id', '=', 'customers_services.customer_id')
                        ->join('services', 'customers_services.service_id', '=', 'services.id')
                        ->join('customers_lessons', 'customers.id', '=', 'customers_lessons.customer_id')
                        ->join('lessons', 'customers_lessons.lesson_id', '=', 'lessons.id')
                        ->join('movements', 'movements.customer_id', '=', 'customers.id')
                        ->select('customers.id', 'dni', 'customers.name', 'last_name', 'status.name as state', 'lessons.code as lesson', 'services.name as service', 'services.amount', DB::raw('max(movements.end) as next_payment'))
                        ->where('movements.deleted_at', '=', null)
                        ->where('status.id', '!=', 2)
                        ->groupBy('customers.id', 'dni', 'customers.name', 'last_name', 'state', 'lesson', 'service', 'services.amount')
                        ->get();
        }
        else 
        {
            // En caso contrario
            // Validamos las fechas 
    
            // Si alguna esta vacia
            if($begin == "" || $end == "")
                return response()->json([
                    'code' => 2,
                    'msg' => ["¡Debe especificar ambas fechas!"],
                ]);
            // Convertimos la cadena de texto a tiempo para poder formatear la fecha y validar
            $b_aux = strtotime($begin);
            $e_aux = strtotime($end);

            // Consultamos si fechas existe en el calendario (ejemplo el 30 de febrero es una fecha que no existe)
            $begin_is_valid = checkdate(date('m',$b_aux),date('d',$b_aux),date('Y',$b_aux));
            $end_is_valid = checkdate(date('m',$e_aux),date('d',$e_aux),date('Y',$e_aux));

            // Si alguna validación es equivalente a falso retornamos un msj de error
            if(!$begin_is_valid || !$end_is_valid)
                return response()->json([
                    'code' => 2,
                    'msg' => ["Una de las fechas no es valida"],
                ]);
            // En caso de que las fechan sean validas, obtenemos todos los clientes que tienen fecha de cobro
            // En el rango de fechas que se ha especificado
            

            $customers = Customer::leftJoin('status', 'status_id', '=', 'status.id')
                        ->join('customers_services', 'customers.id', '=', 'customers_services.customer_id')
                        ->join('services', 'customers_services.service_id', '=', 'services.id')
                        ->join('customers_lessons', 'customers.id', '=', 'customers_lessons.customer_id')
                        ->join('lessons', 'customers_lessons.lesson_id', '=', 'lessons.id')
                        ->join('movements', 'movements.customer_id', '=', 'customers.id')
                        ->select('customers.id', 'dni', 'customers.name', 'last_name', 'status.name as state', 'lessons.code as lesson', 'services.name as service', 'services.amount', DB::raw('max(movements.end) as next_payment'))
                        ->where('movements.deleted_at', '=', null)
                        ->where('status.id', '!=', 2)
                        ->groupBy('customers.id', 'dni', 'customers.name', 'last_name', 'state', 'lesson', 'service', 'services.amount')
                        ->havingRaw("max(movements.end) between '$begin' and '$end' ")
                        ->get();
                        
        }

        $services = Service::all();
        $status = Status::all();

        return view('pago.recargable.listaclientes', compact('customers', 'services', 'status'));
    }

    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'leccion' => 'required',
                'servicio'=>'required',
                'monto'=>'required|numeric',
                'recibo'=>'numeric|nullable',
                'fecha_de_pago'=>'required|date',
                'servicio_inicio'=>'required|date',
                'servicio_corte'=>'required|date',
            ];
        }
        $this->validate($objeto, $rule, $message);
    }

}
