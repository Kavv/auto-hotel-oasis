<?php

namespace App\Http\Controllers;
use App\Category;
use App\MultimediaWeb;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant

class ImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //Enviamos el host y el website relacionado al usuario actual
//        $this->middleware(function ($request, $next) {
//            $this->Connection = new Connection(Auth::user()->hostname,Auth::user()->hostname->website);
//            return $next($request);
//        });
    }
    public function index()
    {
        if(Auth::user()->hostname->website->uuid == "Alquiler_Santana")
        {    
            $video = MultimediaWeb::where('categories_id', '=', '1')
            ->where('selection', '=' ,'1')
            ->first();

            $type = explode(".", $video->image_url);
            $old_type = $type[3];

            for($i = 2; $i<10; $i++) 
            {
                $prueba[] = MultimediaWeb::where('categories_id', '=', $i)
                    ->where('selection', '=', '1')
                    ->where('order', '=','1')
                    ->get()->toArray();
            }

            for($i=0;$i<=7;$i++)
            {
                $images[] = $prueba[$i][0];
            }

            $carus = MultimediaWeb::where('categories_id', '=','10')
                    ->where('selection', '=', '1')
                    ->orderBy('order','asc')
                    ->get();

            $categories = Category::all();
            return view('imagenes.estandar.index', compact('video','images', 'carus','categories','old_type'));
        }
        if(Auth::user()->hostname->website->uuid == "Alquileres_Martha_Varela")
        {
            return 1;
        }
        
    }

    public function changeImg(Request $request){

        if($request->isMethod('post'))
        {
            $op = $request->op;

            \Cloudinary::config(array(
                "cloud_name" => "fguzman",
                "api_key" => "819716184232435",
                "api_secret" => "6z5xZb-E55RVlne7Wswd_mkCVqg"
            ));

            $this->validate($request,[
                'img'=>'required|mimes:jpeg,bmp,jpg,png|between:1, 60000',
            ]);

            $id_ant = substr($request->id , 5);
            $camb = MultimediaWeb::find($id_ant);
            $camb->selection = 0;

            $image_name = $request->file('img')
                ->getRealPath();
            //cambiar esto especificamente a la seccion del carousel a 800*400
            if($op == 2)
                $cloud = \Cloudinary\Uploader::upload($image_name, array(
                    "width" => 800, "height"=> 697, "crop" => "scale"));
            else
                $cloud = \Cloudinary\Uploader::upload($image_name, array(
                    "width" => 400, "height"=> 349, "crop" => "scale"));

            $image_url= $cloud['secure_url'];

            $image = new MultimediaWeb();
            $image->name = $request->file('img')
                ->getClientOriginalName();
            $image->image_url = $image_url;
            $image->categories_id = $camb->categories_id;;
            $image->selection = 1; 
            $image->order = $camb->order;

            $image->save();
            $camb->save();
            
            if((($camb->categories_id>=2 && $camb->categories_id<=9) && ($camb->order==1))){
                return response()->json([
                    'action' => 1,
                    'src' => $image_url,
                    'id' =>'image'.$image->id
                ]);
            }
            else{
                return response()->json([
                    'action' => 0,
                    'src' => $image_url,
                    'id' => 'image'.$image->id,
                ]);
            }
            

        }
    }
    //modificacion a que sea mixto
    public function changeVid(Request $request){

        if($request->isMethod('post'))
        {
            $allowed_ext = array("jpg" => "image/jpg",
                "jpeg" => "image/jpeg",
                "gif" => "image/gif",
                "png" => "image/png");

            \Cloudinary::config(array(
                "cloud_name" => "fguzman",
                "api_key" => "819716184232435",
                "api_secret" => "6z5xZb-E55RVlne7Wswd_mkCVqg"
            ));
            //tipos aceptables
            $this->validate($request,[
                'video'=>'required|mimes:mp4,webm,jpeg,bmp,jpg,png|between:1, 60000',
            ]);
            //variables para probar
            $name_check = $request->name;
            $type_ori = $request->type;
            //
            $url_ant = $request->bg;
            $test = $request->video;
            $id_ant = substr($request->id,5);
            $videoAnt = MultimediaWeb::find($id_ant);

            $videoAnt->selection = 0;
            //
            $video = $request->file('video');
            $video_name = $request->file('video')
                ->getRealPath();
            list($width, $height) = getimagesize($video_name);
            //si es una imagen
            if(in_array($type_ori, $allowed_ext)){
                $cloud = \Cloudinary\Uploader::upload($video_name, array(
                    "width" => 800, "height"=> 697, "crop" => "scale"));
            }
            //si es un video
            else{
                $cloud = \Cloudinary\Uploader::upload_large($video_name, array(
                    "resource_type" => "video"));
            }
            $video_url = $cloud['secure_url'];
            $new_type = $cloud['resource_type'];
            $video = new MultimediaWeb();
            $video->name = $request->file('video')
                ->getClientOriginalName();
            $video->image_url = $video_url;

            $video->categories_id = $videoAnt->categories_id;
            $video->selection = 1;
            $video->order = $videoAnt->order;
            $video->save();
            $videoAnt->save();
            return response()->json([
                'src' => $video_url,
                'id' => "video".$video->id,
                'tipo' => $new_type
            ]);
        }
    }

    public function changeInModal(Request $request){

        if($request->isMethod('POST')){
            $id = substr($request->data,8);
            $images = Category::find($id)->Imagenes()->where('selection',1)->orderBy('order', 'asc')->get();
            
            $html = "";
            foreach ($images as $indexKey=>$image)
            {
                if($indexKey == 0)
                {
                    $html .=  '<div class="inter carousel-item active" style="height:697px">' .
                        '<img id="image' . $image->id . '" class="d-block" style="height:697px" src="' . $image->image_url . '">' .
                        '</div>';

                }
                else
                {
                    $html .= '<div class="inter carousel-item" style="height:697px">' .
                        '<img id="image' . $image->id . '" class="d-block" style="height:697px" src="' . $image->image_url . '">' .
                        '</div>';
                }
            }

            return response()->json($html);
        }
    }

    public function deleteVid(Request $request){
        if($request->isMethod('POST')) {
            //obtengo url
            $videoBase = MultimediaWeb::find(1);
            $id_ant = substr($request->id,5);
            $vid = MultimediaWeb::find($id_ant);

            if($vid->id == $videoBase->id){
                return 1;
            }
            else{
                $videoBase->selection = 1;
                $videoBase->order = 1;

                $vid->selection = 0;
                $vid->order = 0;

                $videoBase->save();
                $vid->save();

                return response()->json($videoBase->image_url);
            }
        }
    }

    public function deleteImg(Request $request){
        if($request->isMethod('POST')){

            $idAnt = substr($request->id,5);

            $imageAnt = MultimediaWeb::find($idAnt);

            $imageBase = MultimediaWeb::where('categories_id',$imageAnt->categories_id)
                          ->Where('order',$imageAnt->order)
                          ->orderBy('id','asc')
                          ->first();
            if($imageAnt->id == $imageBase->id){
                return 1;
            }
            else{
                $imageBase->selection = 1;
                $imageAnt->selection = 0;

                $imageBase->save();
                $imageAnt->save();

                if(($imageAnt->categories_id>=2 && $imageAnt->categories_id<=9) && ($imageAnt->order==1)) {
                    return response()->json([
                        'action' => 2,
                        'src' => $imageBase->image_url,
                        'id' => "image".$imageBase->id
                    ]);
                }
                else{
                    return response()->json([
                        'action' => 0,
                        'src' => $imageBase->image_url,
                        'id' => "image".$imageBase->id
                    ]);
                }
            }
        }
    }
}
