<?php

namespace App\Http\Controllers;
use App\Exports\CustomerExport;//clase creada con unn comando de exports
use App\Exports\ReservationExport;
use App\Reservation;
use App\Staff;
use Illuminate\Http\Request;
use App\Customer;
use App\Phone;
use App\Service;
use App\Status;
use App\Lesson;
use App\Movement;
use App\Calendar;
use App\Http\Requests\clienteAdd;
use App\Http\Requests\clienteUpdate;
use Maatwebsite\Excel\Facades\Excel;//paquete para realizar los exports
use Session;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant
use Auth;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class ClienteController extends Controller
{
    protected $Connection;
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $data = Customer::all();
        return view('cliente.index', compact('data'));
    }
    
    public function store(Request $request)
    {
        $this->validacion($request);
        $customer = Customer::create([
            'dni' => $request['Cedula_Cliente'],
            'name' => $request['Nombre'],
            'phone' => $request['telefono'],
        ]);
        return response()->json([
            'code' => 1,
            'data' => $customer,
            'message' => ['¡Cliente agregado exitosamente!']
        ]);
    }

    public function show(Customer $customer)
    {
        if($customer)
        {
            if($customer->vetoed_id != null)
                return response()->json([
                    'code' => 1,
                    'data' => $customer,
                    'message' => ['¡Datos Autocompletados!',
                                  'El cliente con esta cedula se encuentra vetado' ]
                ]);
            else
                return response()->json([
                    'code' => 1,
                    'data' => $customer,
                    'message' => ['¡Datos Autocompletados!']
                ]);
        }
        // En reservaciones evitara el autocompletar
        return response()->json([
            'code' => 0,
            'data' => $customer,
            'message' => ['Cliente no existe']
        ]);
    }

    public function search_dni($cedula)
    {
        $cliente = Customer::where(strtoupper('dni'),'=',strtoupper($cedula))->first();
        if($cliente)
        {
            if($cliente->vetoed_id != null)
                return response()->json([
                    'code' => 1,
                    'data' => $cliente,
                    'message' => ['¡Datos Autocompletados!',
                                  'El cliente con esta cedula se encuentra vetado' ]
                ]);
            else
                return response()->json([
                    'code' => 1,
                    'data' => $cliente,
                    'message' => ['¡Datos Autocompletados!']
                ]);
        }
        // En reservaciones evitara el autocompletar
        return response()->json([
            'code' => 0,
            'data' => $cliente,
            'message' => ['Cliente no existe']
        ]);
    }

    public function update(Request $request, Customer $customer)
    {
        $this->validacion($request);
        $dni = $request['Cedula_Cliente'];
        $id = $customer->id;
        $exist = Customer::where(strtolower('dni'), '=', strtolower($dni))->where('id', '!=', $id)->first();
        if($exist)
        {
            return response()->json([
                'code' => 0,
                'message' => ['¡No se edito, La cedula se encuentra en uso por otro cliente!']
            ]);
        }

        $customer->dni = $request['Cedula_Cliente'];
        $customer->name = $request['Nombre'];
        $customer->phone = $request['telefono'];
        $customer->save();
        return response()->json([
            'code' => 1,
            'data' => $customer,
            'message' => ['¡Cliente editado exitosamente!']
        ]);
        
    }
    public function destroy($id)
    {
        $customer = Customer::find($id);
        if($customer)
        {
            $customer->delete();
            return response()->json([
                'code' => 1,
                'data' => $customer
            ]);
        }
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro']
            ]);
    }


    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'Cedula_Cliente'=>'required|max:30',
                'Nombre'=>'required|alpha_spaces|max:50',
                'telefono' => 'required|max:20',
            ];
        }
        $this->validate($objeto, $rule, $message);
    }

}
