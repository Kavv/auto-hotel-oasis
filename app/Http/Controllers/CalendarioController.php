<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Lesson;
use App\Reservation;
use App\Staff;
use App\Service;
use App\Status;

class CalendarioController extends Controller
{
    public function index()
    { 
        $customers = Reservation::join('customers', 'customers.id', '=', 'customer_id')
                            ->join('calendars', 'calendars.id', 'calendar_id')
                            ->join('lessons', 'lessons.id', 'calendars.lesson_id')
                            ->join('status', 'status.id', '=', 'status_id')
                            ->join('staff', 'staff.id', '=', 'lessons.staff_id')
                            ->select('customers.*', 'reservations.id as reservation', 'status.name as state', 'lessons.code', 
                            'staff.name as tname', 'staff.last_name as tlast', 'calendars.begin', 'calendars.end')
                            ->where('reservations.deleted_at', null)->get();
        $status = Status::all();
        return view('calendario.reservation_list',compact('customers', 'status'));
    }

    public function store(Request $request)
    {
        // Consultamos la lección original en la que el cliente se encuentra enlazado
        $original_lesson = Lesson::find($request['lesson']);
        // Obtenemos el día actual en el que se esta tabajando
        $actual_day = $request['day'];

        // Consultamos que el horario seleccionado si sea una de las opciones validas
        // Que concida con la leccion, disciplina y día con la que se esta trabajando
        $calendar = Lesson::join('calendars', 'lesson_id', '=', 'lessons.id')
                            ->where('lessons.id', '!=', $original_lesson->id)
                            ->where('discipline_id', $original_lesson->discipline_id)
                            ->where('calendars.day', $actual_day)
                            ->where('calendars.id', $request['calendar'])
                            ->select('calendars.*')
                            ->orderBy('calendars.begin', 'asc')
                            ->first();

        // Si se encontro un registro entonces es valido
        if($calendar)
        {
            // Buscamos el cliente correspondiente
            $customer = Customer::find(session('athlete'));
            if($customer)
            {
                // Creamos la reservacion del cliente con el horario solicitado
                // Esta tabla es una tabla puente/pivote
                Reservation::create([
                    'customer_id' => $customer->id,
                    'calendar_id' => $calendar->id,
                ]);
                return response()->json([
                    'code' => 1
                ]);
            }
            else
                return response()->json([
                    'code' => 1,
                    'msg' => ['No se encontro el cliente especificado']
                ]);

        }
        else
            return response()->json([
                'code' => 1,
                'msg' => ['Ocurrio un error! No se encontro el horario seleccionado']
            ]);
    }

    public function destroy($id)
    {
        $reserva = Reservation::find($id);
        $reserva->delete();

        return response()->json([
            'code' => 1
        ]);
    }

    public function schedules(Request $request) 
    {
        $day = $request['dia'];
        $lesson = Lesson::find($request['leccion']);
        $days = ['', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

        $base_schedule = Lesson::join('calendars', 'lesson_id', '=', 'lessons.id')
                            ->join('staff', 'lessons.staff_id', '=', 'staff.id')
                            ->where('lessons.id', '!=', $lesson->id)
                            ->where('discipline_id', $lesson->discipline_id)
                            ->where('calendars.day', $day)
                            ->select('calendars.day', 'calendars.begin', 'calendars.end', 
                            'staff.name as name_e', 'staff.last_name as last_name_e',
                            'calendars.id as id')
                            ->orderBy('calendars.begin', 'asc')
                            ->get();

        return view('calendario.recargable.otros_horarios', compact('base_schedule', 'days'));
    }

    public function reload()
    {
        $atleta = session('athlete');
        $customer = Customer::find($atleta);
        if($customer)
        {
            // Obtenemos la clase a la que fue asignado
            $lesson = $customer->lessons()->first();
            // Obtenemos los datos del profesor de la clase a la que fue asignado
            $staff = $lesson->staff()->first();
            // Obtenemos el horario base de la clase a la fue asignado
            $base_schedule = $lesson->calendars()->get();

            // Consultamos si tiene alguna reservacion en un horario diferente
            $res = $customer->calendars()
                            ->join('lessons', 'lesson_id', '=', 'lessons.id')
                            ->join('staff', 'staff_id', '=', 'staff.id')
                            ->where('reservations.deleted_at', null)
                            ->select('calendars.id', 'begin', 'end', 'day', 
                            'staff.name', 'staff.last_name', 'reservations.id as res')
                            ->orderBy('day', 'asc')
                            ->get();
            $days = ['', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
            
            return view('calendario.recargable.listar_schedule', compact('lesson', 'staff', 'base_schedule', 'days', 'res'));
        }     
    }

    public function my_calendar()
    {
        $atleta = session('athlete');
        if($atleta != null && $atleta != "")
        {
            $customer = Customer::find($atleta);
            if($customer)
            {
                // Obtenemos la clase a la que fue asignado
                $lesson = $customer->lessons()
                        ->join('disciplines', 'discipline_id', '=', 'disciplines.id')
                        ->select('lessons.*', 'disciplines.name as discipline')
                        ->first();
                // Obtenemos los datos del profesor de la clase a la que fue asignado
                $staff = $lesson->staff()->first();
                // Obtenemos el horario base de la clase a la fue asignado
                $base_schedule = $lesson->calendars()->get();

                // Consultamos si tiene alguna reservacion en un horario diferente
                $res = $customer->calendars()
                                ->join('lessons', 'lesson_id', '=', 'lessons.id')
                                ->join('staff', 'staff_id', '=', 'staff.id')
                                ->where('reservations.deleted_at', null)
                                ->select('calendars.id', 'begin', 'end', 'day', 
                                'staff.name', 'staff.last_name', 'reservations.id as res')
                                ->orderBy('day', 'asc')
                                ->get();
                $days = ['', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
                
                return view('calendario.index', compact('customer', 'lesson', 'staff', 'base_schedule', 'days', 'res'));
            }        
        }
        else
        {
            return redirect('/');
        }
        
    }
    public function my_work_calendar()
    {
        $dni_trainer = session('trainer');
        if($dni_trainer != null && $dni_trainer != "")
        {
            $trainer = Staff::find($dni_trainer);
            $is_trainer = $trainer->cargos()->where('positions.id', 1)->first();

            if($is_trainer)
            {
                // Obtenemos la clase a la que fue asignado
                $lessons = $trainer->lessons()
                        ->join('disciplines', 'discipline_id', '=', 'disciplines.id')
                        ->join('calendars', 'lesson_id', '=', 'lessons.id')
                        ->select('lessons.*', 'disciplines.name as discipline', 
                        'calendars.day', 'calendars.begin', 'calendars.end', 'calendars.id as calendar')
                        ->orderby('disciplines.name', 'asc')
                        ->orderby('calendars.day', 'asc')
                        ->get();
                
                        
                $days = ['', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
                
                return view('calendario.entrenador', compact('trainer', 'lessons', 'days'));
            }
        }
        // Si llega a este punto entonces significa que 
        // ocurrio un porbla al identificar el registro del entrenados
        return redirect('/');
    }
    public function show_athletes(Request $request) 
    {
        $id = $request['leccion'];
        $day = $request['dia'];
        
        $lesson = Lesson::find($id);
        if($lesson)
        {
            $customers_base = $lesson->customers()
                            ->orderBy('customers.name', 'asc')->get();
            $customers_extra = Customer::join('reservations as r', 'r.customer_id', '=', 'customers.id')
                                ->join('calendars', 'r.calendar_id', '=', 'calendars.id')
                                ->where('day', $day)
                                ->where('lesson_id', $id)
                                ->where('r.deleted_at', null)
                                ->select('customers.*', 'begin', 'end')
                                ->orderBy('customers.name', 'asc')
                                ->get();

            return view('calendario.recargable.listar_atletas', compact('customers_base', 'customers_extra'));
        }
    }

    public function restart(Request $request)
    {
        $id = $request['calendar'];
        $reservations = Reservation::where('calendar_id', $id)->get();
        if($reservations)
        {
            foreach ($reservations as $key => $reservation) {
                $reservation->delete();
            }
        }

        return response()->json([
            'code' =>  1
        ]);
    }

    public function schedule_athlete(Request $request)
    {
        $customer = Customer::find($request['cliente']);
        if($customer)
        {
            // Obtenemos la clase a la que fue asignado
            $lesson = $customer->lessons()
                    ->join('disciplines', 'discipline_id', '=', 'disciplines.id')
                    ->select('lessons.*', 'disciplines.name as discipline')
                    ->first();

            // Obtenemos los datos del profesor de la clase a la que fue asignado
            $staff = $lesson->staff()->first();
            // Obtenemos el horario base de la clase a la fue asignado
            $base_schedule = $lesson->calendars()->get();

            // Consultamos si tiene alguna reservacion en un horario diferente
            $res = $customer->calendars()
                            ->join('lessons', 'lesson_id', '=', 'lessons.id')
                            ->join('staff', 'staff_id', '=', 'staff.id')
                            ->where('reservations.deleted_at', null)
                            ->select('calendars.id', 'begin', 'end', 'day', 
                            'staff.name', 'staff.last_name', 'reservations.id as res')
                            ->orderBy('day', 'asc')
                            ->get();
            $days = ['', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
            return view('calendario.index', compact('customer', 'lesson', 'staff', 'base_schedule', 'days', 'res'));
        }
        // Si llega a este punto entonces significa que 
        // ocurrio un porbla al identificar el registro del atleta
        return redirect('/');
    }

    public function athlete_list()
    { 
        $clientes=Customer::join('status', 'status.id', '=', 'status_id')
                            ->leftJoin('customers_lessons', 'customers_lessons.customer_id', '=', 'customers.id')
                            ->leftJoin('lessons', 'customers_lessons.lesson_id', '=', 'lessons.id')
                            ->select('customers.*', 'status.name as state', 'lessons.code')->get();

        return view('calendario.calendars',compact('clientes'));
    }

    public function trainer_list()
    { 
        $trainers = Staff::join('positions_staff', 'staff_id', '=', 'staff.id')
                    ->where('position_id', '=', 1)
                    ->select('staff.*')->get();
        return view('calendario.entrenador_list',compact('trainers'));
    }

    public function set_athlete(Request $request)
    {
        $customer = Customer::find($request['cliente']);
        if($customer)
        {
            session(['athlete' => $customer->id]);
            return redirect("/mi_calendario");
        }
        else
            return redirect("/calendario");
    }
    public function set_trainer(Request $request)
    {
        $trainer = Staff::find($request['entrenador']);
        if($trainer)
        {
            session(['trainer' => $trainer->id]);
            return redirect("/mi_calendario_de_trabajo");
        }
        else
            return redirect("/calendario/entrenador");
    }
    
}
