<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\MultimediaWeb;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant
use Auth;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Environment;

class FrontController extends Controller
{
//    protected $Connection;

    public function __construct()
    {
        $this->middleware('auth',['only' => 'principal']);
    }
    public function index()
    {
        return view('index');
    }
    public function principal()
    {
        $logo = "/img/motivacion.jpg";
        
        return view('principal', compact("logo"));
    }
    
    public function landing()
    {
        
        $hostname = Hostname::find(1);
        $website = website::find(1);
        $environment = app(Environment::class);
        $environment->hostname($hostname);
        $environment->tenant($website);
        $title = 'Alquileres Santana';
        $videos = MultimediaWeb::where('categories_id', '=', '1')
            ->where('selection', '=' ,'1')
            ->get();
        if(count($videos) > 0)
            $url = $videos[0]->image_url;


        for($i = 2; $i<10; $i++) {
            $prueba[] = MultimediaWeb::where('categories_id', '=', $i)
                ->where('selection', '=', '1')
                ->where('order', '=','1')
                ->get()->toArray();
        }
        for($i=0;$i<=7;$i++)
        {
            $images[] = $prueba[$i][0]['image_url'];
        }

//        dd($images);
        $carusel = MultimediaWeb::where('categories_id', '=','10')
            ->where('selection', '=', '1')
            ->orderBy('order','asc')
            ->get();

        for($i=0;$i<=7;$i++)
        {
            $carus[] = $carusel[$i]['image_url'];
        }
//        dd($carus);

        $categories = Category::where('categories.id', '>', '1')
            ->where('categories.id', '<','10')
            ->get();
//        dd($categories);

        return view('landing3')->with('title',$title)
            ->with('videos',$url)
            ->with('images',$images)
            ->with('carus',$carus)
            ->with('categories',$categories);
    }

    public function show(Request $request)
    {
        //por algun extraño motivo no funciona sin eso(investigar luego)
        $data = $request->getContent();
        $camb = Category::where('slug', '=',$data)
                ->get();
        $ids = $camb[0]->id;

        $car_img = MultimediaWeb::select('image_url')
            ->where('categories_id', '=',$ids)
            ->where('selection','=','1')
            ->orderBy('order','asc')
            ->get();

//        dd($car_img);

        return $car_img;
    }

}