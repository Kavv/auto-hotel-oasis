<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vetoed;
use App\Customer;
use App\Staff;
use App\Item;
use App\Service;
use Caffeinated\Shinobi\Models\Role;
use App\Http\Requests\VetadoAdd;
use App\Discharged;
use DB;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant

class VetadoController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        //Enviamos el host y el website relacionado al usuario actual
//        $this->middleware(function ($request, $next) {
//            $this->Connection = new Connection(Auth::user()->hostname,Auth::user()->hostname->website);
//            return $next($request);
//        });
    }
    public function index($tipo='cliente',$view="vetado.indexc",$cedu=null)
    {
        //Si mostrase en la lista tambien la direccion, en el js no tendria que hacer una consulta al metodo descripcion ya que tendria los datos desde la misma tabla
        if($tipo == "cliente")
        {
            $vetados=Customer::where('vetoed_id','!=',null)->get();
        }
        if($tipo == "personal")
        {
            $vetados = Staff::where('vetoed_id','!=',null)->get();
        }
        $inventario = Item::all();
        $servicios = Service::orderBy('name','asc')->get();
        return view($view,compact('vetados','inventario','servicios'));

    }

    public function update($id,request $datos)//Actualizar la descripcion del vetado
    {
        $this->validacion($datos);
    
        $cliente = Customer::find($id);
        if($cliente)
        {
            $cliente->vetado()->update([ 'description' => $datos['descripcion'] ]);
            return 1;
        }
        else
            return "error";
    }
    
    public function listapersonal($view="vetado.createp")
    {
        /* $arreglo=[];
        $personalvetado=Vetoed::where('IdPersonal','!=',null)->get();//obtenemos la lista de clientes vetados
        foreach($personalvetado as $pv)//recorremos la lista con el fin de:
        {
            array_push($arreglo,$pv['IdPersonal']);//almacenar las cedulas de clientes en un arreglo
        }
        //ejecutamos una consulta diciendo que tomara todos los registros menos los que esten contenidos en el arreglo
        $personal = DB::table('personal')
                    ->where('deleted_at','=',null)
                    ->whereNotIn('Cedula_Personal', $arreglo)->get();
        return view($view,compact('personal')); */
        $personal = Staff::where('vetoed_id','=',null)->get();
        $inventario = Item::all();
        $servicios = Service::orderBy('name','asc')->get();
        return view($view,compact('personal','inventario','servicios'));
    }
    public function listacliente($view="vetado.createc")
    {
/*         $arreglo=[];
        $clientevetado=Vetoed::where('IdCliente','!=',null)->get();//obtenemos la lista de clientes vetados
        foreach($clientevetado as $cv)//recorremos la lista con el fin de:
        {
            array_push($arreglo,$cv['IdCliente']);//almacenar las cedulas de clientes en un arreglo
        }
        //ejecutamos una consulta diciendo que tomara todos los registros menos los que esten contenidos en el arreglo
        $clientes = DB::table('cliente')
                    ->where('deleted_at','=',null)
                    ->whereNotIn('Cedula_Cliente', $arreglo)->get(); */
        $clientes = Customer::where('vetoed_id','=',null)->get();
        $inventario = Item::all();
        $servicios = Service::orderBy('name','asc')->get();
        return view($view,compact('clientes','inventario','servicios'));
    }
    public function vetar(Request $datos)
    {
        
        $this->validacion($datos);
            
        if($datos->get("tipo")=="cliente")
        {

            // Obtenemos el cliente al cual se vetara
            $cliente = Customer::find($datos->get("id"));
            //Creamos el registro vetado y luego lo asociamos al cliente
            $cliente->vetado()->associate(Vetoed::create([
                'description' => $datos['descripcion']
            ]));
            $cliente->save();
            return 1;
        }
        return "error";
    }

    public function descripcion($tipo="cliente",$id=null)
    {
        $vetado = Customer::find($id);
        if($vetado)
        {
            $items = $vetado->discharged;
            return response()->json([
                'vetado' => $vetado,
                'items' => $items
                ]);
        }
        else
            return response()->json([
                'message' => ['Opss!... Ocurrio un error, vuelve a intentarlo!']
            ]);
    }
    public function destroy($id, request $datos)
    {
        $cliente = Customer::find($id);
        $cliente->vetado()->delete();
        $cliente->vetado()->dissociate();
        $cliente->save();
        return 1;
    }
    
    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'descripcion' => 'required|max:200'
            ];
        }
        $this->validate($objeto, $rule, $message);
    }
}
