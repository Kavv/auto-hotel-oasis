<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth;
use Redirect;
use App\Customer;
use App\Staff;

class LoginController extends Controller
{
    public function index()
    {
        
        $user = Auth::user();
        $logo = "";
        if($user)
        {
            $logo = "/img/motivacion.jpg";
        }
        if (!Auth::check()) 
            return view('login.login');
        else
            return view('principal', compact("logo"));

        
    }
    public function login(Request $request)
    {
        
        //Buscamos al usuario con email
        $user = User::where(strtolower('email'),'=', strtolower($request['username']))->first();
        //si existe entonces ...
        if($user)
        {
            //Verificamos si la contraseña es valida
            if (Hash::check($request['pass'], $user->password))
            {
                Auth::login($user);
                return response()->json([
                    "action" => 1
                ]);
            }
        }
        return response()->json([
            "action" => 0,
            "message" => "Nombre de usuario o contraseña incorrectos"
        ]);
    }

    public function login_atleta(Request $request)
    {
        
        $dni = $request['username'];
        $pass = $request['pass'];
        // Consultamos el cliente que tenga el dni especificado
        $customer = Customer::where('dni', 'ilike', $dni)->first();
        // En caso de existir
        if($customer)
        {
            // Verificamos si la contraseña coincide
            if(Hash::check($pass, $customer->password))
            {
                // Creamos una variable de sesión
                session(['trainer' => null]);
                session(['athlete' => $customer->id]);
                return response()->json([
                    'code' => 1
                ]);
            }
        }
        // Si llega hasta este punto es por que no cumple con alguna validación
        return response()->json([
            'code' => 2,
            'msg' => ['¡Credenciales no validas!']
        ]);
    }
    public function login_trainer(Request $request)
    {
        $dni = $request['username'];
        $pass = $request['pass'];
        // Consultamos el cliente que tenga el dni especificado
        
        $trainer = Staff::join('positions_staff', 'staff_id', '=', 'staff.id')
                    ->where('dni', 'ilike', $dni)
                    ->where('positions_staff.position_id', '=', 1)
                    ->select('staff.*')->first();
        
        // En caso de existir
        if($trainer)
        {
            // Verificamos si la contraseña coincide
            if(Hash::check($pass, $trainer->password))
            {
                // Creamos una variable de sesión
                session(['athlete' => null]);
                session(['trainer' => $trainer->id]);
                return response()->json([
                    'code' => 1
                ]);
            }
        }
        // Si llega hasta este punto es por que no cumple con alguna validación
        return response()->json([
            'code' => 2,
            'msg' => ['¡Credenciales no validas!']
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
