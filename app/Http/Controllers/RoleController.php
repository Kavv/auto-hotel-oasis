<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Caffeinated\Shinobi\Models\Role;
use App\Role;
use Caffeinated\Shinobi\Models\Permission;
use App\permission_rule;
use App\Rule;
use App\role_rule;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant
class RoleController extends Controller
{
    public function index()
    {
        //$permisos = Permission::all();
        $roles = Role::where('name','!=','SuperAdmin')->get();
        $permisos = Rule::all();
        $valor = null;
        return view('role.index',compact('roles','permisos','valor'));
    }

    public function create()
    {
        // Busca todos los permisos existentes
        $permisos = Rule::all();
        return view('role.create', compact('permisos'));
    }

    public function store(Request $request)
    {
        $this->validacion($request);

        // Consultamos si existe otro rol con el mismo nombre
        $existe = Role::where(strtolower('name'),'=', strtolower($request['nombre']))->get();
        
        // Si el valor es diferente a 0, enviamos un msj de error
        if(count($existe) != 0)
            return response()->json(['El nombre del role ya existe! ingrese un nombre diferente']);

        // Instanciamos un nuevo ROL
        $role = new Role;
        //Asignamos los datos generales del ROL
        $role->name = $request['nombre'];
        $role->slug = str_replace(" ", "_",strtolower($request['nombre']));
        $role->description = $request['descripcion'];
        $role->special = null;
        // Guardamos el ROL
        $role->save();
        
        // Las rules son un conjunto de permisos, lo que el usuario realmente ve son las Rules.
        // Obtenemos las rules que el usuario selecciono
        $rules = $request->get('permiso');
        // Si son mas de 0
        if(count($rules) > 0)
        {
            // Obtenemos los registros de los permisos que posee cada rule
            $permissions = permission_rule::select('permission_id')->whereIn('rule_id', $rules)->get();
            // Con pluck generamos un arreglo de los id de los permisos que estan en la colleccion "permissions"
            $permissions = $permissions->pluck("permission_id");
            // Gestionamos las relaciones entre el nuevo ROL y los permisos 
            $role->permissions()->sync($permissions); 
            // Gestionamos las relaciones entre el nuevo ROL y las rules 
            $role->grupos()->sync($rules); 
        }

        return 1;
    }

    public function show(Role $role)
    {
        return response()->json($role->grupos);
    }

    public function update(Request $request, Role $role)
    {
        $this->validacion($request);

        // Consultamos si existe otro rol con el mismo nombre obvianto el registro actual
        $existe = Role::where(strtolower('name'),'=',strtolower($request['nombre']))
                        ->where('id', '!=', $role['id'])->get();
                      
        // Si el valor es diferente a 0, enviamos un msj de error  
        if(count($existe) != 0)
            return response()->json(['No se edito! El nombre del rol esta en uso']);

        // Editamos la información general del ROL
        $role->name = $request['nombre'];
        $role->slug = str_replace(' ', '_', strtolower($request['nombre']));
        $role->description = $request['descripcion'];
        // Guardamos
        $role->save();
        
        // Las rules son un conjunto de permisos, lo que el usuario realmente ve son las Rules.
        // Obtenemos las rules que el usuario selecciono
        $rules = $request->get('permiso');
        // Si son mas de 0
        if(count($rules) > 0)
        {
            // Obtenemos los registros de los permisos que posee cada rule
            $permissions = permission_rule::select('permission_id')->whereIn('rule_id', $rules)->get();
            // Con pluck generamos un arreglo de los id de los permisos que estan en la colleccion "permissions"
            $permissions = $permissions->pluck("permission_id");
            // Gestionamos las relaciones entre el nuevo ROL y los permisos 
            $role->permissions()->sync($permissions); 
            // Gestionamos las relaciones entre el nuevo ROL y las rules 
            $role->grupos()->sync($rules); 
        }
        return 1;
    }


    public function destroy(Role $role)
    {
        $role->delete();
        return 1;
    }

    public function ver($valor=null)
    {
        $roles = Role::where('name','!=','SuperAdmin')->get();
        $permisos = Rule::all();
        return view('role.index',compact('roles','permisos','valor'));
    }

    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [                
                'nombre' => "required | max:191",
            ];
        }
        $this->validate($objeto, $rule, $message);
    }
}
