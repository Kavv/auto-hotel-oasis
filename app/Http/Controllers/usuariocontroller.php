<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\usuarioAdd;
use App\Http\Requests\usuarioupdate;
use Illuminate\Support\Facades\Hash;
//use Caffeinated\Shinobi\Models\Role;
use App\Role;
use Illuminate\Validation\Rule;
use Session;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant
class usuariocontroller extends Controller
{
    
    public function index($msj = null, $email = "")
    { 
        //"secret" es diferente a null unicamente si las credenciales son consideradas super secretas como las de los super admins
        $usuarios = User::where('secret', null)->get();
        if($msj == "1")
        {
            Session::flash('message','Usuario agregado exitosamente');
            Session::flash('tipo','info');
        }
        Session::flash('valor',$email);
        $roles = Role::where('name','!=','SuperAdmin')->get();
        return view('usuario.index',compact('usuarios','roles'));
    }
    public function create()
    {
        $roles = Role::where('name','!=','SuperAdmin')->get();
        return view('usuario.create', compact('roles'));
    }
    public function store(usuarioAdd $request)
    {
        if($request->get('password')==$request->get('confcontraseña'))
        {
            $usuario=User::create([
                'name'=>$request['name'],    
                'description'=> $request['description'],
                'password'=>Hash::make($request['password']),//encriptamos la contraseña
                'email'=>$request['userName'],
            ]);
            if($request['role']);
                $usuario->roles()->sync($request['role']);
            //$decrypted = decrypt($encryptedValue); ejemplo para desencripta 
            return 1;
        }
        else
            return 0;
    }

    public function update($id,usuarioupdate $request)
    {
        // Buscamos el usuario
        $usuario = User::find($id);
        $usuario['name'] = $request->get('name');
        $usuario['description'] = $request->get('description');
        //Si el correo original es diferente al recibido significa un cambio de correo
        if($usuario['email'] != $request->get('userName'))
        {
            //consultamos si ya existe
            $consulta = User::where(strtolower('email'),'=',strtolower($request->get('userName')))->first();
            if($consulta)
                return response()->json(['El nombre de usuario ingresado ya esta en uso']);
            else
                //si no existe el correo bien puede ser actualizado
                $usuario['email']=$request->get('userName');
        }
        
        // Si el check en la vista fue activado
        if($request->get('restablecerpass')=="1")
        {
            // Verificamos que la contraseña coincida con la confirmación
            if($request->get('password')==$request->get('confcontraseña'))
                $usuario['password'] = Hash::make($request->get('password'));
            else
                return response()->json(['La contraseña no coincidió con la confirmacion']);
        }
        // Sincronizamos los roles del usuario
        if($request['role'])
            $usuario->roles()->sync($request->get('role'));

        $usuario->save();
        return 1;
    }
    
    public function destroy($id)
    {
        $usuario=User::find($id);
        $usuario->delete();
        return 1;
    }

    public function show($id)
    {
        $usuario= User::find($id);
        return response()->json([$usuario->roles()->get()]);
    }
    

    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'Cedula_Cliente'=>'required|max:30',
                'Nombre'=>'required|alpha_spaces|max:30',
                'Apellido'=>'required|alpha_spaces|max:30',
                'Edad'=>'nullable|numeric',
                'Sexo'=>'nullable|alpha|max:15',
                'Direccion' => 'max:200'
            ];
        }
        $this->validate($objeto, $rule, $message);
    }
}
