<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\Category;
use App\Parking;
use App\Description;
use App\Item;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class HabitacionController extends Controller
{

    public function index()
    {
        $data = Room::where('rooms.deleted_at', null)
                ->join('categories as c', 'c.id', '=', 'category_id')
                ->leftJoin('descriptions', 'descriptions.id', '=', 'description_id')
                ->join('parking as p', 'p.id', '=', 'parking_id')
                ->select('rooms.*', 'c.name as category', 'c.amount_hour', 'c.amount_half', 'c.amount_day', 
                'descriptions.content', 'p.quantity', 'p.type', 'p.vip')
                ->get();
        $categories = Category::all();
        $parking = Parking::all();
        // Service_id == 1 es equivalente al servicio de "Habitación"
        $products = Item::where('service_id', 1)->get();
        $type = ["", 'Motocicleta', 'Carro', 'Camioneta', 'Varios'];
        $vip = ['General', 'Privado'];
        return view('habitacion.index', compact('data', 'categories', 'parking', 'type', 'vip', 'products'));
    }
    
    public function store(Request $request)
    {

        $this->validacion($request);
        // Verificamos si existe un registro con este nombre
        $exist = Room::where('name', $request['nombre'])->first();

        if($exist)
            return response()->json([
                'code' => 0,
                'msg' => ['El nombre de esta habitacion ya esta en uso']
            ]);

        $room = Room::create([
            'name' => $request['nombre'],
            'people' => $request['personas'],
            'category_id' => $request['categoria'],
            'parking_id' =>  $request['parqueo'],
        ]);

        if($request['descripcion'] != "")
        {
            $description = Description::create([
                'content' => $request['descripcion']
            ]);
            $room->description_id = $description->id;
            $room->save();
        }
        
        if($request['portada'] != "")
        {
            // El nombre de la imagen sera conformado por el id de la habitación + _ + nombre de la habitación + .jpg
            $new_name = $room->id."_".$request['nombre']. ".jpg";
            $url = Storage::putFileAs('public', $request['portada'], $new_name);

            // Guardamos el nombre de la imagen en el registo recien creado
            $room->image = $new_name;
            $room->save();
        }
        
        if(isset($request['productos']))
            $room->items()->sync($request['productos']);

        // Este codigo es para verificar si el fichero existe, de momento esta en desuso
        /* if (file_exists('storage/'.$new_name)) {
            echo "El fichero $new_name existe";
        } else {
            echo "El fichero $new_name no existe";
        } */

        return response()->json([
            'code' => 1,
            'data' => $room,
        ]);
    }
    public function show($id)
    {
        $room = Room::find($id);
        $room->description = $room->description()->first();
        $room->items = $room->items()->get();
        if($room)
            return response()->json([
                'code' => 1,
                'data' => $room
            ]);
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro']
            ]);
    }
    public function update(Request $request, $id)
    {
        $this->validacion($request);
        // Buscamos la habitación a editar
        $room = Room::find($id);
        if($room)
        {
            $room->name = $request['nombre'];
            $room->category_id = $request['categoria'];
            $room->parking_id = $request['parqueo'];

            // Si el id de la descripcion existe entonces
            if($request['description_id'] != "")
            {
                // Consultamos la descripcion correspondiente
                $description = Description::find($request['description_id']);
                // En caso de existir actualizamos el contenido
                if($description)
                    $description->content = $request['descripcion'];
                // Caso contrario creamos el registro de una nueva descripción
                else
                    $description = Description::create([
                        'content' => $request['descripcion']
                    ]);
            }
            else
            {
                // Si el la descripcion es diferente a vacio entonces creamos el registro de una nueva descripción
                if($request['descripcion'] != "")
                    $description = Description::create([
                        'content' => $request['descripcion']
                    ]);
            }
            // Si existe el valor de una descripción, entonces lo asignamos al registro original
            if(isset($description->id))
                $room->description_id = $description->id;

            // Si se ha especificado algun valor
            if($request['portada'] != "")
            {
                // El nombre de la imagen sera conformado por el id de la habitación + _ + nombre de la habitación + .jpg
                $new_name = $room->id."_".$request['nombre']. ".jpg";
                // Guardamos la nueva imagen, si ya reemplaza 
                $url = Storage::putFileAs('public', $request['portada'], $new_name);
                // Asignamos el nuevo nombre que tienen la imagen
                $room->image = $new_name;
            }
            // Guardamos todos los cambios del registro
            $room->save();
            
            // Sincronizamos los productos que estan se han asociado a la habitación
            if(isset($request['productos']))
                $room->items()->sync($request['productos']);
            else
                $room->items()->sync([]);
            
            return response()->json([
                'code' => 1,
                'data' => $room
            ]);
        }
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro']
            ]);

    }
    public function destroy($id)
    {
        $room = Room::find($id);
        if($room)
        {
            $room->delete();
            return response()->json([
                'code' => 1,
                'data' => $room
            ]);
        }
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro']
            ]);
    }
    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'nombre' => 'required',
                'personas' => 'required',
                'categoria' => 'required',
                'parqueo' => 'required',
                'descripcion' => 'max:200',
                'portada' => 'max:5000|mimes:jpeg,png,jpg,webp'
            ];
        }
        $this->validate($objeto, $rule, $message);
    }
}
