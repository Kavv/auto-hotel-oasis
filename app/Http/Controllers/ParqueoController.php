<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parking;
use App\Repositories\ParkingRepository;
use App\Http\Requests\ParkingSave;

class ParqueoController extends Controller
{
    private $parkingRepo;
    public function __construct(ParkingRepository $parkingRepo)
    {
        $this->parkingRepo = $parkingRepo;
    }

    public function index()
    {
        $parking = $this->parkingRepo->all();
        return view('parqueo.index', compact('parking'));
    }
    public function create()
    {
        return view('parqueo.create');
    }
    public function store(ParkingSave $request)
    {
        $parking = new Parking($request->all());
        $parking = $this->parkingRepo->save($parking);
        return response()->json([
            'code' => 1,
            'data' => $parking
        ]);
    }
    public function show($id)
    {
        $parking = $this->parkingRepo->get($id);
        if($parking)
            return response()->json([
                'code' => 1,
                'data' => $parking
            ]);
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro']
            ]);
    }
    public function update(ParkingSave $request, Parking $parking)
    {
        if($parking)
        {
            $parking->fill($request->all());
            $this->parkingRepo->save($parking);
            
            return response()->json([
                'code' => 1,
                'data' => $parking
            ]);
        }
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro']
            ]);

    }

    public function destroy(Parking $parking)
    {
        if($parking)
        {
            $parking = $this->parkingRepo->delete($parking);
            return response()->json([
                'code' => 1,
                'data' => $parking
            ]);
        }
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro']
            ]);
    }
    

    public function real_time()
    {
        $general_parking = $this->parkingRepo->real_time();
        $type = ["", 'Motocicleta', 'Carro', 'Camioneta', 'Varios'];
        return view('parqueo.parqueo', compact('general_parking', 'type'));
    }

    public function update_quantity(Request $request)
    {
        $id = $request['parking'];
        $general_parking = $this->parkingRepo->get($id);
        if($general_parking)
        {
            $general_parking->temp = $request['quantity'];
            $this->parkingRepo->save($general_parking);
            return response()->json([
                'code' => 1,
                'data' => $general_parking,
            ]);
        }
        return response()->json([
            'code' => 0,
            'msg' => ['Esta ocurriendo un error, contactar al TI']
        ]);
    } 
}
