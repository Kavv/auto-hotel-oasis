<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ArticuloAdd;
use App\Http\Requests\ArticuloUpdate;
use App\Item;
use App\Service;
use Session;
use DB;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant
class InventarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //Enviamos el host y el website relacionado al usuario actual
//        $this->middleware(function ($request, $next) {
//            $this->Connection = new Connection(Auth::user()->hostname,Auth::user()->hostname->website);
//            return $next($request);
//        });
    }
    public function index($msj=null,$filtro=null,$nombre="")
    { 
        $inventario=Item::all();
        if($msj=="1")
        {
            Session::flash('message','Articulo agregado exitosamente');
            Session::flash('tipo','info');
        }
        Session::flash('valor',$nombre);
        $servicios=Service::orderBy('name','asc')->get();
        return view('inventario.index',compact('inventario','servicios'));
    }
    public function create()
    {
        $servicios=Service::orderBy('name','asc')->get();
        return view('inventario.create',['message'=>"",'servicios'=>$servicios]);
    }
    public function store(Request $request)
    { 
        $this->validacion($request);
        $servicios = Service::find($request['ID_Servicio']);
        if(!$servicios)
            return response()->json(['Es necesario especificar un servicio existente']);

        $item = Item::where(strtolower('name'), strtolower($request['Nombre']))
                    ->where('service_id',$request['ID_Servicio'])->first();
        if($item)
            return response()->json(['El nombre del articulo ya existe! ingrese uno diferente']);

        $item = Item::create([
            'service_id' => $request['ID_Servicio'],    
            'name' => $request['Nombre'],    
            'quantity' => $request['Cantidad'],
            'rental_price' => ($request['Costo_Alquiler']),
            'buy_price' => $request['Costo_Objeto'],
        ]);
        
        return response()->json([
            'code' => 1,
            'data' => $item,
        ]);
    }

    public function destroy($id)
    {
        $articulo=Item::find($id);
        $articulo->delete();
        return 1;
    }

    public function show(Item $item)
    {
        if($item)
            return response()->json([
                'code' => 1,
                'data' => $item,
            ]);
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro'],
            ]);


    }

    public function update($id, Request $request)
    {
        $this->validacion($request);
        //Verificamos que se ha ingresado un servicio
        $servicios = Service::find($request['ID_Servicio']);
        if(!$servicios)
            return response()->json([
                'code' => 0,
                'msg' => ['Es necesario especificar un servicio existente']
            ]);
            
        $item = Item::where(strtolower('name'), strtolower($request['Nombre']))
                    ->where('service_id',$request['ID_Servicio'])
                    ->where('id','!=', $id)->first();
        if($item)
            return response()->json([
                'code' => 0,
                'msg' => ['El nombre del articulo ya existe! ingrese uno diferente']
            ]);


        $articulo = Item::find($id);
        if(strcasecmp($articulo['name'], $request['Nombre']) === 0)
        {
            $articulo['service_id'] = $request['ID_Servicio'];
            $articulo['name'] = $request['Nombre'];
            $articulo['quantity'] = $request['Cantidad'];
            $articulo['rental_price'] = $request['Costo_Alquiler'];
            $articulo['buy_price'] = $request['Costo_Objeto'];
            $articulo->save();
        }
        else
        {
            $consulta = Item::where(strtolower('name'), strtolower($request['Nombre']))
                            ->where('service_id',$request['ID_Servicio'])->get();
            if(count($consulta)==0)
            {
                $articulo['service_id'] = $request['ID_Servicio'];
                $articulo['name'] = $request['Nombre'];
                $articulo['quantity'] = $request['Cantidad'];
                $articulo['rental_price'] = $request['Costo_Alquiler'];
                $articulo['buy_price'] = $request['Costo_Objeto'];
                $articulo->save();
            }
            else
                return response()->json([
                    'code' => 0,
                    'msg' => ['Este nombre ya se encuentra en uso en el mismo servicio']
                ]);
        }

        return response()->json([
            'code' => 1,
            'data' => $articulo
        ]);
    }

    public function serviceList()
    {
        return response()->json(Service::orderBy('name','asc')->get());
    }

    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'Nombre'=>'required|max:100',
                'Cantidad'=>'nullable|numeric',
                'Costo_Alquiler'=>'nullable|numeric',
                'Costo_Objeto'=>'nullable|numeric',
            ];
        }
        $this->validate($objeto, $rule, $message);
    }

}