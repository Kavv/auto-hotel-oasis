<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use App\Descripcion;
use App\Service;
use App\Item;
use App\DesRe;
use App\Customer;
use App\Menu;
use App\invenDes;
use App\State;
use App\Room;
use App\Http\Requests\AddReserva;
use App\Http\Requests\MenuAdd;
use Redirect;
use PDF;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant


class ReservacionController extends Controller
{
    public function index()
    {
        return view('reserva.index');
    }
    public function create()
    {
        $rooms = Room::Where('rooms.deleted_at', null)
        ->join('categories as c', 'c.id', '=', 'rooms.category_id')
        ->Leftjoin('descriptions as d', 'd.id', '=', 'rooms.description_id')
        ->join('parking as p', 'p.id', '=', 'rooms.parking_id')
        ->select('rooms.*', 
        'c.amount_hour', 'c.amount_half', 'c.amount_day', 'c.name as category',
        'p.quantity', 'p.type', 'p.vip', 'p.temp',
        'd.content')
        ->orderBy('category_id', 'asc')->get();
        return view('reserva.create', compact('rooms'));
    }
    
    public function store(Request $request)
    {

        // FALTA HACER LA VALIDACIÓN DE DISPONIBILIDAD POR ULTIMA VEZ
        $this->validacion($request);


        // Obtenemos el cuarto que se esta reservando
        $room = Room::find($request['cuarto']);
        // En caso de que exista
        if($room)
        {
            // La función free_room nos regresa la info del cuarto en caso de que este disponible
            $room_available = $this->free_room($request['start'], $request['end'], $room->id);
            // Si el cuarto no esta disponible regresamos un msj de error
            if(!$room_available)
            {
                return response()->json([
                    'code' => 2,
                    'msg' => ['Este cuarto ya no se encuentra disponible, posibles motivos:', 
                            '1) Otro administrador la reservo antes',
                            '2) Algo esta mal en los datos enviados']
                ]);
            }
        }
        else
            return response()->json([
                'code' => 2,
                'msg' => ['Ocurrio un error, no se encontro el cuarto especificado']
            ]);

        // Verificamos si el cliente ya existe en la BD
        $customer = Customer::where(strtolower('dni'), strtolower($request['cedula']))->first();
        // En caso de no existir
        if(!$customer)
        {
            // Verificamos si el input "nombre" existe y es diferente a vacio
            if(isset($request['nombre']) && $request['nombre'] != "")
            {
                // Para proceder a registrar al nuevo usuario
                $pass_default = Hash::make('HotelOasis');
                $customer = Customer::create([
                    'dni' => $request['cedula'],
                    'name' => $request['nombre'],
                    'phone' => $request['telefono'],
                    'password' => $pass_default,
                ]);
            }
            else
            {
                // En caso de que el nombre no fuese digitado, se regresa un msj de forma de error
                return response()->json([
                    'code' => 2,
                    'msg' => ['Debe especificar el nombre del nuevo cliente']
                ]);
            }
        }
        else
        {
            // Si el cliente existe verificamos si el numero de telefono es diferente y lo actualizamos
            // PROPUESTA: ASIGNAR EL NUMERO DE TELEFONO A LA RESERVACIÓN
            if($customer->phone != $request['telefono'])
            {
                $customer->phone = $request['telefono'];
                $customer->save();
            }
        }
        
        // Incio de validación de las fechas
        $begin = $request['inicio'];
        $end = $request['fin'];
        // Convertimos la cadena de texto a tiempo para poder formatear la fecha
        $b_aux = strtotime($begin);
        $e_aux = strtotime($end);



        
        // Si el valor de fecha de inicio es mayor a la fecha de finalización se retorna un error
        if($b_aux >= $e_aux)
            return response()->json([
                'code' => 2,
                'msg' => ["La fecha de inicio es mayor o igual a la fecha de fin"],
            ]);

        // Consultamos si fechas existe en el calendario (ejemplo el 30 de febrero es una fecha que no existe)
        $begin_is_valid = checkdate(date('m',$b_aux),date('d',$b_aux),date('Y',$b_aux));
        $end_is_valid = checkdate(date('m',$e_aux),date('d',$e_aux),date('Y',$e_aux));

        // Si alguna validación es equivalente a falso retornamos un msj de error
        if(!$begin_is_valid || !$end_is_valid || !$b_aux || !$e_aux)
            return response()->json([
                'code' => 2,
                'msg' => ["Una de las fechas no es valida"],
            ]);

        $start = date('Y-m-d H:i',$b_aux);
        $end = date('Y-m-d H:i',$e_aux);

        // Obtenemos el id del usuario(administrador) que esta haciendo la reserva
        $digitizer = auth()->id();

        // Una vez aprobadas todas las validaciones
        // Procedemos a crear el registro de la reservación asignando el estado 2(Reservada)
        $reservation = Reservation::create([
            'start' => $start,
            'end' => $end,
            'customer_id' => $customer->id,
            'state_id' => 2,
            'room_id' => $request['cuarto'],
            'subtotal' => $request['subtotal'],
            'tax' => $request['iva'],
            'discount' => $request['descuento'],
            'total' => $request['total'],
            'user_id' => $digitizer
        ]);

        return response()->json([
            'code' => 1,
            'data' => $reservation
        ]);
    }
    public function show($id)
    {
        $reservation = Reservation::find($id);
        if($reservation)
        {
            $room = Room::join('categories as c', 'c.id', '=', 'rooms.category_id')
                    ->Leftjoin('descriptions as d', 'd.id', '=', 'rooms.description_id')
                    ->join('parking as p', 'p.id', '=', 'rooms.parking_id')
                    ->select('rooms.*', 
                    'c.amount_hour', 'c.amount_half', 'c.amount_day', 'c.name as category',
                    'p.quantity', 'p.type', 'p.vip', 'p.temp',
                    'd.content')
                    ->where('rooms.id', $reservation->room_id)
                    ->first();
            $data = $this->process_time($room, $reservation->start, $reservation->end);
            $customer = $reservation->customer;
            $states = State::all();
            return view('reserva.formulario.detail', compact('reservation', 'states', 'room', 'data', 'customer'));
        }
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro esta reservación']
            ]);
    }
    public function update(Request $request, $id)
    {
        // Validación estandar del formulario
        $this->validacion($request);

        // Definimos cual input definira la fecha con la que se trabajará
        if(isset($request['new_start']) || isset($request['new_end']))
        {
            $s = $request['new_start'];
            $e = $request['new_end'];
        }
        else
        {
            $s = $request['inicio'];
            $e = $request['fin'];
        }
        // Hace diversas validaciones para corroborar que las fechas son correctas
        // En caso de aprobar las validaciones, retorna la fecha de inicio y de fin ya formateadas (YYYY-MM-DD H:i)
        $obj = $this->validate_dates($s, $e);
        // Verificamos por ultima vez antes de guardar si realmente esta disponible la habitación
        $room = $this->free_room($obj->start, $obj->end, $request['cuarto'], $id);
        //  Si es falso entonces retornamos un msj de error
        if(!$room)
            return response()->json([
                'code' => 0,
                'msg' => ['Esta habitación ya no se encuentra disponible en este horario '. $obj->start ." - ". $obj->end]
            ]);
        
        // Obtenemos la reservación
        $reservation = Reservation::find($id);
        // Verificamos si el cliente ya existe en la BD
        $customer = Customer::where(strtolower('dni'), strtolower($request['cedula']))->first();
        // En caso de no existir
        if(!$customer)
        {
            // Verificamos si el input "nombre" existe y es diferente a vacio
            if(isset($request['nombre']) && $request['nombre'] != "")
            {
                // Para proceder a registrar al nuevo usuario
                $pass_default = Hash::make('HotelOasis');
                $customer = Customer::create([
                    'dni' => $request['cedula'],
                    'name' => $request['nombre'],
                    'phone' => $request['telefono'],
                    'password' => $pass_default,
                ]);
            }
            else
            {
                // En caso de que el nombre no fuese digitado, se regresa un msj de forma de error
                return response()->json([
                    'code' => 2,
                    'msg' => ['Debe especificar el nombre del nuevo cliente']
                ]);
            }
        }
        else
        {
            // Si el cliente existe verificamos si el numero de telefono es diferente y lo actualizamos
            // PROPUESTA: ASIGNAR EL NUMERO DE TELEFONO A LA RESERVACIÓN
            if($customer->phone != $request['telefono'])
            {
                $customer->phone = $request['telefono'];
                $customer->save();
            }
        }

        $state = State::find($request['state_re']);
        // En caso de que el estado sea falso, significa que no existe el estado especificado
        if(!$state)
        {
            return response()->json([
                'code' => 2,
                'msg' => ['¡El estado especificado no existe!']
            ]);
        }
        // Obtenemos el id del usuario(administrador) que esta haciendo la reserva
        $digitizer = auth()->id();
        // Si se encontro la reservación actualizamos los datos
        if($reservation)
        {
            $reservation->start = $obj->start;
            $reservation->end = $obj->end;
            $reservation->customer_id = $customer->id;
            $reservation->state_id = $state->id;
            $reservation->room_id = $request['cuarto'];
            $reservation->subtotal = $request['subtotal'];
            $reservation->tax = $request['iva'];
            $reservation->discount = $request['descuento'];
            $reservation->total = $request['total'];
            $reservation->user_id = $digitizer;
            $reservation->save();
            
            return response()->json([
                'code' => 1,
                'data' => $reservation
            ]);
        }
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro']
            ]);

    }

    private function validate_dates($b_aux, $e_aux, $format = true)
    {
        // Si una de las fechas esta vacio, retornamos un error
        if($b_aux == "" || $e_aux == "")
            abort(response()->json([
                'code' => 2,
                'msg' => ["Debe especificar un rango de fecha validox"],
            ]), 200);

        // $format nos auxilia para saber si los parametros necesitan ser formateados
        if($format)
        {
            // Convertimos la cadena de texto a tiempo para poder formatear la fecha
            $b_aux = strtotime($b_aux);
            $e_aux = strtotime($e_aux);
        }
        // Si el valor de fecha de inicio es mayor a la fecha de finalización se retorna un error
        if($b_aux >= $e_aux)
            abort(response()->json([
                'code' => 2,
                'msg' => ["La fecha de inicio es mayor o igual a la fecha de fin"],
            ]), 200);

        // Consultamos si fechas existe en el calendario (ejemplo el 30 de febrero es una fecha que no existe)
        $begin_is_valid = checkdate(date('m',$b_aux),date('d',$b_aux),date('Y',$b_aux));
        $end_is_valid = checkdate(date('m',$e_aux),date('d',$e_aux),date('Y',$e_aux));
        // Si alguna validación es equivalente a falso retornamos un msj de error
        if(!$begin_is_valid || !$end_is_valid || !$b_aux || !$e_aux)
            abort(response()->json([
                'code' => 2,
                'msg' => ["Una de las fechas no es valida"],
            ]), 200);
        
        $start = date('Y-m-d H:i',$b_aux);
        $end = date('Y-m-d H:i',$e_aux);

        $object = new \stdClass();
        $object->start = $start;
        $object->end = $end;
        return $object;
    }

    public function destroy($id)
    {
        $reservation = Reservation::find($id);
        if($reservation)
        {
            $reservation->delete();
            return response()->json([
                'code' => 1,
                'data' => $reservation,
                'msg' => ['Se ha cancelado la reservación']
            ]);
        }
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro']
            ]);
    }
    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'cedula'=>'required|max:30',
                'nombre'=>'max:50',
                'telefono'=>'required|max:20',
                'subtotal'=>'required|numeric',
                'iva'=>'nullable|numeric',
                'descuento'=>'nullable|numeric',
                'total'=>'required|numeric',
                'inicio' => 'required',
                'fin' => 'required',
            ];
        }
        $this->validate($objeto, $rule, $message);
    }

    public function availability(Request $request)
    {
        // Modificamos el rango de fecha especificado por Regala de negocio "Limpieza 20min"
        // Ej: el cuarto se reserva de 8-10am
        // Si consultamos el rango de 10-12m sera valido pero enrealidad posterior a las 2h de uso
        // Hay 20 min de limpieza por lo tanto no deberia estar disponible
        // Por ello a la hora de inicio se le restan 20 min y se suman 20min a la hora de fin
        // Para así verificar que no se montara una reserva sobre otra
        $aux1 = strtotime ( '-20 minute' , strtotime($request['start']) );
        $aux2 = strtotime ( '+20 minute' , strtotime($request['end']) );

        // En caso de aprobar las validaciones, retorna la fecha de inicio y de fin ya formateadas (YYYY-MM-DD H:i)
        $obj = $this->validate_dates($aux1, $aux2, false);

        //Convertimos a un formato reconocido por sql (año-mes-día hora:minutos)
        $start = $obj->start;
        $end = $obj->end;


        // Info auxiliar para condicional del estado
        // < 5 mostrara las reservas con todos los estados disponibles 1,2,3,4
        $state = 5;
        $conditional = "<";
        if(isset($request['state']) && $request['state'] > 0)
        {
            $state = $request['state'];
            $conditional = "=";
        }
        
        // Seleccionamos los cuartos que poseen reservaciones en el rango de tiempo especificado
        // Cuando el estado es diferente a 3
        // Cuando las fechas de reservación estan en el rango de tiempo especificado 
        $reservations = Reservation::join('rooms', 'reservations.room_id', '=', 'rooms.id')
        ->join('categories as c', 'c.id', '=', 'rooms.category_id')
        ->leftJoin('descriptions as d', 'd.id', '=', 'rooms.description_id')
        ->join('parking as p', 'p.id', '=', 'rooms.parking_id')
        ->join('customers as cu', 'cu.id', '=', 'reservations.customer_id')
        ->join('states as s', 's.id', '=', 'reservations.state_id')
        ->select('rooms.*', 
        'c.amount_hour', 'c.amount_half', 'c.amount_day', 'c.name as category',
        'p.quantity', 'p.type', 'p.vip', 'p.temp',
        'd.content',
        'reservations.start', 'reservations.end', 'reservations.id as reservation', 'reservations.state_id',
        'cu.name as customer', 'cu.dni', 'cu.phone',
        's.name as state')
        ->where('reservations.state_id', $conditional, $state)
        ->where(function($query) use ($start, $end){
            $query->WhereBetween('reservations.start', [$start, $end])
                  ->orWhereBetween('reservations.end', [$start, $end]);
        })
        ->orderBy('cu.name', 'asc')->get();

        // UTILIZAR PLUCK PARA GENERAR LA LISTA Y AGREGARLA A LA CONSULTA DE LAS HABITACIONES DISPONIBLES
        $exclude = $reservations->pluck('id');
        // Si el parametro only_reservation existe entonces no hace el proceso de retornar los cuartos disponibles
        if(!isset($request['only_reservation']))
        {
            //Cuartos disponibles
            $rooms = $this->room(null, $exclude, true);
            // Retornamos una vista con las habitaciones disponibles y ocupadas
            return view('reserva.recargable.disponible', compact('rooms', 'reservations'));
        }
        else
        {
            return view('reserva.recargable.only_reservation', compact('reservations'));
        }

    }


    function confirm(Request $request)
    {
        // ID del cuarto
        $room_id = $request['room'];
        // Si este valor es vacio, inferior o igual a 0 entonces retornamos un error
        if($room_id == "" && $room_id <= 0)
        {
            return response()->json([
                'code' => 0,
                'msg' => ['El código de la habitación no es valido']
            ]);
        }
        // La funcion free_room nos retorna Falso si encuentra que la habitacion ya se encuentra ocupado en el lapso de tiempo
        // En caso de que no este ocupada entonces retorena los datos de la habitación
        // Si el valor json existe en los coleccion de datos recibida entonces
        if(isset($request['json']))// Enviamos el rango de fecha, la habitacion y la reservion para generar la consulta correspondiente
            $room = $this->free_room($request['start'], $request['end'], $room_id, $request['reservation']);
        else// Enviamos el rango de fecha y la habitacion para generar la consulta correspondiente
            $room = $this->free_room($request['start'], $request['end'], $room_id);

        //var_dump($room); exit();

        //  Si es falso entonces retornamos un msj de error
        if(!$room)
        {
            return response()->json([
                'code' => 0,
                'msg' => ['Esta habitación ya no se encuentra disponible en este horario '. $request['start'] ." - ". $request['end']]
            ]);
        }
        // process_time retorna los datos relevante al costo de la reservación en base al tiempo
        $data = $this->process_time($room, $request['start'], $request['end']);

        // Si json existe entonces retornamos la infromación en un formato JSON
        if(isset($request['json']))
            return response()->json([
                'code' => 1,
                'msg' => ['Esta habitación esta disponible en el horario solicitado '. $request['start'] ." - ". $request['end']],
                'data' => $data,
                'room' => $room
            ]);
        else// Caso contrario, retornamos una vista que se genera en base a los datos recolectados "room" y "data"
            return view('reserva.formulario.datos', compact('room', 'data'));
    }

    function process_time($room, $start, $end)
    {
        
        // Utilizamos un formateo diferente con el que podemos utilizar la funcion diff
        // Dicha función nos retorna la diferencia de tiempo entre dos fechas
        $aux_start = new \dateTime($start);
        $aux_end = new \dateTime($end);
        $i = $aux_start->diff($aux_end);
 
        // Arreglo que posee la diferencia de días, horas, minutos 
        $duration = [$i->format('%d'), $i->format('%h'), $i->format('%i')];
        // Precio por minuto (1 min)
        $price_minute = $room->amount_hour/60;
        // El precio por hora (60 min)
        $price_hour = $room->amount_hour;
        // Precio por medio día (12 h)
        $price_half = $room->amount_half;
        // Precio por día completo (24 h)
        $price_day = $room->amount_day;

        // Total de costo por día = La cantidad de dias x el precio por día
        $t_day = round($duration[0] * $price_day, 2);
        // Total de costo por hora 
        // Inicializamos en 0
        $t_hour = 0; 
        // El restante es inicializado con el valor de las horas de diferencia 
        $remaining = $duration[1];
        // Inicializamos la variable que controla la cantidad de horas que contempla "1/2 día"
        $half_day = 12;

        // Es importante mencionar que cada (1/2 día) se cobran al valor correspondiente de 1/2 día 
        // y las horas restantes se cobran como horas extras (precio x hora).

        // Mientras el restante no sea 0
        while($remaining > 0)
        {
            // Si el restante es superior o igual a 12h entonces el se aplicara la siguiente formula
            if($remaining >= $half_day)
            {
                // Costo total por horas += el precio de 1/2 día
                $t_hour += $price_half;  
                // A la variable que controla el restante se le resta (1/2 día) 
                $remaining -= $half_day;
            }
            // Si el restante es inferior a (1/2 día) se aplica la siguiente formula
            else
            {
                // costo total por horas += cantidad de horas restantes x precio por hora
                $t_hour += $remaining * $price_hour;  
                // El restante se asigna a 0
                $remaining = 0;
            }
        }
        $t_hour = round($t_hour, 2);
        // Costo por minutos = cantidad de minutos x precio por minuto
        $t_min = round($duration[2] * $price_minute, 2);

        // La suma de los 3 totales es equivalente al subtotal (teorico)
        $subtotal = round($t_day + $t_hour + $t_min, 2);
        // Iva
        $tax = round($subtotal * 0.15, 2);
        $amount = round($subtotal + $tax, 2);

        // Creamos un objeto para retornar toda la infomación
        $object = new \stdClass();
        $object->start = $aux_start->format('d-m-Y h:i a');
        $object->end = $aux_end->format('d-m-Y h:i a');
        $object->duration = $duration;
        $object->t_day = $t_day;
        $object->t_hour = $t_hour;
        $object->t_min = $t_min;
        $object->subtotal = $subtotal;
        $object->tax = $tax;
        $object->amount = $amount;

        return $object;
    }

    function free_room($start, $end, $id, $reservation = null)
    {
        // A la fecha le restamos y sumamos 20 equivalente a la limpiesa previa o posterior
        $aux1 = strtotime ( '-20 minute' , strtotime($start) );
        $aux2 = strtotime ( '+20 minute' , strtotime($end) );
        // Reformatiamos
        $start = date('Y-m-d H:i',$aux1);
        $end = date('Y-m-d H:i',$aux2);
        // Iniciamos el proceso de la consulta
        // Retorna un registro en caso de identificar una reserva de esta misma habitación en el rango de tiempo solicitado
        // Que tengan estado "solicitud" o "reservada"
        $exist = DB::table('rooms') 
        ->join('categories as c', 'c.id', '=', 'rooms.category_id')
        ->Leftjoin('descriptions as d', 'd.id', '=', 'rooms.description_id')
        ->join('parking as p', 'p.id', '=', 'rooms.parking_id')
        ->leftJoin('reservations as r', 'rooms.id', '=', 'r.room_id')
        ->select('rooms.*', 
        'c.amount_hour', 'c.amount_half', 'c.amount_day', 'c.name as category',
        'p.quantity', 'p.type', 'p.vip', 'p.temp',
        'd.content',
        'r.start', 'r.end', 'r.state_id')
        ->where(function($query) use ($start, $end, $id) {
            $query->Where('rooms.id', $id);
            $query->Where('rooms.deleted_at', null);
            $query->Where('r.state_id', '<', 3);
            $query->whereNull('r.deleted_at');
            $query->where(function($query) use ($start, $end) {
                $query->whereBetween('r.start', [$start, $end]);
                $query->orWhereBetween('r.end', [$start, $end]);
            });
        });
        // En caso de que exista el valor reservation 
        // Agregariamos una clausula where para excluir una reserva en especifico
        // Esto generalmente se utiliza cuando se quiere extender el tiempo de una reservación
        if($reservation != null)
            $exist = $exist->where('r.id', '!=', $reservation);

        // Aplicamos el ordenamiento de los datos y el metodo para obtener los mismo
        $exist = $exist->orderBy('category_id', 'asc')->first();

        // Si es falso, entonces significa la reserva de la habitación es posible
        if(!$exist)
            $room = $this->room($id, null);// Retorna todos los datos necesarios de la habitación
        else // En caso contrario asignamos falso
            $room = false; 

        return $room;
    }
    private function room($id = null, $exclude = null, $collection = false)
    {
        // Consulta dinamica para retornar los datos de las habitaciones
        $room = DB::table('rooms')
        ->join('categories as c', 'c.id', '=', 'rooms.category_id')
        ->Leftjoin('descriptions as d', 'd.id', '=', 'rooms.description_id')
        ->join('parking as p', 'p.id', '=', 'rooms.parking_id')
        ->select('rooms.*', 
        'c.amount_hour', 'c.amount_half', 'c.amount_day', 'c.name as category',
        'p.quantity', 'p.type', 'p.vip', 'p.temp',
        'd.content')->where('rooms.deleted_at', null);

        // Si existe el id de la habitación agregamos el where correspondiente
        if($id != null)
            $room = $room->Where('rooms.id', $id);
        // Si existe el arreglo exclude agregamos el where correspondiente
        if($exclude != null)
            $room = $room->whereNotIn('rooms.id', $exclude);
        // So el parametro colleccition es true el metodo para devolver los datos es get
        if($collection)
            $room = $room->get();
        else// en caso contrario usamos first
            $room = $room->first();

        return $room;
    }


    public function change_state(Request $request)
    {
        $reservation = Reservation::find($request['finish']);
        if($reservation)
        {
            // Cambiamos al estado 3
            $reservation->state_id = 3;
            $reservation->save();
            return response()->json([
                'code' => 1,
                'msg' => ['Se ha finalizado la reservación']
            ]);
        }
        else
        {
            return response()->json([
                'code' => 0,
                'msg' => ['No hemos encontrado la reservación']
            ]);
        }
    }
}
