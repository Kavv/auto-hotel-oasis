<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoriaController extends Controller
{

    public function index()
    {
        $data = Category::all();
        return view('categoria.index', compact('data'));
    }
    
    public function store(Request $request)
    {
        $this->validacion($request);
        // Verificamos si existe un registro con este nombre
        $exist = Category::where('name', $request['nombre'])->first();

        if($exist)
            return response()->json([
                'code' => 0,
                'msg' => ['El nombre de esta categoria ya esta en uso']
            ]);


        $category = Category::create([
            'name' => $request['nombre'],
            'amount_hour' => $request['valor_hora'],
            'amount_half' =>  $request['valor_mediodia'],
            'amount_day' => $request['valor_dia'],
        ]);
        return response()->json([
            'code' => 1,
            'data' => $category
        ]);
    }
    public function show($id)
    {
        $category = Category::find($id);
        if($category)
            return response()->json([
                'code' => 1,
                'data' => $category
            ]);
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro']
            ]);
    }
    public function update(Request $request, $id)
    {
        $this->validacion($request);
        $category = Category::find($id);
        
        if($category)
        {
            $category->name = $request['nombre'];
            $category->amount_hour = $request['valor_hora'];
            $category->amount_half = $request['valor_mediodia'];
            $category->amount_day = $request['valor_dia'];
            $category->save();
            
            return response()->json([
                'code' => 1,
                'data' => $category
            ]);
        }
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro']
            ]);

    }
    public function destroy($id)
    {
        $category = Category::find($id);
        if($category)
        {
            $category->delete();
            return response()->json([
                'code' => 1,
                'data' => $category
            ]);
        }
        else
            return response()->json([
                'code' => 0,
                'msg' => ['No se encontro este registro']
            ]);
    }
    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'nombre' => 'required',
                'valor_hora' => 'required|numeric',
                'valor_mediodia' => 'required|numeric',
                'valor_dia' => 'required|numeric'
            ];
        }
        $this->validate($objeto, $rule, $message);
    }
    
}
