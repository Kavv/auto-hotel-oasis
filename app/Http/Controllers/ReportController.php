<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use App\Customer;
use App\Staff;
use App\Reservation;
use Carbon\Carbon;
use DB;
use PDF;
class ReportController extends Controller
{
    //Valida que los valores start y end sean fechas
    public function DateValidation($start, $end)
    {
        if($start == null || $end == null || !strtotime($start) || !strtotime($end))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public function index()
    {
        return view("reporte.index2");
    }
    
    public function ClientesAgregados($start=null,$end=null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10 
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end); 

        //retornamos todos los clientes creados en el rango de tiempo aunque esten en estado borrado 
        $clientes = Customer::withTrashed()->whereBetween('created_at',[$start,$end])->get();
        //Llamamos a la funcion obtenerfechas enviano una lista y retornando un arreglo de fechas ordenadas
        //$dataDays = $this->ObtenerFechas($clientes);
        //Retornamos el json

        $start = strtotime($start);
        $start = date('d-m-Y',$start);
        $end = strtotime($end);
        $end = date('d-m-Y ',$end); 
        $report_title = '<h1 style="text-align:center;">Clientes Nuevos</h1>';
        $report_data = '<div class="row">'.
                            '<div class="" >'.
                                'Se agregaron '.count($clientes).' clientes<br>'. 
                                'Desde: '.$start.'<br>Hasta: '.$end. 
                            '</div>'.
                        '</div>';

        $thead = '<th style="width:20%;">Cedula</th>'.
        '<th >Nombre</th>'.
        '<th >Apellido</th>'.
        '<th style=" width:10%;">Edad</th>'.
        '<th >Sexo</th>'.
        '<th  style=" max-width:20%;">Dirección</th>';
        //return $thead;
        $tbody = '';
        foreach($clientes as $cliente)
        {
            $tbody .= '<tr>';
            $tbody .= '<td>'.$cliente->dni.'</td>';
            $tbody .= '<td>'.$cliente->name.'</td>';
            $tbody .= '<td>'.$cliente->last_name.'</td>';
            $tbody .= '<td>'.$cliente->age.'</td>';
            $tbody .= '<td>'.$cliente->gender.'</td>';
            $tbody .= '<td>'.$cliente->address.'</td>';
            $tbody .= '</tr>';
        }

        $pdf=PDF::loadView('reporte.reporte', compact("thead","tbody", "report_title", "report_data"));
        $now = new \DateTime();
        
        return $pdf->stream('Reporte'.$now->format('Y-m-d_H_i_s').'.pdf');
    }

    public function ClientesEliminados($start=null,$end=null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10 
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end); 

        //retornamos todos los clientes creados en el rango de tiempo
        $clientes = Customer::withTrashed()->whereBetween('deleted_at',[$start,$end])->get();

        $start = strtotime($start);
        $start = date('Y-m-d',$start);
        $end = strtotime($end);
        $end = date('Y-m-d ',$end); 
        $report_title = '<h1 style="text-align:center;">Clientes Eliminados</h1>';
        $report_data = '<div class="row">'.
                            '<div class="" >'.
                                'Se eliminaron '.count($clientes).' clientes<br>'. 
                                'Desde: '.$start.'<br>Hasta: '.$end. 
                            '</div>'.
                        '</div>';

        $thead = '<th style="width:20%;">Cedula</th>'.
        '<th >Nombre</th>'.
        '<th >Apellido</th>'.
        '<th style=" width:10%;">Edad</th>'.
        '<th >Sexo</th>'.
        '<th  style=" max-width:20%;">Dirección</th>';
        //return $thead;
        $tbody = '';
        foreach($clientes as $cliente)
        {
            $tbody .= '<tr>';
            $tbody .= '<td>'.$cliente->dni.'</td>';
            $tbody .= '<td>'.$cliente->name.'</td>';
            $tbody .= '<td>'.$cliente->last_name.'</td>';
            $tbody .= '<td>'.$cliente->age.'</td>';
            $tbody .= '<td>'.$cliente->gender.'</td>';
            $tbody .= '<td>'.$cliente->address.'</td>';
            $tbody .= '</tr>';
        }

        $pdf=PDF::loadView('reporte.reporte', compact("thead","tbody", "report_title", "report_data"));
        $now = new \DateTime();
        
        return $pdf->stream('Reporte'.$now->format('Y-m-d_H_i_s').'.pdf');
        
    } 
    
    public function PersonalAgregados($start=null,$end=null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10 
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end); 

        //retornamos todos los clientes creados en el rango de tiempo aunque esten en estado borrado 
        $personal = Staff::withTrashed()->whereBetween('created_at',[$start,$end])->get();
        
        $start = strtotime($start);
        $start = date('Y-m-d',$start);
        $end = strtotime($end);
        $end = date('Y-m-d ',$end); 
        $report_title = '<h1 style="text-align:center;">Personal Nuevo</h1>';
        $report_data = '<div class="row">'.
                            '<div class="" >'.
                                'Se agregaron '.count($personal).'registros al personal<br>'. 
                                'Desde: '.$start.'<br>Hasta: '.$end. 
                            '</div>'.
                        '</div>';

        $thead = '<th style="width:20%;">Cedula</th>'.
        '<th >Nombre</th>'.
        '<th >Apellido</th>'.
        '<th style=" width:10%;">Edad</th>'.
        '<th  style=" max-width:30%;">Dirección</th>';
        //return $thead;
        $tbody = '';
        foreach($personal as $persona)
        {
            $tbody .= '<tr>';
            $tbody .= '<td>'.$persona->dni.'</td>';
            $tbody .= '<td>'.$persona->name.'</td>';
            $tbody .= '<td>'.$persona->last_name.'</td>';
            $tbody .= '<td>'.$persona->age.'</td>';
            $tbody .= '<td>'.$persona->address.'</td>';
            $tbody .= '</tr>';
        }

        $pdf=PDF::loadView('reporte.reporte', compact("thead","tbody", "report_title", "report_data"));
        $now = new \DateTime();
        
        return $pdf->stream('Reporte'.$now->format('Y-m-d_H_i_s').'.pdf');
    } 

    public function PersonalEliminados($start=null,$end=null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10 
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end); 

        //retornamos todos los clientes creados en el rango de tiempo
        $personal = Staff::withTrashed()->whereBetween('deleted_at',[$start,$end])->get();
        
        $start = strtotime($start);
        $start = date('Y-m-d',$start);
        $end = strtotime($end);
        $end = date('Y-m-d ',$end); 
        $report_title = '<h1 style="text-align:center;">Personal Eliminado</h1>';
        $report_data = '<div class="row">'.
                            '<div class="" >'.
                                'Se eliminarón '.count($personal).' registros del personal<br>'. 
                                'Desde: '.$start.'<br>Hasta: '.$end. 
                            '</div>'.
                        '</div>';

        $thead = '<th style="width:20%;">Cedula</th>'.
        '<th >Nombre</th>'.
        '<th >Apellido</th>'.
        '<th style=" width:10%;">Edad</th>'.
        '<th  style=" max-width:30%;">Dirección</th>';
        //return $thead;
        $tbody = '';
        foreach($personal as $persona)
        {
            $tbody .= '<tr>';
            $tbody .= '<td>'.$persona->dni.'</td>';
            $tbody .= '<td>'.$persona->name.'</td>';
            $tbody .= '<td>'.$persona->last_name.'</td>';
            $tbody .= '<td>'.$persona->age.'</td>';
            $tbody .= '<td>'.$persona->address.'</td>';
            $tbody .= '</tr>';
        }

        $pdf=PDF::loadView('reporte.reporte', compact("thead","tbody", "report_title", "report_data"));
        $now = new \DateTime();
        
        return $pdf->stream('Reporte'.$now->format('Y-m-d_H_i_s').'.pdf');
    } 

    //Reservacion agregadas en x fecha a y fecha
    public function ReservacionesAgregadas($start=null,$end=null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10 
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end); 
        //retornamos todos los clientes creados en el rango de tiempo
        $reservations = Reservation::withTrashed()->whereBetween('created_at',[$start,$end])->get();
        
        $start = strtotime($start);
        $start = date('d-m-Y',$start);
        $end = strtotime($end);
        $end = date('d-m-Y ',$end); 
        $report_title = '<h1 style="text-align:center;">Reservaciones Agregadas</h1>';
        $report_data = '<div class="row">'.
                            '<div class="" >'.
                                'Se agregarón '.count($reservations).' reservaciones<br>'. 
                                'Desde: '.$start.'<br>Hasta: '.$end. 
                            '</div>'.
                        '</div>';

        $customer = $FI = $FF = $dates = [];
        foreach($reservations as $reservacion)
        {
            array_push($customer, $reservacion->clienteTrash);
            $temp = strtotime($reservacion['begin_date']);
            array_push($FI, date('d-m-Y',$temp));
            $temp = strtotime($reservacion['end_date']);
            array_push($FF, date('d-m-Y',$temp));
            //Hacemos conversion a tiempo
            $fullday = strtotime($reservacion->created_at);
            //Convertimos a una fecha con un formato de Año-mes-dia
            array_push($dates, [date("d", $fullday), date("m", $fullday), date("Y", $fullday)]);
        }
        
        //return view('reporte.reservas_agregadas', compact('CC', 'NC', 'DL','FI', 'FF', 'dates', 'reservaciones', "report_title", "report_data"));
        $pdf=PDF::loadView('reporte.reservas_agregadas', compact('customer','FI', 'FF', 'dates', 'reservations', "report_title", "report_data"));
        $now = new \DateTime();
        //definimos el nombre por defecto del archivo
        return $pdf->stream('Reporte'.$now->format('Y-m-d_H_i_s').'.pdf');
    } 
    
    //Reservaciones que se realizaran de x a y fecha
    public function Eventos($start=null,$end=null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10 
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end); 
        //retornamos todos los clientes creados en el rango de tiempo
        $reservations = Reservation::withTrashed()->whereBetween('begin_date',[$start,$end])
                        ->orWhere(function ($query) use($start,$end) {
                            $query->whereBetween("end_date", [$start, $end]);
                        })->get();

        
        $start = strtotime($start);
        $start = date('d-m-Y',$start);
        $end = strtotime($end);
        $end = date('d-m-Y ',$end); 
        $report_title = '<h1 style="text-align:center;">Reservaciones Agregadas</h1>';
        $report_data = '<div class="row">'.
                            '<div class="" >'.
                                'Se agregarón '.count($reservations).' reservaciones<br>'. 
                                'Desde: '.$start.'<br>Hasta: '.$end. 
                            '</div>'.
                        '</div>';

        $customer = $FI = $FF = $dates = [];
        foreach($reservations as $reservacion)
        {
            array_push($customer, $reservacion->clienteTrash);
            $temp = strtotime($reservacion['begin_date']);
            array_push($FI, date('d-m-Y',$temp));
            $temp = strtotime($reservacion['end_date']);
            array_push($FF, date('d-m-Y',$temp));
            //Hacemos conversion a tiempo
            $fullday = strtotime($reservacion->created_at);
            //Convertimos a una fecha con un formato de Año-mes-dia
            array_push($dates, [date("d", $fullday), date("m", $fullday), date("Y", $fullday)]);
        }
        //return view('reporte.reservas_agregadas', compact('CC', 'NC', 'DL','FI', 'FF', 'dates', 'reservaciones', "report_title", "report_data"));
        $pdf=PDF::loadView('reporte.reservas_agregadas', compact('customer','FI', 'FF', 'dates', 'reservations', "report_title", "report_data"));
        $now = new \DateTime();
        //definimos el nombre por defecto del archivo
        return $pdf->stream('Reporte'.$now->format('Y-m-d_H_i_s').'.pdf');
    } 

    //Reservacion agregadas en x fecha a y fecha
    public function ingresos($start=null,$end=null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10 
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end); 
        //retornamos todos los clientes creados en el rango de tiempo
        $reservations = Reservation::withTrashed()->whereBetween('created_at',[$start,$end])->get();

        $start = strtotime($start);
        $start = date('d-m-Y',$start);
        $end = strtotime($end);
        $end = date('d-m-Y ',$end); 
        $report_title = '<h1 style="text-align:center;">Reservaciones Agregadas</h1>';
        $report_data = '<div class="row">'.
                            '<div class="" >'.
                                'Se agregarón '.count($reservations).' reservaciones<br>'. 
                                'Desde: '.$start.'<br>Hasta: '.$end. 
                            '</div>'.
                        '</div>';

        $FI = $FF = $dates = $ingresos = [];
        $customer = [];
        foreach($reservations as $x => $reservacion)
        {
            array_push($customer, $reservacion->clienteTrash);
            $temp = strtotime($reservacion['begin_date']);
            array_push($FI, date('d-m-Y',$temp));
            $temp = strtotime($reservacion['end_date']);
            array_push($FF, date('d-m-Y',$temp));
            //Hacemos conversion a tiempo
            $fullday = strtotime($reservacion->created_at);
            //Convertimos a una fecha con un formato de Año-mes-dia
            array_push($dates, [date("d", $fullday), date("m", $fullday), date("Y", $fullday)]);
            array_push($ingresos,[$customer[$x]->name." ".$customer[$x]->last_name, $customer[$x]->dni, $dates[$x][0]."-".$dates[$x][1]."-".$dates[$x][2]]);
        }
        
        
        //return view('reporte.ingresos', compact('CC', 'NC', 'DL','FI', 'FF', 'dates', 'reservaciones', "report_title", "report_data", "ingresos", "start", "end"));
        $pdf=PDF::loadView('reporte.ingresos', compact('customer','FI', 'FF', 'dates', 'reservations', "report_title", "report_data", "ingresos", "start", "end"));
        $now = new \DateTime();
        //definimos el nombre por defecto del archivo
        return $pdf->stream('Reporte'.$now->format('Y-m-d_H_i_s').'.pdf');
    }

    public function inventario($start = null, $end = null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end);

        //retornamos todos los clientes creados en el rango de tiempo aunque esten en estado borrado
        $inventarios = Item::withTrashed()->whereBetween('created_at',[$start,$end])->get();

        $start = strtotime($start);
        $start = date('d-m-Y',$start);
        $end = strtotime($end);
        $end = date('d-m-Y ',$end);
        $report_title = '<h1 style="text-align:center;">Articulos Nuevos</h1>';
        $report_data = '<div class="row">'.
                            '<div class="" >'.
                                'Se agregaron '.count($inventarios).' Articulos<br>'.
                                'Desde: '.$start.'<br>Hasta: '.$end.
                            '</div>'.
                        '</div>';

        $thead = '<th style="width:20%;">Nombre</th>'.
            '<th style="width:10%;">Cantidad</th>'.
            '<th style="width:15%;">Precio de Alquiler</th>'.
            '<th style=" width:15%;">Precio de Adquisicion</th>'.
            '<th style=" width:15%;">Servicio</th>';

        $tbody = '';
        foreach($inventarios as $inventario)
        {
            $tbody .= '<tr>';
            $tbody .= '<td>'.$inventario->name.'</td>';
            $tbody .= '<td>'.$inventario->quantity.'</td>';
            $tbody .= '<td>'.$inventario->rental_price.'</td>';
            $tbody .= '<td>'.$inventario->buy_price.'</td>';
            $tbody .= '<td>'.$inventario->servicio->name.'</td>';
            $tbody .= '</tr>';
        }

        $pdf = PDF::loadView('reporte.inventario', compact("thead", "tbody", "report_title", "report_data"));
        $now = new \DateTime();

        return $pdf->stream('Reporte'.$now->format('Y-m-d_H_i_s'). '.pdf');
    }
    //articulos mas usados
    public function articuloMasUsado($start = null, $end = null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end);

        //retornamos todos las reservaciones creadas en el rango de tiempo aunque esten borrados
        $reservaciones = Reservation::withTrashed()->whereBetween('begin_date',[$start,$end])
            ->orWhere(function ($query) use($start,$end) {
                $query->whereBetween("end_date", [$start, $end]);
            })->get();
        //creacion de una coleccion de datos
        $origi  = $ori_cant = $item_fin = $cant_fin = [];

        foreach ($reservaciones as $i => $articulos)
        {
            $cont = count($articulos->items);
            for($j = 0; $j < $cont; $j++)
            {
                array_push($origi, $articulos->items[$j]->name);
                array_push($ori_cant, $articulos->items[$j]->qu);
            }
        }
        //forma en php de quitar elementos repetidos en un arreglo
        $item_fin = array_unique($origi);
        $cont = count($origi);
        //busqueda lineal
        foreach ($item_fin as $i => $item)
        {
            //reset de variable cuando cambia de elemento
            $suma = 0;
            for($j = 0; $j < $cont; $j++)
            {
                //comparacion de elementos
                if($item_fin[$i] == $origi[$j])
                {
                    $suma += $ori_cant[$j];
                    $cant_fin[$i] = $suma;
                }
            }
        }
        //combinacion de arreglos
        $fin = array_combine($cant_fin, $item_fin );

        //forma de ordenar en php de forma descendiente, mantieniendo los datos intactos
        krsort($fin);
        //todo para el stream el pdf
        $report_title = '<h1 style="text-align:center;"> Articulos mas utilizados</h1>';
        $report_data = '<div class="row">'.
            '<div class="" >'.
            '<br>'.
            'Desde: '.$start.'<br>Hasta: '.$end.
            '</div>'.
            '</div>';

        $thead = '<th style="width:20%;">Nombre</th>'.
            '<th style="width:10%;">Cantidad</th>';
//            '<th style="width:15%;">Precio de Alquiler</th>'.
//            '<th style=" width:15%;">Precio de Adquisicion</th>'.
//            '<th style=" width:15%;">Servicio</th>';

//        dd($fin);

        $tbody = '';

        foreach($fin as $i => $data)
        {
            $tbody .= '<tr>';
            $tbody .= '<td>'. $data .'</td>';
            $tbody .= '<td>'. $i .'</td>';
            $tbody .= '</tr>';
        }

        $pdf = PDF::loadView('reporte.artMasUsado', compact("thead", "tbody", "report_title", "report_data"));
        $now = new \DateTime();

        return $pdf->stream('Reporte'.$now->format('Y-m-d_H_i_s'). '.pdf');
    }

    //buffet mas usado
    public function buffetMasUsado($start = null, $end = null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end);

        //retornamos todos las reservaciones creadas en el rango de tiempo aunque esten borrados
        $reservaciones = Reservation::withTrashed()->whereBetween('begin_date',[$start,$end])
            ->orWhere(function ($query) use($start,$end) {
                $query->whereBetween("end_date", [$start, $end]);
            })->get();
        //creacion de una coleccion de datos
        $origi  = $ori_cant = $item_fin = $cant_fin = [];

        foreach ($reservaciones as $i => $buffet)
        {
            $cont = count($buffet->menu);
            for($j = 0; $j < $cont; $j++)
            {
                array_push($origi, $buffet->menu[$j]->description);
                array_push($ori_cant, $buffet->menu[$j]->qu);
            }
        }

        //forma en php de quitar elementos repetidos en un arreglo
        $item_fin = array_unique($origi);
        $cont = count($origi);
        //busqueda lineal
        foreach ($item_fin as $i => $item)
        {
            //reset de variable cuando cambia de elemento
            $suma = 0;
            for($j = 0; $j < $cont; $j++)
            {
                //comparacion de elementos
                if($item_fin[$i] == $origi[$j])
                {
                    $suma += $ori_cant[$j];
                    $cant_fin[$i] = $suma;
                }
            }
        }
        //combinacion de arreglos
        $fin = array_combine($cant_fin, $item_fin );

        //forma de ordenar en php de forma descendiente, mantieniendo los datos intactos
        krsort($fin);
        //todo para el stream el pdf
        $report_title = '<h1 style="text-align:center;"> Articulos mas utilizados</h1>';
        $report_data = '<div class="row">'.
            '<div class="" >'.
            '<br>'.
            'Desde: '.$start.'<br>Hasta: '.$end.
            '</div>'.
            '</div>';

        $thead = '<th style="width:20%;">Nombre</th>'.
            '<th style="width:10%;">Cantidad</th>';
//            '<th style="width:15%;">Precio de Alquiler</th>'.
//            '<th style=" width:15%;">Precio de Adquisicion</th>'.
//            '<th style=" width:15%;">Servicio</th>';

//        dd($fin);

        $tbody = '';

        foreach($fin as $i => $data)
        {
            $tbody .= '<tr>';
            $tbody .= '<td>'. $data .'</td>';
            $tbody .= '<td>'. $i .'</td>';
            $tbody .= '</tr>';
        }

        $pdf = PDF::loadView('reporte.artMasUsado', compact("thead", "tbody", "report_title", "report_data"));
        $now = new \DateTime();

        return $pdf->stream('Reporte'.$now->format('Y-m-d_H_i_s'). '.pdf');
    }

/*
    public function index()
    {
        return view("reporte.index");
    }

    public function ClientesNuevos($start="2019-04-01",$end="2019-05-30 15:07:00")
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end);

        //retornamos todos los clientes creados en el rango de tiempo aunque esten en estado borrado
        $clientes = Customer::withTrashed()->whereBetween('created_at',[$start,$end])->get();
        //Llamamos a la funcion obtenerfechas enviano una lista y retornando un arreglo de fechas ordenadas
        $dataDays = $this->ObtenerFechas($clientes);
        //Retornamos el json
        return response()->json([
            "clientes" => $clientes,
            "state"=>true,
            "data"=>$dataDays,
            "name"=>"Clientes Nuevos",
            "title"=>"Clientes"
        ]);
    }

    public function ClientesEliminados($start="2019-04-01",$end="2019-05-30 15:07:00")
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end);

        //retornamos todos los clientes creados en el rango de tiempo
        $clientes = Customer::withTrashed()->whereBetween('deleted_at',[$start,$end])->get();

        //Llamamos a la funcion obtenerfechas enviano una lista y retornando un arreglo de fechas ordenadas
        $dataDays = $this->ObtenerFechas($clientes);
        //Retornamos el json
        return response()->json([
            "state"=>true,
            "data"=>$dataDays,
            "name"=>"Clientes Eliminados",
            "title"=>"Clientes"
        ]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    //PERSONAL//
    ////////////////////////////////////////////////////////////////////////////////////////

    public function PersonalNuevos($start="2019-04-01",$end="2019-05-30 15:07:00")
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end);

        //retornamos todos los clientes creados en el rango de tiempo aunque esten en estado borrado
        $personal = Staff::withTrashed()->whereBetween('created_at',[$start,$end])->get();
        //Llamamos a la funcion obtenerfechas enviano una lista y retornando un arreglo de fechas ordenadas
        $dataDays = $this->ObtenerFechas($personal);
        //Retornamos el json
        return response()->json([
            "state"=>true,
            "data"=>$dataDays,
            "name"=>"Personal Nuevos",
            "title"=>"Personal"
        ]);
    }

    public function PersonalEliminados($start="2019-04-01",$end="2019-05-30 15:07:00")
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end);

        //retornamos todos los clientes creados en el rango de tiempo
        $personal = Staff::withTrashed()->whereBetween('deleted_at',[$start,$end])->get();

        //Llamamos a la funcion obtenerfechas enviano una lista y retornando un arreglo de fechas ordenadas
        $dataDays = $this->ObtenerFechas($personal);
        //Retornamos el json
        return response()->json([
            "state"=>true,
            "data"=>$dataDays,
            "name"=>"Personal Eliminados",
            "title"=>"Personal"
        ]);
    }
*/
    //Reservacion agregadas en x fecha a y fecha
    public function GraficaReservacionesNuevas($start=null,$end=null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10 
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end); 
        //retornamos todos los clientes creados en el rango de tiempo
        $reservaciones = Reservation::withTrashed()->whereBetween('created_at',[$start,$end])->get();

        //Llamamos a la funcion obtenerfechas enviando una lista y retornando un arreglo de fechas ordenadas
        $dataDays = $this->ObtenerFechas($reservaciones);
        //Llamamos a la funcion obtenerIngresos enviando una lista y retornando un arreglo de fechas ordenadas
        $ingresos = $this->ObtenerIngresos($reservaciones);

        //Retornamos el json
        return response()->json([
            "state"=>true,
            "data"=>$dataDays,
            "ingresos" => $ingresos,
            "name"=> ["Reservaciones agregadas", "Ingresos"],
            "title"=>"Reservaciones"
        ]);
    } 
    /*
    //Reservaciones que se realizaran de x a y fecha
    public function ReservacionesDates($start=null,$end=null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10 
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end); 
        //retornamos todos los clientes creados en el rango de tiempo
        $reservaciones = Reservation::withTrashed()->whereBetween('begin_date',[$start,$end])
                        ->orWhere(function ($query) use($start,$end) {
                            $query->whereBetween("end_date", [$start, $end]);
                        })->get();
        $dataDays = $this->ObtenerReservaciones($reservaciones);

        //$reservaciones = Reservation::withTrashed()->whereBetween('end_date',[$start,$end])->get();
        //$dataDays = $this->ObtenerReservaciones($reservaciones, $dataDays);
    
        //Ordenamos de menor a mayor segun la key 
        //ksort($dataDays);

        
        //Ordenamos de menor a mayor segun la fecha de inicio
        usort($dataDays, array($this, "cmp"));


        //Retornamos el json
        return response()->json([
            "state"=>true,
            "data"=>$dataDays,
            "name"=>"Reservaciones",
            "title"=>"Reservaciones"
        ]);
    } 

    public function ObtenerReservaciones($lista, $dataDays = null)
    {
        if($dataDays == null)
            $dataDays = [];

        //Desde 0 hasta la cantidad de clientes
        for($i = 0; $i < count($lista); $i++ )
        {

            //Hacemos conversion a tiempo
            $fullday = strtotime($lista[$i]->begin_date);
            //Convertimos a una fecha con un formato de Año-mes-dia
            $day = date("Y-m-d", $fullday);
            
            $endDate = strtotime($lista[$i]->end_date);
            $endDate = date("Y-m-d", $endDate);



            $dataDays[$i]["begin_date"] = $day;
            $dataDays[$i]["end_date"] = $endDate;
            $dataDays[$i]["cliente"] = $lista[$i]->cliente->name.$lista[$i]->cliente->last_name;
            

            
            //Existe la key ? como key(indice) utilizamos el dia de creacion del cliente 
            //if(array_key_exists($day, $dataDays))
            //{
                //Si ya existe entonces hay que aumentar la cantidad y asignar el resto de valores
                //$dataDays[$day]["cantidad"] = $dataDays[$day]["cantidad"] + 1; 
                //array_push($dataDays[$day]["nombre"], $lista[$i]->name);
            //}
            //else
            //{
                //Si no existe entonces se deben declarar los indices y asignar el resto de valores
                //$dataDays[$day]["cantidad"] = 1;
                //$dataDays[$day]["nombre"] = []; 
                //$dataDays[$day]["end_date"] = []; 
                //array_push($dataDays[$day]["nombre"], $lista[$i]->name);
            //}
        }
        return $dataDays;
    }
    //Funcion de apoyo para ordenar un arreglo bidimencional
    public function cmp($a, $b)
    {
        return strcmp($a["begin_date"], $b["begin_date"]);
    }




    //Valida que los valores start y end sean fechas
    public function DateValidation($start, $end)
    {
        if($start == null || $end == null || !strtotime($start) || !strtotime($end))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    */
    //Llamamos a la funcion obtenerfechas recibiendo una lista y retornando un arreglo de fechas ordenadas
    public function ObtenerFechas($lista)
    {
        //declaramos el arreglo
        $dataDays = [];
        $ingresos = [];
        //Desde 0 hasta la cantidad de clientes
        for($i = 0; $i < count($lista); $i++ )
        {
            $tax = $lista[$i]->tax;
            $discount = $lista[$i]->discount;
            //Hacemos conversion a tiempo
            $fullday = strtotime($lista[$i]->created_at);
            //Convertimos a una fecha con un formato de Año-mes-dia
            $day = date("Y-m-d", $fullday);
            
            //Existe la key ? como key(indice) utilizamos el dia de creacion del cliente 
            if(array_key_exists($day, $dataDays))
            {
                //Si ya existe entonces hay que aumentar la cantidad y asignar el resto de valores
                $dataDays[$day]["cantidad"] = $dataDays[$day]["cantidad"] + 1;
                array_push($dataDays[$day]["nombre"], $lista[$i]->name);
            }
            else
            {
                //Si no existe entonces se deben declarar los indices y asignar el resto de valores
                $dataDays[$day]["cantidad"] = 1;
                $dataDays[$day]["nombre"] = []; 
                array_push($dataDays[$day]["nombre"], $lista[$i]->name);
            }
        }
        //Ordenamos de menor a mayor segun la key 
        ksort($dataDays);
        return $dataDays;
    }
    
    //OBTENEMOS LOS INGRESOS POR DIA DONDE SE HUBIESEN HECHO RESERVACIONES
    public function ObtenerIngresos($reservaciones)
    {
        $ingresos_day = [];
        for($k = 0; $k < count($reservaciones); $k++)
        {
            //La cantidad de filas multiplicado por la pagina de factura actual
            $items = $reservaciones[$k]->items;
            $menu = $reservaciones[$k]->menu;
            $filas_totales = count($items) + count($menu);
            $pago = 0;
            $ingreso = [];
            for($j = 0; $j < $reservaciones[$k]->invoice_row; $j++)
            {
                if($j < $filas_totales)
                {
                    if(isset($items[$j]))
                    {
                        $pago += ($items[$j]->qu * $items[$j]->up * $items[$j]->day);
                    }
                    else
                    {
                        if($menu)
                        {
                            $pago += ($menu[0]->qu * $menu[0]->up * $menu[0]->day);
                        }
                    }
                }
            }
            $iva = $reservaciones[$k]->tax;
            $descuento = $reservaciones[$k]->discount;

            if($iva == 0 && $descuento == 0)
            {
                $total = $pago;
            }

            if($iva != 0 && $descuento == 0)
            {
                $temp_iva = $pago*$iva;
                $total = $pago + $temp_iva;
            }

            if($iva == 0 && $descuento != 0)
            {
                $temp_descuento = $pago*$descuento;
                $total = $pago - $temp_descuento;
            }

            if($iva != 0 && $descuento != 0)
            {
                $temp_iva = $pago*$iva;
                $temp_descuento = $pago*$descuento;
                $total = $pago-$temp_descuento+$temp_iva;
            }
            
            //Hacemos conversion a tiempo
            $fullday = strtotime($reservaciones[$k]->created_at);
            //Convertimos a una fecha con un formato de Año-mes-dia
            $day = date("Y-m-d", $fullday);
            //Existe la key ? como key(indice) utilizamos el dia de creacion del cliente 
            if(array_key_exists($day, $ingresos_day))
            {
                //Si ya existe entonces hay que aumentar el ingreso 
                $ingresos_day[$day]["ingreso"] += $total;
            }
            else
            {
                //Si no existe entonces se deben declarar los indices 
                $ingresos_day[$day]["ingreso"] = $total;
            }
        }

        //Ordenamos de menor a mayor segun la key 
        ksort($ingresos_day);
        return $ingresos_day;
    }

}
