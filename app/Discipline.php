<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discipline extends Model
{
    protected $table='disciplines';
    public $primaryKey='id';
    public $incrementing = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'id','name',
    ];

    
    public function services()
    {
        // Uno a muchos
        return $this->hasMany('App\Service','discipline_id','id');
    }
}
