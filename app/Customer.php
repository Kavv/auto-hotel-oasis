<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    
    protected $table='customers';
    public $primaryKey ='id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vetoed_id','dni', 'name','age','gender','address','created_at','phone', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*protected $hidden = [
        'password', 'remember_token',
    ];*/
    public function vetado()
    {
        return $this->belongsTo('App\Vetoed','vetoed_id','id');
    }

    public function phones()
    {
        return $this->belongsToMany('App\Phone', 'customers_phones', 'customer_id', 'phone_id');
    }

    public function discharged()
    {
        return $this->hasMany('App\discharged','customer_id','id');
    }


    public function services()
    {
        // Muchos a muchos
        return $this->belongsToMany('App\Service', 'customers_services', 'customer_id', 'service_id');
    }
    public function status()
    {
        // Uno a uno
        return $this->belongsTo('App\Status','status_id','id');
    }
    
    public function movements()
    {
        // Uno a muchos
        return $this->hasMany('App\Movement','customer_id','id');
    }
    public function lessons()
    {
        // Uno a muchos
        return $this->belongsToMany('App\Lesson', 'customers_lessons', 'customer_id', 'lesson_id');
    }
    public function calendars()
    {
        // Uno a muchos
        return $this->belongsToMany('App\Calendar', 'reservations', 'customer_id', 'calendar_id');
    }
}
