<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class permission_rule extends Model
{
    protected $table='permissions_rules';
    public $incrementing = false;
    protected $fillable = [
        'permission_id','rule_id',
    ];
}
