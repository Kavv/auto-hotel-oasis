<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class EmptyExport implements FromView, ShouldAutoSize
{
    private $start, $end, $opc, $i;
    public function __construct($start, $end, $valor3, $i)
    {
        $this->start = $start;
        $this->end = $end;
        $this->opc = $valor3;
        $this->i = $i;
    }

    public function view(): View
    {
        $start = $this->start;
        $end = $this->end;
        $opc = $this->opc;
        $i = $this->i;
        return view('exports.Empty', compact('start','end', 'opc', 'i'));
    }

}
