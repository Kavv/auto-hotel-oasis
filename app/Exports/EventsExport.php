<?php

namespace App\Exports;

use App\Reservation;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class EventsExport implements WithMultipleSheets, ShouldAutoSize
{

    protected $reservation, $start, $end, $opt, $cant, $data = [];
    public function __construct($reservation, $start,$end,$valor3, $cant)
    {

        $this->reservation = Reservation::withTrashed()
            ->whereBetween('begin_date', [$start, $end])
            ->orWhere(function ($query) use ($start, $end) {
                $query->whereBetween("end_date", [$start, $end]);
            })->get();

        $this->start = $start;
        $this->end = $end;
        $this->opt = $valor3;
        $this->cant = $cant;
    }

    public function sheets(): array
    {
        $cant = $this->cant;
        $m_total = 0;
        $ingresos =  $nomb = $cedula = $dni = $res_date =[];


        // todo lo que esta abajo de esto es funcional
        $sheets = [];
        if($cant > 0) {
            foreach ($this->reservation as $i => $reservas) {
                //variable ingresos. nombre. dni . rest_date son solo para ingresos
                if ($this->opt == "ingresos")
                    $sheets[] = new PerReservationExport($reservas, $this->start, $this->end, $this->opt, $cant, $i, $ingresos, $m_total, $nomb, $dni, $res_date);
                else
                    $sheets[] = new PerReservation2Export($reservas, $this->start, $this->end, $this->opt, $cant, $i);
                $i++;
            }
            if ($this->opt == "ingresos")
                $sheets[] = new PerReservationExport($reservas, $this->start, $this->end, $this->opt, $cant, $i, $ingresos, $m_total, $nomb, $dni, $res_date);

            return $sheets;
        }
        else{
            $i = 0;
            if ($this->opt == "ingresos")
                for($i = 0; $i<2; $i++)
                    $sheets[] = new EmptyExport($this->start, $this->end, $this->opt, $i);
            else
                $sheets[] = new EmptyExport($this->start, $this->end, $this->opt, $i);

            return $sheets;
        }
    }
}
