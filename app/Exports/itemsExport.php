<?php

namespace App\Exports;

use App\Item;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class itemsExport implements FromView, ShouldAutoSize
{
    protected $start, $end, $opt, $cont, $name = [], $cant = [], $p1 = [], $p2 = [], $services = [];

    public function __construct($start, $end, $valor3, $name,$cant, $p1, $p2, $services, $cont)
    {
        $this->start = $start;
        $this->end = $end;
        $this->opt = $valor3;
        $this->name = $name;
        $this->cant = $cant;
        $this->p1 = $p1;
        $this->p2 = $p2;
        $this->services = $services;
        $this->cont = $cont;
    }

    public function view(): View
    {
        $start = strtotime($this->start);
        $start = date('d-m-Y',$start);
        $end = strtotime($this->end);
        $end = date('d-m-Y ',$end);

        $count = $this->cont;
        $name = $this->name;
        $cant = $this->cant;
        $p1 = $this->p1;
        $p2 = $this->p2;
        $services = $this->services;

        // TODO: Implement view() method.
        return view('exports.inventario', compact('start', 'end', 'count', 'name', 'cant', 'p1', 'p2', 'services'));
    }
}
