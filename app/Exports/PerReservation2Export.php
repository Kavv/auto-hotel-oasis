<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Reservation;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class PerReservation2Export implements FromView, ShouldAutoSize, WithColumnFormatting
{

    private $start, $end, $reserva, $opt, $cant, $i;

    public function __construct(Reservation $reservacion, $start, $end, $opcion, $cant, $i)
    {
        $this->reserva = $reservacion;
        $this->start = $start;
        $this->end = $end;
        $this->opt = $opcion;
        $this->cant = $cant;
        $this->i = $i;
    }

    public function columnFormats(): array
    {
        return [
          'D' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }

    public function view(): View
    {

        //definicion de cantidad que lleva el ciclo para la cantidad de hojas que creara
        $i = $this->i;

//        $reservaciones = Reservation::withTrashed()
//            ->whereBetween('created_at',[$this->start,$this->end])
//            ->get();
        $reservaciones = $this->reserva;
        //formato para la factura
        $count = $this->cant;
        $start = strtotime($this->start);
        $start = date('d-m-Y',$start);
        $end = strtotime($this->end);
        $end = date('d-m-Y ',$end);

        //variables de tipo arreglo para los demas datos que se utilizaran
        $OPC = $CC = $NC = $DL = $FI = $FF = $dates = $ingresos = $nomb = $dni = $res_date = [];

//        foreach($reservaciones as $reservacion)
//        {
            array_push($OPC, $this->opt);
            array_push($CC, $this->reserva->clienteTrash->dni);
            array_push($NC, $this->reserva->clienteTrash->name." ".$this->reserva->clienteTrash->last_name);
            array_push($DL, $this->reserva['event_address']);
            $temp = strtotime($this->reserva['begin_date']);
            array_push($FI, date('d-m-Y',$temp));
            $temp = strtotime($this->reserva['end_date']);
            array_push($FF, date('d-m-Y',$temp));

            //Hacemos conversion a tiempo
            $fullday = strtotime($this->reserva->created_at);
            //Convertimos a una fecha con un formato de Año-mes-dia
            array_push($dates, date("d", $fullday));
            array_push($dates, date("m", $fullday));
            array_push($dates, date("y", $fullday));

//        }

        return view('exports.ResInvoice', compact('count','start','end','OPC','CC', 'NC', 'DL','FI', 'FF', 'dates', 'reservaciones', 'i'));

    }
}
