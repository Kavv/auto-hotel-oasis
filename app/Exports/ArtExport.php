<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ArtExport implements FromView, ShouldAutoSize
{
    protected $start, $end, $opt, $fin;

    public function __construct($start, $end, $valor3, $fin)
    {
        $this->start = $start;
        $this->end = $end;
        $this->opt = $valor3;
        $this->fin = $fin;
    }

    public function view(): View
    {
        $start = strtotime($this->start);
        $start = date('d-m-Y',$start);
        $end = strtotime($this->end);
        $end = date('d-m-Y ',$end);

        $fin = $this->fin;

        // TODO: Implement view() method.
        if($this->opt == "artMasUsado")
            return view('exports.ArtMas', compact('start', 'end', 'fin'));
        else
            return view('exports.BuffMas', compact('start','end', 'fin'));

    }
}
