<?php

namespace App\Exports;

use App\Customer;
use App\Http\Controllers\ClienteController;
use App\Reservation;
use App\Staff;
use App\Vetoed;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class CustomerExport implements FromView, ShouldAutoSize, WithHeadings
{

    protected $start, $end, $opt, $data = [];

    public function __construct($start,$end,$valor3)
    {
        $this->start = $start;
        $this->end = $end;
        $this->opt = $valor3;

        switch ($this->opt)
        {
            case "clientes_agregados":
                $this->data = [
                    'Cedula',
                    'Nombre',
                    'Apellido',
                    'Edad',
                    'Sexo',
                    'Direccion'
                ];
            break;
            case "clientes_eliminados":
                $this->data = [
                    'Cedula',
                    'Nombre',
                    'Apellido',
                    'Edad',
                    'Sexo',
                    'Direccion'
                ];
                break;
            case "personal_agregados":
                $this->data = [
                    'Cedula',
                    'Nombre',
                    'Apellido',
                    'Edad',
                    'Direccion'
                ];
            break;
            case "personal_eliminados":
                $this->data = [
                    'Cedula',
                    'Nombre',
                    'Apellido',
                    'Edad',
                    'Direccion'
                ];
                break;
        }

    }

    public function headings(): array
    {
        return $this->data;
    }

    public function view(): View
    {
        $start = $this->start;
        $end = $this->end;
        $opt = $this->opt;
        switch ($opt) {
            case "clientes_agregados":
                $customer = Customer::withTrashed()
                    ->select('dni', 'name', 'last_name', 'age', 'gender', 'address')
                    ->whereBetween('created_at', [$this->start, $this->end])
                    ->get();
                $count = count($customer);
                $temp1 = strtotime($start);
                $temp2 = strtotime($end);
                $ini = date('d-m-Y', $temp1);
                $fin = date('d-m-Y', $temp2);
//                dd($dateR);

                return view('exports.CustomersInvoice', compact('customer', 'count', 'ini', 'fin', 'opt'));

                break;

            case "clientes_eliminados":
                $customer = Customer::withTrashed()
                    ->select('dni', 'name', 'last_name', 'age', 'gender', 'address')
                    ->whereBetween('deleted_at', [$this->start, $this->end])
                    ->get();
                $count = count($customer);
                $temp1 = strtotime($start);
                $temp2 = strtotime($end);
                $ini = date('d-m-Y', $temp1);
                $fin = date('d-m-Y', $temp2);

                return view('exports.CustomersInvoice', compact('customer', 'count', 'ini', 'fin', 'opt'));

                break;

            case "personal_agregados":
                $customer = Staff::withTrashed()
                    ->select('dni', 'name', 'last_name', 'age', 'address')
                    ->whereBetween('created_at', [$this->start, $this->end])
                    ->get();
                $count = count($customer);
                $temp1 = strtotime($start);
                $temp2 = strtotime($end);
                $ini = date('d-m-Y', $temp1);
                $fin = date('d-m-Y', $temp2);

                return view('exports.CustomersInvoice', compact('customer', 'count', 'ini', 'fin', 'opt'));

                break;

            case "personal_eliminados":
                $customer = Staff::withTrashed()
                    ->select('dni', 'name', 'last_name', 'age', 'address')
                    ->whereBetween('deleted_at', [$this->start, $this->end])
                    ->get();
                $count = count($customer);
                $temp1 = strtotime($start);
                $temp2 = strtotime($end);
                $ini = date('d-m-Y', $temp1);
                $fin = date('d-m-Y', $temp2);

                return view('exports.CustomersInvoice', compact('customer', 'count', 'ini', 'fin', 'opt'));

                break;

        }
    }
}
