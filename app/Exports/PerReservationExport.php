<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Reservation;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class PerReservationExport implements FromView, ShouldAutoSize, WithColumnFormatting
{

    private $start, $end, $reserva, $opt, $cant, $i, $ingresos = [], $m_total, $nomb =[], $dni = [], $res_date = [];

    public function __construct(Reservation $reservacion, $start, $end, $opcion, $cant, $i, $ingresos, $m_total, $nomb, $dni, $res_date)
    {
        $this->reserva = $reservacion;
        $this->start = $start;
        $this->end = $end;
        $this->opt = $opcion;
        $this->cant = $cant;
        $this->i = $i;
        $this->ingresos = $ingresos;
        $this->m_total = $m_total;
        $this->nomb = $nomb;
        $this->dni = $dni;
        $this->res_date = $res_date;
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }

    public function view(): View
    {

        //definicion de cantidad que lleva el ciclo para la cantidad de hojas que creara
        $i = $this->i;
        $m_total = $this->m_total;

//        $reservaciones = Reservation::withTrashed()
//            ->whereBetween('created_at',[$this->start,$this->end])
//            ->get();
        $reservaciones = $this->reserva;
        //formato para la factura
        $count = $this->cant;
        $start = strtotime($this->start);
        $start = date('d-m-Y',$start);
        $end = strtotime($this->end);
        $end = date('d-m-Y ',$end);

        //variables de tipo arreglo para los demas datos que se utilizaran
        $OPC = $this->opt;
        $CC = $NC = $DL = $FI = $FF = $dates = $ingresos = $nomb = $dni = $res_date = [];

//        foreach($reservaciones as $reservacion)
//        {
            array_push($CC, $this->reserva->clienteTrash->dni);
            array_push($NC, $this->reserva->clienteTrash->name." ".$this->reserva->clienteTrash->last_name);
            array_push($DL, $this->reserva['event_address']);
            $temp = strtotime($this->reserva['begin_date']);
            array_push($FI, date('d-m-Y',$temp));
            $temp = strtotime($this->reserva['end_date']);
            array_push($FF, date('d-m-Y',$temp));

            //Hacemos conversion a tiempo
            $fullday = strtotime($this->reserva->created_at);
            //Convertimos a una fecha con un formato de Año-mes-dia
            array_push($dates, date("d", $fullday));
            array_push($dates, date("m", $fullday));
            array_push($dates, date("y", $fullday));
            // estos arreglos son solo para el reporte de ingreso
            array_push($ingresos, $this->ingresos);
            array_push($nomb, $this->nomb);
            array_push($dni, $this->dni);

            for($j = 0; $j < $count; $j++)
            {
                $temp = strtotime($this->res_date[$j]);
                array_push($res_date, date('d-m-Y', $temp));
            }

//        }

        return view('exports.ResInvoice', compact('count','start','end','OPC','CC', 'NC', 'DL','FI', 'FF', 'dates', 'reservaciones', 'i', 'ingresos', 'm_total', 'nomb', 'dni', 'res_date'));

    }
}
