<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Position extends Model
{
    
    protected $table='positions';
    public $primaryKey='id';
    public $incrementing = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name',
    ];

    public function personal()
    {
        return $this->belongsToMany('App\Staff','positions_staff','position_id','staff_id');
    }

}
