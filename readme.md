
## About the project

This is a project focused on the management of rentals bussiness developed to an specific commerce.

This project was developed using:

- [Laravel PHP Framework](https://laravel.com)
- [Vue JS Library](https://vuejs.org)
- [MySQL Database](https://mysql.org)
- [Datatables Plugin](https://datatables.net)
- [JQuery](https://jquery.org)
- [Bootstrap 4](https://getbootstrap.org)
