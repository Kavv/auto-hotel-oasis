OJO: Leer migrations_aws.txt antes.
1)
Secuencia de migraciones para la bd principal (system_tenant):
php artisan migrate --path=/database/migrations/migraciones_system1
php artisan migrate
php artisan migrate --path=/database/migrations/migraciones_system2

php artisan migrate --path=/database/migrations/migraciones_tenant1
php artisan migrate --path=/database/migrations/migraciones_tenant2

2)
Creacion de un nuevo tenant:
php artisan tenant:create-tenant "Nombre_de_la_BD" "Nombre_de_la_BD + .com"
Ejemplo:
php artisan tenant:create-tenant "Alquiler_Santana" "Alquiler_Santana.com"
php artisan tenant:create-tenant "Alquileres_Martha_Varela" "Alquileres_Martha_Varela.com"
Ojo: El nombre de la bd(Primer parametro) debe ser unico al igual que el nombre del host(Segundo parametro)

3)
ojo: Los siguientes comandos realizaran las migraciones en todos los tenant existentes,
si gusta hacer las migraciones en un tenant especifico puede utilizar --website_id=1
(websites es la tabla que posee el nombre de la bd)

Migraciones para los tenant:
php artisan tenancy:migrate --path=/database/migrations/migraciones_tenant1
php artisan tenancy:migrate --path=/database/migrations/migraciones_tenant2


4) 
Esta semilla es unicamente para el system 
Ejecutar los Seeders de la BD principal:
php artisan db:seed --class=SystemSeeder


5)
seeding in tenants:
php artisan tenancy:db:seed --class=TenantSeeder