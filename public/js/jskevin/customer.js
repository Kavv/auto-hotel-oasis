
    /* Busqueda, Agregación, limpieza de campos, autocompleta edad */
    var campocedula = $("#CC");
    var $customer_msg = $("#mensaje");
    var reservation_view = false;
    
    //Agrega al cliente mediante una consulta AJAX
    function customer_save(decision)
    {
      var ruta = "/cliente";
      var token = $("#token").val();
      $(".btnSent").attr("disabled",true);
      $("#loading").css('display','block');
      $('body').animate({scrollTop:0}, 'fast');
      return $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:$("#data").serialize(),
        success: function(res){
          if(res === 1)
          {
            if(decision=="guardar")
            {
              message(["Se agrego el cliente correctamente"],{objeto:$customer_msg, manual:true})
              limpiar();
              $(".btnSent").attr("disabled",false);
              $("#loading").css('display','none');
            }
            if(decision=="guardarv")//redireccion a la lista de clientes
              location.href ="/cliente/clientev/1/"+$("#CC").val();
            if(decision=="guardarr")//redireccion a la reservacion
              location.href ="/reservacion/reservacliente/"+$("#CC").val();
          }
          else
          {
            message(res,{objeto:$customer_msg, manual:true,tipo:"danger"});
            $(".btnSent").attr("disabled",false);
            $("#loading").css('display','none');
          }
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
          message(jqXHR,{objeto:$customer_msg, tipo:"danger"});
          $(".btnSent").attr("disabled",false);
          $("#loading").css('display','none');
      });
    }
    function limpiar()
    {
        if(reservation_view)
        {
            $("#Cedu").val($("#CC").val());
            $("#Nom").val($("#Nombre").val()+" "+$("#Apellido").val());
            $("#Dir").val($("#Direccion").val());
        }
            
        $("#CC").val("");
        $("#Nombre").val("");
        $("#Apellido").val("");
        $("#Edad").val("");
        $("#Sexo").val("");
        $("#Direccion").val("");
        $("#telefono").val("");
        $("#password").val("");
        $(".telefono-clone").remove();
    }
    $("#nacional").on("change",function()
    {
        if($("#nacional").prop('checked')==true)//Si es idenficacion Nica
        {
          campocedula.val("");
          $("#Edad").val("");
        }
    });
    function mostraredad()
    {
      if(edad!=0 && !isNaN(edad))
        $("#Edad").val(edad);
    }
    
    function addPhone()
    {
      telefono = $("#telefono");
      if(telefono.val() != "")
      {
        html= '<div class="telefono-clone col-md-3">'+
                  '<div class="form-group">'+
                    '<div class="input-group ">'+
                      '<input value="'+ telefono.val() +'" class="form-control telefono" placeholder="Ingrese el numero" name="telefono[]" type="tel">'+
                      '<div class="input-group-append">'+
                        '<button class="btn btn-danger fa fa-trash" onclick="removePhone(this);" type="button"></button>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                '</div>';

        telefono.val("");
        $("#telefonos").append(html);
        $("#telefono").focus()
      }
    }
    
    function removePhone(btn)
    {
      $(btn).parents('.telefono-clone').remove();
    }

    function polimorfismo({$customer_msg=$("#mensaje"), reservation_view=false}={})
    {
        this.$customer_msg = $customer_msg;
        this.reservation_view = reservation_view;
    }