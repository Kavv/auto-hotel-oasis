
        
        var row;
        $('#deleteModal').on('show.bs.modal', function (event) {
            //Mantenemos el id del elemento seleccionado
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            if(value != 0)
            $("#id").val(value);


            //Mantenemos la fila del elemento seleccionado
            row = button.parents('tr');
        });

        $('#delete').on( 'click', function () {
            if(typeof(input_delete) != "undefined" && input_delete != null)
                var data = $("#"+input_delete).val();
            else
                var data = $("#id").val();
            var route="/"+ base +"/"+ data;
            var token=$("#token").val();
            
            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                success: function(res){
                    
                    if(typeof(standar) != "undefined" && standar != null)
                        self[standar](res);
                    else{
                        message(['Se elimino correctamente'],{manual:true});
                        table.row(row).remove().draw( false );
                        $('body').animate({scrollTop:0}, 'fast');
                    }
                    
                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#deleteModalMessage"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });