var fila;
function editar(element, {success = "fill", id = "#id", f_data = "#form_edit"} ) {
    var $element = $(element);
    var value = $element.val();
    fila = $element.parents('tr');
    $(id).val(value);

    let route = '/' + base +'/show/'+value;
    $("#loading").css('display','block');
    $(f_data)[0].reset();

    $.get(route, function(res){
        if(success != null)
            self[success](res)
    });
}
// ContentType por defecto
let def_CT = 'application/x-www-form-urlencoded; charset=UTF-8';
function actualizar({success = "update_success", type = "PUT", id = "#id", 
f_data = "#form_edit", msg = "#mensaje", ctype = def_CT, pdata = false } )
{
    var route = "/" + base + "/" + $(id).val();
    var token = $("#token").val();
    $("#btn_actualizar").attr("disabled",true);
    $("#loading").css('display','block');
    if(!ctype)
        var formData = new FormData($(f_data)[0]);
    else
        var formData = $(f_data).serialize();
    return $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: type,
        dataType: 'json',
        data: formData,
        contentType: ctype,
        processData: pdata,
        success: function(res){
            if(success != null)
            {
                self[success](res);
                if(res.code == 1)
                    $(f_data)[0].reset();
            }
        }
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        console.log(msg)
        message(jqXHR,{objeto:$(msg), tipo:"danger"});
        $("#btn_actualizar").attr("disabled",false);
        $("#loading").css('display','none');
    });
}