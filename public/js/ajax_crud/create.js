function save({success = "success", msg = "#mensaje", f_data = "#form_data", ctype = false, pdata = false, scroll = '#add' })
{
    var route = "/" + base;
    var token = $("#token").val();
    $("#btn_guardar").attr("disabled",true);
    $("#loading").css('display','block');
    var formData = new FormData($(f_data)[0]);
    return $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data: formData,
        contentType: ctype,
        processData: pdata,
        success: function(res){
            if(success != null)
            {
                self[success](res)
                if(res.code == 1)
                $(f_data)[0].reset();
            }
                
        }
    }).fail( function( jqXHR, textStatus, errorThrown ) {
        message(jqXHR,{objeto:$(msg), tipo:"danger"});
        $("#btn_guardar").attr("disabled",false);
        $("#loading").css('display','none');
        $(scroll).animate({scrollTop:0}, 'fast');
    });
}