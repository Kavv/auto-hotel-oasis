
    /*==================================================================
    [ Focus input ]*/

    //Mantiene la estructura visual de cada input
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        });    
    });
  
  
    /*==================================================================
    [ Validate ]*/
    var input_atleta = $('.validate-input .input100');
    var input_user = $('.validate-input2 .input100');
    var input_trainer = $('.validate-input3 .input100');

    function Validation(login = 1) {
        var check = true;
        if(login == 1)
        {
            var input = input_atleta;
            var messages = ['Ingrese Identificación', 'Ingrese Contraseña'];
            var msg = $("#message").empty();
        }
        else if(login == 2)
        {
            var input = input_user;
            var messages = ['Ingrese Usuario', 'Ingrese Contraseña'];
            var msg = $("#message2").empty();
        }
        else if(login == 3)
        {
            var input = input_trainer;
            var messages = ['Ingrese Identificación', 'Ingrese Contraseña'];
            var msg = $("#message3").empty();
        }
            
        for(var i=0; i<input.length; i++) {
            if(input[i].value == "")
            {
                showValidate(input[i], messages[i]);
                check = false;
            }
        }
        msg.empty();

        return check;
    }



    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function showValidate(input,message="") {
        var thisAlert = $(input).parent();
        if(message != "")
            thisAlert[0].dataset.validate = message;

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    /*==================================================================
    [ Show pass ]*/
    var showPass = 0;
    function ShowPass(element)
    {
        var btn = $(element);
        var input = btn.siblings(".input100").eq(0);
        if(input.attr("type") == "password" )
            input.attr("type","text")
        else
            input.attr('type','password');
    }

