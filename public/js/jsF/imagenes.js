        var idDivAux;
        //apertura cuando toca una categoria de imagenes
        function ShowModalPart2(objeto)
        {
            idDivAux = objeto;

            //layout de carga
            $("#loading").css('display','block');
            jQuery.ajax({
                url : "imagenes/changeModal",
                method: 'POST',
                data: {data: objeto.id},
                success: function(res)
                {
                    if( res )
                    {
                        $("#interno").empty();
                        $("#interno").append(res);
                        $('#imageModal').modal('show');
                    }
                    else
                    {
                        console.log('nop');
                    }
            
                    $("#loading").css('display','none');
                }
            }); 
        }


        //function for csrf_token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("#token").val()
            }
        });
        

        function ChangeVideo()
        {
            if( !$('#videoFileInput').val()) {
                message(["Ingrese el video o imagen!"],{objeto:$("#mensaje"), manual:true, tipo:"danger" });
            }
            else{
                let $input, bg, data;
                if($("#vid").children().is('video')){
                    $input = $('#videoFileInput');
                    bg = $("#vid").children().children();
                    data = new FormData();
                }
                else{
                    $input = $('#videoFileInput');
                    bg = $("#vid").children();
                    data = new FormData();
                }
                // console.log(bg);
                //funciona
                data.append('name', $input[0].files[0].name);
                data.append('type', $input[0].files[0].type);
                data.append('video', $input[0].files[0]);
                data.append('bg', bg.attr('src'));
                data.append('id', bg[0].id);
                //impresiones de prueba
                // console.log($input[0].files[0].name);
                // console.log($input[0].files[0].type);
                // console.log($input[0].files[0]);
                // console.log(bg.attr('src'));
                // console.log(bg[0].id);
                message(["Espere a que el video sea subido!!"],{objeto:$("#mensaje"), manual:true, tipo:"danger" });
                $('#boton_submit').prop('disabled',true);
                // console.log($input[0].files[0]);
                //layout de carga
                $("#loading").css('display','block');
                $.ajax({
                    url: "imagenes/change/video",
                    type: 'POST',
                    method: 'POST',
                    dataType: "json",
                    data: data,
                    processData: false,
                    contentType: false,
                    success:function (res) {
                        $("#mensaje").empty();
                        // console.log(res.tipo);
                        //funcional
                        message(["Contenido subido!"],{objeto:$("#mensaje"), manual:true, tipo:"info" });
                        //declaracion de container general
                        let container_Mix = $('#vid').children();

                        //si lo que se subio es video
                        if(res.tipo === "video"){
                            //si el contenedor es un video
                            if( container_Mix[0].tagName === 'VIDEO' ) {
                                // console.log('video cambia a video');
                                bg.attr('src', res.src);
                                bg[0].id = res.id;
                                container_Mix[0].load();
                            }
                            //si el contenedor es una imagen
                            else{
                                // console.log('imagen cambia a video');
                                let contenedor = container_Mix[0];
                                let mixto = document.getElementById("vid");

                                let videoElement = document.createElement('video');
                                videoElement.autoplay = true;
                                videoElement.loop = true;

                                let source = document.createElement('source');
                                source.setAttribute("id", res.id);
                                source.src = res.src;
                                videoElement.appendChild(source);

                                mixto.replaceChild(videoElement, contenedor);
                            }
                        }
                        //si lo que se subio es imagen
                        else{
                            //si el contenedor es una imagen
                            if( container_Mix[0].tagName === 'IMG' ) {
                                // console.log('imagen cambia a imagen');
                                bg.attr('src',res.src);
                                bg[0].id = res.id;
                            }
                            //si el contenedor no es una imagen
                            else {
                                // console.log('video cambia a imagen');
                                let contenedor = document.getElementsByTagName('video');
                                let mixto = document.getElementById("vid");

                                let img = document.createElement('img');
                                img.setAttribute("id", res.id);
                                img.setAttribute('src', res.src);
                                img.style.height = "720";
                                img.setAttribute('class', 'w-100');

                                // video.parentNode.removeChild(video[0]);
                                mixto.replaceChild(img, contenedor[0]);
                            }
                        }
                        $('#video').empty();
                        $('#videoFileInput').val('');
                        $('#boton_submit').prop('disabled', false);
                        $("#loading").css('display','none');

                    }
                }).fail(function (jqXHR, textSatus, errorThrown) {
                    message(jqXHR,{tipo:"danger"});
                    $('#boton_submit').prop('disabled',false);

                    $("#loading").css('display','none');
                });
            }
        }
        //borrar video
        $("#borrarVideo").click(function (e) {
            //var bg = $("#vid").children().attr('src');
            let bg, data;
            if($("#vid").children().is('video')){
                bg = $("#vid").children().children();
                data = new FormData();
                data.append('id', bg[0].id);
            }
            else{
                bg = $("#vid").children();
                data = new FormData();
                data.append('id', bg[0].id);
            }

            //layout de carga
            $("#loading").css('display','block');
            $.ajax({
                url: 'imagenes/deleteVid',
                type: 'POST',
                method: 'POST',
                dataType: "json",
                data: data,
                processData: false,
                contentType: false,
                success:function (msm) {
                    $("#loading").css('display','none');

                    if(msm == 1){
                        $("#mensajeB").empty();
                        message(["No se puede borrar, contenido por defecto"],{objeto:$("#mensajeB"), manual:true, tipo:"danger" });
                    }
                    else{
                        let container_Mix = $('#vid').children();

                        $("#loading").css('display','none');
                        $("#mensajeB").empty();

                        if( container_Mix[0].tagName === 'VIDEO' ) {
                            message(["Se elimino correctamente"], {objeto: $("#mensajeB"), manual: true, tipo: "info"});
                            let video = $('#vid').children();
                            bg.attr('src', msm);
                            bg[0].id = "video1";
                            video[0].load();
                        }
                        else{

                            let contenedor = container_Mix[0];
                            let mixto = document.getElementById("vid");

                            let videoElement = document.createElement('video');
                            videoElement.setAttribute("class", "col-md-12");
                            videoElement.height = "720";
                            videoElement.autoplay = true;
                            videoElement.loop = true;

                            let source = document.createElement('source');
                            source.setAttribute("id", "video1");
                            source.src = msm;
                            videoElement.appendChild(source);

                            mixto.replaceChild(videoElement, contenedor);

                        }
                    }
                }
            }).fail(function (jqXHR, textSatus, errorThrown) {
                // message(jqXHR,{tipo:"danger"});
                // $('#boton_submit4').prop('disabled',false);

                $("#loading").css('display','none');
            });
        });

        function ChangeImage()
        {
            if (!$('#ImageFileInput').val()) {
                message(['Ingrese la imagen'],{objeto:$("#mensaje2"), manual:true, tipo:"danger"});
            }
            else{
                data = new FormData();
                let $input = $('#ImageFileInput');
                //
                if(part == 3){
                    bg = $('div.cb.carousel-item.active').children();
                    data.append('op','1');
                    console.log('1');
                }
                //
                else{
                    bg = $('div.inter.carousel-item.active').children();
                    data.append('op','2');
                    console.log('2');
                }


                data.append('img', $input[0].files[0]);
                data.append('bg', bg.attr('src'));
                data.append('id', bg[0].id);

                message(['Espere a que sea subida la Imagen'],{objeto:$("#mensaje2"), manual:true, tipo:"danger"});
                $('#boton_submit4').prop('disabled',true);
            
                //layout de carga
                $("#loading").css('display','block');
                $.ajax({
                    url: "imagenes/change/image",
                    type: 'POST',
                    method: 'POST',
                    dataType: "json",
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function ( res ) {
                        if( res.action == 1 ){
                            $("#mensaje2").empty();
                            message(['Imagen subida! '],{objeto:$("#mensaje2"), manual:true, tipo:"info"});
                            bg.attr('src', res.src);
                            bg[0].id = res.id;
                            let imagen2 = $("#" + idDivAux.id);
                            imagen2.css("background-image","url(" + res.src + ")");
                            $('#boton_submit4').prop('disabled',false);
                        }
                        else{
                            $("#mensaje2").empty();
                            message(['Imagen subida! '],{objeto:$("#mensaje2"), manual:true, tipo:"info"});
                            bg.attr('src',res.src);
                            bg[0].id = res.id;

                            $('#boton_submit4').prop('disabled',false);
                        }
            
                        $("#loading").css('display','none');
                    }
                }).fail(function (jqXHR, textSatus, errorThrown) {
                    message(jqXHR,{tipo:"danger"});
                    $('#boton_submit4').prop('disabled',false);
                    
                    $("#loading").css('display','none');
                });
            }
        }


        //borrar imagen
        function ImageDelete()
        {
            if(part == 3)
                bg = $('div.cb.carousel-item.active').children();
            else
                bg = $('div.inter.carousel-item.active').children();

            var cajon = $('div#inter.carousel-item.active').children();
            
            data = new FormData();
            data.append('id', bg[0].id);
            console.log(bg[0].id);
            
            //layout de carga
            $("#loading").css('display','block');
            $.ajax({
                url:'imagenes/deleteImg',
                type:'POST',
                method: 'POST',
                data: data,
                dataType: 'json',
                processData: false,
                contentType: false,
                success:function (res) {
                    if( res == 1 ){
                        $("#mensajeB2").empty();
                        message(["No se puede borrar, Imagen por defecto"],{objeto:$("#mensajeB2"), manual:true, tipo:"danger" });
                    }
                    else if(res.action == 2){
                        message(["Se elimino correctamente"],{objeto:$("#mensajeB2"), manual:true, tipo:"info" });
                        bg.attr('src', res.src);
                        bg[0].id = res.id;
                        let imagen2 = $("#"+ idDivAux.id);
                        imagen2.css("background-image","url(" + res.src + ")");
                    }
                    else{
                        message(["Se elimino correctamente"],{objeto:$("#mensajeB2"), manual:true, tipo:"info" });
                        bg.attr('src', res.src);
                        bg[0].id = res.id;
                    }
            
                    $("#loading").css('display','none');
                }
            });
        }



        //evento cuando se va a subir una imagen, la muestra
        $('#ImageFileInput').change(function(e) {
            let box = $("#mensaje2");
            let imag = $("#imagen4");
            if(!$('#ImageFileInput').val()){
                box.empty();
                imag.empty();
                message(['Ingrese una Imagen! '],{objeto:$(box), manual:true, tipo:"danger"});
            }
            else{
                //declaracion de lector
                let reader = new FileReader();
                arch = e.target.files[0];

                //si no es imagen
                if((!arch.type.match('image.*'))) {
                    box.empty();
                    imag.empty();
                    message(['No ingreso una Imagen! '],{objeto:$(box), manual:true, tipo:"danger"});
                }
                else{
                    box.empty();
                    imag.empty();
                    reader.onload = function() {
                        let imagen = document.getElementById('imagen4');
                            image = document.createElement('img');
                        //le pasamos la imagen al source
                        image.src = reader.result;
                        image.setAttribute('class','mx-auto d-block');

                        imagen.innerHTML = '';
                        imagen.append(image);
                    };
                    reader.readAsDataURL(arch);
                }
            }
        });
        //evento cuando se va a subir el video-imagen, lo muestra
        $('#videoFileInput').change(function(e) {
            let box = $("#mensaje");
            let vide = $("#video");
            // si el campo esta vacio
            if(!$('#videoFileInput').val()){
                box.empty();
                vide.empty();
                message(['Ingrese un Video! '],{objeto:$(box), manual:true, tipo:"danger"});
            }
            // si no esta vacio
            else{
                // declaracion de lector
                let reader = new FileReader();
                arch = e.target.files[0];

                // si no es video o imagen(proceso actualizado)
                if(!arch.type.match('video.*') && (!arch.type.match('image.*')))
                {
                    box.empty();
                    vide.empty();
                    message(['No ingreso un Video o una Imagen! '],{objeto:$(box), manual:true, tipo:"danger"});
                    return;
                }
                else{
                    // limpieza de contenedores
                    box.empty();
                    vide.empty();
                    // previsualizacion de un video
                    if(arch.type.match('video.*')){
                        reader.onload = function() {
                            var videon = document.getElementById('video');
                            vid = document.createElement('video');
                            //le pasamos la imagen al source
                            vid.src = reader.result;
                            vid.setAttribute('class','mx-auto d-block');
                            vid.autoplay = true;
                            vid.loop = true;

                            videon.innerHTML = '';
                            // añadimos el video
                            videon.append(vid);
                        };
                        reader.readAsDataURL(arch);
                    }
                    //previsualizacion si es una imagen
                    else{
                        reader.onload = function() {
                            let imagen = document.getElementById('video');
                            image = document.createElement('img');
                            //le pasamos la imagen al source
                            image.src = reader.result;
                            image.setAttribute('class','mx-auto d-block');

                            imagen.innerHTML = '';
                            // añadimos la imagen
                            imagen.append(image);
                        };
                        reader.readAsDataURL(arch);
                    }
                }
            }
        });

        var part = 0;
        function SelectPart(value)
        {
            part = value;
        }

        //limpiar division y contenido cuando se cierra el modal(video)
        $("#videoSaveModal").on('hidden.bs.modal', function (e) {
            $("#mensaje").empty();
            $("#video").empty();
            $("#videoFileInput").val('');
        });

        $("#saveModal").on('hidden.bs.modal', function (e) {
            $("#mensaje2").empty();
            $("#imagen4").empty();
            $("#ImageFileInput").val('');
        });


        $("#videoDeleteModal").on('hidden.bs.modal', function (e) {
            $("#mensajeB").empty();
        });
        
        $("#imageDeleteModal").on('hidden.bs.modal', function (e) {
            $("#mensajeB2").empty();
        });