<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

route::get('/', 'LoginController@index')->name('login.index');
route::get('/login', 'LoginController@index')->name('login');
route::get('/logout', 'LoginController@logout')->name('logout');
route::post('/newlogin','LoginController@login')->name('login.in');

Route::get('/landing','FrontController@landing');
Route::post('/showCaru','FrontController@show');

Route::get('/home', 'HomeController@index')->name('home');



//Si lo se, debo aplicarlo con resource, lo hare en la segunda version!
Route::middleware(['auth'])->group(function() {
    Route::get('/principal','FrontController@principal')->name('principal.index');
    
    // Parqueo
    Route::group(['prefix' => 'parqueo'],function(){
        Route::get('/','ParqueoController@index')->name('parqueo.index')
        ->middleware('permission:cliente.index');//parqueo ver

        Route::get('/create','ParqueoController@create')->name('parqueo.create')
        ->middleware('permission:cliente.create');//parqueo crear

        Route::post('/','ParqueoController@store')->name('parqueo.store')
        ->middleware('permission:cliente.create');//parqueo crear

        Route::put('/{parking}','ParqueoController@update')->name('parqueo.update')
        ->middleware('permission:cliente.edit');//parqueo editar

        Route::get('/show/{valor}','ParqueoController@show')->name('parqueo.show')
        ->middleware('permission:cliente.index');//parqueo ver

        Route::delete('/{parking}','ParqueoController@destroy')->name('parqueo.destroy')
        ->middleware('permission:cliente.destroy');//parqueo eliminar

        Route::get('/tiempo_real','ParqueoController@real_time')->name('parqueo.real_time')
        ->middleware('permission:cliente.index');//parqueo ver
        Route::post('/update_quantity','ParqueoController@update_quantity')->name('parqueo.update_quantity')
        ->middleware('permission:cliente.index');//parqueo ver
    });


    // Categorias
    Route::group(['prefix' => 'categoria'],function(){
        Route::get('/','CategoriaController@index')->name('categoria.index')
        ->middleware('permission:cliente.index');//parqueo ver

        Route::get('/create','CategoriaController@create')->name('categoria.create')
        ->middleware('permission:cliente.create');//parqueo crear

        Route::post('/','CategoriaController@store')->name('categoria.store')
        ->middleware('permission:cliente.create');//parqueo crear

        Route::put('/{valor}','CategoriaController@update')->name('categoria.update')
        ->middleware('permission:cliente.edit');//parqueo editar

        Route::get('/show/{valor}','CategoriaController@show')->name('categoria.show')
        ->middleware('permission:cliente.index');//parqueo ver

        Route::delete('/{valor}','CategoriaController@destroy')->name('categoria.destroy')
        ->middleware('permission:cliente.destroy');//parqueo eliminar
    });

    // Habitacion
    Route::group(['prefix' => 'habitacion'],function(){
        Route::get('/','HabitacionController@index')->name('habitacion.index')
        ->middleware('permission:cliente.index');//parqueo ver

        Route::get('/create','HabitacionController@create')->name('habitacion.create')
        ->middleware('permission:cliente.create');//parqueo crear

        Route::post('/','HabitacionController@store')->name('habitacion.store')
        ->middleware('permission:cliente.create');//parqueo crear

        Route::put('/{valor}','HabitacionController@update')->name('habitacion.update')
        ->middleware('permission:cliente.edit');//parqueo editar

        Route::get('/show/{valor}','HabitacionController@show')->name('habitacion.show')
        ->middleware('permission:cliente.index');//parqueo ver

        Route::delete('/{valor}','HabitacionController@destroy')->name('habitacion.destroy')
        ->middleware('permission:cliente.destroy');//parqueo eliminar
    });

    //Cliente
    Route::group(['prefix' => 'cliente'],function(){
        //RESOURCES
        Route::get('/','ClienteController@index')->name('cliente.index')
        ->middleware('permission:cliente.index');//clientes ver

        Route::get('/create','ClienteController@create')->name('cliente.create')
        ->middleware('permission:cliente.create');//cliente crear

        Route::post('/','ClienteController@store')->name('cliente.store')
        ->middleware('permission:cliente.create');//cliente crear

        Route::get('/{valor}/edit','ClienteController@edit')->name('cliente.edit')
        ->middleware('permission:cliente.edit');//cliente editar

        Route::put('/{customer}','ClienteController@update')->name('cliente.update')
        ->middleware('permission:cliente.edit');//cliente editar

        Route::get('/show/{customer}','ClienteController@show')->name('cliente.show')
        ->middleware('permission:cliente.index');//cliente ver

        Route::get('/search_dni/{valor}','ClienteController@search_dni')->name('reservacion.search_dni')
        ->middleware('permission:reservacion.index');

        Route::get('/detail/{valor}','ClienteController@detail')->name('cliente.detail')
        ->middleware('permission:cliente.index');//cliente ver

        Route::delete('/{valor}','ClienteController@destroy')->name('cliente.destroy')
        ->middleware('permission:cliente.destroy');//cliente eliminar
        //

        Route::get('/clientev/{valor?}/{valor2?}','ClienteController@index')->name('clientev.index')
        ->middleware('permission:cliente.index');//luego de agregar un cliente redireccionamos al index
        
        Route::get('/telefonos/{valor?}','ClienteController@telefonos')->name('cliente.telefonos')
        ->middleware('permission:cliente.index');//clientes ver
    });


    //Reservacion
    Route::group(['prefix' => 'reservacion'], function() {
        
        Route::get('/','ReservacionController@index')->name('reservacion.index')
        ->middleware('permission:reservacion.index');
        
        Route::get('/create','ReservacionController@create')->name('reservacion.create')
        ->middleware('permission:reservacion.index');
        
        Route::post('/','ReservacionController@store')->name('reservacion.store')
        ->middleware('permission:reservacion.create');
        
        Route::get('/{valor}/edit','ReservacionController@edit')->name('reservacion.edit')
        ->middleware('permission:reservacion.edit');
        
        Route::put('/{valor}','ReservacionController@update')->name('reservacion.update')
        ->middleware('permission:reservacion.edit');
        
        Route::get('/show/{customer}','ReservacionController@show')->name('reservacion.show')
        ->middleware('permission:reservacion.index');


        Route::delete('/{valor}','ReservacionController@destroy')->name('reservacion.destroy')
        ->middleware('permission:reservacion.destroy');


        
        Route::get('/disponibilidad','ReservacionController@availability')->name('reservacion.availability')
        ->middleware('permission:reservacion.index');

        Route::get('/confirmar','ReservacionController@confirm')->name('reservacion.confirm')
        ->middleware('permission:reservacion.index');

        Route::get('/verify','ReservacionController@verify')->name('reservacion.verify')
        ->middleware('permission:reservacion.index');

        Route::post('/change_state','ReservacionController@change_state')->name('reservacion.change_state')
        ->middleware('permission:reservacion.index');


        Route::get('/calendario','CalendarController@index')->name('calendario.index')
        ->middleware('permission:reservacion.index'); 
        
        //redireccion a reserva con los datos del cliente recien agregado
        Route::get('/reservacliente/{valor?}','ReservacionController@create')->name('reserva.create')
        ->middleware('permission:reservacion.create');
        //crea una nueva comida en el menu
        Route::post('/reservamenu','ReservacionController@menu')->name('reserva.create')
        ->middleware('permission:menus.create');
        Route::get('/recargaarticulos/{valor1}/{valor2}','ReservacionController@recargaarticulos')->name('reserva.recargaArticulo')
        ->middleware('permission:inventario.index');  

        //Mostrar la proforma
        Route::post('/proforma','ReservacionController@proforma')->name('reserva.proforma')
        ->middleware('permission:reservacion.index');
        
    });


    //Pagos
    Route::group(['prefix' => 'pago'],function(){
        //RESOURCES
        Route::get('/','PagoController@index')->name('pago.index')
        ->middleware('permission:cliente.index');//Pagos ver

        Route::get('/create','PagoController@create')->name('pago.create')
        ->middleware('permission:cliente.create');//Pagos crear

        Route::post('/','PagoController@store')->name('pago.store')
        ->middleware('permission:cliente.create');//Pagos crear

        Route::get('/{valor}/edit','PagoController@edit')->name('pagos.edit')
        ->middleware('permission:cliente.edit');//Pagos editar

        Route::put('/{valor}','PagoController@update')->name('pago.update')
        ->middleware('permission:cliente.edit');//Pagos editar

        Route::get('/show/{valor}','PagoController@show')->name('pago.show')
        ->middleware('permission:cliente.index');//Pagos ver

        Route::get('/payments/{valor}','PagoController@payments')->name('pago.show')
        ->middleware('permission:cliente.index');//Pagos ver
        
        
        Route::delete('/{valor}','PagoController@destroy')->name('pago.destroy')
        ->middleware('permission:cliente.destroy');//Pagos eliminar
        
        Route::get('/f-payday','PagoController@filtrar')->name('pago.filtrar')
        ->middleware('permission:cliente.index');//Pagos ver

        Route::post('/actualizarinfo','PagoController@actualizarinfo')->name('pago.update_info')
        ->middleware('permission:cliente.index');//Pagos ver

        Route::get('/list','PagoController@payments_list')->name('pago.payments_list')
        ->middleware('permission:cliente.index');//Pagos ver

        
    });
    
    //leccion
    Route::group(['prefix' => 'leccion'],function(){
        //RESOURCES
        Route::get('/','LeccionController@index')->name('leccion.index')
        ->middleware('permission:cliente.index');//Pagos ver

        Route::get('/create','LeccionController@create')->name('leccion.create')
        ->middleware('permission:cliente.create');//Pagos crear

        Route::post('/','LeccionController@store')->name('leccion.store')
        ->middleware('permission:cliente.create');//Pagos crear

        Route::put('/{valor}','LeccionController@update')->name('leccion.update')
        ->middleware('permission:cliente.edit');//Pagos editar

        Route::get('/show/{valor}','LeccionController@show')->name('leccion.show')
        ->middleware('permission:cliente.index');//Pagos ver
        
        Route::delete('/{valor}','LeccionController@destroy')->name('leccion.destroy')
        ->middleware('permission:cliente.destroy');//Pagos eliminar
        
        Route::get('/calendar/{valor}','LeccionController@calendar')->name('leccion.calendar')
        ->middleware('permission:cliente.index');//Pagos ver

        Route::get('/servicios/{valor}/{valor2?}','LeccionController@services')->name('leccion.services')
        ->middleware('permission:cliente.index');//Pagos ver
    });


    // Calendario
    Route::group(['prefix' => 'calendario'],function(){
        //RESOURCES
        Route::get('/','CalendarioController@index')->name('calendar.index')
        ->middleware('permission:cliente.index');//Pagos ver

        Route::get('/show/{valor}','CalendarioController@show')->name('calendar.show')
        ->middleware('permission:cliente.index');//Pagos ver

        Route::get('/atlhetas','CalendarioController@athlete_list')->name('calendar.athlete_list')
        ->middleware('permission:cliente.index');//Pagos ver

        Route::get('/horario_atleta','CalendarioController@set_athlete')->name('calendar.set_athlete')
        ->middleware('permission:cliente.index');//Pagos ver

        Route::get('/entrenador','CalendarioController@trainer_list')->name('calendar.trainer_list')
        ->middleware('permission:cliente.index');//Pagos ver

        Route::get('/horario_entrenador','CalendarioController@set_trainer')->name('calendar.set_trainer')
        ->middleware('permission:cliente.index');//Pagos ver
    });

    //Personal
    Route::group(['prefix' => 'personal'], function() {
        //RESOURCES
        Route::get('/','PersonalController@index')->name('personal.index')
        ->middleware('permission:personal.index');

        Route::get('/create','PersonalController@create')->name('personal.create')
        ->middleware('permission:personal.create');

        Route::post('/','PersonalController@store')->name('personal.store')
        ->middleware('permission:personal.create');

        //ruta para las listas dependiendo del cargo
        Route::get('/cargo/{valor}', 'PersonalController@pcargo')->name('personal.cargo');

        Route::get('/{valor}/edit','PersonalController@edit')->name('personal.edit')
        ->middleware('permission:personal.edit');
        
        Route::put('/{valor}','PersonalController@update')->name('personal.update')
        ->middleware('permission:personal.edit');
        
        Route::get('/show/{valor}','PersonalController@show')->name('personal.show')
        ->middleware('permission:personal.index');
        
        Route::delete('/{valor}','PersonalController@destroy')->name('personal.destroy')
        ->middleware('permission:personal.destroy');
        //

        Route::get('/personalv/{valor?}/{valor2?}','PersonalController@index')->name('personalv.index')
        ->middleware('permission:personal.index');
        /* Route::post('/personalcargo','PersonalController@agregarcargo')->name('personal.agregarcargo')
        ->middleware('permission:cargo.index'); */
        /* Route::post('/pcargoupdate','PersonalController@actualizarcargos')->name('personal.actualizarcargo')
        ->middleware('permission:cargo.index'); */
        Route::get('/cargopersonal/{cedula?}','PersonalController@cargos')->name('personal.cargos')
        ->middleware('permission:cargo.index');

        //Retornar los numeros de telefono del personal
        Route::get('/telefonos/{valor?}','PersonalController@telefonos')->name('personal.telefonos')
        ->middleware('permission:personal.index');
    });

    //Cargo
    Route::group(['prefix' => 'cargo'], function() {
        //RESOURCES
        
        Route::get('/','CargosController@index')->name('cargo.index')
        ->middleware('permission:cargo.index');
        
        Route::get('/create','CargosController@create')->name('cargo.create')
        ->middleware('permission:cargo.create');

        //ruta para listar el personal con el respectivo cargo seleccionado
        Route::get('/list', 'CargosController@list')->name('cargo.lista');

        Route::post('/','CargosController@store')->name('cargo.store')
        ->middleware('permission:cargo.create');
        
        Route::get('/{valor}/edit','CargosController@edit')->name('cargo.edit')
        ->middleware('permission:cargo.edit');
        
        Route::put('/{valor}','CargosController@update')->name('cargo.update')
        ->middleware('permission:cargo.edit');
        
        Route::delete('/{valor}','CargosController@destroy')->name('cargo.destroy')
        ->middleware('permission:cargo.destroy');
        
        Route::get('/cargov/{valor?}/{valor2?}','CargosController@index')->name('cargov.index')
        ->middleware('permission:cargo.index');
        
        Route::post('/cargosave','CargosController@guardar')->name('cargosave.guardar')
        ->middleware('permission:cargo.create');

        Route::get('/listacargo','CargosController@recargarlista')->name('listacargo.recargar')
        ->middleware('permission:cargo.index');

    });

    //Vetado
    Route::group(['prefix' => 'vetado'], function() {
        //RESOURCES
        Route::get('/','vetadocontroller@index')->name('vetado.index')
        ->middleware('permission:vetado.index');
        
        Route::get('/create','vetadocontroller@create')->name('vetado.create')
        ->middleware('permission:vetado.create');
        
        Route::post('/','vetadocontroller@store')->name('vetado.store')
        ->middleware('permission:vetado.create');
        
        Route::get('/{valor}/edit','vetadocontroller@edit')->name('vetado.edit')
        ->middleware('permission:vetado.edit');
        
        Route::put('/{valor}','vetadocontroller@update')->name('vetado.update')
        ->middleware('permission:vetado.edit');
        
        Route::get('/show/{valor}','vetadocontroller@show')->name('vetado.show')
        ->middleware('permission:vetado.index');
        
        Route::delete('/{valor}','vetadocontroller@destroy')->name('vetado.destroy')
        ->middleware('permission:vetado.destroy');

        Route::get('/filtro/{valor0?}/{valor1?}/{valor2?}','vetadocontroller@index')->name('vetados.index')
        ->middleware('permission:vetado.index');


        Route::post('/vetar','vetadocontroller@vetar')->name('vetar.vetar')
        ->middleware('permission:vetado.create');
        
        Route::get('/listacliente/{valor?}','vetadocontroller@listacliente')->name('listac.lc')
        ->middleware('permission:vetado.index');
        Route::get('/listapersonal/{valor?}','vetadocontroller@listapersonal')->name('listap.lp')
        ->middleware('permission:vetado.index');
        Route::get('/descripcion/{valor?}/{valor2?}','vetadocontroller@descripcion')->name('descripcion.vetado')
        ->middleware('permission:vetado.index');
        
    });

    //Servicios
    Route::group(['prefix' => 'servicio'], function() {
        Route::get('/','ServicioController@index')->name('servicio.index')
        ->middleware('permission:servicio.index');
        
        Route::get('/create','ServicioController@create')->name('servicio.create')
        ->middleware('permission:servicio.create');
        
        Route::post('/','ServicioController@store')->name('servicio.store')
        ->middleware('permission:servicio.create');
        
        Route::get('/{valor}/edit','ServicioController@edit')->name('servicio.edit')
        ->middleware('permission:servicio.edit');
        
        Route::put('/{valor}','ServicioController@update')->name('servicio.update')
        ->middleware('permission:servicio.edit');
        
        Route::get('/show/{valor}','ServicioController@show')->name('servicio.show')
        ->middleware('permission:servicio.index');
        
        Route::delete('/{valor}','ServicioController@destroy')->name('servicio.destroy')
        ->middleware('permission:servicio.destroy');

        Route::get('/serviciov/{valor?}/{valor2?}/{valor3?}','ServicioController@index')->name('serviciov.index')
        ->middleware('permission:servicio.index');
    });
    //Inventario
    Route::group(['prefix' => 'inventario'], function() {
        Route::get('/','InventarioController@index')->name('inventario.index')
        ->middleware('permission:inventario.index');

        Route::get('/create','InventarioController@create')->name('inventario.create')
        ->middleware('permission:inventario.create');

        Route::post('/','InventarioController@store')->name('inventario.store')
        ->middleware('permission:inventario.create');

        Route::get('/{valor}/edit','InventarioController@edit')->name('inventario.edit')
        ->middleware('permission:inventario.edit');

        Route::put('/{valor}','InventarioController@update')->name('inventario.update')
        ->middleware('permission:inventario.edit');

        Route::get('/show/{item}','InventarioController@show')->name('inventario.show')
        ->middleware('permission:inventario.show');

        Route::delete('/{valor}','InventarioController@destroy')->name('inventario.destroy')
        ->middleware('permission:inventario.destroy');

        Route::get('/inventariov/{valor?}/{valor2?}/{valor3?}','InventarioController@index')->name('inventariov.index')
        ->middleware('permission:inventario.index');    

        Route::get('/servicelist','InventarioController@serviceList')->name('inventario.servicelist')
        ->middleware('permission:inventario.create');
    });

    //Menu
    Route::group(['prefix' => 'menus'], function() {
        
        Route::get('/','MenuController@index')->name('menus.index')
        ->middleware('permission:menus.index');
        
        Route::get('/create','MenuController@create')->name('menus.create')
        ->middleware('permission:menus.create');
        
        Route::post('/','MenuController@store')->name('menus.store')
        ->middleware('permission:menus.create');
        
        Route::get('/{valor}/edit','MenuController@edit')->name('menus.edit')
        ->middleware('permission:menus.edit');
        
        Route::put('/{valor}','MenuController@update')->name('menus.update')
        ->middleware('permission:menus.edit');
        
        Route::get('/show/{valor}','MenuController@show')->name('menus.show')
        ->middleware('permission:menus.index');
        
        Route::delete('/{valor}','MenuController@destroy')->name('menus.destroy')
        ->middleware('permission:menus.destroy');
        
        Route::get('/{valor?}/{valor2?}','MenuController@index');//luego de agregar un menu redireccionamos al index        
    });


    //Roles
    Route::group(['prefix' => 'roles'], function() {
        Route::get('/','RoleController@index')->name('roles.index')
        ->middleware('permission:role.index');

        Route::get('/create','RoleController@create')->name('roles.create')
        ->middleware('permission:role.create');
        
        Route::post('/','RoleController@store')->name('roles.store')
        ->middleware('permission:role.create');
        
        Route::get('/{role}/edit','RoleController@edit')->name('roles.edit')
        ->middleware('permission:role.edit');
        
        Route::put('/{role}','RoleController@update')->name('roles.update')
        ->middleware('permission:role.edit');
        
        Route::get('/show/{role}','RoleController@show')->name('roles.show')
        ->middleware('permission:role.show');
        
        Route::delete('/{role}','RoleController@destroy')->name('roles.destroy')
        ->middleware('permission:role.destroy');

        Route::get('/ver/{valor?}','RoleController@ver')->name('roles.ver')
        ->middleware('permission:role.index');
    });

    //Usuario
    Route::group(['prefix' => 'usuarios'], function() {
        //RESOURCES
        Route::get('/','usuariocontroller@index')->name('users.index')
        ->middleware('permission:users.index');
        
        Route::get('/create','usuariocontroller@create')->name('users.create')
        ->middleware('permission:users.create');
        
        Route::post('/','usuariocontroller@store')->name('users.store')
        ->middleware('permission:users.create');
        
        Route::get('/{role}/edit','usuariocontroller@edit')->name('users.edit')
        ->middleware('permission:users.edit');
        
        Route::put('/{role}','usuariocontroller@update')->name('users.update')
        ->middleware('permission:users.edit');
        
        Route::get('/show/{role}','usuariocontroller@show')->name('users.show')
        ->middleware('permission:users.show');
        
        Route::delete('/{role}','usuariocontroller@destroy')->name('users.destroy')
        ->middleware('permission:users.destroy'); 

        //
        Route::get('/usuariov/{valor?}/{valor2?}','usuariocontroller@index');
        Route::get('/userexist/{valor1}/{valor2}','usuariocontroller@existe');//verificar si existe el correo o el name

    });

    //imagenes
    Route::group(['prefix' => 'imagenes'], function() {
        //RESOURCES
        Route::get('/', 'ImageController@index')->name('imagenes.index')
            ->middleware('permission:imagenes.index');

        Route::post('/changeModal', 'ImageController@changeInModal')->name('imagenes.changeModal')
            ->middleware('permission:imagenes.index');

        Route::post('/change/image', 'ImageController@changeImg')->name('imagenes.changeImage')
            ->middleware('permission:imagenes.create');

        Route::post('/change/video', 'ImageController@changeVid')->name('imagenes.changeVideo')
            ->middleware('permission:imagenes.create');

        Route::post('/deleteVid', 'ImageController@deleteVid')->name('imagenes.deleteVideo')
            ->middleware('permission:imagenes.destroy');

        Route::post('/deleteImg', 'ImageController@deleteImg')->name('imagenes.deleteImage')
            ->middleware('permission:imagenes.destroy');
    });

    //Reportes
    Route::group(['prefix' => 'reportes'], function() {
        //INDEX
        Route::get('/','ReportController@index')->name('reporte.index')
        ->middleware('permission:reporte.index');
        Route::get('/clientesNuevos/{valor1?}/{valor2?}','ReportController@ClientesNuevos')->name('reportes.clientesNuevos')
        ->middleware('permission:reporte.index');
        //Cliente
        Route::get('/clientesAgregados/{valor1?}/{valor2?}','ReportController@ClientesAgregados')->name('reportes.clientesAgregados')
        ->middleware('permission:reporte.index');
        Route::get('/clientesEliminados/{valor1?}/{valor2?}','ReportController@ClientesEliminados')->name('reportes.clientesEliminados')
        ->middleware('permission:reporte.index');

        //Personal
        Route::get('/personalAgregados/{valor1?}/{valor2?}','ReportController@PersonalAgregados')->name('reportes.personalAgregados')
        ->middleware('permission:reporte.index');
        Route::get('/personalEliminados/{valor1?}/{valor2?}','ReportController@PersonalEliminados')->name('reportes.personalEliminados')
        ->middleware('permission:reporte.index');

        //Reservaciones
        Route::get('/reservacionesAgregadas/{valor1?}/{valor2?}','ReportController@ReservacionesAgregadas')->name('reportes.reservacionesAgregadas')
        ->middleware('permission:reporte.index');
        Route::get('/eventos/{valor1?}/{valor2?}','ReportController@eventos')->name('reportes.eventos')
        ->middleware('permission:reporte.index');
        Route::get('/ingresos/{valor1?}/{valor2?}','ReportController@ingresos')->name('reportes.ingresos')
        ->middleware('permission:reporte.index');

        //lipe excel
        Route::get('/excel/{valor1?}/{valor2?}/{valor3?}', 'ReportLController@excel')->name('reportes.excel')
            ->middleware('permission:reporte.index');
        Route::get('/excelReser/{valor1?}/{valor2?}/{valor3?}', 'ReportLController@excel_res')->name('reportes.excelRes')
            ->middleware('permission:reporte.index');
        Route::get('/excelEven/{valor1?}/{valor2?}/{valor3?}', 'ReportLController@excel_even')->name('reportes.excelEven')
            ->middleware('permission:reporte.index');
        Route::get('/excelInven/{valor1?}/{valor2?}/{valor3?}', 'ReportLController@excel_inv')->name('reportes.excelInven')
            ->middleware('permission:reporte.index');
        //solo estan planteados, no los he hecho 11/09/19
        //articulo y buffet mas usado
        Route::get('/excelArtMasUsado/{valor1?}/{valor2?}/{valor3?}', 'ReportLController@artMasUsado')->name('reportes.excelArt')
            ->middleware('permission:reporte.index');
        Route::get('/excelBuffMasUsado/{valor1?}/{valor2?}/{valor3?}', 'ReportLController@buffMasUsado')->name('reportes.excelBuff')
            ->middleware('permission:reporte.index');

        //inventario
        Route::get('inventario/{valor1?}/{valor2?}', 'ReportController@inventario')->name('reporte.inventario')
            ->middleware('permission:reporte.index');
        //Articulo y buffet mas usado(estan en proceso 11/09/19)
        Route::get('artMasUsado/{valor1?}/{valor2?}', 'ReportController@articuloMasUsado')->name('reporte.artMasUsado')
            ->middleware('permission:reporte.index');
        Route::get('buffMasUsado/{valor1?}/{valor2?}', 'ReportController@buffetMasUsado')->name('reporte.buffMasUsado')
            ->middleware('permission:reporte.index');
        
    });
    //Reportes
    Route::group(['prefix' => 'graficas'], function() {
        Route::get('/ingresos/{valor1?}/{valor2?}','ReportController@GraficaReservacionesNuevas')->name('graficos.ReservacionesNuevas')
        ->middleware('permission:reporte.index');
        //ruta de lipe para el grafico de inventario
        /*Route::get('/ingresos/{valor1?}/{valor2?}','ReportController@GraficaReservacionesNuevas')->name('graficos.ReservacionesNuevas')
        ->middleware('permission:reporte.index');*/
    });
});