<?php

use     Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //Cliente
        Permission::create([
            'name' => 'Navegar cliente',
            'slug' => 'cliente.index',
            'description' => 'Lista y navega todos los clientes del sistema',
        ]);
        Permission::create([
            'name' => 'Ver detalle de cliente',
            'slug' => 'cliente.show',
            'description' => 'Ver en detalle cada cliente del sistema',
        ]);
        Permission::create([
            'name' => 'Crear cliente',
            'slug' => 'cliente.create',
            'description' => 'Crear un cliente del sistema',
        ]);
        Permission::create([
            'name' => 'Edicion cliente',
            'slug' => 'cliente.edit',
            'description' => 'Editar cualquier dato de un cliente del sistema',
        ]);
        Permission::create([
            'name' => 'Eliminar cliente',
            'slug' => 'cliente.destroy',
            'description' => 'Eliminar cualquier cliente del sistema',
        ]);

         //Personal
         Permission::create([
            'name' => 'Navegar personal',
            'slug' => 'personal.index',
            'description' => 'Lista y navega el personal del sistema',
        ]);
        Permission::create([
            'name' => 'Ver detalle de personal',
            'slug' => 'personal.show',
            'description' => 'Ver en detalle cada personal del sistema',
        ]);
        Permission::create([
            'name' => 'Edicion personal',
            'slug' => 'personal.edit',
            'description' => 'Editar cualquier dato de un personal del sistema',
        ]);
        Permission::create([
            'name' => 'Eliminar personal',
            'slug' => 'personal.destroy',
            'description' => 'Eliminar cualquier personal del sistema',
        ]);
        Permission::create([
            'name' => 'Crear personal',
            'slug' => 'personal.create',
            'description' => 'Crear un personal del sistema',
        ]);


        //Cargo
        Permission::create([
            'name' => 'Navegar cargo',
            'slug' => 'cargo.index',
            'description' => 'Lista y navega los cargos del sistema',
        ]);
        Permission::create([
            'name' => 'Ver detalle de cargo',
            'slug' => 'cargo.show',
            'description' => 'Ver en detalle cada cargo del sistema',
        ]);
        Permission::create([
            'name' => 'Edicion cargo',
            'slug' => 'cargo.edit',
            'description' => 'Editar cualquier dato de un cargo del sistema',
        ]);
        Permission::create([
            'name' => 'Eliminar cargo',
            'slug' => 'cargo.destroy',
            'description' => 'Eliminar cualquier cargo del sistema',
        ]);
        Permission::create([
            'name' => 'Crear cargo',
            'slug' => 'cargo.create',
            'description' => 'Crear un cargo del sistema',
        ]);

        //Vetado
         Permission::create([
            'name' => 'Navegar vetado',
            'slug' => 'vetado.index',
            'description' => 'Lista y navega el vetado del sistema',
        ]);
        Permission::create([
            'name' => 'Ver detalle de vetado',
            'slug' => 'vetado.show',
            'description' => 'Ver en detalle cada vetado del sistema',
        ]);
        Permission::create([
            'name' => 'Edicion vetado',
            'slug' => 'vetado.edit',
            'description' => 'Editar cualquier dato de un vetado del sistema',
        ]);
        Permission::create([
            'name' => 'Eliminar vetado',
            'slug' => 'vetado.destroy',
            'description' => 'Eliminar cualquier vetado del sistema',
        ]);
        Permission::create([
            'name' => 'Crear vetado',
            'slug' => 'vetado.create',
            'description' => 'Crear un vetado del sistema',
        ]);

        //Servicios
        Permission::create([
            'name' => 'Navegar servicios',
            'slug' => 'servicio.index',
            'description' => 'Lista y navega el servicios del sistema',
        ]);
        Permission::create([
            'name' => 'Ver detalle de servicios',
            'slug' => 'servicio.show',
            'description' => 'Ver en detalle cada servicios del sistema',
        ]);
        Permission::create([
            'name' => 'Edicion servicios',
            'slug' => 'servicio.edit',
            'description' => 'Editar cualquier dato de un servicios del sistema',
        ]);
        Permission::create([
            'name' => 'Eliminar servicios',
            'slug' => 'servicio.destroy',
            'description' => 'Eliminar cualquier servicios del sistema',
        ]);
        Permission::create([
            'name' => 'Crear servicios',
            'slug' => 'servicio.create',
            'description' => 'Crear un servicios del sistema',
        ]);


        //Inventario
        Permission::create([
            'name' => 'Navegar inventario',
            'slug' => 'inventario.index',
            'description' => 'Lista y navega el inventario del sistema',
        ]);
        Permission::create([
            'name' => 'Ver detalle de inventario',
            'slug' => 'inventario.show',
            'description' => 'Ver en detalle cada inventario del sistema',
        ]);
        Permission::create([
            'name' => 'Edicion inventario',
            'slug' => 'inventario.edit',
            'description' => 'Editar cualquier dato de un inventario del sistema',
        ]);
        Permission::create([
            'name' => 'Eliminar inventario',
            'slug' => 'inventario.destroy',
            'description' => 'Eliminar cualquier inventario del sistema',
        ]);
        Permission::create([
            'name' => 'Crear inventario',
            'slug' => 'inventario.create',
            'description' => 'Crear un inventario del sistema',
        ]);


        //Menu
        Permission::create([
            'name' => 'Navegar menu',
            'slug' => 'menus.index',
            'description' => 'Lista y navega el menu del sistema',
        ]);
        Permission::create([
            'name' => 'Ver detalle de menu',
            'slug' => 'menus.show',
            'description' => 'Ver en detalle cada menu del sistema',
        ]);
        Permission::create([
            'name' => 'Edicion menu',
            'slug' => 'menus.edit',
            'description' => 'Editar cualquier dato de un menu del sistema',
        ]);
        Permission::create([
            'name' => 'Eliminar menu',
            'slug' => 'menus.destroy',
            'description' => 'Eliminar cualquier menu del sistema',
        ]);
        Permission::create([
            'name' => 'Crear menu',
            'slug' => 'menus.create',
            'description' => 'Crear un menu del sistema',
        ]);


        //Reservacion
        Permission::create([
            'name' => 'Navegar reservacion',
            'slug' => 'reservacion.index',
            'description' => 'Lista y navega el reservacion del sistema',
        ]);
        Permission::create([
            'name' => 'Ver detalle de reservacion',
            'slug' => 'reservacion.show',
            'description' => 'Ver en detalle cada reservacion del sistema',
        ]);
        Permission::create([
            'name' => 'Edicion reservacion',
            'slug' => 'reservacion.edit',
            'description' => 'Editar cualquier dato de un reservacion del sistema',
        ]);
        Permission::create([
            'name' => 'Eliminar reservacion',
            'slug' => 'reservacion.destroy',
            'description' => 'Eliminar cualquier reservacion del sistema',
        ]);
        Permission::create([
            'name' => 'Crear reservacion',
            'slug' => 'reservacion.create',
            'description' => 'Crear un reservacion del sistema',
        ]);
        
        //Usuarios
        Permission::create([
            'name' => 'Navegar usuario',
            'slug' => 'users.index',
            'description' => 'Lista y navega todos los usuarios del sistema',
        ]);
        Permission::create([
            'name' => 'Ver detalle de usuario',
            'slug' => 'users.show',
            'description' => 'Ver en detalle cada usuario del sistema',
        ]);
        Permission::create([
            'name' => 'Crear usuario',
            'slug' => 'users.create',
            'description' => 'Crear un usuario del sistema',
        ]);
        Permission::create([
            'name' => 'Edicion usuario',
            'slug' => 'users.edit',
            'description' => 'Editar cualquier dato de un usuario del sistema',
        ]);
        Permission::create([
            'name' => 'Eliminar usuario',
            'slug' => 'users.destroy',
            'description' => 'Eliminar cualquier usuario del sistema',
        ]);

        //Imagenes
        Permission::create([
            'name' => 'Navegar imagene',
            'slug' => 'imagenes.index',
            'description' => 'Lista y navega todos los imagene del sistema',
        ]);
        Permission::create([
            'name' => 'Ver detalle de imagene',
            'slug' => 'imagenes.show',
            'description' => 'Ver en detalle cada imagene del sistema',
        ]);
        Permission::create([
            'name' => 'Crear imagene',
            'slug' => 'imagenes.create',
            'description' => 'Crear un imagene del sistema',
        ]);
        Permission::create([
            'name' => 'Edicion imagene',
            'slug' => 'imagenes.edit',
            'description' => 'Editar cualquier dato de un imagene del sistema',
        ]);
        Permission::create([
            'name' => 'Eliminar imagene',
            'slug' => 'imagenes.destroy',
            'description' => 'Eliminar cualquier imagene del sistema',
        ]);

        //Roles
        Permission::create([
            'name' => 'Navegar rol',
            'slug' => 'role.index',
            'description' => 'Lista y navega todos los roles del sistema',
        ]);
        Permission::create([
            'name' => 'Ver detalle de rol',
            'slug' => 'role.show',
            'description' => 'Ver en detalle cada rol del sistema',
        ]);
        Permission::create([
            'name' => 'Edicion rol',
            'slug' => 'role.edit',
            'description' => 'Editar cualquier dato de un rol del sistema',
        ]);
        Permission::create([
            'name' => 'Eliminar rol',
            'slug' => 'role.destroy',
            'description' => 'Eliminar cualquier rol del sistema',
        ]);
        Permission::create([
            'name' => 'Crear rol',
            'slug' => 'role.create',
            'description' => 'Crear un rol del sistema',
        ]);

        //reportes
        Permission::create([
            'name' => 'Navegar Reportes',
            'slug' => 'reporte.index',
            'description' => 'Lista y navega todos los reportes del sistema',
        ]);
    }
}
