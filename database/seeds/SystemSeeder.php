<?php

use Illuminate\Database\Seeder;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(RolesUserSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(PermissionsRoleTableSeeder::class);
        $this->call(GrupoSeeder::class);
    }
}
