<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(PermissionTableSeeder::class);
//        $this->call(UserTableSeeder::class);
//        $this->call(PermissionRoleTableSeeder::class);

//        seeders funcionando --------------------------
        $this->call(UserTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(RolesUserSeeder::class);
        $this->call(PermissionTableSeeder::class); 
        $this->call(PermissionsRoleTableSeeder::class);
        $this->call(GrupoSeeder::class);
        //$this->call(CategoryTableSeeder::class);
        //$this->call(UploadsTableSeeder::class);
        //$this->call(GrupoRoleTableSeeder::class);//Da error
    }
}
