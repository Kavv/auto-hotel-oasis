<?php

use Illuminate\Database\Seeder;

class GrupoRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1; $i<47; $i++)//Crea una relacion con un role que se llama administrador (especificar el primer id y el ultimo id de los permisos)
        {
            App\role_rule::create([
                'rule_id' => $i,
                'role_id' => 2,//Especificar el id del administrador
            ]);
        } 
    }
}
