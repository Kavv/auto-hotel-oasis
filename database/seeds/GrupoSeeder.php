<?php

use Illuminate\Database\Seeder;
use App\permission_rule;
use App\Rule;
class GrupoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Regla de Clientes

        Rule::create([
            'name' => 'Cliente-Crear',
            'description' => 'Agregar nuevos clientes',
            'module' => 'Cliente'
        ]);
        Rule::create([
            'name' => 'Cliente-Ver',
            'description' => 'Ver los registros de clientes',
            'module' => 'Cliente'
        ]);
        Rule::create([
            'name' => 'Cliente-Editar',
            'description' => 'Editar datos de clientes',
            'module' => 'Cliente'
        ]);
        Rule::create([
            'name' => 'Cliente-Eliminar',
            'description' => 'Eliminar clientes',
            'module' => 'Cliente'
        ]);

        // Regla de Personal

        Rule::create([
            'name' => 'Personal-Crear',
            'description' => 'Agregar nuevo personal',
            'module' => 'Personal'
        ]);
        Rule::create([
            'name' => 'Personal-Ver',
            'description' => 'Ver los registros del personal',
            'module' => 'Personal'
        ]);
        Rule::create([
            'name' => 'Personal-Editar',
            'description' => 'Editar datos del personal',
            'module' => 'Personal'
        ]);
        Rule::create([
            'name' => 'Personal-Eliminar',
            'description' => 'Eliminar personal',
            'module' => 'Personal'
        ]);

        // Regla de Cargo

        Rule::create([
            'name' => 'Cargo-Crear',
            'description' => 'Agregar nuevos cargo',
            'module' => 'Cargo'
        ]);
        Rule::create([
            'name' => 'Cargo-Ver',
            'description' => 'Ver los registros de cargo',
            'module' => 'Cargo'
        ]);
        Rule::create([
            'name' => 'Cargo-Editar',
            'description' => 'Editar datos de cargo',
            'module' => 'Cargo'
        ]);
        Rule::create([
            'name' => 'Cargo-Eliminar',
            'description' => 'Eliminar cargo',
            'module' => 'Cargo'
        ]);

        //Vetado - cliente

        Rule::create([
            'name' => 'Vetado-Cliente',
            'description' => 'Agregar nuevos vetados',
            'module' => 'Vetado'
        ]);
        Rule::create([
            'name' => 'Vetado-Ver-Cliente',
            'description' => 'Ver los registros de vetados',
            'module' => 'Vetado'
        ]);

        //vetado-editar-cliente

        Rule::create([
            'name' => 'Vetado-Editar-Cliente',
            'description' => 'Editar cliente vetados',
            'module' => 'Vetado'
        ]);
        //vetado-eliminar-cliente

        Rule::create([
            'name' => 'Vetado-Eliminar-Cliente',
            'description' => 'Eliminar cliente vetados',
            'module' => 'Vetado'
        ]);

        //Servicio

        Rule::create([
            'name' => 'Servicio-Crear',
            'description' => 'Agregar nuevos servicios',
            'module' => 'Servicio'
        ]);
        Rule::create([
            'name' => 'Servicio-Ver',
            'description' => 'Ver los registros de servicios',
            'module' => 'Servicio'
        ]);
        Rule::create([
            'name' => 'Servicio-Editar',
            'description' => 'Editar datos de servicios',
            'module' => 'Servicio'
        ]);
        Rule::create([
            'name' => 'Servicio-Eliminar',
            'description' => 'Eliminar servicio',
            'module' => 'Servicio'
        ]);

        //Inventario

        Rule::create([
            'name' => 'Inventario-Crear',
            'description' => 'Agregar nuevos inventario',
            'module' => 'Inventario'
        ]);
        Rule::create([
            'name' => 'Inventario-Ver',
            'description' => 'Ver los registros de inventario',
            'module' => 'Inventario'
        ]);
        Rule::create([
            'name' => 'Inventario-Editar',
            'description' => 'Editar datos de inventario',
            'module' => 'Inventario'
        ]);
        Rule::create([
            'name' => 'Inventario-Eliminar',
            'description' => 'Eliminar inventario',
            'module' => 'Inventario'
        ]);

        //Menu

        Rule::create([
            'name' => 'Menu-Crear',
            'description' => 'Agregar nuevos menu',
            'module' => 'Menu'
        ]);
        Rule::create([
            'name' => 'Menu-Ver',
            'description' => 'Ver los registros de menu',
            'module' => 'Menu'
        ]);
        Rule::create([
            'name' => 'Menu-Editar',
            'description' => 'Editar datos de menu',
            'module' => 'Menu'
        ]);
        Rule::create([
            'name' => 'Menu-Eliminar',
            'description' => 'Eliminar menu',
            'module' => 'Menu'
        ]);

        //Reservacion

        Rule::create([
            'name' => 'Reservacion-Crear',
            'description' => 'Agregar nueva reservacio',
            'module' => 'Reservacion'
        ]);
        Rule::create([
            'name' => 'Reservacion-Ver',
            'description' => 'Ver los registros de reservacion',
            'module' => 'Reservacion'
        ]);
        Rule::create([
            'name' => 'Reservacion-Editar',
            'description' => 'Editar datos de reservacion',
            'module' => 'Reservacion'
        ]);
        Rule::create([
            'name' => 'Reservacion-Eliminar',
            'description' => 'Eliminar reservacion',
            'module' => 'Reservacion'
        ]);

        //Rol

        Rule::create([
            'name' => 'Rol-Crear',
            'description' => 'Agregar nueva rol',
            'module' => 'Rol'
        ]);
        Rule::create([
            'name' => 'Rol-Ver',
            'description' => 'Ver los registros de rol',
            'module' => 'Rol'
        ]);
        Rule::create([
            'name' => 'Rol-Editar',
            'description' => 'Editar datos de rol',
            'module' => 'Rol'
        ]);
        Rule::create([
            'name' => 'Rol-Eliminar',
            'description' => 'Eliminar rol',
            'module' => 'Rol'
        ]);

        //Usuario

        Rule::create([
            'name' => 'Usuario-Crear',
            'description' => 'Agregar nueva usuario',
            'module' => 'Usuario'
        ]);
        Rule::create([
            'name' => 'Usuario-Ver',
            'description' => 'Ver los registros de usuario',
            'module' => 'Usuario'
        ]);
        Rule::create([
            'name' => 'Usuario-Editar',
            'description' => 'Editar datos de usuario',
            'module' => 'Usuario'
        ]);
        Rule::create([
            'name' => 'Usuario-Eliminar',
            'description' => 'Eliminar usuario',
            'module' => 'Usuario'
        ]);

        //Imagenes

        Rule::create([
            'name' => 'Imagenes-Crear',
            'description' => 'Agregar nueva imagenes',
            'module' => 'Imagen'
        ]);
        Rule::create([
            'name' => 'Imagenes-Ver',
            'description' => 'Ver los registros de imagenes',
            'module' => 'Imagen'
        ]);
        Rule::create([
            'name' => 'Imagenes-Editar',
            'description' => 'Editar datos de imagenes',
            'module' => 'Imagen'
        ]);
        Rule::create([
            'name' => 'Imagenes-Eliminar',
            'description' => 'Eliminar imagenes',
            'module' => 'Imagen'
        ]);

        //Vetado - personal

        Rule::create([
            'name' => 'Vetado-Personal',
            'description' => 'Agregar nuevos vetados',
            'module' => 'Vetado'
        ]);
        Rule::create([
            'name' => 'Vetado-Ver-Personal',
            'description' => 'Ver los registros de vetados',
            'module' => 'Vetado'
        ]);

        //vetado-editar-personal

        Rule::create([
            'name' => 'Vetado-Editar-Personal',
            'description' => 'Editar personal vetados',
            'module' => 'Vetado'
        ]);

        //vetado-eliminar-personal

        Rule::create([
            'name' => 'Vetado-Eliminar-Personal',
            'description' => 'Eliminar personal vetados',
            'module' => 'Vetado'
        ]);

        //Regla de Reporte
        //49
        Rule::create([
            'name' => 'Reportes-Ver',
            'description' => 'Ver todos los reportes',
            'module' => 'Reporte'
        ]);

        ///Aplicar correctamente los permisos
        //index para habilitar el modulo    (1)
        //create para crear registros       (2)
        //show para mostrar los registros   (3)
        //edit para editar los resgistros   (4)
        //delete para eliminar los registros(5)
        //Estado actual erroneo index funciona como show y show no es utilizado

        //Cliente crear
        permission_rule::create([
            'permission_id' => 1,//index
            'rule_id' => 1
        ]);
        permission_rule::create([
            'permission_id' => 2,//show
            'rule_id' => 1
        ]);
        permission_rule::create([
            'permission_id' => 3,//crear
            'rule_id' => 1
        ]);

        //Cliente ver
        permission_rule::create([
            'permission_id' => 1,//index
            'rule_id' => 2
        ]);
        permission_rule::create([
            'permission_id' => 2,//show
            'rule_id' => 2
        ]);

        //Cliente Editar
        permission_rule::create([
            'permission_id' => 1,//index
            'rule_id' => 3
        ]);
        permission_rule::create([
            'permission_id' => 2,//show
            'rule_id' => 3
        ]);
        permission_rule::create([
            'permission_id' => 4,//editar
            'rule_id' => 3
        ]);

        //Cliente Eliminar
        permission_rule::create([
            'permission_id' => 1,//index
            'rule_id' => 4
        ]);
        permission_rule::create([
            'permission_id' => 2,//show
            'rule_id' => 4
        ]);
        permission_rule::create([
            'permission_id' => 5,//eliminar
            'rule_id' => 4
        ]);

        //Personal crear con cargos
        permission_rule::create([
            'permission_id' => 6,//index
            'rule_id' => 5
        ]);
        permission_rule::create([
            'permission_id' => 7,//show
            'rule_id' => 5
        ]);
        permission_rule::create([
            'permission_id' => 10,//crear
            'rule_id' => 5
        ]);
        permission_rule::create([
            'permission_id' => 11,//index cargo
            'rule_id' => 5
        ]);
        permission_rule::create([
            'permission_id' => 12,//show cargo
            'rule_id' => 5
        ]);

        //Personal ver 
        permission_rule::create([
            'permission_id' => 6,//index
            'rule_id' => 6
        ]);
        permission_rule::create([
            'permission_id' => 7,//show
            'rule_id' => 6
        ]);
        permission_rule::create([
            'permission_id' => 11,//index cargos
            'rule_id' => 6
        ]);
        permission_rule::create([
            'permission_id' => 12,//show cargos
            'rule_id' => 6
        ]);
        
        //Personal Editar
        permission_rule::create([
            'permission_id' => 6,//index
            'rule_id' => 7
        ]);
        permission_rule::create([
            'permission_id' => 7,//show
            'rule_id' => 7
        ]);
        permission_rule::create([
            'permission_id' => 8,//editar
            'rule_id' => 7
        ]);
        permission_rule::create([
            'permission_id' => 11,//index cargo
            'rule_id' => 7
        ]);
        permission_rule::create([
            'permission_id' => 12,//show cargo
            'rule_id' => 7
        ]);

        //Personal Eliminar
        permission_rule::create([
            'permission_id' => 6,//index
            'rule_id' => 8
        ]);
        permission_rule::create([
            'permission_id' => 7,//show
            'rule_id' => 8
        ]);
        permission_rule::create([
            'permission_id' => 9,//eliminar
            'rule_id' => 8
        ]);


        //Cargo crear y agregar cargos a personal
        permission_rule::create([
            'permission_id' => 11,//index
            'rule_id' => 9
        ]);
        permission_rule::create([
            'permission_id' => 12,//show
            'rule_id' => 9
        ]);
        permission_rule::create([
            'permission_id' => 15,//crear
            'rule_id' => 9
        ]);
        permission_rule::create([
            'permission_id' => 6,//index personal
            'rule_id' => 9
        ]);
        permission_rule::create([
            'permission_id' => 7,//show personal
            'rule_id' => 9
        ]);

        //Cargo ver
        permission_rule::create([
            'permission_id' => 11,//index
            'rule_id' => 10
        ]);
        permission_rule::create([
            'permission_id' => 12,//index
            'rule_id' => 10
        ]);

        //Cargo editar
        permission_rule::create([
            'permission_id' => 11,//index
            'rule_id' => 11
        ]);
        permission_rule::create([
            'permission_id' => 12,//show
            'rule_id' => 11
        ]);
        permission_rule::create([
            'permission_id' => 13,//edit
            'rule_id' => 11
        ]);

        //Cargo eliminar
        permission_rule::create([
            'permission_id' => 11,//index
            'rule_id' => 12
        ]);
        permission_rule::create([
            'permission_id' => 12,//show
            'rule_id' => 12
        ]);
        permission_rule::create([
            'permission_id' => 14,//eliminar
            'rule_id' => 12
        ]);
        
        //Vetar cliente
        permission_rule::create([
            'permission_id' => 16,//index
            'rule_id' => 13
        ]);
        permission_rule::create([
            'permission_id' => 17,//show
            'rule_id' => 13
        ]);
        permission_rule::create([
            'permission_id' => 20,//crear
            'rule_id' => 13
        ]);
        permission_rule::create([
            'permission_id' => 1,//index cliente
            'rule_id' => 13
        ]);
        permission_rule::create([
            'permission_id' => 2,//show cliente
            'rule_id' => 13
        ]);

        //Vetado ver cliente
        permission_rule::create([
            'permission_id' => 16,//index
            'rule_id' => 14
        ]);
        permission_rule::create([
            'permission_id' => 17,//show
            'rule_id' => 14
        ]);
        permission_rule::create([
            'permission_id' => 1,//index cliente
            'rule_id' => 14
        ]);
        permission_rule::create([
            'permission_id' => 2,//show cliente
            'rule_id' => 14
        ]);

        //Vetar personal
        permission_rule::create([
            'permission_id' => 16,//index
            'rule_id' => 45
        ]);
        permission_rule::create([
            'permission_id' => 17,//show
            'rule_id' => 45
        ]);
        permission_rule::create([
            'permission_id' => 20,//crear
            'rule_id' => 45
        ]);
        permission_rule::create([
            'permission_id' => 6,//index personal
            'rule_id' => 45
        ]);
        permission_rule::create([
            'permission_id' => 7,//show personal
            'rule_id' => 45
        ]);

        //Vetado ver personal
        permission_rule::create([
            'permission_id' => 16,//index
            'rule_id' => 46
        ]);
        permission_rule::create([
            'permission_id' => 17,//show
            'rule_id' => 46
        ]);
        permission_rule::create([
            'permission_id' => 6,//index personal
            'rule_id' => 46
        ]);
        permission_rule::create([
            'permission_id' => 7,//show personal
            'rule_id' => 46
        ]);

        //Vetado editar-cliente
        permission_rule::create([
            'permission_id' => 16,//index
            'rule_id' => 15
        ]);
        permission_rule::create([
            'permission_id' => 17,//show
            'rule_id' => 15
        ]);
        permission_rule::create([
            'permission_id' => 18,//editar
            'rule_id' => 15
        ]);
        permission_rule::create([
            'permission_id' => 1,//index cliente
            'rule_id' => 15
        ]);
        permission_rule::create([
            'permission_id' => 2,//show cliente
            'rule_id' => 15
        ]);

        //Vetado editar-personal
        permission_rule::create([
            'permission_id' => 16,//index
            'rule_id' => 47
        ]);
        permission_rule::create([
            'permission_id' => 17,//show
            'rule_id' => 47
        ]);
        permission_rule::create([
            'permission_id' => 18,//editar
            'rule_id' => 47
        ]);
        permission_rule::create([
            'permission_id' => 6,//index personal
            'rule_id' => 47
        ]);
        permission_rule::create([
            'permission_id' => 7,//show personal
            'rule_id' => 47
        ]);

        //Vetado eliminar cliente
        permission_rule::create([
            'permission_id' => 16,//index
            'rule_id' => 16
        ]);
        permission_rule::create([
            'permission_id' => 17,//show
            'rule_id' => 16
        ]);
        permission_rule::create([
            'permission_id' => 19,//eliminar
            'rule_id' => 16
        ]);
        permission_rule::create([
            'permission_id' => 1,//index cliente
            'rule_id' => 16
        ]);
        permission_rule::create([
            'permission_id' => 2,//show cliente
            'rule_id' => 16
        ]);

        //Vetado eliminar personal
        permission_rule::create([
            'permission_id' => 16,//index
            'rule_id' => 48
        ]);
        permission_rule::create([
            'permission_id' => 17,//show
            'rule_id' => 48
        ]);
        permission_rule::create([
            'permission_id' => 19,//eliminar
            'rule_id' => 48
        ]);
        permission_rule::create([
            'permission_id' => 6,//index personal
            'rule_id' => 48
        ]);
        permission_rule::create([
            'permission_id' => 7,//show personal
            'rule_id' => 48
        ]);

        //Servicio Crear
        permission_rule::create([
            'permission_id' => 21,//index
            'rule_id' => 17
        ]);
        permission_rule::create([
            'permission_id' => 22,//show
            'rule_id' => 17
        ]);
        permission_rule::create([
            'permission_id' => 25,//crear
            'rule_id' => 17
        ]);

        //Servicio Ver
        permission_rule::create([
            'permission_id' => 21,//index
            'rule_id' => 18
        ]);
        permission_rule::create([
            'permission_id' => 22,//show
            'rule_id' => 18
        ]);

        //Servicio editar
        permission_rule::create([
            'permission_id' => 21,//index
            'rule_id' => 19
        ]);
        permission_rule::create([
            'permission_id' => 22,//show
            'rule_id' => 19
        ]);
        permission_rule::create([
            'permission_id' => 23,//editar
            'rule_id' => 19
        ]);

        //Servicio Eliminar
        permission_rule::create([
            'permission_id' => 21,//index
            'rule_id' => 20
        ]);
        permission_rule::create([
            'permission_id' => 22,//show
            'rule_id' => 20
        ]);
        permission_rule::create([
            'permission_id' => 24,//eliminar
            'rule_id' => 20
        ]);

        //Inventario Crear
        permission_rule::create([
            'permission_id' => 26,//index
            'rule_id' => 21
        ]);
        permission_rule::create([
            'permission_id' => 27,//show
            'rule_id' => 21
        ]);
        permission_rule::create([
            'permission_id' => 30,//crear
            'rule_id' => 21
        ]);
        permission_rule::create([
            'permission_id' => 21,//index servicio
            'rule_id' => 21
        ]);
        permission_rule::create([
            'permission_id' => 22,//show servicio
            'rule_id' => 21
        ]);
        permission_rule::create([
            'permission_id' => 25,//crear servicio
            'rule_id' => 21
        ]);

        //Inventario Ver
        permission_rule::create([
            'permission_id' => 26,//index
            'rule_id' => 22
        ]);
        permission_rule::create([
            'permission_id' => 27,//show
            'rule_id' => 22
        ]);
        permission_rule::create([
            'permission_id' => 21,//index servicio
            'rule_id' => 22
        ]);
        permission_rule::create([
            'permission_id' => 23,//show servicio
            'rule_id' => 22
        ]);

        //Inventario editar
        permission_rule::create([
            'permission_id' => 26,//index
            'rule_id' => 23
        ]);
        permission_rule::create([
            'permission_id' => 27,//show
            'rule_id' => 23
        ]);
        permission_rule::create([
            'permission_id' => 28,//editar
            'rule_id' => 23
        ]);
        permission_rule::create([
            'permission_id' => 21,//index servicio
            'rule_id' => 23
        ]);
        permission_rule::create([
            'permission_id' => 22,//show servicio
            'rule_id' => 23
        ]);
        permission_rule::create([
            'permission_id' => 25,//crear servicio
            'rule_id' => 23
        ]);

        //Inventario Eliminar
        permission_rule::create([
            'permission_id' => 26,//index
            'rule_id' => 24
        ]);
        permission_rule::create([
            'permission_id' => 27,//show
            'rule_id' => 24
        ]);
        permission_rule::create([
            'permission_id' => 29,//eliminar
            'rule_id' => 24
        ]);

        //Menu Ver
        permission_rule::create([
            'permission_id' => 31,//index
            'rule_id' => 26
        ]);
        permission_rule::create([
            'permission_id' => 32,//show
            'rule_id' => 26
        ]);

        //Menu Editar
        permission_rule::create([
            'permission_id' => 31,//index
            'rule_id' => 27
        ]);
        permission_rule::create([
            'permission_id' => 32,//show
            'rule_id' => 27
        ]);
        permission_rule::create([
            'permission_id' => 33,//editar
            'rule_id' => 27
        ]);

        //Menu Eliminar
        permission_rule::create([
            'permission_id' => 31,//index
            'rule_id' => 28
        ]);
        permission_rule::create([
            'permission_id' => 32,//show
            'rule_id' => 28
        ]);
        permission_rule::create([
            'permission_id' => 34,//eliminar
            'rule_id' => 28
        ]);

        //Menu Crear
        permission_rule::create([
            'permission_id' => 31,//index
            'rule_id' => 25
        ]);
        permission_rule::create([
            'permission_id' => 32,//show
            'rule_id' => 25
        ]);
        permission_rule::create([
            'permission_id' => 35,//crear
            'rule_id' => 25
        ]);

        //Reservacion Crear Full
        permission_rule::create([
            'permission_id' => 36,//index
            'rule_id' => 29
        ]);
        permission_rule::create([
            'permission_id' => 37,//show
            'rule_id' => 29
        ]);
        permission_rule::create([
            'permission_id' => 40,//crear
            'rule_id' => 29
        ]);
        permission_rule::create([
            'permission_id' => 31,//index menu
            'rule_id' => 29
        ]);
        permission_rule::create([
            'permission_id' => 32,//show menu
            'rule_id' => 29
        ]);
        permission_rule::create([
            'permission_id' => 35,//create menu
            'rule_id' => 29
        ]);
        permission_rule::create([
            'permission_id' => 26,//index inventario
            'rule_id' => 29
        ]);
        permission_rule::create([
            'permission_id' => 27,//show inventario
            'rule_id' => 29
        ]);
        permission_rule::create([
            'permission_id' => 21,//index servicio
            'rule_id' => 29
        ]);
        permission_rule::create([
            'permission_id' => 22,//show servicio
            'rule_id' => 29
        ]);
        permission_rule::create([
            'permission_id' => 1,//index cliente
            'rule_id' => 29
        ]);
        permission_rule::create([
            'permission_id' => 2,//show cliente
            'rule_id' => 29
        ]);

        //Reservacion ver
        permission_rule::create([
            'permission_id' => 36,//index
            'rule_id' => 30
        ]);
        permission_rule::create([
            'permission_id' => 37,//show
            'rule_id' => 30
        ]);
        permission_rule::create([
            'permission_id' => 1,//index cliente
            'rule_id' => 30
        ]);
        permission_rule::create([
            'permission_id' => 2,//show cliente
            'rule_id' => 30
        ]);

        //Reservacion Editar
        permission_rule::create([
            'permission_id' => 36,//index
            'rule_id' => 31
        ]);
        permission_rule::create([
            'permission_id' => 37,//show
            'rule_id' => 31
        ]);
        permission_rule::create([
            'permission_id' => 38,//editar
            'rule_id' => 31
        ]);
        permission_rule::create([
            'permission_id' => 31,//index menu
            'rule_id' => 31
        ]);
        permission_rule::create([
            'permission_id' => 32,//show menu
            'rule_id' => 31
        ]);
        permission_rule::create([
            'permission_id' => 35,//create menu
            'rule_id' => 31
        ]);
        permission_rule::create([
            'permission_id' => 26,//index inventario
            'rule_id' => 31
        ]);
        permission_rule::create([
            'permission_id' => 27,//show inventario
            'rule_id' => 31
        ]);
        permission_rule::create([
            'permission_id' => 21,//index servicio
            'rule_id' => 31
        ]);
        permission_rule::create([
            'permission_id' => 22,//show servicio
            'rule_id' => 31
        ]);
        permission_rule::create([
            'permission_id' => 1,//index cliente
            'rule_id' => 31
        ]);
        permission_rule::create([
            'permission_id' => 2,//show cliente
            'rule_id' => 31
        ]);

        //Reservacion Eliminar
        permission_rule::create([
            'permission_id' => 36,//index
            'rule_id' => 32
        ]);
        permission_rule::create([
            'permission_id' => 37,//show
            'rule_id' => 32
        ]);
        permission_rule::create([
            'permission_id' => 39,//eliminar
            'rule_id' => 32
        ]);

        //Rol Crear
        permission_rule::create([
            'permission_id' => 51,//index
            'rule_id' => 33
        ]);
        permission_rule::create([
            'permission_id' => 52,//show
            'rule_id' => 33
        ]);
        permission_rule::create([
            'permission_id' => 55,//crear
            'rule_id' => 33
        ]);
        //Role ver
        permission_rule::create([
            'permission_id' => 51,//index
            'rule_id' => 34
        ]);
        permission_rule::create([
            'permission_id' => 52,//show
            'rule_id' => 34
        ]);

        //Role editar
        permission_rule::create([
            'permission_id' => 51,//index
            'rule_id' => 35
        ]);
        permission_rule::create([
            'permission_id' => 52,//show
            'rule_id' => 35
        ]);
        permission_rule::create([
            'permission_id' => 53,//editar
            'rule_id' => 35
        ]);

        //Role Eliminar
        permission_rule::create([
            'permission_id' => 51,//index
            'rule_id' => 36
        ]);
        permission_rule::create([
            'permission_id' => 52,//show
            'rule_id' => 36
        ]);
        permission_rule::create([
            'permission_id' => 54,//eliminar
            'rule_id' => 36
        ]);

        //Usuario crear
        permission_rule::create([
            'permission_id' => 41,//index
            'rule_id' => 37
        ]);
        permission_rule::create([
            'permission_id' => 42,//show
            'rule_id' => 37
        ]);
        permission_rule::create([
            'permission_id' => 43,//crear
            'rule_id' => 37
        ]);
        permission_rule::create([
            'permission_id' => 51,//index role
            'rule_id' => 37
        ]);
        permission_rule::create([
            'permission_id' => 52,//show role
            'rule_id' => 37
        ]);

        //Usuario ver
        permission_rule::create([
            'permission_id' => 41,//index
            'rule_id' => 38
        ]);
        permission_rule::create([
            'permission_id' => 42,//show
            'rule_id' => 38
        ]);
        permission_rule::create([
            'permission_id' => 51,//index role
            'rule_id' => 38
        ]);
        permission_rule::create([
            'permission_id' => 52,//show role
            'rule_id' => 38
        ]);

        //Usuario editar
        permission_rule::create([
            'permission_id' => 41,//index
            'rule_id' => 39
        ]);
        permission_rule::create([
            'permission_id' => 42,//show
            'rule_id' => 39
        ]);
        permission_rule::create([
            'permission_id' => 44,//editar
            'rule_id' => 39
        ]);
        permission_rule::create([
            'permission_id' => 51,//index role
            'rule_id' => 39
        ]);
        permission_rule::create([
            'permission_id' => 52,//show role
            'rule_id' => 39
        ]);

        //Usuario eliminar
        permission_rule::create([
            'permission_id' => 41,//index
            'rule_id' => 40
        ]);
        permission_rule::create([
            'permission_id' => 42,//show
            'rule_id' => 40
        ]);
        permission_rule::create([
            'permission_id' => 45,//eliminar
            'rule_id' => 40
        ]);

        //Agregar des el grupo 41 al 44 (imagenes)
        //Imagenes crear
        permission_rule::create([
           'permission_id' => 46, //index
           'rule_id' => 41
        ]);

        permission_rule::create([
            'permission_id' => 47, //show
            'rule_id' => 41
        ]);

        permission_rule::create([
            'permission_id' => 48, //crear
            'rule_id' => 41
        ]);

        //imagenes ver
        permission_rule::create([
            'permission_id' => 46, //index
            'rule_id' => 42
        ]);

        permission_rule::create([
            'permission_id' => 47, //show
            'rule_id' => 42
        ]);

        //imagenes editar
        permission_rule::create([
            'permission_id' => 46, //index
            'rule_id' => 43
        ]);

        permission_rule::create([
            'permission_id' => 47, //show
            'rule_id' => 43
        ]);

        permission_rule::create([
            'permission_id' => 49, //Edicion
            'rule_id' => 43
        ]);

        //imagenes eliminar
        permission_rule::create([
            'permission_id' => 46, //index
            'rule_id' => 44
        ]);

        permission_rule::create([
            'permission_id' => 47, //show
            'rule_id' => 44
        ]);

        permission_rule::create([
            'permission_id' => 50, //eliminar
            'rule_id' => 44
        ]);

        //reportes
        permission_rule::create([
            'permission_id' => 56,// index reportes
            'rule_id' => 49 //Reportes-Ver
        ]);
    }
}
