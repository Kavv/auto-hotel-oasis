<?php

use Illuminate\Database\Seeder;
use App\MultimediaWeb;

class UploadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MultimediaWeb::insert([
            [
                'name' => 'wine',
                'image_url' => 'https://res.cloudinary.com/fguzman/video/upload/v1533763368/wine.webm',
                'selection' => '1',
                'order' => '1',
                'categories_id' => '1'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534988998/cetzv1xhjlujm2iatxlm.jpg',
                'selection' => '1',
                'order' => '1',
                'categories_id' => '2'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989007/jami0xr1cgxvc1tvv46j.jpg',
                'selection' => '1',
                'order' => '2',
                'categories_id' => '2'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989014/g4zjpki91ifv32brnxaz.jpg',
                'selection' => '1',
                'order' => '3',
                'categories_id' => '2'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989033/kahc9hosvpf5qrpmeglt.jpg',
                'selection' => '1',
                'order' => '4',
                'categories_id' => '2'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989041/jhipageq2xhwpmbhronf.jpg',
                'selection' => '1',
                'order' => '1',
                'categories_id' => '3'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989047/tiki4o9mtfmltlxgnozw.jpg',
                'selection' => '1',
                'order' => '2',
                'categories_id' => '3'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989053/bt0nkx1n0tyxpcsnxr8n.jpg',
                'selection' => '1',
                'order' => '3',
                'categories_id' => '3'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989061/efw5cqdgnozsezfb2k7c.jpg',
                'selection' => '1',
                'order' => '4',
                'categories_id' => '3'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989169/yp2crxivmchmrkgpbdkv.jpg',
                'selection' => '1',
                'order' => '1',
                'categories_id' => '4'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989180/ybmd0joi0x1osjosxwqg.jpg',
                'selection' => '1',
                'order' => '2',
                'categories_id' => '4'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989187/nca8y0xpest1qoxnk37v.jpg',
                'selection' => '1',
                'order' => '3',
                'categories_id' => '4'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989195/rjgh6g61baxx6qawnvc6.jpg',
                'selection' => '1',
                'order' => '4',
                'categories_id' => '4'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989203/ikqrgpttqwylqv72rjlw.jpg',
                'selection' => '1',
                'order' => '1',
                'categories_id' => '5'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989209/gubr3f7ba9tzabyoz1tm.jpg',
                'selection' => '1',
                'order' => '2',
                'categories_id' => '5'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989215/tixh268d1wgntsdj8vpb.jpg',
                'selection' => '1',
                'order' => '3',
                'categories_id' => '5'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989220/lxjkcgxnixizx8eritnv.jpg',
                'selection' => '1',
                'order' => '4',
                'categories_id' => '5'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989287/jfuuxfr6k7d4eupsbk9w.jpg',
                'selection' => '1',
                'order' => '1',
                'categories_id' => '6'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989293/ra9b8df6bbjwve5zv8rg.jpg',
                'selection' => '1',
                'order' => '2',
                'categories_id' => '6'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989299/rxleqimrxcjbvmi4byyk.jpg',
                'selection' => '1',
                'order' => '3',
                'categories_id' => '6'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989305/vyc2wskg3boaatmegqtv.jpg',
                'selection' => '1',
                'order' => '4',
                'categories_id' => '6'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989310/mjpi5xyqtd0hy6m7gbsf.jpg',
                'selection' => '1',
                'order' => '1',
                'categories_id' => '7'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989317/qhrf4a9l30n3xp4ojnce.jpg',
                'selection' => '1',
                'order' => '2',
                'categories_id' => '7'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989323/m1gqpkxxpot9srmpu573.jpg',
                'selection' => '1',
                'order' => '3',
                'categories_id' => '7'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989329/gdwte3ai45hazxlynk8d.jpg',
                'selection' => '1',
                'order' => '4',
                'categories_id' => '7'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989410/gxqxr62y73w6no65axna.jpg',
                'selection' => '1',
                'order' => '1',
                'categories_id' => '8'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989416/qpllfaxzhen7z2ubgguk.jpg',
                'selection' => '1',
                'order' => '2',
                'categories_id' => '8'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989421/gp1ilqhs4exsnmufgz5p.jpg',
                'selection' => '1',
                'order' => '3',
                'categories_id' => '8'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989426/hvtmkfwv2ipk6tunivjv.jpg',
                'selection' => '1',
                'order' => '4',
                'categories_id' => '8'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989433/bzvq2lvyuvfn3vsx1wcp.jpg',
                'selection' => '1',
                'order' => '1',
                'categories_id' => '9'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989439/jwckyv4chituulbkeksg.jpg',
                'selection' => '1',
                'order' => '2',
                'categories_id' => '9'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989444/gn6i9twdinllpgvzwhwz.jpg',
                'selection' => '1',
                'order' => '3',
                'categories_id' => '9'
            ],
            [
                'name' => 'logoSantana',
                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989453/j4ro7ngxwygmc6tehd0e.jpg',
                'selection' => '1',
                'order' => '4',
                'categories_id' => '9'
            ],
//              Borrar todo esto, el carousel se elimino
//            [
//                'name' => 'logoSantana',
//                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989640/fahpuzblhumen4vohjsq.jpg',
//                'selection' => '1',
//                'order' => '1',
//                'categories_id' => '10'
//            ],
//            [
//                'name' => 'logoSantana',
//                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989646/wwiakbyuiyhmwjeoaafz.jpg',
//                'selection' => '1',
//                'order' => '2',
//                'categories_id' => '10'
//            ],
//            [
//                'name' => 'logoSantana',
//                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989657/ssampql5wzaotiljopzn.jpg',
//                'selection' => '1',
//                'order' => '3',
//                'categories_id' => '10'
//            ],
//            [
//                'name' => 'logoSantana',
//                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989672/jdyxg43at3hdll0fluiy.jpg',
//                'selection' => '1',
//                'order' => '4',
//                'categories_id' => '10'
//            ],
//            [
//                'name' => 'logoSantana',
//                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989679/xbk3yqnj5qfcteatqpqb.jpg',
//                'selection' => '1',
//                'order' => '5',
//                'categories_id' => '10'
//            ],
//            [
//                'name' => 'logoSantana',
//                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989685/ebssqefhmbvkr14dojhl.jpg',
//                'selection' => '1',
//                'order' => '6',
//                'categories_id' => '10'
//            ],
//            [
//                'name' => 'logoSantana',
//                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989692/sdyarsx2fhqxh8npccoy.jpg',
//                'selection' => '1',
//                'order' => '7',
//                'categories_id' => '10'
//            ],
//            [
//                'name' => 'logoSantana',
//                'image_url' => 'https://res.cloudinary.com/fguzman/image/upload/v1534989698/xf8dtiov9rjpdfadqmtm.jpg',
//                'selection' => '1',
//                'order' => '8',
//                'categories_id' => '10'
//            ],
        ]);
    }
}
