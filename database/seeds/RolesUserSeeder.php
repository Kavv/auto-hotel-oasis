<?php

use Illuminate\Database\Seeder;
use App\User;
class RolesUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin = User::where('description','=','SuperAdmin')->get();
        foreach($superAdmin as $sa)
        {
            $sa->roles()->sync(1);
        }
    }
}
