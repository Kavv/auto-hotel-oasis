<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AjustmentCustomersAndReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->float('subtotal', 8,2);
            $table->float('tax', 8,2)->nullable();
            $table->float('discount', 8,2)->nullable();
            $table->renameColumn('payment', 'total');
        });

        Schema::table('customers', function (Blueprint $table) {
            //$table->dropColumn(['votes', 'avatar', 'location']);
            $table->string('name', 50)->change();
            $table->string('phone', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
