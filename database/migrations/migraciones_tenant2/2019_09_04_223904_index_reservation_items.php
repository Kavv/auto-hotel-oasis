<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IndexReservationItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items_reservations', function (Blueprint $table) {
            $table->integer('index')->nullable();
        });
        Schema::table('menu_reservations', function (Blueprint $table) {
            $table->integer('index')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items_reservations', function (Blueprint $table) {
            //
        });
    }
}
